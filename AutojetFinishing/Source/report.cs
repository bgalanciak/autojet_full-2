﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace AutojetFinishing
{
    public partial class autojetFinishingMainForm : Form
    {
        //public const int REPORT_STROER = 1;
        //public const int REPORT_STROER_WARSZAWA = 2;
        public const int REPORT_DATE = 3;       //
        public const int REPORT_KP = 4;         //
        public const int REPORT_KPLKP = 5;      //
        //public const int REPORT_ZPL = 6;
        //public const int REPORT_UNIT = 7;
        public const int REPORT_INFO = 8;

        public int ReportType = REPORT_INFO;
        public string ReportWhere = "";
        public string ReportName = "";
        public string ReportParam1 = "";
        public string ReportParam2 = "";

        void showReportTab()
        {
            tabControlMain.SelectedTab = tpReports;
            tpReportsRemoveAll();
            ReportType = REPORT_INFO;
        }

        private void bReportDate_Click(object sender, EventArgs e)
        {
            reportDateShow();
        }

        private void reportDateShow()
        {
            tpReportsRemoveAll();
            tcReports.TabPages.Add(tpDate);
            ReportType = REPORT_DATE;
        }

        private void reportKpShow()
        {
            tpReportsRemoveAll();
            tcReports.TabPages.Add(tpKP);
            ReportType = REPORT_KP;
        }

        private void reportKpLkpShow()
        {
            tpReportsRemoveAll();
            tcReports.TabPages.Add(tpKPLKP);
            ReportType = REPORT_KPLKP;
        }

        private void bReportKp_Click(object sender, EventArgs e)
        {
            reportKpShow();
        }

        private void bReportKpLkp_Click(object sender, EventArgs e)
        {
            reportKpLkpShow();
        }

        private void tpReportsRemoveAll()
        {
            tcReports.TabPages.Remove(tpDate);
            tcReports.TabPages.Remove(tpKP);
            tcReports.TabPages.Remove(tpKPLKP);
            tcReports.TabPages.Remove(tpInfo);
            lReportKpError.Visible = false;
            lReportKpLkpError.Visible = false;
        }

        public bool reportMode = false;

        public void reportBarcodeAnalize(string barcode)
        {
            if (barcode.Equals("RS00000001"))
            {
                generateReport();
            }
            else if (ReportType == REPORT_DATE)
            {
                //
            }
            else if (ReportType == REPORT_KP)
            {
                tbReportKp.Text = barcode;
            }
            else if (ReportType == REPORT_KPLKP)
            {
                tbReportKpLkp.Text = barcode;
            }
        }

        public void generateReport()
        {
            //check conditions
            if (ReportType == REPORT_DATE)
            {
                
            }
            else if (ReportType == REPORT_KP)
            {
                if (!checkKp(tbReportKp.Text))
                {
                    lReportKpError.Visible = true;
                    return;
                }
            }
            else if (ReportType == REPORT_KPLKP)
            {
                if (!checkKpLkp(tbReportKpLkp.Text))
                {
                    lReportKpLkpError.Visible = true;
                    return;
                }
            }

            //prepare where
            reportMadeWhere();

            //show data
            reportPrepare();
        }

        public List<dispachReportClass> DispachReports;
        public string ReportSelect;

        void reportPrepare()
        {
            (DispachReports, ReportSelect) = dispachReport(ReportWhere);

            showReportTp();
            if (DispachReports != null && DispachReports.Count > 0)
            {
                dgvReport.DataSource = DispachReports;

                DgvListReportInit(dgvReport, true);
            }
        }

        public void showReportTp()
        {
            if (ReportType == REPORT_DATE)
            {
                reportDateShow();
            }
            else if (ReportType == REPORT_KP)
            {
                reportKpShow();
            }
            else if (ReportType == REPORT_KPLKP)
            {
                reportKpLkpShow();
            }
        }

        public bool checkKp(string inStr)
        {
            return int.TryParse(inStr, out _);
        }

        public bool checkKpLkp(string inStr)
        {
            string[] strArray = inStr.Split('.');
            if (strArray.Count() != 2)
                return false;
            else
            {
                bool p1 = int.TryParse(strArray[0], out _);
                bool p2 = int.TryParse(strArray[1], out _);
                return p1 && p2;
            }
        }

        private void reportMadeWhere()
        {
            tpReportsRemoveAll();

            tcReports.TabPages.Add(tpInfo);

            
            if (ReportType == REPORT_DATE)
            {
                /*
                WHERE (da.dispatch_date=['DATA Z KONTROLKI]' OR jd.dispatch_date=' [DATA Z KONTROLK] ')
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)
                */
                int notpacked = 0;
                int notsent = 0;
                string courier = "";


                notpacked = dtpReportNP.Checked ? 1 : 0;
                notsent = dtpReportNS.Checked ? 1 : 0; ;
                courier = Courier.SelectedItem.ToString();


                string date = $"{dtpReportParam.Value.Year.ToString("D4")}-{dtpReportParam.Value.Month.ToString("D2")}-{dtpReportParam.Value.Day.ToString("D2")}";
                string where = $"WHERE (da.dispatch_date like '{date}%' OR (jd.dispatch_date like '{date}%' AND da.dispatch_date IS NULL) OR dd.release_time_stamp like '{date}%') ";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)";
                where += " AND (da.courier_type <> 'FEDEX-S' AND sc.dispatch_company IS NULL) AND jd.jobstatus<>'ANULOWANA'";

                if (notpacked == 1)
                {
                    where += " AND di.pack_date IS NULL";
                }

                if (notsent == 1)
                {
                    where += " AND dd.release_time_stamp IS NULL";
                }

                if (!courier.Equals("") && !courier.Equals("WSZYSCY"))
                {
                    where += $" AND (da.courier_type='{courier}' or da.courier_type IS NULL)";
                }

                ReportWhere = where;
                //scannerBarcodeRevieved("RS00000001", CradleId.ToString());

                ReportName = "Raport po dacie wysyłki";
                ReportParam1 = $"Data: {date}";
                ReportParam2 = $"";
            }
            else if (ReportType == REPORT_KP)
            {
                /*
                WHERE (di.jdnr=[KP Z KONTROLKI] OR jd.jdnr=[KP Z KONTROLKI])
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)
                */
                string kp = $"{tbReportKp.Text}";
                string where = $"WHERE (di.jdnr={kp} OR jd.jdnr={kp})";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL) AND jd.jobstatus<>'ANULOWANA'";
                //where += " AND (da.courier_type <> 'FEDEX-S' AND sc.dispatch_company IS NULL)";

                ReportWhere = where;
                //scannerBarcodeRevieved("RS00000001", CradleId.ToString());

                ReportName = "Raport po numerze KP";
                ReportParam1 = $"KP: {kp}";
                ReportParam2 = $"";
            }
            else if (ReportType == REPORT_KPLKP)
            {
                /*
                WHERE (CONCAT(di.jdnr,'.',di.jdline)='[TESKT Z KONTROLKI]' OR CONCAT(jd.jdnr,'.',jd.jdline)='[TEKST Z KONTROLKI]')
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)*/
                string kplkp = $"{tbReportKpLkp.Text}";
                string where = $"WHERE (CONCAT(di.jdnr,'.',di.jdline)='{kplkp}' OR CONCAT(jd.jdnr,'.',jd.jdline)='{kplkp}')";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL) AND jd.jobstatus<>'ANULOWANA'";
                //where += " AND (da.courier_type <> 'FEDEX-S' AND sc.dispatch_company IS NULL)";

                ReportWhere = where;
                //scannerBarcodeRevieved("RS00000001", CradleId.ToString());

                ReportName = "Raport po numerze KP i LKP";
                ReportParam1 = $"KP.LKP: {kplkp}";
                ReportParam2 = $"";
            }
            
        }

        #region pobieranie danych
        public void DgvListReportInit(DataGridView dgv, bool colour = false)
        {
            //dgv.Columns["KP_LKP"].HeaderText = "KP.LKP";
            //dgv.Columns["D_Pak"].HeaderText = "D.Pak";
            //dgv.Columns["D_Alok"].HeaderText = "D.Alok";
            //dgv.Columns["D_Wys"].HeaderText = "D.Wys";

            dgv.Columns["Kurier"].Width = 60;
            dgv.Columns["Kurier"].ReadOnly = true;
            dgv.Columns["KP_LKP"].Width = 60;
            dgv.Columns["KP_LKP"].ReadOnly = true;
            dgv.Columns["Odbiorca"].Width = 90;
            dgv.Columns["Odbiorca"].ReadOnly = true;
            dgv.Columns["Ile"].Width = 20;
            dgv.Columns["Ile"].ReadOnly = true;
            dgv.Columns["Spak"].Width = 20;
            dgv.Columns["Spak"].ReadOnly = true;
            dgv.Columns["Typ"].Width = 55;
            dgv.Columns["Typ"].ReadOnly = true;
            dgv.Columns["D_Wys_CM"].Width = 65;
            dgv.Columns["D_Wys_CM"].ReadOnly = true;
            dgv.Columns["D_Wys_PM"].Width = 65;
            dgv.Columns["D_Wys_PM"].ReadOnly = true;
            dgv.Columns["D_Wys_R"].Width = 65;
            dgv.Columns["D_Wys_R"].ReadOnly = true;
            dgv.Columns["D_Pak"].Width = 65;
            dgv.Columns["D_Pak"].ReadOnly = true;
            dgv.Columns["D_Alok"].Width = 65;
            dgv.Columns["D_Alok"].ReadOnly = true;
            dgv.Columns["NRMO"].Width = 80;
            dgv.Columns["NRMO"].ReadOnly = true;
            dgv.Columns["KurierInfo"].Width = 80;
            dgv.Columns["KurierInfo"].ReadOnly = true;
            dgv.Columns["Bryt"].Width = 80;
            dgv.Columns["Bryt"].ReadOnly = true;
            dgv.Columns["OpisLinii"].Width = 250;
            dgv.Columns["OpisLinii"].ReadOnly = true;
            dgv.Columns["Stacja"].Width = 60;
            dgv.Columns["Stacja"].ReadOnly = true;
            dgv.Columns["PowodNiewyk"].Width = 250;
            dgv.Columns["Referenceid"].Visible = false;
            dgv.Columns["Reffrom"].Visible = false;
            dgv.Columns["Refto"].Visible = false;

            //dgv.Columns["Odbiorca"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.RowHeadersVisible = true;

            if (colour)
            {
                foreach (DataGridViewRow dgvr in dgv.Rows)
                {
                    if (dgvr.Cells["D_Pak"].Value.ToString().Equals("-"))
                        dgvr.Cells["D_Pak"].Style.BackColor = Color.Red;

                    if (dgvr.Cells["D_Wys_R"].Value.ToString().Equals("-"))
                        dgvr.Cells["D_Wys_R"].Style.BackColor = Color.Red;

                    //Dopsiane ŁPA Pomarańczowy jak jest powód niewydania
                    if (!dgvr.Cells["PowodNiewyk"].Value.ToString().Equals("") && (dgvr.Cells["D_Pak"].Value.ToString().Equals("-") || dgvr.Cells["D_Wys_R"].Value.ToString().Equals("-")))
                        dgvr.DefaultCellStyle.BackColor = Color.Green;
                }
            }
            //station[stationId].DgvAllocationList.Columns["Package_barcode"].Visible = false;

            /*
            
        public string Kurier { get; set; }
        public string KP_LKP { get; set; }
        public string Odbiorca { get; set; }
        public string Ile { get; set; }
        public string Spak { get; set; }
        public string Typ { get; set; }
        public string D_Wys_CM { get; set; }
        public string D_Wys_PM { get; set; }
        public string D_Wys_R { get; set; }
        public string D_Pak { get; set; }
        public string D_Alok { get; set; }
        public string NRMO { get; set; }
        public string KurierInfo { get; set; }
        public string Bryt { get; set; }
             */
        }

        public class dispachReportClass
        {

            public string Kurier { get; set; }
            public string KP_LKP { get; set; }
            public string Odbiorca { get; set; }
            public string Ile { get; set; }
            public string Spak { get; set; }
            public string Typ { get; set; }
            public string D_Wys_CM { get; set; }
            public string D_Wys_PM { get; set; }
            public string D_Wys_R { get; set; }
            public string D_Pak { get; set; }
            public string D_Alok { get; set; }
            public string NRMO { get; set; }
            public string KurierInfo { get; set; }
            public string Bryt { get; set; }
            public string OpisLinii { get; set; }
            public string Stacja { get; set; }
            public string PowodNiewyk { get; set; }
            public string Referenceid { get; set; }
            public string Reffrom { get; set; }
            public string Refto { get; set; }
        }

        public (List<dispachReportClass>, string) dispachReport(string where)
        {
            List<dispachReportClass> retVal = new List<dispachReportClass>();
            string selectQuery = "";

            selectQuery += " SELECT ";
            selectQuery += " IFNULL(da.courier_type,'-') As 'Kurier',";
            selectQuery += " CASE WHEN di.jdnr IS NULL THEN CONCAT(jd.jdnr,'.',jd.jdline) ELSE CONCAT(di.jdnr,'.',di.jdline) END AS 'KP_LKP',";
            selectQuery += " CASE ";
            selectQuery += " WHEN di.jdnr IS NULL THEN jd.customer ";
            selectQuery += " WHEN da.dispatch_company<>'' THEN da.dispatch_company ";
            selectQuery += " ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) ";
            selectQuery += " END AS 'Odbiorca',";
            selectQuery += " IFNULL(di.qty_to_pack,jd.qtyordered) AS 'Ile',";
            selectQuery += " IFNULL(di.qty_packed,0)  AS 'Spak',";
            selectQuery += " IFNULL(da.pack_type,'-') AS 'Typ',";
            selectQuery += " IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') 'D_Wys_CM',";
            selectQuery += " IFNULL(DATE_FORMAT(jd.dispatch_date,'%Y-%m-%d'),'-') 'D_Wys_PM',";
            selectQuery += " IFNULL(DATE_FORMAT(dd.release_time_stamp,'%Y-%m-%d'),'-') 'D_Wys_R',";
            selectQuery += " IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectQuery += " IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectQuery += " IFNULL(dd.storage_unit_id,'-') AS NRMO,";
            selectQuery += " IFNULL(actual_status,'-') AS 'Kurier Info',";
            selectQuery += " IFNULL(di.tilename,'-') AS 'Bryt',";
            selectQuery += " IFNULL(jd.linedesc,'-') AS 'Opis_linii',";
            selectQuery += " CASE WHEN IFNULL(di.booked_station_id,0)=0 THEN IFNULL(di.stationId,0) ELSE IFNULL(di.booked_station_id,0) END AS 'Stacja',";
            selectQuery += " IFNULL(di.reason_not_done,'') AS 'Powod_niewyk',";
            selectQuery += " di.reference_id as referenceid,di.ref_from as reffrom,di.ref_to as refto";
            selectQuery += " FROM dispatch_address_table da";
            selectQuery += " LEFT JOIN dispatch_item_table di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectQuery += " LEFT JOIN dispatch_statuses ds on da.deliv_list_no=ds.deliv_list_no";
            selectQuery += " LEFT JOIN stroer_companies sc on da.dispatch_company=sc.dispatch_company";
            selectQuery += " LEFT JOIN dispatch_delivery dd on da.reference_id=dd.reference_id and da.ref_from=dd.ref_from and da.ref_to=dd.ref_to";
            selectQuery += " JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectQuery += $" {where}";
            selectQuery += " ORDER BY dd.release_time_stamp,";
            selectQuery += " IFNULL(da.courier_type,'-'),";
            selectQuery += " CASE ";
            selectQuery += " WHEN di.jdnr IS NULL THEN jd.customer ";
            selectQuery += " WHEN da.dispatch_company<>'' THEN da.dispatch_company ";
            selectQuery += " ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) ";
            selectQuery += " END,";
            selectQuery += " CASE WHEN di.jdnr IS NULL THEN CONCAT(jd.jdnr,'.',jd.jdline) ELSE CONCAT(di.jdnr,'.',di.jdline) END";

            DataTable dTabel = SQLConnection.GetData(selectQuery);

            foreach (DataRow dRow in dTabel.Rows)
            {
                dispachReportClass dispachReport = DataRowTodispachReportClass(dRow);
                retVal.Add(dispachReport);
            }

            return (retVal, selectQuery);
        }

        public dispachReportClass DataRowTodispachReportClass(DataRow dRow)
        {
            dispachReportClass retVal = new dispachReportClass();

            retVal.Kurier = dRow["Kurier"].ToString();
            retVal.KP_LKP = dRow["KP_LKP"].ToString();
            retVal.Odbiorca = dRow["Odbiorca"].ToString();
            retVal.Ile = dRow["Ile"].ToString();
            retVal.Spak = dRow["Spak"].ToString();
            retVal.Typ = dRow["Typ"].ToString();
            retVal.D_Wys_CM = dRow["D_Wys_CM"].ToString();
            retVal.D_Wys_PM = dRow["D_Wys_PM"].ToString();
            retVal.D_Wys_R = dRow["D_Wys_R"].ToString();
            retVal.D_Pak = dRow["D_Pak"].ToString();
            retVal.D_Alok = dRow["D_Alok"].ToString();
            retVal.NRMO = dRow["NRMO"].ToString();
            retVal.KurierInfo = dRow["Kurier Info"].ToString();
            retVal.Bryt = dRow["Bryt"].ToString();
            retVal.OpisLinii = dRow["Opis_linii"].ToString();
            retVal.Stacja = dRow["Stacja"].ToString();
            retVal.PowodNiewyk = dRow["Powod_niewyk"].ToString();
            retVal.Referenceid = dRow["referenceid"].ToString();
            retVal.Reffrom = dRow["reffrom"].ToString();
            retVal.Refto = dRow["refto"].ToString();

            return retVal;
        }

        #endregion
    }
}
