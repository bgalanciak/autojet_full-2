﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;


namespace AutojetFinishing
{

    public partial class autojetFinishingMainForm : Form
    {
        //workMode states
        const int workModeSingleRow = 0;
        const int workModeAllPoster = 1;
        const int workModeFinishing2 = 2;
        const int workModeDisable = 3;      //brak kompletacji

        const int checkResNoPartOfPoster = 0;
        const int checkResPartOfPoster = 1;
        const int checkResDoubleTile = 2;

        /// <summary>
        /// 
        /// </summary>
        public void showMultiscanError()
        {
           
        }

        ///
        public void multiScanAnalize(String multiScanStr)
        {
            //uniemożliwia anulowanie plakatu po otrzymaniu sygnału ruchu z PLC
            global.multiscanAnalizeOn = true;
             
            List<String> scanStrList = new List<string>();      //kody odczytane ze skanerów (całe)
            List<String> scanStrListNew = new List<string>();   //kody odczytane ze skanerów (w kodzie jest tylko id)
            //
            string[] parts = multiScanStr.Split(';');
            foreach (string part in parts)
            {
                if (part.Length == 16)  //nie ma takiej możliwości, drukujemy tylko ID z bazy
                {
                    scanStrList.Add(part);
                }
                else if (part.Length == 8)  //
                {
                    scanStrListNew.Add(part);
                }
            }

            Program.autojetFinishingMain.Hm.Info(string.Format("multiScanAnalize(): scanStrList.Count={0}", scanStrList.Count));
            Program.autojetFinishingMain.Hm.Info(string.Format("multiScanAnalize(): scanStrListNew.Count={0}", scanStrListNew.Count));

            //pobranie danych o brycie z bazy danych, jesli zeskanowany kod to id
            if (scanStrListNew.Count > 0)
            {
                List<string> barcodes = new List<string>();
                barcodes = getBarcodedataDB(scanStrListNew);

                //dodanie kodów kreskowych utworzonych na podstawie id i danych pobranych z bazy do listy zeskanowanych kodów kreskowych 
                foreach (string barcode in barcodes)
                {
                    scanStrList.Add(barcode);
                }
            }

            //sortowanie tak, aby bryty A były zawsze przed B
            //sortowanie po 10 znaku 
            //scanStrList = scanStrList.OrderByDescending(q => q[10]).ToList();
            scanStrList = scanStrList.OrderBy(q => q[10]).ToList();

            bool dispachAviableError = false;
            //sprawdzenie, czy dla pierwszego brytu istnieją jakieś wysyłki, jeśli nie to pokazuję błąd i kończę analizę
            if ((Program.Settings.Data.dispachMandatory) && (scanStrList.Count>0))
            {
                //dla zwykłego wszystkich trybów poza dispModeWaybillSubpackAdding i dispModeStroerSubpack
                if ((global.dispachMode != global.dispModeWaybillSbpack) && (global.dispachMode != global.dispModeStroerSubpack))
                {
                    //string barcodeVal = string.Format("{0}{1}{2}{3}", jdnr.ToString("D6"), jdline.ToString("D3"), tiles.Substring(0, 4), tilecounter.ToString("D4"));
                    string jdnr = scanStrList[0].Substring(0, 6);
                    string jdline = scanStrList[0].Substring(6, 3);
                    FinishingDispatchAddressAndItem i = dispachDataAcces.findNextDispachAddressAndItem(jdline, jdnr, Program.Settings.Data.stationId);
                    if (i.item_reference_id == null)
                        dispachAviableError = true;
                }
                else //dla trybu dispModeWaybillSubpackAdding lub dispModeStroerSubpack
                {
                    string jdnr = scanStrList[0].Substring(0, 6);
                    string jdline = scanStrList[0].Substring(6, 3);
                    if (!isPosterOnSubpackList(global.subpackList, jdline, jdnr))
                        dispachAviableError = true;
                }
            }

            //analiza wszystkich zekskanowanych kodów kreskowych
            //tylko jesli znaleziono wysyłkę 
            this.SuspendLayout();
            if (!dispachAviableError)
            {
                foreach (string scanItem in scanStrList)
                {
                    multiScan = true;
                    //właściwa analiza:
                    bool scanAnalizeError = !scanAnalyze(scanItem);     //scanAnalizeError oznacza, że jest błąd do wyświetlenia
                    multiScan = false;
                    if (scanAnalizeError)
                        multiScanErrorShow = true;
                }
            }
            this.ResumeLayout();

            //wyświetlenie formulaza z błędami,
            //multiScanDataPending==0 znaczy,że nie ma już żadnych danych do analizy
            if (multiScanErrorShow && (multiScanDataPending == 0) && !dispachAviableError)
            {
                Program.autojetFinishingMain.Hm.Info(string.Format("multiScanAnalize(): multiErrorShow(), multiScanDataPending={0}", multiScanDataPending.ToString()));
                multiErrorShow();
            }

            //sygnalizacja błędu nieskompletowanego plakatu 
            //(czyli, że po zakończeniu skanowania istnieje jakiś niezeskanowany kafelek), 
            //ale tyko jeśli nie ma innego błędu),
            int errorTiles = countUnscanedTiles(posterAct);

            if (errorTiles>0 && (multiScanDataPending == 0) && (!multiScanErrorShow) && (posterAct.rowCount != 0) && (posterAct.colCount != 0) && !dispachAviableError)
            {
                if (!Program.Settings.Data.showPosterCloseError)
                {
                    global.posterCompletionError = true;
                    plcWriteError(global.posterCompletionError);
                    tErrorReset.Interval = Program.Settings.Data.completionErrorShowTime;
                    tErrorReset.Enabled = true;
                }
                else
                {
                    //pomijaj dane przychodzące ze skanera
                    global.skipScanerData = true;

                    //tworzenie formularza
                    //Zmiana ŁP 2018-11-27 bo pokazywał czasem 0 z 0 przensoze to wyżej
                    errorSourceScanForm form = new errorSourceScanForm();
                    //form.errorSourceCount = countUnscanedTiles(posterAct);
                    form.errorSourceCount = errorTiles;

                    //zmiana uchwytu zdarzenia od skanerów ręcznych
                    for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                    {
                        this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                        this.scannerSerialPorts[i].dataRecieved += form.scannerSerialRecieved;
                    }

                    //sygnalizacja błędu do PLC
                    plcWriteError(true);
                    plcWriteHoldScan(true);

                    //dane przychodzące z kamer cognex będą pomijane
                    global.skipScanerData = true;

                    //DOPISANE ŁP 2018-09-05
                    Program.autojetFinishingMain.Activate();

                    //pokaż formularz błędu
                    if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): Błędy kompletacji...");

                        bool closeVirtual = true;   //zakladam, ze bedzie mozna zamknac plakat wirtualnie

                        Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): global.scanErrorList.Count={0}", global.scanErrorList.Count.ToString());
                        foreach (int i in form.errorList)
                        {
                            scanError scanErrorAct = global.findErrorById(i, global.scanErrorList);
                            if (scanErrorAct != null)
                            {
                                Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): scanErrorAct.closeVirtual={0}", scanErrorAct.closeVirtual.ToString());
                                if (!scanErrorAct.closeVirtual)
                                    closeVirtual = false;
                                insertErrorSource(i);
                                Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): Błędy kompletacji, insertErrorSource({0})", i);
                            }
                        }

                        Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): closeVirtual={0}", closeVirtual);

                        if (Program.Settings.Data.closePosterVirtual && closeVirtual)
                        {
                            foreach (poster.tileType t in posterAct.tilesList)
                            {
                                if (t.tileScanDone == false)
                                {
                                    t.isVirtual = true;
                                    t.tileScanDone = true;
                                }
                            }
                            setPosterTilesAsVirtual(posterAct, global.userAct.id, Program.Settings.Data.stationId);

                            closeActPoster();
                            posterDoneMsg();

                            //
                            if (global.actAddressAndItem.item_reference_id != null)
                            {
                                dispachUpdateDone();
                            }
                            posterAct = new poster();
                            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
                            updateVisu();

                            Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): closeActPoster() done");
                        }
                    }

                    //koniec
                    global.skipScanerData = false;

                    //koniec: sygnalizacja błędu do PLC
                    plcWriteError(false);
                    plcWriteHoldScan(false);

                    //zmiana uchwytu zdarzenia od skanerów ręcznych
                    for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                    {
                        this.scannerSerialPorts[i].dataRecieved -= form.scannerSerialRecieved;
                        if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                            this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                    }

                    global.skipScanerData = false;
                }
            }

            if ((multiScanDataPending == 0) && dispachAviableError)
            {
				//pokaż błąd braku wysyłki
				//DOPISANE ŁP 2018-09-05
				Program.autojetFinishingMain.Activate();
                showNoDispachErrorForm();
            }

            //kasowanie zmiennej
            if (multiScanDataPending == 0)
            {
                multiScanErrorShow = false;
            }

            //pozwala na anulowanie plakatu po otrzymaniu sygnału ruchu z PLC
            global.multiscanAnalizeOn = false;
        }

        private void showNoDispachErrorForm()
        {
			//DOPISANE ŁP 2018-09-05
			Program.autojetFinishingMain.Activate();

			noDispachErrorForm noDispachError = new noDispachErrorForm();
				
            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                this.scannerSerialPorts[i].dataRecieved += noDispachError.scannerSerialRecieved;
            }
            //pomijaj przchodzące dane ze skanera
            global.skipIncomingScan = true;

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            noDispachError.ShowDialog();

            global.skipIncomingScan = false;

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= noDispachError.scannerSerialRecieved;
                if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                    this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
            }
        }

        private void dispachUpdateDone()
        {
            //inkrementacja ilości skompletowanych paczek i druk etykiety jeśli to koniec paczki/subpaczki
            updateDispachCount(posterAct);
            //pobieranie wysyłek dla tego rodzaju plakatu
            if (global.dispachMode == global.dispModeWaybillSbpack)
            {
                //update: 2017-11-28
                //if (!global.subpackAddingComplete)
                //{
                //    //if package is not coplete show package items to operator, 
                //    findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL
                //}
                //else
                //{
                //    //swich off subpackAdding mode
                //    //dispWarehouseOn();
                //}
                findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL
            }
            else
            {
                findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach
            }
        }

        private void dispWarehouseOn()  //brak trybu 
        {
            Program.autojetFinishingMain.Hm.Warning("dispWarehouseOn()");
            //clear data
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //global.zplData = new global.zplDataType();
            global.subpackAddingComplete = false;
            
            //set new mode
            global.dispachMode = global.dispModeWarehouse;
            global.dispachAllDone = false;

            //change colour of buttons
            bDispSubpackAddingOn.BackColor = default(Color);
            bDispWaybillOn.BackColor = default(Color);
            //bDispWarehouseOn.BackColor = Color.LightGreen;
            bDispStroerOn.BackColor = default(Color);
            bDispStroerSubpackOn.BackColor = default(Color);

            //
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                tableLayoutPanel1.ColumnStyles[0].Width = tabPageControl.Width - 6;
                bOperationAddManual.Enabled = false;
                bOperationRollback.Enabled = false;
            }
            else
            {
                tableLayoutPanel1.ColumnStyles[0].Width = global.tableLayoutPanelWidth;
                bOperationAddManual.Enabled = true;
                bOperationRollback.Enabled = true;
            }
        }

        private void dispWaybillOn()
        {
            Program.autojetFinishingMain.Hm.Warning("dispWaybillOn()");
            //clear data
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //global.zplData = new global.zplDataType();
            global.subpackAddingComplete = false;

            //set new mode
            global.dispachMode = global.dispModeWaybill;
            global.dispachAllDone = false;

            //change colour of buttons
            bDispSubpackAddingOn.BackColor = default(Color);
            bDispWaybillOn.BackColor = Color.LightGreen;
            //bDispWarehouseOn.BackColor = default(Color);
            bDispStroerOn.BackColor = default(Color);
            bDispStroerSubpackOn.BackColor = default(Color);

            //
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                tableLayoutPanel1.ColumnStyles[0].Width = tabPageControl.Width - 6;
                bOperationAddManual.Enabled = false;
                bOperationRollback.Enabled = false;
            }
            else
            {
                tableLayoutPanel1.ColumnStyles[0].Width = global.tableLayoutPanelWidth;
                bOperationAddManual.Enabled = true;
                bOperationRollback.Enabled = true;
            }
        }

        private void dispWaybillSubpackOn()
        {
            Program.autojetFinishingMain.Hm.Warning("dispSubpackAddingOn()");
            //clear data
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //global.zplData = new global.zplDataType();
            global.subpackAddingComplete = false;

            //set new mode
            global.dispachMode = global.dispModeWaybillSbpack;
            global.dispachAllDone = false;

            //change colour of buttons
            bDispSubpackAddingOn.BackColor = Color.LightGreen;
            bDispWaybillOn.BackColor = default(Color);
            //bDispWarehouseOn.BackColor = default(Color);
            bDispStroerOn.BackColor = default(Color);
            bDispStroerSubpackOn.BackColor = default(Color);

            //
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                tableLayoutPanel1.ColumnStyles[0].Width = tabPageControl.Width - 6;
                bOperationAddManual.Enabled = false;
                bOperationRollback.Enabled = false;
            }
            else
            {
                tableLayoutPanel1.ColumnStyles[0].Width = global.tableLayoutPanelWidth;
                bOperationAddManual.Enabled = true;
                bOperationRollback.Enabled = true;
            }
        }

        private void dispStroerOn()
        {
            Program.autojetFinishingMain.Hm.Warning("dispStroerOn()");
            //clear data
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //global.zplData = new global.zplDataType();
            global.subpackAddingComplete = false;

            //set new mode
            global.dispachMode = global.dispModeStroer;
            global.dispachAllDone = false;

            //change colour of buttons
            bDispSubpackAddingOn.BackColor = default(Color);
            bDispWaybillOn.BackColor = default(Color);
            //bDispWarehouseOn.BackColor = default(Color);
            bDispStroerOn.BackColor = Color.LightGreen;
            bDispStroerSubpackOn.BackColor = default(Color);

            //
            tableLayoutPanel1.ColumnStyles[0].Width = tabPageControl.Width - 6;
            bOperationAddManual.Enabled = false;
            bOperationRollback.Enabled = false;
        }

        private void dispStroerSubpackOn()
        {
            Program.autojetFinishingMain.Hm.Warning("dispStroerSubpackOn()");
            //clear data
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //global.zplData = new global.zplDataType();
            global.subpackAddingComplete = false;

            //set new mode
            global.dispachMode = global.dispModeStroerSubpack;
            global.dispachAllDone = false;

            //change colour of buttons
            bDispSubpackAddingOn.BackColor = default(Color);
            bDispWaybillOn.BackColor = default(Color);
            //bDispWarehouseOn.BackColor = default(Color);
            bDispStroerOn.BackColor = Color.LightGreen;
            bDispStroerSubpackOn.BackColor = default(Color);

            //
            tableLayoutPanel1.ColumnStyles[0].Width = tabPageControl.Width - 6;
            bOperationAddManual.Enabled = false;
            bOperationRollback.Enabled = false;
        }

        public List<string> getBarcodedataDB(List<string> ids)
        {
            List<string> barcodes = new List<string>();
            /*
            jdnr = scanStr.Substring(0, 6);
            jdline = scanStr.Substring(6, 3);
            tiles = scanStr.Substring(9, 4);
            machine = Program.Settings.Data.stationId.ToString();
            tilecounter = int.Parse(scanStr.Substring(13, 3));
             
            string queryCalc = string.Format("SELECT DISTINCT tiles FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1", item.jdnr, item.jdline);
            DataTable dTableCalc = SQLConnection.GetData(queryCalc);
            
             
            jdnr = scanStr.Substring(0, 6);
            jdline = scanStr.Substring(6, 3);
            tiles = scanStr.Substring(9, 4);
            machine = Program.Settings.Data.stationId.ToString();
            tilecounter = int.Parse(scanStr.Substring(13, 3));
            */

            //test zawartości kodu kreskowego
            List<string> idsNew = new List<string>();

            for (int i = 0; i < ids.Count; i++)
            {
                int intOut;
                bool isInt = int.TryParse(ids[i], out intOut);
                if (isInt)
                    idsNew.Add(ids[i]);
            }

            if (idsNew.Count<1)
            {
                return barcodes;
            }


            //tworzenie zapytania
            string queryCalc = string.Format("SELECT * FROM Printtemp WHERE (id={0})", idsNew[0].ToString());
            for (int i = 1; i < idsNew.Count; i++)
            {
                queryCalc += string.Format(" or (id={0})", idsNew[i].ToString());
            }
            queryCalc += string.Format(" order by tiles asc");
            DataTable dTableCalc = SQLConnection.GetData(queryCalc);

            foreach (DataRow dRow in dTableCalc.Rows)
            {
                int jdnr = int.Parse(dRow["jdnr"].ToString());
                int jdline = int.Parse(dRow["jdline"].ToString());
                string tiles = dRow["tiles"].ToString();
                int printer = int.Parse(dRow["printer"].ToString());
                int tilecounter = int.Parse(dRow["tilecounter"].ToString());

                string barcodeVal = string.Format("{0}{1}{2}{3}", jdnr.ToString("D6"), jdline.ToString("D3"), tiles.Substring(0, 4), tilecounter.ToString("D4"));

                barcodes.Add(barcodeVal);
            }

            return barcodes;
        }

        public void insertErrorSource(int errorSource)
        {

            //CREATE TABLE `tjdata`.`tile_scan_error_log` (
            //  `id` INT NOT NULL AUTO_INCREMENT,
            //  `errorId` INT NOT NULL,
            //  `jdnr` VARCHAR(6) NOT NULL,
            //  `jdline` VARCHAR(3) NOT NULL,
            //  `machine` VARCHAR(15) NOT NULL,
            //  `operatorId` INT NOT NULL,
            //  `operationTimeStamp` DATETIME NOT NULL,
            //  PRIMARY KEY (`id`));

            //string insertQuery = string.Format("INSERT INTO tjdata.tile_scan_error_log(errorId, jdnr, jdline, machine, operatorId, operationTimeStamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', NOW())", errorSource, posterAct.jdnr, posterAct.jdline, Program.Settings.Data.stationId, global.userAct.id);
            //wersja bez "machine":
            string insertQuery = string.Format("INSERT INTO tjdata.tile_scan_error_log(errorId, jdnr, jdline, machine, operatorId, operationTimeStamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', NOW())", errorSource, posterAct.jdnr, posterAct.jdline, "", global.userAct.id);

            SQLConnection.InsertData(insertQuery);
        }


        public int countUnscanedTiles(poster p)
        {
            int retVal=0;
            foreach (poster.tileType t in p.tilesList)
                if (!t.tileScanDone)
                    retVal++;
            return retVal;
        }


        public bool multiScan = false;

        /// <summary>
        /// Analiza zeskanowanego kodu kreskowego, utworzenie na podstawie zeskanowanych danych obiektu scanItem
        /// </summary>
        /// <param name="scanStr"></param>
        /// <returns></returns>
        public bool scanAnalyze(string scanStr)
        {
            scanItem item = new scanItem();

            //zamiana danych o brycie na obiekt typu scanItem
            item = scanDataToScanItem(scanStr);

            //sprawdzenie, czy przy włączonym trybie subpackAdding kod kreskowy pasuje do którejś subpaczki w tej paczce
            if (global.dispachMode == global.dispModeWaybillSbpack)   //&& !posterAct.initDone
            {
                if (!isPosterOnSubpackList(global.subpackList, item.jdline, item.jdnr))
                {
                    //Program.autojetFinishingMain.Hm.Warning(string.Format("Błąd - plakat nie jest częścią kompletowanej wysyłki jdline={0}, jdnr={1}", item.jdline, item.jdnr));
                    tileIsNotPartOfDispachError(item);
                    return false;
                }
            }

            //sprawdzenie, czy wybrano dobry tryb pracy
            if ((!posterAct.initDone) && (Program.Settings.Data.workMode != workModeFinishing2))
            {
                if (!properWorkMode(item))
                {
                    Program.autojetFinishingMain.Hm.Warning("wrong workMode, new workMode={0}", Program.Settings.Data.workMode);
                    updateWorkModeVisu();
                }
            }
            
            //analiza zeskanowanego kodu kreskowego
            if (!posterAct.initDone)
            //skanowanie pierwszego kafelka
            { 
                Program.autojetFinishingMain.Hm.Warning("scanAnalyze():!posterAct.initDone");
                // 3. czy zeskanowany kod nie był wcześniej użyty (sprawdzenie w bazie danych): checkTileRescan1
                if ((Program.Settings.Data.workMode == workModeAllPoster) || (Program.Settings.Data.workMode == workModeSingleRow))
                {
                    // wynik >0 oznacza, że kafelek jest już w bazie = błąd rescanTileError
                    // wynik =0 oznacza, że kafelek nie był nigdy skanowany, wszystko OK,
                    // wynik -1 oznacza, że kafelka nie ma w bazie danych 
                    int checkTileRescanRes = checkTileRescan1(item);
                    if (checkTileRescanRes> 0)
                    {
                        //oznaczam kafelek jako niezeskanowany 
                        foreach (poster.tileType t in posterAct.tilesList)
                        {
                            if (t.tiles.Equals(item.tiles))
                            {
                                t.tileScanDone = false;
                            }
                        }
                        Program.autojetFinishingMain.Hm.Warning("scanAnalyze():checkTileRescan1(item) > 0");
                        rescanTileError(item.tiles);
                        return false;
                    }
                    else if (checkTileRescanRes == -1)
                    {
                        Program.autojetFinishingMain.Hm.Warning("scanAnalyze():checkTileRescan1(item) = -1");
                        databasePrinttempError();
                        return false;
                    }
                }
                else
                {
                    // wynik >0 oznacza, że kafelek jest już w bazie = błąd rescanTileError
                    // wynik =0 oznacza, że kafelek nie był nigdy skanowany, wszystko OK,
                    // wynik -1 oznacza, że kafelka nie ma w bazie danych 
                    int checkTileRescanRes = checkTileRescan2(item);

                    if (checkTileRescanRes > 0)
                    {
                        //oznaczam kafelek jako niezeskanowany 
                        foreach (poster.tileType t in posterAct.tilesList)
                        {
                            if (t.tiles.Equals(item.tiles))
                            {
                                t.tileScanDone = false;
                            }
                        }

                        Program.autojetFinishingMain.Hm.Warning("scanAnalyze():checkTileRescan2(item) > 0");
                        rescanTileError(item.tiles);
                        return false;
                    }
                    else if (checkTileRescanRes == -1)
                    {
                        Program.autojetFinishingMain.Hm.Warning("scanAnalyze():checkTileRescan2(item) = -1");
                        databasePrinttempError();
                        return false;
                    } 
                }

                Program.autojetFinishingMain.Hm.Warning("scanAnalyze():getOpenPosterData(item)");
                posterAct = new poster(getOpenPosterData(item));

                //jesli jobformat==null to znaczy, ze kafelka nie ma w bazie Jobsdata
                if (posterAct.jobformat == null)
                {
                    databasePrinttempError();
                    return false;
                }
                else
                {
                    posterAct.initDone = true;
                }
                
                Program.autojetFinishingMain.Hm.Warning("scanAnalyze(): qtyPrinted={0}", posterAct.qtyPrinted.ToString());
                //lTst.Text = posterAct.qtyPrinted.ToString();
                posterAnalize(posterAct);

                //rezerwowanie wysyłki i pobieranie jej danych do global.actAddressAndItem 
                if ((Program.Settings.Data.workMode == workModeAllPoster) || ((Program.Settings.Data.workMode == workModeSingleRow) && (posterAct.addSubcolection)) || (Program.Settings.Data.workMode == workModeFinishing2))
                {
                    if (global.dispachMode == global.dispModeWaybillSbpack)   //dodawanie subpaczki do zeskanowanego ZPLa
                    {
                        findDispachForZPL(global.subpackList, item.jdline, item.jdnr);  //find and book next dispach for ZPL
                    }
                    else
                        findDispach(posterAct);                         //find and book next dispach
                    Program.autojetFinishingMain.Hm.Warning("scanAnalyze(): findDispach(posterAct) done OK");
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Warning("scanAnalyze(): No dispach waybill will be done in this mode...");
                    global.actAddressAndItem = new FinishingDispatchAddressAndItem();
                    dgvDispatchTable.DataSource = global.actAddressAndItem;
                }
                
                //dodawanie do bazy danych
                if ((Program.Settings.Data.workMode == workModeAllPoster) || (Program.Settings.Data.workMode == workModeSingleRow))
                {
                    //
                    insertScanedItem1(item, global.userAct.id, Program.Settings.Data.stationId);
                    
                    //jesli bede dodawał też bryt B to pierwszy bryt wchodzi także do bazy finishing2_log
                    if (posterAct.addSubcolection)
                    {
                        insertScanedItem2(item, global.userAct.id, Program.Settings.Data.stationId);
                    }
                }
                else
                {
                    insertScanedItem2(item, global.userAct.id, Program.Settings.Data.stationId);
                }

                //określenie numeru rzędu skanowanego projektu, na potrzeby obsługi tabeli poster_closeed_log
                posterAct.jdrow = checkPosterJdrow(item, posterAct);
                
                //pobranie informacji o ilości zamkniętych plakatów
                posterAct.qtyClosed = getClosedCount(posterAct);
                posterAct.qtyClosedVirtual = getClosedVirtualCount(posterAct);

                //
                if (checkPosterDone() == false)
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkPosterDone() == false");
                    //updateGlobalData();
                    getJobsData(posterAct);
                    //getDispatchData(posterAct);   //stary sposób,

                    //rezerwacja wysyłki tylko w wybranych tryybach
                    if ((Program.Settings.Data.workMode == workModeAllPoster) || ((Program.Settings.Data.workMode == workModeSingleRow) && (posterAct.addSubcolection)) || (Program.Settings.Data.workMode == workModeFinishing2))
                    {
                        //pobieranie wysyłek dla tego rodzaju plakatu
                        if (global.actAddressAndItem.item_reference_id != null)
                            if (global.dispachMode == global.dispModeWaybillSbpack)
                            {
                                findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL
                            }
                            else
                            {
                                findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach
                            }
                        else
                        {
                            //clear DGV
                            dgvDispatchTable.DataSource = global.actAddressAndItem;
                        }
                    }
                    updateVisu();
                    //getJobsData(posterAct);
                    return true;
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkPosterDone() == true");
                    closeActPoster();
                    posterDoneMsg();
                    //updateGlobalData();
                    //getJobsData(posterAct);
                    
                    //druk etykiety i pobieranie zakualizowanej listy adresów
                    if (global.actAddressAndItem.item_reference_id != null)
                    {
                        //updateDispachCount(posterAct);

                        //if (global.dispachMode == global.dispSubpackAdding)
                        //{
                        //    if (!global.subpackAddingComplete)
                        //    {
                        //        //if package is not coplete show package items to operator, 
                        //        findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL
                        //    }
                        //    else
                        //    {
                        //        //swich off subpackAdding mode
                        //        dispWarehouseOn();
                        //    }
                        //}
                        //else
                        //{
                        //    findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach
                        //}
                        dispachUpdateDone();
                    }

                    posterAct = new poster();
                    global.actAddressAndItem = new FinishingDispatchAddressAndItem();
                    updateVisu();
                    return true;
                }
            }
            else
            {
                //skanowanie kolejnego kafelka
                // 1. czy zeskanowany kod jest częścią plakatu, czy jest zdublowany: checkTile()
                // 
                // odpada : 2. czy kafelek nie był wcześniej zeskanowany (czy nie jest wśród kafli w aktualnie otwartym plakacie): checkTileDouble
                //
                // 3. czy zeskanowany kod nie był wcześniej użyty (sprawdzenie w bazie danych): checkTileRescan
                //


                Program.autojetFinishingMain.Hm.Info("scanAnalyze(): Skanowanie kolejnego kafelka...");

                // 1. czy zeskanowany kod jest kodem z aktualnego plakatu: jdnr, jdline, ?tiles?
                /*if ((item.jdnr != posterAct.jdnr) || (item.jdline != posterAct.jdline))
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): (item.jdnr != posterAct.jdnr) || (item.jdline != posterAct.jdline)");
                    wrongTileError();
                    return false;
                }*/

                //1.
                int checkTileRes = checkTile(item);
                if (checkTileRes == checkResDoubleTile)
                {
                    Program.autojetFinishingMain.Hm.Info("checkTileRes == checkResDoubleTile");
                    doubleTileError(item.tiles);
                    return false;
                }
                else if (checkTileRes == checkResNoPartOfPoster)
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkTileRes == checkResNoPartOfPoster");
                    wrongTileError(item.tiles);
                    return false;
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info("checkTileRes == checkResPartOfPoster");
                }

                // 2. czy kafelek nie był wcześniej zeskanowany (czy nie jest wśród kafli w aktualnie otwartym plakacie): checkTileDouble
                /*
                if (checkTileDouble(item) == true)
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkTileDouble(item) == true");
                    doubleTileError();
                    return false;
                }*/

                //3. czy zeskanowany kod nie był wcześniej użyty (sprawdzenie w bazie danych): checkTileRescan1 lub checkTileRescan2
                if ((Program.Settings.Data.workMode == workModeAllPoster) || (Program.Settings.Data.workMode == workModeSingleRow))
                {
                    if ((posterAct.addSubcolection) && (item.tiles.Substring(1, 1).Equals("B")))
                    {
                        //sprawdzam w bazie finishing2
                        if (checkTileRescan2(item) != 0)
                        {
                            foreach (poster.tileType t in posterAct.tilesList)
                            {
                                if (t.tiles.Equals(item.tiles))
                                {
                                    t.tileScanDone = false;
                                }
                            }

                            Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkTileRescan2(item) != 0");
                            rescanTileError(item.tiles);
                            return false;
                        }
                    }
                    else
                    {
                        //sprawdzam w bazie finishing
                        if (checkTileRescan1(item) != 0)
                        {
                            foreach (poster.tileType t in posterAct.tilesList)
                            {
                                if (t.tiles.Equals(item.tiles))
                                {
                                    t.tileScanDone = false;
                                }
                            }

                            Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkTileRescan1(item) != 0");
                            rescanTileError(item.tiles);
                            return false;
                        }
                    }
                }
                else
                {
                    //sprawdzam w bazie finishing2
                    if (checkTileRescan2(item) != 0)
                    {
                        foreach (poster.tileType t in posterAct.tilesList)
                        {
                            if (t.tiles.Equals(item.tiles))
                            {
                                t.tileScanDone = false;
                            }
                        }

                        Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkTileRescan2(item) != 0");
                        rescanTileError(item.tiles);
                        return false;
                    }
                }
                //todo

                /*
                //4. czy zeskanowany kafelek (tiles) nie jest wśród zeksanowanych kafli
                if (checkTileDouble(item) == true)
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkTileDouble(item) == true");
                    doubleTileError();
                    return false;
                }*/

                Program.autojetFinishingMain.Hm.Info("scanAnalyze(): wszystkie testy OK!");

                //wszystkie testy OK!
                if ((Program.Settings.Data.workMode == workModeAllPoster) || (Program.Settings.Data.workMode == workModeSingleRow))
                {
                    if ((posterAct.addSubcolection) && (item.tiles.Substring(1, 1).Equals("B")))
                    {
                        insertScanedItem2(item, global.userAct.id, Program.Settings.Data.stationId);
                        Program.autojetFinishingMain.Hm.Info("scanAnalyze(): insertScanedItem2()");
                    }
                    else
                    {
                        insertScanedItem1(item, global.userAct.id, Program.Settings.Data.stationId);
                        Program.autojetFinishingMain.Hm.Info("scanAnalyze(): insertScanedItem1()");
                    }
                }
                else
                {
                    insertScanedItem2(item, global.userAct.id, Program.Settings.Data.stationId);
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): insertScanedItem2()");
                }

                //przeniesione do checkTile
                //markScanedItem(item);
                //Program.autojetFinishingMain.Hm.Info("scanAnalyze(): markScanedItem()");

                if (checkPosterDone() == false)
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkPosterDone() == false");
                    //updateGlobalData();
                    updateVisu();
                    //getJobsData(posterAct);
                    return true;
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info("scanAnalyze(): checkPosterDone() == true");
                    closeActPoster();
                    posterDoneMsg();
                    //updateGlobalData();
                    //getJobsData(posterAct);
                    if (global.actAddressAndItem.item_reference_id != null)
                    {
                        ////inkrementacja ilości skompletowanych paczek i druk etykiety jeśli to koniec paczki/subpaczki
                        //updateDispachCount(posterAct);
                        ////pobieranie wysyłek dla tego rodzaju plakatu
                        //if (global.dispachMode == global.dispSubpackAdding)
                        //    if (!global.subpackAddingComplete)
                        //    {
                        //        //if package is not coplete show package items to operator, 
                        //        findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL
                        //    }
                        //    else
                        //    {
                        //        //inkrementacja ilości skompletowanych paczek i druk etykiety jeśli to koniec paczki/subpaczki
                        //        updateDispachCount(posterAct);
                        //        //pobieranie wysyłek dla tego rodzaju plakatu
                        //        if (global.dispachMode == global.dispSubpackAdding)
                        //        {
                        //            if (!global.subpackAddingComplete)
                        //            {
                        //                //if package is not coplete show package items to operator, 
                        //                findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL
                        //            }
                        //            else
                        //            {
                        //                //swich off subpackAdding mode
                        //                dispWarehouseOn();
                        //            }

                        //        }
                        //        else
                        //        {
                        //            findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach
                        //        }
                        //    }
                        //else
                        //{
                        //    findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach
                        //}

                        dispachUpdateDone();
                    }

                    posterAct = new poster();
                    global.actAddressAndItem = new FinishingDispatchAddressAndItem();
                    updateVisu();
                    return true;
                }

            }
            return true;
        }

        public void updateDispachCount(poster posterAct)
        {
            if (global.actAddressAndItem.item != null)
            {
                global.actAddressAndItem.item.qty_packed++;
                if (global.actAddressAndItem.item.qty_packed >= global.actAddressAndItem.item.qty_to_pack)
                {
                    //to samo zostało przeniesione do jdnrScanAnalizeMain(), 
                    //start beep
                    tDispatchDone.Enabled = true;
                    plcWriteError(true);

                    if ((global.dispachMode == global.dispModeWaybill) || (global.dispachMode == global.dispModeWaybillSbpack))
                    {
                        //2018-02-20 - register weight
                        registerPackageWeight();
                    }

                    //close item id DB
                    dispachDataAcces.UpdateDispachItemPackedQty(global.actAddressAndItem.item);           //update packed_qty
                    dispachDataAcces.UpdateDispachItemcompletationDone(global.actAddressAndItem.item, global.userAct.id, Program.Settings.Data.stationId);    //update completation_done and booked_station_id
                    //print label depending of type of package
                    printDispachLabel(global.actAddressAndItem, global.zplData, global.actAddressAndItem.item.weight_act);
                    //
                    //save data
                    global.lastLabelAdresAndItem = global.actAddressAndItem;
                    global.lastLabelZpl = global.zplData;
                    global.lastLabelWeight = global.actAddressAndItem.item.weight_act;
                    //clear data
                    if (global.dispachMode == global.dispModeWaybillSbpack)
                    {
                        global.subpackAddingComplete = true;
                    }
                }
                else
                {
                    //update counter in db
                    dispachDataAcces.UpdateDispachItemPackedQty(global.actAddressAndItem.item);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void registerPackageWeight()
        {

            //set done colour for all tiles
            foreach (poster.tileType t in posterAct.tilesList)
            {
                    buttonList[t.colNum - 1, t.rowNum - 1].BackColor = Color.Green;
            }

            //check if selected weight model is proper
            if ((global.weightModel.jdnr == global.actAddressAndItem.item.jdnr) && (global.weightModel.jdline == global.actAddressAndItem.item.jdline) && (global.weightModel.pack_type == global.actAddressAndItem.address.pack_type))
            {
                //weight model OK
            }
            else
            {
                //check if model is in DB
                global.weightBarcodeScan jdnrScan = new global.weightBarcodeScan();
                jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
                jdnrScan.jdline = global.actAddressAndItem.item.jdline;
                jdnrScan.pack_type = global.actAddressAndItem.address.pack_type;
                jdnrScan.tileName = global.actAddressAndItem.item.tileName;
                jdnrScan.modelType = dispachDataAcces.GetModelModeFromAdresAndItem(global.actAddressAndItem);
                jdnrScan.model_scan_count = 1;// global.actAddressAndItem.item.qty_to_pack;

                global.weightModelDataType weightModelFromDB = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
                Program.autojetFinishingMain.Hm.Info($"registerPackageWeight: weightModelFromDB.id = {weightModelFromDB.id} ");
                if (weightModelFromDB.id > -1)
                {
                    Program.autojetFinishingMain.Hm.Info($"registerPackageWeight: weightModelFromDB OK");
                    //pobrano model z DB, przepisać do global.weightModel
                    global.weightModel.jdnr = weightModelFromDB.jdnr;
                    global.weightModel.jdline = weightModelFromDB.jdline;
                    global.weightModel.pack_type = weightModelFromDB.pack_type;
                    global.weightModel.id = weightModelFromDB.id;
                    global.weightModel.weight_model = weightModelFromDB.weight_model;
                    global.weightModel.weight_tare = weightModelFromDB.weight_tare;
                    global.weightModel.weight_base = 0; //ToDo
                    Program.autojetFinishingMain.Hm.Info($"registerPackageWeight: pobierałem weightModel z DB");
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info($"registerPackageWeight: pobieram weightModel z formularza ");
                    //pobrać model z wagi:
                    global.weightModelDataType model = getWeightModelFromForm(jdnrScan, 1);       //0 = model weight + tare weight, 1 = model weight + tare weight , 2 = tare
                    //if ((model.weight_model > 50) && (model.weight_tare > 50))
                    //{
                        dispachDataAcces.insertWeightModel(model);
                        Program.autojetFinishingMain.Hm.Info($"registerPackageWeight: pobrałem weightModel z formularza ");
                        global.weightModel = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
                        weightModelVisuUpdate(global.weightModel);
                    //}
                }
            }

            //register weight
            weightRegistrationForm weightRegistration = new weightRegistrationForm();
            this.weightSerialPorts.dataRecieved -= weightSerialRecieved;
            this.weightSerialPorts.dataRecieved += weightRegistration.weightSerialRecieved;
            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                this.scannerSerialPorts[i].dataRecieved += weightRegistration.scannerSerialRecieved;
            }

            //
            List<string> weightMotives = new List<string>();
            if (global.dispachMode == global.dispModeWaybillSbpack)
            {
                weightMotives.Add($"{global.actAddressAndItem.item.qty_packed} szt. KP {global.actAddressAndItem.item.jdnr} Linia {global.actAddressAndItem.item.jdline} " + Environment.NewLine);
                //weightMotives = getMotivesName(global.subpackList);
                global.weightModel.weight_base = 0; // calcWeightBase(global.subpackList); 2018-04-10
            }
            else
            {
                weightMotives.Add($"{global.actAddressAndItem.item.qty_packed} szt. KP {global.actAddressAndItem.item.jdnr} Linia {global.actAddressAndItem.item.jdline} " + Environment.NewLine);
                global.weightModel.weight_base = 0;
            }

            weightRegistration.weightMotives = weightMotives;

            plcWriteHoldScan(true);

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            weightRegistration.ShowDialog();
            plcWriteHoldScan(false);

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= weightRegistration.scannerSerialRecieved;
                if (this.scannerSerialPorts[i].eventCount() == 0)       //attach event only if there is no other event attached
                    this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
            }
            this.weightSerialPorts.dataRecieved -= weightRegistration.weightSerialRecieved;
            if (this.weightSerialPorts.eventCount() == 0)               //attach event only if there is no other event attached
                this.weightSerialPorts.dataRecieved += weightSerialRecieved;
        }

        private int calcWeightBase(List<FinishingDispatchAddressAndItem> subpackList)
        {
            int retVal = 0;
            foreach (FinishingDispatchAddressAndItem addres in subpackList)
            {
                if (addres.completation_done)
                    retVal += addres.item.weight_act;
            }
            return retVal;
        }

        private List<string> getMotivesName(List<FinishingDispatchAddressAndItem> subpackList)
        {
            List<string> retVal = new List<string>();
            foreach (FinishingDispatchAddressAndItem addres in subpackList)
            {
                if (addres.completation_done)
                    retVal.Add($"{addres.item.qty_packed} szt. KP {addres.item.jdnr} Linia {addres.item.jdline} " + Environment.NewLine);
            }
            retVal.Add($"{global.actAddressAndItem.item.qty_packed} szt. KP {global.actAddressAndItem.item.jdnr} Linia {global.actAddressAndItem.item.jdline} " + Environment.NewLine);
            return retVal;
        }

        private bool checkPartialPackageComplete(global.zplDataType zpl)
        {
            if (dispachDataAcces.countUncompletedSupackage(zpl) == 0)
                return true;
            else
                return false;
        }

        private bool properWorkMode(scanItem item)
        {
            string queryCalc = string.Format("SELECT DISTINCT tiles FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1", item.jdnr, item.jdline);
            Program.autojetFinishingMain.Hm.Info("properWorkMode(), queryCalc={0}", queryCalc);
            DataTable dTableCalc = SQLConnection.GetData(queryCalc);

            Program.autojetFinishingMain.Hm.Info("properWorkMode(), poster tiles count={0}", dTableCalc.Rows.Count);

            /*
            retVal.jobformat = getPosterSize(retVal);

                    if (!string.IsNullOrEmpty(retVal.jobformat))
                    {
                        Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): singleRowSellect");

                        string singleRowSelQuery = string.Format("SELECT * FROM tjdata.typed_sizes_table where format='{0}'", retVal.jobformat);
                        DataTable singleRowSelTable = SQLConnection.GetData(singleRowSelQuery);

                        Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): singleRowSelTable.Rows.Count={0}, ", singleRowSelTable.Rows.Count);

                        if ((singleRowSelTable.Rows.Count > 0) && (item.tiles.Substring(1, 1).Equals("A")))
                            retVal.addSubcolection = true;
                        else
                            retVal.addSubcolection = false;

             */
            bool typedJobFormat = false;
            string jobformat = getPosterSize(item);
            if (!string.IsNullOrEmpty(jobformat))
            {
                string singleRowSelQuery = string.Format("SELECT * FROM tjdata.typed_sizes_table where format='{0}'", jobformat);
                DataTable singleRowSelTable = SQLConnection.GetData(singleRowSelQuery);
                if (singleRowSelTable.Rows.Count > 0)
                {
                    typedJobFormat = true;
                }
            }

            int properWorkMode;
            bool abcFound = false;

            if (typedJobFormat || (dTableCalc.Rows.Count > Program.Settings.Data.maxTilesCountForModeAllPoster))
            {
                for (int iii=0;iii< dTableCalc.Rows.Count;iii++)
                {
                    if (dTableCalc.Rows[iii][0].ToString().Contains("A") || dTableCalc.Rows[iii][0].ToString().Contains("B") || dTableCalc.Rows[iii][0].ToString().Contains("C"))
                    {
                        abcFound = true;
                    }
                }

                if (!abcFound)
                {
                    properWorkMode = workModeAllPoster;
                    Program.autojetFinishingMain.Hm.Info("properWorkMode(), properWorkMode = workModeAllPoster");
                }
                else
                {
                    properWorkMode = workModeSingleRow;
                    Program.autojetFinishingMain.Hm.Info("properWorkMode(), properWorkMode = workModeSingleRow");
                }
            }
            else
            {
                properWorkMode = workModeAllPoster;
                Program.autojetFinishingMain.Hm.Info("properWorkMode(), properWorkMode = workModeAllPoster");
            }

            if (Program.Settings.Data.workMode != properWorkMode)
            {
                Program.Settings.Data.workMode = properWorkMode;
                return false;
            }
            else
                return true;
        }



        /// <summary>
        /// Zwraca numer rzędu aktualnie skanowanej pracy (na potrzeby poster_closeed_log) na podstawie pierwszego zeskanowanego kafelka
        /// </summary>
        /// <param name="i">pierwszy zeskanowany kafelek</param>
        /// <param name="p">dane o plakacie</param>
        /// <returns></returns>
        public int checkPosterJdrow(scanItem i, poster p)
        {
            if (Program.Settings.Data.workMode == workModeAllPoster)
            {
                return 0;   //w trybie workModeAllPoster: 0
            }
            else if (Program.Settings.Data.workMode == workModeSingleRow)
            {
                //w trybie workModeSingleRow: numer wiersza
                if (i.tiles.Equals("0000"))
                {
                    return 1;
                }
                else
                {
                    if (p.isTyped)
                    {
                        if (i.tiles.Substring(1, 1).Equals("A"))
                            return 1;
                        else if (i.tiles.Substring(1, 1).Equals("B"))
                            return 2;
                        else if (i.tiles.Substring(1, 1).Equals("C"))
                            return 3;
                        else return 4;
                    }
                    else
                    {
                        int rowAct = int.Parse(i.tiles.Substring(2, 2));
                        rowAct = (rowAct - p.rowCount - 1) * -1;
                        return rowAct;
                    }
                }
            }
            else if (Program.Settings.Data.workMode == workModeFinishing2)
            {
                return -1;      //w trybie workModeFinishing2: -1
            }
            else
                return -2;
        }

        /// <summary>
        /// Pobiera z bazy poster_closeed_log ilość plakatów (rzędów) zamkniętych
        /// </summary>
        /// <param name="p">Plakat</param>
        public int getClosedCount(poster p)
        {
            //
            //string queryCalc = string.Format("SELECT COUNT(*) FROM tjdata.poster_closeed_log where jdnr={0} and jdline={1} and jdrow={2} and machine={3} and closedVirtual=false", p.jdnr, p.jdline, p.jdrow, Program.Settings.Data.stationId);
            
            //wersja bez "machine":
            //string queryCalc = string.Format("SELECT COUNT(*) FROM tjdata.poster_closeed_log where jdnr={0} and jdline={1} and jdrow={2} and closedVirtual=false", p.jdnr, p.jdline, p.jdrow);

            //dodanie stationId
            string queryCalc = string.Format("SELECT COUNT(*) FROM tjdata.poster_closeed_log where jdnr={0} and jdline={1} and jdrow={2} and closedVirtual=false and stationId={3}", p.jdnr, p.jdline, p.jdrow, Program.Settings.Data.stationId);

            DataTable dTableCalc = SQLConnection.GetData(queryCalc);

            return int.Parse(dTableCalc.Rows[0][0].ToString());
        }

        /// <summary>
        /// Pobiera z bazy poster_closeed_log ilość plakatów (rzędów) zamkniętych virtualnie
        /// </summary>
        /// <param name="p">Plakat</param>
        public int getClosedVirtualCount(poster p)
        {
            //
            //string queryCalc = string.Format("SELECT COUNT(*) FROM tjdata.poster_closeed_log where jdnr={0} and jdline={1} and jdrow={2} and machine={3} and closedVirtual=true", p.jdnr, p.jdline, p.jdrow, Program.Settings.Data.stationId);

            //wersja bez "machine":
            //string queryCalc = string.Format("SELECT COUNT(*) FROM tjdata.poster_closeed_log where jdnr={0} and jdline={1} and jdrow={2} and closedVirtual=true", p.jdnr, p.jdline, p.jdrow);

            //dodanie stationId
            string queryCalc = string.Format("SELECT COUNT(*) FROM tjdata.poster_closeed_log where jdnr={0} and jdline={1} and jdrow={2} and closedVirtual=true and stationId={3}", p.jdnr, p.jdline, p.jdrow, Program.Settings.Data.stationId);

            DataTable dTableCalc = SQLConnection.GetData(queryCalc);

            return int.Parse(dTableCalc.Rows[0][0].ToString());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        public void insertClosedPoster(poster p, bool is_final)
        {
            bool isVirtual = false;
            foreach (poster.tileType t in p.tilesList)
            {
                if (t.isVirtual)
                    isVirtual = true;
            }

            //MySql.Data.MySqlClient.MySqlException was unhandled
            //Message: An unhandled exception of type 'MySql.Data.MySqlClient.MySqlException' occurred in System.Windows.Forms.dll
            //Additional information: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ' , 0, 1, 9, NOW(), False)' at line 1

            //string query = string.Format("INSERT INTO tjdata.finishing_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6})", item.jdnr, item.jdline, item.tiles, item.machine, item.tilecounter, operatorId, stationId);
            
            //string query = string.Format("INSERT INTO tjdata.poster_closeed_log(jdnr, jdline, jdrow, machine, operatorId, operationTimeStamp, closedVirtual) values ({0}, {1}, {2}, {3}, {4}, NOW(), {5});", p.jdnr, p.jdline, p.jdrow, Program.Settings.Data.stationId, global.userAct.id, isVirtual);
            
            //wersja bez "machine":
            //string query = string.Format("INSERT INTO tjdata.poster_closeed_log(jdnr, jdline, jdrow, machine, operatorId, operationTimeStamp, closedVirtual) values ({0}, {1}, {2}, '{3}', {4}, NOW(), {5});", p.jdnr, p.jdline, p.jdrow, "", global.userAct.id, isVirtual);

            //dodanie stationId
            //string query = string.Format("INSERT INTO tjdata.poster_closeed_log(jdnr, jdline, jdrow, machine, operatorId, operationTimeStamp, closedVirtual, stationId) values ({0}, {1}, {2}, '{3}', {4}, NOW(), {5}, {6});", p.jdnr, p.jdline, p.jdrow, "", global.userAct.id, isVirtual, Program.Settings.Data.stationId);

            //dodanie stationId, dodanie is_final
            string query = string.Format("INSERT INTO tjdata.poster_closeed_log(jdnr, jdline, jdrow, machine, operatorId, operationTimeStamp, closedVirtual, stationId, is_final) values ({0}, {1}, {2}, '{3}', {4}, NOW(), {5}, {6}, {7});", p.jdnr, p.jdline, p.jdrow, "", global.userAct.id, isVirtual, Program.Settings.Data.stationId, is_final);

            SQLConnection.InsertData(query);
        }

        /// <summary>
        /// nowa wersja, List<scanItem> items => scanItem item
        /// Pobiera dane o wszystkich brytach wchodzących w skład plakatu
        /// </summary>
        /// <param name="item">dane pierwszego zeskanowanego kafelka</param>
        /// <returns></returns>
        public poster getOpenPosterData(scanItem item)
        {
            poster retVal = new poster();

            string queryCalc = string.Format("SELECT DISTINCT tiles FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1", item.jdnr, item.jdline);
            Program.autojetFinishingMain.Hm.Info("getOpenPosterData(), queryCalc={0}", queryCalc);
            DataTable dTableCalc = SQLConnection.GetData(queryCalc);

            Program.autojetFinishingMain.Hm.Info("getOpenPosterData(), dTableCalc.Rows.Count={0}", dTableCalc.Rows.Count.ToString());

            //sprawdzenie, czy przy zalaczonym trybie Program.Settings.Data.workModeSingleRow skanowany plakat kwalifikuje się do pracy w tym trybie
            bool singleRowSellect = false;
            if (Program.Settings.Data.workMode == workModeSingleRow)
            {
                if ((item.tiles.Substring(1, 1).Equals("A")) || (item.tiles.Substring(1, 1).Equals("B")) || (item.tiles.Substring(1, 1).Equals("C")))
                    singleRowSellect = true;
            }

            string query;
            if (dTableCalc.Rows.Count > 1)
                if (singleRowSellect)
                {
                    //query = string.Format("SELECT DISTINCT tiles FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 and tiles<>'0000' and tiles LIKE '_{2}__' order by tiles", item.jdnr, item.jdline, item.tiles.Substring(1, 1));

                    query = string.Format("SELECT tiles, COUNT(*) as qty FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 and tiles<>'0000' and tiles LIKE '_{2}__' GROUP BY tiles order by tiles", item.jdnr, item.jdline, item.tiles.Substring(1, 1));
                    //                     SELECT tiles, COUNT(*) as qty FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 and tiles<>'0000' and tiles LIKE '_{2}__' GROUP BY tiles order by tiles
                }
                else
                {
                    //query = string.Format("SELECT DISTINCT tiles FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 and tiles<>'0000' order by tiles", item.jdnr, item.jdline);
                    query = string.Format("SELECT tiles, COUNT(*) as qty FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 and tiles<>'0000' GROUP BY tiles order by tiles", item.jdnr, item.jdline);

                    //                     SELECT tiles, COUNT(*) as qty FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 and tiles<>'0000' GROUP BY tiles order by tiles
                }
            else
            {
                //query = string.Format("SELECT DISTINCT tiles FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 order by tiles", item.jdnr, item.jdline);

                query = string.Format("SELECT tiles, COUNT(*) as qty FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 GROUP BY tiles order by tiles", item.jdnr, item.jdline);
                //                     SELECT tiles, COUNT(*) as qty FROM Printtemp WHERE jdnr={0} and jdline={1} and isprinted=1 GROUP BY tiles order by tiles
            }

            Program.autojetFinishingMain.Hm.Info("getOpenPosterData(), query={0}", query);
            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count == 0)
            {
                //MessageBox.Show(string.Format("Błąd w zapytaniu:\n{0}", query));
                retVal.jobformat = null;
                return retVal;
            }
            //
            retVal.jdline = item.jdline;
            retVal.jdnr = item.jdnr;
            retVal.machine = item.machine;

            //obliczenie wydrukowanej ilośći (ilość najmniejsza ze wszystkich wydrukowanych kafelków)
            int qtyPrinted = int.Parse(dTable.Rows[0]["qty"].ToString());
            foreach (DataRow dRow in dTable.Rows)
            {
                if (int.Parse(dRow["qty"].ToString()) < qtyPrinted)
                    qtyPrinted = int.Parse(dRow["qty"].ToString());
                Program.autojetFinishingMain.Hm.Info("getOpenPosterData(),  tiles: {0}, qty: {1}", dRow["tiles"].ToString(), dRow["qty"].ToString());
            }

            retVal.qtyPrinted = qtyPrinted;
            Program.autojetFinishingMain.Hm.Info("getOpenPosterData(), qtyPrinted={0}", retVal.qtyPrinted);

            /*
             0101
             0102
             0103
             0201
             0202
             0203
             0301
             0302
             0303
             */
            if (Program.Settings.Data.workMode != workModeFinishing2)
            {
                //sprawdzenie, czy rozmiar plakatu jest na liscie, jesli tak i skanujemy bryty A to nalezy dodac jeden plakat B
                if (singleRowSellect)
                {
                    //retVal.jobformat = 
                    retVal.jobformat = getPosterSize(retVal);

                    if (!string.IsNullOrEmpty(retVal.jobformat))
                    {
                        Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): singleRowSellect");

                        string singleRowSelQuery = string.Format("SELECT * FROM tjdata.typed_sizes_table where format='{0}'", retVal.jobformat);
                        DataTable singleRowSelTable = SQLConnection.GetData(singleRowSelQuery);

                        Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): singleRowSelTable.Rows.Count={0}, ", singleRowSelTable.Rows.Count);

                        if ((singleRowSelTable.Rows.Count > 0) && (item.tiles.Substring(1, 1).Equals("A")))
                            retVal.addSubcolection = true;
                        else
                            retVal.addSubcolection = false;

                        Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): addSubcolection={0}, ", retVal.addSubcolection.ToString());
                    }
                    else
                        return retVal;
                }

                if (dTable.Rows.Count > 0)
                {
                    retVal.tilesCount = dTable.Rows.Count;
                    foreach (DataRow dRow in dTable.Rows)
                    {
                        poster.tileType tile = new poster.tileType();
                        tile.tiles = dRow["tiles"].ToString();
                        tile.tileScanDone = false;
                        retVal.tilesList.Add(tile);
                        Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): tilesList.Add(tile):tiles={0}", tile.tiles);
                    }

                    if (retVal.addSubcolection)
                    {
                        //dodaje kafelek B konieczny do zakonczenia kompletacji
                        //poster.tileType tile = new poster.tileType(retVal.tilesList[retVal.tilesCount - 1]);
                        poster.tileType tile = new poster.tileType();
                        tile.tiles = retVal.tilesList[retVal.tilesCount - 1].tiles;
                        //tile.tiles = tile.tiles.Substring(0, 1) + "B" + tile.tiles.Substring(2, 2);
                        tile.tiles = "0B__";
                        tile.tileScanDone = false;
                        retVal.tilesList.Add(tile);
                        retVal.tilesCount++;
                    }
                }
                else
                {
                    retVal.tilesCount = 0;
                }

                //oznaczenie aktualnego kafelak jako zeskanowanego
                foreach (poster.tileType l in retVal.tilesList)
                {
                    Program.autojetFinishingMain.Hm.Info("getOpenPosterData(): tilesList.tiles={0}", l.tiles);
                    if (l.tiles == item.tiles)
                    {
                        l.tileScanDone = true;
                    }
                }
            }
            else
            {
                //
                retVal.tilesCount = 0;
                List<string> findRowNum = new List<string>();
                Program.autojetFinishingMain.Hm.Warning("getOpenPosterData(): workModeFinishing2");

                foreach (DataRow dRow in dTable.Rows)
                {
                    string actRowNum = dRow["tiles"].ToString().Substring(1, 1);
                    bool newRow = true;
                    foreach (string rowNum in findRowNum)
                    {
                        Program.autojetFinishingMain.Hm.Warning(string.Format("getOpenPosterData(): rowNum:{0}, actRowNum:{1}", rowNum, actRowNum));
                        if (rowNum.Equals(actRowNum))
                            newRow = false;
                    }

                    if (newRow)
                    {
                        Program.autojetFinishingMain.Hm.Warning(string.Format("getOpenPosterData(): newRow"));
                        findRowNum.Add(actRowNum);

                        poster.tileType tile = new poster.tileType();
                        tile.tiles = string.Format("0{0}__", actRowNum);
                        tile.tileScanDone = false;
                        retVal.tilesList.Add(tile);
                        retVal.tilesCount++;
                        Program.autojetFinishingMain.Hm.Warning(string.Format("getOpenPosterData(): newRow: tiles: _{0}__", actRowNum));
                    }
                }

                foreach (poster.tileType l in retVal.tilesList)
                {
                    if (l.tiles.Substring(1, 1).Equals(item.tiles.Substring(1, 1)))
                    {
                        l.tileScanDone = true;
                        Program.autojetFinishingMain.Hm.Warning(string.Format("getOpenPosterData(): item.tiles={0} =>.tileScanDone", item.tiles));
                    }
                }
            }
            
            return retVal;
        }


        /// <summary>
        /// okreslenie parametrów, analiza aktualnie skanowanego plakatu (np. rozmiaru) na podstawie pierwszego zeskanowanego brytu
        /// Aktualizacja wyświetlania plakatu
        /// </summary>
        public bool posterAnalize(poster p)
        {
            Program.autojetFinishingMain.Hm.Info($"posterAnalize(): Start");

            StopDrawing();
            //this.SuspendLayout();

            if ((Program.Settings.Data.workMode == workModeAllPoster) || (Program.Settings.Data.workMode == workModeSingleRow))
            {
                bool isTyped = false;
                int outInt = 0;

                //sprawdzenie czy w tiles są same liczby
                foreach (poster.tileType t in p.tilesList)
                {
                    if (!int.TryParse(t.tiles, out outInt))
                    {
                        isTyped = true;
                    }
                }

                p.isTyped = isTyped;
                Program.autojetFinishingMain.Hm.Info($"posterAnalize(): p.isTyped = {isTyped.ToString()}");
                /*
                 R C  => C  R
                 0101 => 01 02
                 0102 => 01 01
                 0201 => 02 02
                 0202 => 02 01             
                 0301 => 03 02             
                 0302 => 03 01             
                 */


                //dodać obsługę kodu 0000
                if (p.tilesList[0].tiles.Equals("0000"))
                {
                    Program.autojetFinishingMain.Hm.Info($"posterAnalize(): tiles = '0000'");
                    //zakładam, że jeśli jest to bryt o oznaczeniu "0000" to może mieć tylko jeden kafelek
                    foreach (poster.tileType t in p.tilesList)
                    {
                        t.colNum = 1;
                        t.rowNum = 1;  
                    }

                    p.colCount = 1;
                    p.rowCount = 1;
                }
                else if (!isTyped)
                {
                    Program.autojetFinishingMain.Hm.Info($"posterAnalize(): isTyped = false");
                    int colMax = 0;
                    int colAct = 0;
                    int rowMax = 0;
                    int rowAct = 0;
                    foreach (poster.tileType t in p.tilesList)
                    {
                        colAct = int.Parse(t.tiles.Substring(0, 2));
                        rowAct = int.Parse(t.tiles.Substring(2, 2));
                        t.colNum = colAct;
                        t.rowNum = rowAct;  //wstępne wyliczenie, jeszcze trzeba odwrócić...
                        if (colAct > colMax)
                            colMax = colAct;
                        if (rowAct > rowMax)
                            rowMax = rowAct;
                    }

                    foreach (poster.tileType t in p.tilesList)
                    {
                        t.rowNum = (t.rowNum - rowMax - 1) * -1;
                    }
                    p.colCount = colMax;
                    p.rowCount = rowMax;
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info($"posterAnalize(): isTyped = true");
                    //plakat typowy (posiadający literowe oznaczenie kolumn/rzędów)
                    int colMax = 0;
                    int colAct = 0;
                    int rowMax = 0;
                    int rowAct = 0;
                    if (!p.addSubcolection)
                    {
                        Program.autojetFinishingMain.Hm.Info($"posterAnalize(): addSubcolection = false");
                        foreach (poster.tileType t in p.tilesList)
                        {
                            if (Program.Settings.Data.workMode == workModeAllPoster)
                            {
                                if (t.tiles.Substring(1, 1).Equals("A"))
                                {
                                    rowAct = 1;
                                }
                                else if (t.tiles.Substring(1, 1).Equals("B"))
                                {
                                    rowAct = 2;
                                }
                                else if (t.tiles.Substring(1, 1).Equals("C"))
                                {
                                    rowAct = 3;
                                }
                                else
                                {
                                    //this.ResumeLayout();
                                    StartDrawing();
                                    return false;
                                }
                            }
                            else
                            {
                                rowAct = 1;
                            }

                            colAct = int.Parse(t.tiles.Substring(2, 2));

                            if (colAct > colMax)
                                colMax = colAct;
                            if (rowAct > rowMax)
                                rowMax = rowAct;

                            t.colNum = colAct;
                            t.rowNum = rowAct;
                        }
                        p.colCount = colMax;
                        p.rowCount = rowMax;
                    }
                    else
                    {
                        Program.autojetFinishingMain.Hm.Info($"posterAnalize(): addSubcolection = true");
                        //plakat 

                        foreach (poster.tileType t in p.tilesList)
                        {
                            if (t.tiles.Substring(1, 1).Equals("A"))
                                t.colNum = int.Parse(t.tiles.Substring(2, 2));
                            else
                                t.colNum = p.tilesCount;
                            t.rowNum = 1;
                        }
                        p.colCount = p.tilesCount;
                        p.rowCount = 1;
                    }
                }

                //wyswietlenie danych 
                if (Program.Settings.Data.workMode == workModeAllPoster)
                    Program.autojetFinishingMain.Hm.Info("posterAnalize(): workMode = workModeAllPoster");
                if (Program.Settings.Data.workMode == workModeSingleRow)
                    Program.autojetFinishingMain.Hm.Info("posterAnalize(): workMode = workModeSingleRow");
                Program.autojetFinishingMain.Hm.Info("posterAnalize(): colCount = {0}", p.colCount);
                Program.autojetFinishingMain.Hm.Info("posterAnalize(): rowCount = {0}", p.rowCount);
                Program.autojetFinishingMain.Hm.Info("posterAnalize(): tilesCount = {0}", p.tilesCount);


                foreach (poster.tileType t in p.tilesList)
                {
                    Program.autojetFinishingMain.Hm.Info("posterAnalize(): tilesList.tiles={0}, colNum={1}, rowNum={2}", t.tiles, t.colNum, t.rowNum);
                }

                //ukrycie zbędnych przycisków             
                //kolumny
                for (int i = 0; i < global.maxColCount; i++)
                {
                    if (i < p.colCount)
                    {
                        ColumnStyle cs = new ColumnStyle(SizeType.Percent, 10F);
                        tlpMain.ColumnStyles[i] = cs;
                    }
                    else
                    {
                        ColumnStyle cs = new ColumnStyle(SizeType.Absolute, 0F);
                        tlpMain.ColumnStyles[i] = cs;
                    }
                }
                Program.autojetFinishingMain.Hm.Info($"posterAnalize(): Test1 ");

                //rzedy
                for (int j = 0; j < global.maxRowCount; j++)
                {
                    Program.autojetFinishingMain.Hm.Info($"posterAnalize(): j = {j}");
                    if (j < p.rowCount)
                    {
                        RowStyle rs = new RowStyle(SizeType.Percent, 10F);
                        tlpMain.RowStyles[j] = rs;
                    }
                    else
                    {
                        RowStyle rs = new RowStyle(SizeType.Absolute, 0F);
                        tlpMain.RowStyles[j] = rs;
                    }
                }
                Program.autojetFinishingMain.Hm.Info($"posterAnalize(): Test2 ");

                //zerowanie kolorów przyciskow
                for (int i = 0; i < global.maxColCount; i++)
                {
                    for (int j = 0; j < global.maxRowCount; j++)
                    {
                        buttonList[i, j].Enabled = false;
                        buttonList[i, j].BackColor = Color.Gray;
                        buttonList[i, j].Text = "";
                    }
                }
                Program.autojetFinishingMain.Hm.Info($"posterAnalize(): Test3 ");
                
                //nadanie właściwych kolorów
                foreach (poster.tileType t in p.tilesList)
                {
                    buttonList[t.colNum - 1, t.rowNum - 1].Text = t.tiles;
                    buttonList[t.colNum - 1, t.rowNum - 1].BackColor = Color.Yellow;
                }
                Program.autojetFinishingMain.Hm.Info($"posterAnalize(): Test4 ");

                //
                buttonSizeChanged(null, null);
                //this.ResumeLayout();
                StartDrawing();
                return true;
            }
            else
            {
                p.colCount = p.tilesCount;
                p.rowCount = 1;

                int colAct = 1;
                foreach (poster.tileType t in p.tilesList)
                {
                    t.rowNum = 1;
                    t.colNum = colAct;
                    colAct++;
                }

                Program.autojetFinishingMain.Hm.Warning(string.Format("posterAnalize(): workModeFinishing2: colCount={0}, rowCount={1}", p.colCount, p.rowCount));

                //ukrycie zbędnych przycisków             
                //kolumny
                for (int i = 0; i < global.maxColCount; i++)
                {
                    if (i < p.colCount)
                    {
                        ColumnStyle cs = new ColumnStyle(SizeType.Percent, 10F);
                        tlpMain.ColumnStyles[i] = cs;
                    }
                    else
                    {
                        ColumnStyle cs = new ColumnStyle(SizeType.Absolute, 0F);
                        tlpMain.ColumnStyles[i] = cs;
                    }
                }

                Program.autojetFinishingMain.Hm.Warning(string.Format("posterAnalize(): workModeFinishing2: Test1"));
                //rzedy
                for (int j = 0; j < global.maxRowCount; j++)
                {
                    if (j < p.rowCount)
                    {
                        RowStyle rs = new RowStyle(SizeType.Percent, 10F);
                        tlpMain.RowStyles[j] = rs;
                    }
                    else
                    {
                        RowStyle rs = new RowStyle(SizeType.Absolute, 0F);
                        tlpMain.RowStyles[j] = rs;
                    }
                }

                Program.autojetFinishingMain.Hm.Warning(string.Format("posterAnalize(): workModeFinishing2: Test2"));
                //zerowanie kolorów przyciskow
                for (int i = 0; i < global.maxColCount; i++)
                {
                    for (int j = 0; j < global.maxRowCount; j++)
                    {
                        buttonList[i, j].Enabled = false;
                        buttonList[i, j].BackColor = Color.Gray;
                        buttonList[i, j].Text = "";

                    }
                }

                Program.autojetFinishingMain.Hm.Warning(string.Format("posterAnalize(): workModeFinishing2: Test3"));
                //nadanie właściwych kolorów
                foreach (poster.tileType t in p.tilesList)
                {
                    buttonList[t.colNum - 1, t.rowNum - 1].Text = t.tiles;
                    buttonList[t.colNum - 1, t.rowNum - 1].BackColor = Color.Yellow;
                }

                Program.autojetFinishingMain.Hm.Warning(string.Format("posterAnalize(): workModeFinishing2: Test4"));
                //
                buttonSizeChanged(null, null);

                Program.autojetFinishingMain.Hm.Warning(string.Format("posterAnalize(): workModeFinishing2: Test5"));
                //this.ResumeLayout();
                StartDrawing();
                return true;
            }
        }



        /// <summary>
        /// Dodaje do bazy finishing_log dane o zeskanowanym kafelku
        /// </summary>
        /// <param name="item">dane zeskanowanego kafelka</param>
        /// <param name="operatorId">id operatora</param>
        /// <param name="stationId">id stacji skanowania</param>
        /// <returns></returns>
        public bool insertScanedItem1(scanItem item, int operatorId, int stationId)
        {
            //INSERT INTO `tjdata`.`finishing_log` (`id`, `jdnr`, `jdline`, `tiles`, `machine`, `operatorId`, `stationId`) VALUES ('1', '1', '1', '1', '1', '1', '1');

            //string query = string.Format("INSERT INTO tjdata.finishing_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId, reference_id, ref_from, ref_to, subpack_from, subpack_to) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6}, '{7}', {8}, {9}, {10}, {11})", item.jdnr, item.jdline, item.tiles, item.machine, item.tilecounter, operatorId, stationId, global.actAddressAndItem.item.reference_id, global.actAddressAndItem.item.ref_from, global.actAddressAndItem.item.ref_to, global.actAddressAndItem.item.subpack_from, global.actAddressAndItem.item.subpack_to);
            //wersja bez "machine":
            string query = string.Format("INSERT INTO tjdata.finishing_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId, reference_id, ref_from, ref_to, subpack_from, subpack_to) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6}, '{7}', {8}, {9}, {10}, {11})", item.jdnr, item.jdline, item.tiles, "", item.tilecounter, operatorId, stationId, global.actAddressAndItem.item.reference_id, global.actAddressAndItem.item.ref_from, global.actAddressAndItem.item.ref_to, global.actAddressAndItem.item.subpack_from, global.actAddressAndItem.item.subpack_to);

            SQLConnection.InsertData(query);

            return true;
        }

        /// <summary>
        /// Dodaje do bazy finishing2_log dane o zeskanowanym kafelku
        /// </summary>
        /// <param name="item">dane zeskanowanego kafelka</param>
        /// <param name="operatorId">id operatora</param>
        /// <param name="stationId">id stacji skanowania</param>
        /// <returns></returns>
        public bool insertScanedItem2(scanItem item, int operatorId, int stationId)
        {
            /*
            global.actAddressAndItem
            ADD  column `reference_id` varchar(50) COLLATE utf8_polish_ci NOT NULL,
            ADD  column  `ref_from` int(4) NOT NULL,
            ADD  column  `ref_to` int(4) NOT NULL,
            ADD  column  `subpack_from` int(4) NOT NULL,
            ADD  column  `subpack_to` int(4) NOT NULL;
            */
            //string query = string.Format("INSERT INTO tjdata.finishing2_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId, finishingId, reference_id, ref_from, ref_to, subpack_from, subpack_to) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6}, {7}, '{8}', {9}, {10}, {11}, {12})", item.jdnr, item.jdline, item.tiles, item.machine, item.tilecounter, operatorId, stationId, item.finishingId, global.actAddressAndItem.item.reference_id, global.actAddressAndItem.item.ref_from, global.actAddressAndItem.item.ref_to, global.actAddressAndItem.item.subpack_from, global.actAddressAndItem.item.subpack_to);//, reference_id, ref_from, ref_to, subpack_from, subpack_to
            //wersja bez "machine":
            string query = string.Format("INSERT INTO tjdata.finishing2_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId, finishingId, reference_id, ref_from, ref_to, subpack_from, subpack_to) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6}, {7}, '{8}', {9}, {10}, {11}, {12})", item.jdnr, item.jdline, item.tiles, "", item.tilecounter, operatorId, stationId, item.finishingId, global.actAddressAndItem.item.reference_id, global.actAddressAndItem.item.ref_from, global.actAddressAndItem.item.ref_to, global.actAddressAndItem.item.subpack_from, global.actAddressAndItem.item.subpack_to);//, reference_id, ref_from, ref_to, subpack_from, subpack_to

            SQLConnection.InsertData(query);

            return true;
        }


        /// <summary>
        /// Zwraca wartość > 0 jeśli kafelek jest już w bazie finishing_log         
        /// </summary>
        /// <param name="item"></param>
        /// <returns>0  - if this tile was not scaned before</returns>
        /// <returns>id of row, if this tile was scaned before</returns>
        /// <returns>-1 - if tile is not i datatable</returns>
        public int checkTileRescan1(scanItem item)
        {
            //string query = string.Format("SELECT * FROM tjdata.finishing_log where jdnr='{0}' and jdline='{1}' and tiles='{2}' and machine='{3}' and tilecounter={4} and operationRollback=false", item.jdnr, item.jdline, item.tiles, item.machine, item.tilecounter.ToString());
            //wersja bez "machine":
            string query = string.Format("SELECT * FROM tjdata.finishing_log where jdnr='{0}' and jdline='{1}' and tiles='{2}' and tilecounter={3} and operationRollback=false", item.jdnr, item.jdline, item.tiles, item.tilecounter.ToString());
            DataTable dTable = SQLConnection.GetData(query);

            try
            {
                if (dTable.Rows.Count > 0)
                {
                    Program.autojetFinishingMain.Hm.Info($"checkTileRescan1(): return = {int.Parse(dTable.Rows[0]["id"].ToString())}");
                    return int.Parse(dTable.Rows[0]["id"].ToString());
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info($"checkTileRescan1(): return = 0");
                    return 0;
                }
            }
            catch (Exception)
            {
                Program.autojetFinishingMain.Hm.Info($"checkTileRescan1(): return = -1");
                return -1;
            }
        }

        /// <summary>
        /// Zwraca wartość > 0 jeśli kafelek jest już w bazie finishing2_log         
        /// </summary>
        /// <param name="item"></param>
        /// <returns>0  - if this tile was not scaned before</returns>
        /// <returns>id of row, if this tile was scaned before</returns>
        /// <returns>-1 - if tile is not i datatable</returns>
        public int checkTileRescan2(scanItem item)
        {
            //string query = string.Format("SELECT * FROM tjdata.finishing2_log where jdnr='{0}' and jdline='{1}' and tiles='{2}' and machine='{3}' and tilecounter={4} and operationRollback=false", item.jdnr, item.jdline, item.tiles, item.machine, item.tilecounter.ToString());
            //wersja bez "machine":
            string query = string.Format("SELECT * FROM tjdata.finishing2_log where jdnr='{0}' and jdline='{1}' and tiles='{2}' and tilecounter={3} and operationRollback=false", item.jdnr, item.jdline, item.tiles, item.tilecounter.ToString());
            DataTable dTable = SQLConnection.GetData(query);

            try
            {
                if (dTable.Rows.Count > 0)
                {
                    Program.autojetFinishingMain.Hm.Info($"checkTileRescan1(): return = {int.Parse(dTable.Rows[0]["id"].ToString())}");
                    return int.Parse(dTable.Rows[0]["id"].ToString());
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info($"checkTileRescan1(): return = 0");
                    return 0;
                }
            }
            catch (Exception)
            {
                Program.autojetFinishingMain.Hm.Info($"checkTileRescan1(): return = -1");
                return -1;
            }
        }



        /// <summary>
        /// sprawdza czy zeskanowany kafelek jest częścią plakatu lub czy jest zdublowany
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns>0=kafelek nie jest częścią plakatu, 1=kafelek jest częścią plakatu, 2=zdublowany kafelek</returns>
        public int checkTile(scanItem item)
        {
            /*
            const int checkResNoPartOfPoster = 0;
            const int checkResPartOfPoster = 1;
            const int checkResDoubleTile = 2;
            */

            //zakładam, że kafelek może nie być na liście kafelków do zeskanowania...
            int retVal = checkResNoPartOfPoster;

            //błędny numer pracy:
            //2017-08-30: dodano jdline do testu
            if ((item.jdnr != posterAct.jdnr) || (item.jdline != posterAct.jdline))
                return checkResNoPartOfPoster;

            if ((Program.Settings.Data.workMode == workModeAllPoster) || (Program.Settings.Data.workMode == workModeSingleRow))
            {
                if ((posterAct.addSubcolection) && (item.tiles.Substring(1, 1).Equals("B")))
                {
                    foreach (poster.tileType l in posterAct.tilesList)
                    {
                        if (l.tiles.Substring(1, 1).Equals(item.tiles.Substring(1, 1)))
                        {
                            if (l.tileScanDone)
                            {
                                retVal = checkResDoubleTile;
                                Program.autojetFinishingMain.Hm.Info("checkTileDouble(): exit1");
                            }
                            else
                            {
                                l.tileScanDone = true;
                                retVal = checkResPartOfPoster;
                            }
                        }
                    }
                }
                else
                {
                    foreach (poster.tileType l in posterAct.tilesList)
                    {
                        if (l.tiles == item.tiles)
                        {
                            if (l.tileScanDone)
                            {
                                retVal = checkResDoubleTile;
                                Program.autojetFinishingMain.Hm.Info("checkTileDouble(): exit2");
                            }
                            else
                            {
                                l.tileScanDone = true;
                                retVal = checkResPartOfPoster;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (poster.tileType l in posterAct.tilesList)
                {
                    if (l.tiles.Substring(1, 1).Equals(item.tiles.Substring(1, 1)))
                    {
                        if (l.tileScanDone)
                        {
                            retVal = checkResDoubleTile;
                            Program.autojetFinishingMain.Hm.Info("checkTileDouble(): exit3");
                        }
                        else
                        {
                            l.tileScanDone = true;
                            retVal = checkResPartOfPoster;
                        }
                    }
                }
            }

            return retVal;
        }

        /// <summary>
        /// Zamyka aktualny plakat, zapisuje dane do bazy danych
        /// </summary>
        public void closeActPoster()
        {
            //zabezpieczenie
            if (posterAct.initDone == false)
                return;

            if ((Program.Settings.Data.workMode == workModeAllPoster) || ((Program.Settings.Data.workMode == workModeSingleRow) && (!posterAct.addSubcolection)))
            {
                //get actual poster id
                string querySelect = string.Format("SELECT IFNULL(max(finishingId), 0) FROM tjdata.finishing_log where stationId={0}", Program.Settings.Data.stationId);
                DataTable dtActData = SQLConnection.GetData(querySelect);
                int actPosterNum = int.Parse(dtActData.Rows[0][0].ToString());

                //update poster tiles as done
                string query = string.Format("update tjdata.finishing_log set posterDone=true, finishingId={1} where posterDone=false and operationRollback=false and stationId={0}", Program.Settings.Data.stationId, actPosterNum + 1);

                SQLConnection.UpdateData(query);

                Program.autojetFinishingMain.Hm.Info("Poster closed with finishingId={0}", actPosterNum + 1);
            }
            else if ((Program.Settings.Data.workMode == workModeSingleRow) && (posterAct.addSubcolection))
            {
                //zamykanie w bazie finishing_log
                //get actual poster id
                string querySelect = string.Format("SELECT IFNULL(max(finishingId), 0) FROM tjdata.finishing_log where stationId={0}", Program.Settings.Data.stationId);
                DataTable dtActData = SQLConnection.GetData(querySelect);
                int actPosterNum = int.Parse(dtActData.Rows[0][0].ToString());

                //update poster tiles as done
                string query = string.Format("update tjdata.finishing_log set posterDone=true, finishingId={1} where posterDone=false and operationRollback=false and stationId={0}", Program.Settings.Data.stationId, actPosterNum + 1);

                SQLConnection.UpdateData(query);

                Program.autojetFinishingMain.Hm.Info("Poster closed with finishingId={0}", actPosterNum + 1);

                //zamykanie w bazie finishing_log2
                //get actual poster id
                string querySelect2 = string.Format("SELECT IFNULL(max(finishingId), 0) FROM tjdata.finishing2_log where stationId={0}", Program.Settings.Data.stationId);
                DataTable dtActData2 = SQLConnection.GetData(querySelect2);
                int actPosterNum2 = int.Parse(dtActData2.Rows[0][0].ToString());

                //update poster tiles as done
                string query2 = string.Format("update tjdata.finishing2_log set posterDone=true, finishingId={1} where posterDone=false and operationRollback=false and stationId={0}", Program.Settings.Data.stationId, actPosterNum2 + 1);

                SQLConnection.UpdateData(query2);

                Program.autojetFinishingMain.Hm.Info("Poster closed with finishing2Id={0}", actPosterNum2 + 1);

            }
            else if (Program.Settings.Data.workMode == workModeFinishing2)
            {
                //zamykanie w bazie finishing_log2
                //get actual poster id
                string querySelect2 = string.Format("SELECT IFNULL(max(finishingId), 0) FROM tjdata.finishing2_log where stationId={0}", Program.Settings.Data.stationId);
                DataTable dtActData2 = SQLConnection.GetData(querySelect2);
                int actPosterNum2 = int.Parse(dtActData2.Rows[0][0].ToString());

                //update poster tiles as done
                string query2 = string.Format("update tjdata.finishing2_log set posterDone=true, finishingId={1} where posterDone=false and stationId={0}", Program.Settings.Data.stationId, actPosterNum2 + 1);

                SQLConnection.UpdateData(query2);

                Program.autojetFinishingMain.Hm.Info("Poster closed with finishing2Id={0}", actPosterNum2 + 1);

            }
            else
            {
                MessageBox.Show("BŁĄD KRYTYCZNY, SKONTAKTUJ SIĘ Z PRODUCENTEM PROGRAMU\n APLIKACJA ZOSTANIE ZAMKNIĘTA!!!");
                this.Close();
            }

            //zapisanie danych w bazie poster_closeed_log
            if ((Program.Settings.Data.workMode == workModeFinishing2) || (Program.Settings.Data.workMode == workModeAllPoster) || ((Program.Settings.Data.workMode == workModeSingleRow) && (posterAct.addSubcolection)))
                insertClosedPoster(posterAct, true);
            else
                insertClosedPoster(posterAct, false);


            //sprawdzenie jak zamykamy plakat: normalnie czy wirtualnie
            bool isVirtual = false;
            foreach (poster.tileType t in posterAct.tilesList)
            {
                if (t.isVirtual)
                    isVirtual = true;
            }

            if (!isVirtual)
                posterAct.qtyClosed++;
            else
                posterAct.qtyClosedVirtual++;

            //aktualizacja danych na ekranie, na prośbę p. Łukasza,
            lQtyClosed.Text = posterAct.qtyClosed.ToString() + "+" + posterAct.qtyClosedVirtual.ToString() + "=" + (posterAct.qtyClosed + posterAct.qtyClosedVirtual).ToString();
        }

        /// <summary>
        /// Cast barcode scan data to scanItem 
        /// </summary>
        /// <param name="scanStr"></param>
        /// <returns></returns>
        public scanItem scanDataToScanItem(string scanStr)
        {
            scanItem item = new scanItem();

            string jdnr = "";
            string jdline = "";
            string tiles = "";
            string machine = "";
            int tilecounter = 0;

            //code 128
            //podział stringa wejściowego na częśći i utworzenie scanItem
            if (scanStr.Length == 17)
            {
                try
                {
                    jdnr = scanStr.Substring(0, 6);
                    jdline = scanStr.Substring(6, 3);
                    tiles = scanStr.Substring(9, 4);
                    machine = Program.Settings.Data.stationId.ToString();
                    tilecounter = int.Parse(scanStr.Substring(13, 4));                      //poprawa 2017-07-06, bylo tilecounter = int.Parse(scanStr.Substring(13, 3));

                    item = new scanItem(jdnr, jdline, tiles, tilecounter, machine);

                }
                catch (Exception ex)
                {
                    Program.autojetFinishingMain.Hm.Warning("Błąd skanowania kodu kreskowego. Kod: '{0}'. Błąd: {1}", scanStr, ex.Message);
                    return null;
                }
            }
            else
            {
                Program.autojetFinishingMain.Hm.Warning("Błąd skanowania kodu kreskowego. Kod: '{0}' nie jest poprawnym kodem kreskowym", scanStr);
                return null;
            }

            return item;
        }

        public void closePosterErrorScan()
        {
            {
                //pomijaj dane przychodzące ze skanera
                global.skipScanerData = true;

                //tworzenie formularza
                errorSourceScanForm form = new errorSourceScanForm();
                form.errorSourceCount = countUnscanedTiles(posterAct);

                //zmiana uchwytu zdarzenia od skanerów ręcznych
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += form.scannerSerialRecieved;
                }

                //sygnalizacja błędu do PLC
                plcWriteError(true);
                plcWriteHoldScan(true);

                //dane przychodzące z kamer cognex będą pomijane
                global.skipScanerData = true;

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                //pokaż formularz błędu
                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): Błędy kompletacji...");

                    bool closeVirtual = true;   //zakladam, ze bedzie mozna zamknac plakat wirtualnie

                    Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): global.scanErrorList.Count={0}", global.scanErrorList.Count.ToString());
                    foreach (int i in form.errorList)
                    {
                        scanError scanErrorAct = global.findErrorById(i, global.scanErrorList);
                        if (scanErrorAct != null)
                        {
                            Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): scanErrorAct.closeVirtual={0}", scanErrorAct.closeVirtual.ToString());
                            if (!scanErrorAct.closeVirtual)
                                closeVirtual = false;
                            insertErrorSource(i);
                            Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): Błędy kompletacji, insertErrorSource({0})", i);
                        }
                    }

                    Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): closeVirtual={0}", closeVirtual);

                    if (Program.Settings.Data.closePosterVirtual && closeVirtual)
                    {
                        foreach (poster.tileType t in posterAct.tilesList)
                        {
                            if (t.tileScanDone == false)
                            {
                                t.isVirtual = true;
                                t.tileScanDone = true;
                            }
                        }
                        setPosterTilesAsVirtual(posterAct, global.userAct.id, Program.Settings.Data.stationId);

                        closeActPoster();
                        posterDoneMsg();

                        //
                        if (global.actAddressAndItem.item_reference_id != null)
                        {
                            dispachUpdateDone();
                        }
                        posterAct = new poster();
                        global.actAddressAndItem = new FinishingDispatchAddressAndItem();
                        updateVisu();

                        Program.autojetFinishingMain.Hm.Info("multiScanAnalize(): closeActPoster() done");
                    }
                }

                //koniec
                global.skipScanerData = false;

                //koniec: sygnalizacja błędu do PLC
                plcWriteError(false);
                plcWriteHoldScan(false);

                //zmiana uchwytu zdarzenia od skanerów ręcznych
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= form.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }

                global.skipScanerData = false;
            }
        }
    }
}
