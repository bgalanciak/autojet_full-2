﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

using System.Windows.Forms;
using Aviad.Utils.Communication.Core;

namespace AutojetFinishing
{
    public partial class autojetFinishingMainForm
    {

        bool cognexEthConnected = false;
        bool cognexEthConnected2 = false;

        /* używane z Aviad.Utils.Communication.Core
        //tcpServer
        void m_Terminal_ClientDisConnected(System.Net.Sockets.Socket socket)
        {
            PublishMessage(listLog, string.Format("Client {0} has been disconnected!", socket.LocalEndPoint));
            cognexEthConnected = false;
        }

        void m_Terminal_ClientConnected(System.Net.Sockets.Socket socket)
        {
            PublishMessage(listLog, string.Format("Client {0} has been connected!", socket.LocalEndPoint));
            cognexEthConnected = true;
        }

        void m_Terminal_MessageRecived(string message)
        {
            PublishMessage(listMessages, message);

            // Send Echo
            //m_serverTerminal.SendMessage("Echo: " + message);
        }

        private void createTerminal(int alPort)
        {
            m_serverTerminal = new ServerTerminal();

            m_serverTerminal.MessageRecived += m_Terminal_MessageRecived;
            m_serverTerminal.ClientConnect += m_Terminal_ClientConnected;
            m_serverTerminal.ClientDisconnect += m_Terminal_ClientDisConnected;

            m_serverTerminal.StartListen(alPort);
            PublishMessage(listLog, string.Format("TCP Server started at port:{0}", alPort));

        }

        private void closeTerminal()
        {
            m_serverTerminal.MessageRecived -= new TCPTerminal_MessageRecivedDel(m_Terminal_MessageRecived);
            m_serverTerminal.ClientConnect -= new TCPTerminal_ConnectDel(m_Terminal_ClientConnected);
            m_serverTerminal.ClientDisconnect -= new TCPTerminal_DisconnectDel(m_Terminal_ClientDisConnected);

            m_serverTerminal.Close();

            PublishMessage(listLog, string.Format("TCP Server stoped!"));
        }

        private void PublishMessage(ListBox listBox, string mes)
        {
            if (InvokeRequired)
            {
                BeginInvoke((ThreadStart)delegate { PublishMessage(listBox, mes); });
                return;
            }

            listBox.Items.Add(mes);
        }
        */

        static int rack = 0;
        static int slot = 1;

        /// <summary>
        /// connection for PLC
        /// </summary>
        public void connectPLC()
        {
            //fds.rfd = libnodave.openSocket(102, "192.168.1.150");
            fds.rfd = libnodave.openSocket(102, Program.Settings.Data.plcIp);
            fds.wfd = fds.rfd;
            if (fds.rfd > 0)
            {
                di = new libnodave.daveInterface(fds, "IF1", 0, libnodave.daveProtoISOTCP, libnodave.daveSpeed187k);
                di.setTimeout(1000000);

                dc = new libnodave.daveConnection(di, 0, rack, slot);
                if (0 == dc.connectPLC())
                {
                    updateVisuPlcConnected();
                }
                else
                {
                    updateVisuPlcNotConnected();
                }
            }
        }

        public void updateVisuPlcConnected()
        {
            //lPLCStatus.Text = "Connected";
            statusStripePLC.Text = "PLC: połączono";
            statusStripePLC.ForeColor = Color.Green;
            statusStripePLC.Image = Properties.Resources.yes;
            plcConnected = true;
            plcWriteHoldScan(false);
            //kasowanie wyjścia "Błąd", po nawiązaniu połączenia nie wystawiam błędu!!!
            plcWriteError(false);
        }
        public void updateVisuPlcNotConnected()
        {
            //lPLCStatus.Text = "Disconnected";
            statusStripePLC.Text = "PLC: nie połączono";
            statusStripePLC.ForeColor = Color.Red;
            statusStripePLC.Image = Properties.Resources.no;
            plcConnected = false;
        }

        public bool plcStartOnOld = false;

        public bool plcRead()
        {
            if (plcConnected)
            {

                int res, a = 0, b = 0;

                res = dc.readBytes(libnodave.daveFlags, 1, 44, 2, null);
                if (res == 0)
                {
                    plcStartOnOld = global.plcStartOn;

                    a = dc.getS8();
                    b = dc.getS8();
                    if (b > 0)
                        global.plcStartOn = true;
                    else
                        global.plcStartOn = false;
                    //plcPrintOn = Convert.ToBoolean(b);
                    //Program.autojetFinishingMain.Hm.Info("plcRead(): plcStartOn ={0}, b={1}", a.ToString(), b.ToString());
                    /*
                    int val = cbPCError.Checked ? 1 : 0;
                    int outVal = libnodave.daveSwapIed_16(val);
                    dc.writeBytes(libnodave.daveFlags, 1, 42, 2, BitConverter.GetBytes(outVal));
                    */
                    if (global.plcStartOn != plcStartOnOld)
                    {
                        Program.autojetFinishingMain.Hm.Info("plcRead(): new value: plcStartOn = {0}", global.plcStartOn.ToString());
                        if ((posterAct.colCount != 0) &&(!global.multiscanAnalizeOn)) 
                            rollbackAllPosters();
                    }
                    return global.plcStartOn;
                }
                else
                {
                    updateVisuPlcNotConnected();
                    Program.autojetFinishingMain.Hm.Info(string.Format("error {0}, {1}", res.ToString(), libnodave.daveStrerror(res)));
                }
            }
            global.plcStartOn = false;
            return false;
        }
        
        public void plcWriteError(bool errorVal)
        {
            if (plcConnected)
            {
                int val = errorVal ? 1 : 0;
                int outVal = libnodave.daveSwapIed_16(val);
                dc.writeBytes(libnodave.daveFlags, 1, 42, 2, BitConverter.GetBytes(outVal));

                Program.autojetFinishingMain.Hm.Info("plcWrite(): PC Error={0}", val.ToString());
                //if (errorVal)
                //{
                //    tErrorReset.Interval = 1000;
                //    tErrorReset.Enabled = true;
                //}
                //if (posterAct.colCount != 0)
                //    rollbackAllPosters();
            }
        }

        public void plcWriteHoldScan(bool errorVal)
        {
            if (plcConnected)
            {
                byte val = errorVal ? (byte)1 : (byte)0;
                //int outVal = libnodave.daveSwapIed_8(val);
                dc.writeBytes(libnodave.daveFlags, 1, 48, 1, BitConverter.GetBytes(val));

                Program.autojetFinishingMain.Hm.Info("plcWrite(): PC Error={0}", val.ToString());
                //if (errorVal)
                //{
                //    tErrorReset.Interval = 1000;
                //    tErrorReset.Enabled = true;
                //}
                //if (posterAct.colCount != 0)
                //    rollbackAllPosters();
            }
        }
    }
}
