﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutojetFinishing
{
    public class scanError
    {
        /*
        CREATE TABLE `tjdata`.`tile_scan_error_txt` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `error_txt` VARCHAR(60) NOT NULL,
        PRIMARY KEY(`id`));

        ALTER TABLE `tjdata`.`tile_scan_error_txt` 
        ADD COLUMN `closeVirtual` BIT(1) NULL DEFAULT false AFTER `error_txt`;
        */

        public int id { get; set; }
        public string error_txt { get; set; }
        public bool closeVirtual { get; set; }

        public scanError()
        {
            this.id = 0;
            this.error_txt = "";
            this.closeVirtual = false;
        }

        public scanError(scanError e)
        {
            this.id = e.id;
            this.error_txt = e.error_txt;
            this.closeVirtual = e.closeVirtual;
        }
    }
}
