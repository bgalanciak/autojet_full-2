﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    static class Program
    {
        public static autojetFinishingMainForm autojetFinishingMain;
        public static FileTree Ft;
        public static Settings Settings;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            autojetFinishingMain = new autojetFinishingMainForm();

            //
            Program.Ft = new FileTree();
            Program.Settings = new AutojetFinishing.Settings(Program.Ft.Get(FileTree.ID.FT_DEFAULT_SETTINGS));

            //
            if (utilities.chceckMultipleInstatnce())
            {
                MessageBox.Show("Aplikacja jest już uruchomiona.\n", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MultipleInstatnceExit = true;
                Application.Exit();
                return;
            }
            else
            {
                Application.Run(autojetFinishingMain);
            }
        }
    }
}
