﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Threading;
using Aviad.Utils.Communication.Core;
using System.Net.Sockets;
using System.Runtime.InteropServices;
        
namespace AutojetFinishing
{
    public partial class autojetFinishingMainForm : Form
    {
        private bool modeSimulation = false;    //if == true no user login form displayed, only for debug!
        public poster posterAct = new poster();

        public System.IO.Ports.SerialPort[] serialPorts;
        public serialPortConnection[] scannerSerialPorts;

        //waga
        public System.IO.Ports.SerialPort serialPortWeight;
        public serialPortConnection weightSerialPorts;

        public HealthMonitor Hm;
        //public user userAct = new user(0, "root", "", "root", 1, true, true, true);

        //tcp server
        //ServerTerminal m_serverTerminal;

        //
        public bool plcPingOk;

        //
        Thread pingPlcThread;

        //
        public bool plcConnected = false;
        static libnodave.daveOSserialType fds;
        static libnodave.daveInterface di;
        static libnodave.daveConnection dc;


        public autojetFinishingMainForm()
        {

            InitializeComponent();
            initButtons();
        }

        bool runDevel = false;
        bool saveDebug = false;
        bool showDebug = false;

        //public bool plcStartOn = false;

        private static readonly object SyncObject = new object();

        //http://stackoverflow.com/questions/2612487/how-to-fix-the-flickering-in-user-controls
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        //        return cp;
        //    }
        //}

        //http://stackoverflow.com/questions/2612487/how-to-fix-the-flickering-in-user-controls
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void autojetFinishingMainForm_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            foreach (string str in args)
            {
                if (str == "-devel")
                {
                    runDevel = true;
                }

                if (str == "-showDebug")
                {
                    showDebug = true;
                }

                if (str == "-saveDebug")
                {
                    saveDebug = true;
                    showDebug = true;   //aby dzialalo zapisywanie musi byc wlaczone pokazywanie debuga
                }

                if (str == "-rolkiOn")
                {
                    Program.Settings.Data.stationType = global.stationTypeRolka;
                }

                if (str == "-rolkiOff")
                {
                    Program.Settings.Data.stationType = global.stationTypeNotRolka;
                }
            }

            initForm();
            //tabControlMain.TabPages.Remove(tabPageDebug);
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //formatDispachDgv(null);

            //
            dispWaybillOn();
            //dispWarehouseOn();
        }

        public void initForm()
        {
            this.Text = utilities.setWindowTitle();
            utilities.logAppStart();

            this.Hm = new HealthMonitor(this.debugTestBox, Program.Ft.Get(FileTree.ID.FT_DEBUG_DMP), saveDebug);
            this.Hm.skipLoging = !showDebug;
            showDebugToolStripMenuItem.Checked = showDebug;

            Hm.Info("Started! autojetFinishing Version: {0}. ", utilities.GetProgramVersion());

            serialPorts = new System.IO.Ports.SerialPort[Program.Settings.Data.scanerCount];
            scannerSerialPorts = new serialPortConnection[Program.Settings.Data.scanerCount];

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.serialPorts[i] = new System.IO.Ports.SerialPort(this.components);
                this.scannerSerialPorts[i] = new serialPortConnection(serialPorts[i]);
                this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
            }

            //weight3;//
            this.serialPortWeight = new System.IO.Ports.SerialPort(this.components);

            //serialPortWeight.ReceivedBytesThreshold = 98;    //113;  //186;// 216; //113;
            if (Program.Settings.Data.scaleThreshold > 0)
            {
                serialPortWeight.ReceivedBytesThreshold = Program.Settings.Data.scaleThreshold;
            }
            else
            {
                serialPortWeight.ReceivedBytesThreshold = 98;
            }
            this.weightSerialPorts = new serialPortConnection(serialPortWeight);
            this.weightSerialPorts.dataRecieved += weightSerialRecieved;

            global.tableLayoutPanelWidth = tableLayoutPanel1.ColumnStyles[0].Width;
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                tableLayoutPanel1.ColumnStyles[0].Width = tabPageControl.Width - 6;
                bOperationAddManual.Enabled = false;
                bOperationRollback.Enabled = false;
            }

            updateVisu();
            updateWorkModeVisu();

            if (!runDevel)
            {
                //tcTest.TabPages.Remove(tpSelect);
                //tcTest.TabPages.Remove(tpInsert);
                tabControlMain.TabPages.Remove(tpUnused);
            }

            //ping plc
            pingPlcThread = new Thread(new ThreadStart(utilities.pingPlcMain));
            pingPlcThread.Start();

            // używane z Aviad.Utils.Communication.Core
            //create Ethernet Socket for Cognex camera, port 1000
            //createTerminal(1000);

            //http://www.codeproject.com/Articles/488668/Csharp-TCP-Server
            tcpServerCgnex.Port = 1000;
            tcpServerCgnex.Open();

            tConnections.Enabled = true;
            tConnections_Tick(null, null);

            Courier.Items.Add("WSZYSCY");
            Courier.Items.Add("CHESTER");
            Courier.Items.Add("DPD");
            Courier.Items.Add("DROZDZ");
            Courier.Items.Add("KURIERKLIENTA");
            Courier.Items.Add("PDPL");
            Courier.Items.Add("SNAMAGAZYN");
            Courier.Items.Add("SHELL");
            Courier.Items.Add("SPLOGIS");
            Courier.Items.Add("VIVA");
            Courier.Items.Add("UPS");
            Courier.Items.Add("MURAWSKI");
            Courier.Items.Add("WŁASNY");
            Courier.SelectedIndex = Courier.FindStringExact("WSZYSCY");
        }

        public void updateWorkModeVisu()
        {
            if (Program.Settings.Data.workMode == workModeSingleRow)
            {
                bWorkModeSingleRow.BackColor = Color.LightGreen;
                bWorkModeAllPoster.BackColor = default(Color);
                bWorkModeFinishing2.BackColor = default(Color);
                if (Program.Settings.Data.stationType == global.stationTypeNotRolka)
                    bOperationAddManual.Enabled = true;
            }
            else if (Program.Settings.Data.workMode == workModeAllPoster)
            {
                bWorkModeSingleRow.BackColor = default(Color);
                bWorkModeAllPoster.BackColor = Color.LightGreen;
                bWorkModeFinishing2.BackColor = default(Color);
                if (Program.Settings.Data.stationType == global.stationTypeNotRolka)
                    bOperationAddManual.Enabled = true;
            }
            else if (Program.Settings.Data.workMode == workModeAllPoster)
            {
                bWorkModeSingleRow.BackColor = default(Color);
                bWorkModeAllPoster.BackColor = Color.LightGreen;
                bWorkModeFinishing2.BackColor = default(Color);
                if (Program.Settings.Data.stationType == global.stationTypeNotRolka)
                    bOperationAddManual.Enabled = true;
            }
            else
            {
                bWorkModeSingleRow.BackColor = default(Color);
                bWorkModeAllPoster.BackColor = default(Color);
                bWorkModeFinishing2.BackColor = Color.LightGreen;
                bOperationAddManual.Enabled = false;
            }
        }

        TabPage tpLastBeforeReport; //

        public void scannerSerialRecieved(string recStr)
        {
            Program.autojetFinishingMain.Hm.Info("scannerSerialRecieved: recStr={0} ", recStr);

            //pomijam przychodzące dane, jeśli wyświetlany jest komunikat z błędem
            if (errorIsVisible)
                return;

            //
            //067052-001-0B01-2-045
            //if (recStr.Length == 21)
            //code 128
            if (recStr.Equals("XM000000000000000015"))
            {
                if (!reportMode)
                {
                    reportMode = true;
                    tpLastBeforeReport = tabControlMain.SelectedTab;
                    showReportTab();
                }
                else
                {
                    reportMode = false;
                    tabControlMain.SelectedTab = tpLastBeforeReport;
                }
            }
            else if (reportMode)
            {
                reportBarcodeAnalize(recStr);
            }
            else if ((Program.Settings.Data.stationType == global.stationTypeRolka) || (global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))
            {
                if (recStr.Length == 20)        //kod sterujący
                {
                    commandAnalyze(recStr);
                }
                else if ((recStr.Length == 30) || ((recStr.Length == 31) && (recStr.Substring(30, 1).Equals("C"))))   //kod ZPL z etykiety częściowej
                {
                    //kod z etykiety ZPL
                    //anuluj kompletacje
                    Program.autojetFinishingMain.Hm.Info($"scannerSerialRecieved(), recStr.Length == {recStr.Length}");
                    if (posterAct.tilesCount > 0)
                    {
                        Program.autojetFinishingMain.Hm.Info("scannerSerialRecieved(), rollbackAllPosters()");
                        rollbackAllPosters();
                        updateVisu();
                    }
                    //pobieranie danych
                    findSubpackToZPL(recStr);
                    //pokazać komunikat
                    if ((global.dispachMode == global.dispModeStroerSubpack) && (global.subpackList.Count > 0))
                        tableLayoutPanel7.RowStyles[2].Height = 70;
                }
                //else if ((recStr.Length == 31) && (recStr.Substring(30, 1).Equals("K")))  //obsługa w jdnrScanAnalizeMain()
                //{ }
                else
                {
                    //praca z wagą lub typu Stroer, otrzymany kod to skan karty prcy
                    if (global.dispachAllDone)
                    {
                        Program.autojetFinishingMain.Hm.Info($"scannerSerialRecieved(): global.dispachMode = {global.dispachMode}, No dispach aviable for incoming scan, data will be omitted");
                        shwoNoDispachAviableError();
                        return;
                    }
                    else
                        jdnrScanAnalizeMain(recStr);
                }
            }
            else if (recStr.Length == 16)    //nie na takiej możliwości, drukowane są tylko id obiektów
            {
                //kod kreskowy odczytany z brytu
                scanAnalyze(recStr);
            }
            else if (recStr.Length == 20)
            {
                //sterujący kod kreskowy 
                commandAnalyze(recStr);
            }
            else if (global.labelReprintMode)
            {
                //jesli jest zalaczony tryb labelReprintMode to odczytane dane kierujemy do findDispachForScan
                Program.autojetFinishingMain.Hm.Info("scannerSerialRecieved(), global.labelReprintMode==true");
                findDispachForScan(recStr); //znajduje przesyłkę i drukuje etykietę
            }
            else if (recStr.Length == 8)
            {
                //kod kreskowy z brytu

                //jeśli zakończono wszystkie wysyłki a przychodzą nowe dane to należy pokazać komunikat z błędem
                if (global.dispachAllDone && ((global.dispachMode == global.dispModeWaybillSbpack) || (global.dispachMode == global.dispModeWaybill)))
                {
                    Program.autojetFinishingMain.Hm.Info($"scannerSerialRecieved(): global.dispachMode = {global.dispachMode}, No dispach aviable for incoming scan, data will be omitted");
                    shwoNoDispachAviableError();
                    return;
                }


                //wersja przed 2018-04-20
                List<string> barcodes = new List<string>();
                barcodes.Add(recStr);
                List<string> barcode = getBarcodedataDB(barcodes);
                if (barcode.Count > 0)
                {
                    bool dispachAviableError = false;

                    if (Program.Settings.Data.dispachMandatory)
                    {
                        //dla zwykłego wszystkich trybów poza dispModeWaybillSubpackAdding i dispModeStroerSubpack
                        if ((global.dispachMode != global.dispModeWaybillSbpack) && (global.dispachMode != global.dispModeStroerSubpack))
                        {
                            //string barcodeVal = string.Format("{0}{1}{2}{3}", jdnr.ToString("D6"), jdline.ToString("D3"), tiles.Substring(0, 4), tilecounter.ToString("D4"));
                            string jdnr = barcode[0].Substring(0, 6);
                            string jdline = barcode[0].Substring(6, 3);
                            FinishingDispatchAddressAndItem i = dispachDataAcces.findNextDispachAddressAndItem(jdline, jdnr, Program.Settings.Data.stationId);
                            if (i.item_reference_id == null)
                                dispachAviableError = true;
                        }
                        else //dla trybu dispModeWaybillSubpackAdding lub dispModeStroerSubpack
                        {
                            string jdnr = barcode[0].Substring(0, 6);
                            string jdline = barcode[0].Substring(6, 3);
                            if (!isPosterOnSubpackList(global.subpackList, jdline, jdnr))
                                dispachAviableError = true;
                        }
                    }

                    if (dispachAviableError)
                    {
                        //pokaż błąd braku wysyłki
                        showNoDispachErrorForm();
                        return;
                    }

                    scanAnalyze(barcode[0]);
                }
            }

            else if ((recStr.Length == 30) || (recStr.Length == 31))  //kod ZPL z etykiety częściowej
            {
                //kod z etykiety ZPL
                //anuluj kompletacje
                Program.autojetFinishingMain.Hm.Info($"scannerSerialRecieved(), recStr.Length == {recStr.Length}");
                if (posterAct.tilesCount > 0)
                {
                    Program.autojetFinishingMain.Hm.Info("scannerSerialRecieved(), rollbackAllPosters()");
                    rollbackAllPosters();
                    updateVisu();
                }
                //pobieranie danych
                findSubpackToZPL(recStr);
            }
            else
            {
                scanedBarcodeError(recStr);
            }
            tbInputStr.Text = recStr;
        }

        private void commandAnalyze(string recStr)
        {
            //const int workModeSingleRow = 0;
            //const int workModeAllPoster = 1;
            //const int workModeFinishing2 = 2;

            if (recStr.Equals("XM000000000000000000"))
            {
                //zmiana trybu:  = workModeSingleRow
                //anuluj kompletacje
                if (posterAct.tilesCount > 0)
                {
                    rollbackAllPosters();
                    updateVisu();
                }

                if (posterAct.tilesCount == 0)
                {
                    Program.Settings.Data.workMode = workModeSingleRow;
                    updateWorkModeVisu();
                }
                //czyszczenie tabeli wysyłek
                //dispWarehouseOn();
                dispWaybillOn();
                //czyszczenie wyswietlania (2018-06-11)
                cleanJobData();
            }
            else if (recStr.Equals("XM000000000000000001"))
            {
                //zmiana trybu:  = workModeAllPoster
                //anuluj kompletacje
                if (posterAct.tilesCount > 0)
                {
                    rollbackAllPosters();
                    updateVisu();
                }

                if (posterAct.tilesCount == 0)
                {
                    Program.Settings.Data.workMode = workModeAllPoster;
                    updateWorkModeVisu();
                }
                //czyszczenie tabeli wysyłek
                //dispWarehouseOn();
                dispWaybillOn();
                //czyszczenie wyswietlania (2018-06-11)
                cleanJobData();
            }
            else if (recStr.Equals("XM000000000000000002"))
            {
                //zmiana trybu:  = workModeFinishing2
                //anuluj kompletacje
                if (posterAct.tilesCount > 0)
                {
                    rollbackAllPosters();
                    updateVisu();
                }

                if (posterAct.tilesCount == 0)
                {
                    Program.Settings.Data.workMode = workModeFinishing2;
                    updateWorkModeVisu();
                }
                //czyszczenie tabeli wysyłek
                //dispWarehouseOn();                
                dispWaybillOn();
                //czyszczenie wyswietlania (2018-06-11)
                cleanJobData();
            }
            else if (recStr.Equals("XM000000000000000003"))
            {
                //zmiana trybu:  = workModeDisable
                //anuluj kompletacje
                if (posterAct.tilesCount > 0)
                {
                    rollbackAllPosters();
                    updateVisu();
                }

                if (posterAct.tilesCount == 0)
                {
                    Program.Settings.Data.workMode = workModeDisable;
                    updateWorkModeVisu();
                }
                //czyszczenie tabeli wysyłek
                dispStroerOn();
                //czyszczenie wyswietlania (2018-06-11)
                cleanJobData();
            }
            else if (recStr.Equals("XM000000000000000010"))
            {
                //przełącz w tryb: bez wysyłek (oposc tryb wazenia)
                //anuluj kompletacje
                if (posterAct.tilesCount > 0)
                {
                    rollbackAllPosters();
                    updateVisu();
                }

                tableLayoutPanel7.RowStyles[2].Height = 0;
                //czyszczenie tabeli wysyłek
                if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))
                    dispStroerOn();
                else
                    dispWaybillOn();
                //dispWarehouseOn();
                //czyszczenie wyswietlania (2018-06-11)
                cleanJobData();
            }
            else if (recStr.Equals("XM000000000000000011"))
            {
                //Przejdz do druku kopii etykiet
                if (global.labelReprintMode)
                    labelReprintModeOFF();
                else
                    labelReprintModeON();
                //findDispachForScan(recStr);
            }
            else if (recStr.Equals("XM000000000000000020"))
            {
                //close poster
                Program.autojetFinishingMain.Hm.Info("commandAnalyze(): close poster");
                if (posterAct.tilesCount > 0)
                {
                    closePosterErrorScan();
                }
            }
            else if (recStr.Equals("XC000000000000000002"))
            {
                Program.autojetFinishingMain.Hm.Info("commandAnalyze(): aplication close.", global.userAct.login);
                this.Close();
            }
            else if (recStr.Equals("XM000000000000000101"))
            {
                cognexEnableSetup(1);
            }
            else if (recStr.Equals("XM000000000000000102"))
            {
                cognexEnableSetup(2);
            }
            else if (recStr.Equals("XW000000000000000001"))
            {
                //get weight model 
                Program.autojetFinishingMain.Hm.Info("commandAnalyze(): starting getWeightModel()");
                if (global.actAddressAndItem.item_reference_id != null)
                    getNewWeightModel();
            }
            else if (recStr.Equals("XW000000000000000002"))
            {
                //print last label
                Program.autojetFinishingMain.Hm.Info("commandAnalyze(): print last label");
                if ((global.lastLabelAdresAndItem != null) && (global.lastLabelAdresAndItem.item_reference_id != null))
                    printDispachLabel(global.lastLabelAdresAndItem, global.lastLabelZpl, global.lastLabelWeight);
            }
            else
            {
                //logowaniu użytkownika:
                global.userList = global.getAllUsers();
                user logUser = global.findUserByHash(recStr, global.userActiveList);

                if (logUser.id != -1)
                {
                    global.userAct = new user(logUser);
                    Program.autojetFinishingMain.Hm.Info("commandAnalyze(): password correct, user login: {0}", global.userAct.login);
                    tsslUser.Text = "Użytkownik: " + global.userAct.login;
                    //lUserName.Text = "Użytkownik: " + global.userAct.login;
                    lUserName.Text = global.userAct.login;

                }
            }
        }        

        public void cognexEnableSetup(int setupNum)
        {
            //||>  Default Header.  
            //SETUP.USE int
            //state
            //[1 - 15]
            //[ON | OFF]  Setup to change
            //enable / disable Disables / Enables setup to be used. Adds or removes a setup from the list of usable Read Setups. DM8600, DM150, DM260, DM503 and DM300 series readers.

            if (setupNum == 1)
            {
                //tcpServerCgnex.Send("||>SETUP.USE 1 ON\r\n");
                //Thread.Sleep(200);
                //tcpServerCgnex.Send("||>SETUP.USE 2 OFF\r\n");

                tcpServerCgnex.Send("||>SET SETUP.PROG-TARGET 0\r\n");
                Thread.Sleep(100);
                tcpServerCgnex.Send("||>SET SETUP.ENABLE ON\r\n");
                Thread.Sleep(100);
                tcpServerCgnex.Send("||>SET SETUP.PROG-TARGET 1\r\n");
                Thread.Sleep(100);
                tcpServerCgnex.Send("||>SET SETUP.ENABLE OFF\r\n");
                //Thread.Sleep(100);

                Program.Settings.Data.barcodeMode = global.barcodeModeLader;
                pBarcodesLadder.BackColor = Color.Green;
                pBarcodesFence.BackColor = Color.Transparent;
                Program.autojetFinishingMain.Hm.Info("cognexEnableSetup(): cognex setup 1 enable");
            }
            else if (setupNum == 2)
            {
                //tcpServerCgnex.Send("||>SETUP.USE 2 ON\r\n");
                //Thread.Sleep(200);
                //tcpServerCgnex.Send("||>SETUP.USE 1 OFF\r\n");

                tcpServerCgnex.Send("||>SET SETUP.PROG-TARGET 1\r\n");
                Thread.Sleep(100);
                tcpServerCgnex.Send("||>SET SETUP.ENABLE ON\r\n");
                Thread.Sleep(100);
                tcpServerCgnex.Send("||>SET SETUP.PROG-TARGET 0\r\n");
                Thread.Sleep(100);
                tcpServerCgnex.Send("||>SET SETUP.ENABLE OFF\r\n");
                //Thread.Sleep(100);
                Program.Settings.Data.barcodeMode = global.barcodeModeFence;
                pBarcodesLadder.BackColor = Color.Transparent;
                pBarcodesFence.BackColor = Color.Green;
                Program.autojetFinishingMain.Hm.Info("cognexEnableSetup(): cognex setup 2 enable");
            }

        }

        public void labelReprintModeON()
        {
            Program.autojetFinishingMain.Hm.Info("labelReprintModeON()");
            global.labelReprintMode = true;
            //anuluj kompletacje
            if (posterAct.tilesCount > 0)
            {
                rollbackAllPosters();
                updateVisu();
            }
            //czyszczenie tabeli wysyłek
            //dispWarehouseOn();
            dispWaybillOn();
            //
            tabControlMain.SelectTab("tpLabelReprint");

        }

        public void labelReprintModeOFF()
        {
            Program.autojetFinishingMain.Hm.Info("labelReprintModeOFF()");
            global.labelReprintMode = false;
            //anuluj kompletacje
            if (posterAct.tilesCount > 0)
            {
                rollbackAllPosters();
                updateVisu();
            }
            //czyszczenie tabeli wysyłek
            dispWaybillOn();
            //dispWarehouseOn();

            //

            tabControlMain.SelectTab("tabPageControl");

        }

        public int sqlConnectionCount = 0;

        private void tConnections_Tick(object sender, EventArgs e)
        {
            if ((plcPingOk) && (!plcConnected))
                connectPLC();

            plcRead();

            //update
            if (cognexEthConnected != global.cognexEthConnectedLast)
            {
                global.cognexEthConnectedLast = cognexEthConnected;
                if (cognexEthConnected)
                {
                    ststusStripCognex.Text = "Skaner Cognex: połączono";
                    ststusStripCognex.ForeColor = Color.Green;
                    ststusStripCognex.Image = Properties.Resources.yes;
                    if (Program.Settings.Data.barcodeMode == global.barcodeModeLader)
                        cognexEnableSetup(1);
                    else
                        cognexEnableSetup(2);
                }
                else
                {
                    ststusStripCognex.Text = string.Format("Skaner Cognex: nie połączono");
                    ststusStripCognex.ForeColor = Color.Red;
                    ststusStripCognex.Image = Properties.Resources.no;
                }
            }

            //if (runDevel)
            //    return;



            if (runDevel)
                return;

            //połączenie ze skanerami
            bool serialConnectionStatus = true;
            int serialConnectionCount = 0;

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                if (!this.scannerSerialPorts[i].Connected())
                {
                    serialConnectionStatus = false;
                    this.scannerSerialPorts[i].Connect(Program.Settings.Data.scanerPortName[i], "9600");
                }
                else
                {
                    serialConnectionCount++;
                }
            }

            //status połączenia ze skanerami
            if (serialConnectionStatus)
            {
                scannersConnectionStatus.Text = "Wszystkie skanery połączone";
                scannersConnectionStatus.ForeColor = Color.Green;
                scannersConnectionStatus.Image = Properties.Resources.yes;
            }
            else
            {
                scannersConnectionStatus.Text = string.Format("Połączono {0} z {1} skanerów", serialConnectionCount.ToString(), Program.Settings.Data.scanerCount.ToString());
                scannersConnectionStatus.ForeColor = Color.Red;
                scannersConnectionStatus.Image = Properties.Resources.no;
            }

            //sprawdzam połączenie co 10 wywołań timera (sugestia p. Łukasza 2018-11-19)
            if (sqlConnectionCount < 3)
            {
                if (!SQLConnection.IsActive())
                    SQLConnection.Connect(OnConnectionStateChange);
            }
            else
                sqlConnectionCount++;

            //waga
            //if (Program.Settings.Data.stationType == true)
            //{
            if (!this.weightSerialPorts.Connected())
                {
                    global.weightConnectionStatus = false;
                    this.weightSerialPorts.Connect(Program.Settings.Data.weightPortName, "9600");
                }
                else
                {
                    global.weightConnectionStatus = true;
                }
            //}

            if (global.weightConnectionStatus)
            {
                tsslWeight.Text = "Waga: połączono";
                tsslWeight.ForeColor = Color.Green;
                tsslWeight.Image = Properties.Resources.yes;
            }
            else
            {
                tsslWeight.Text = "Waga: NIE połączono";
                tsslWeight.ForeColor = Color.Red;
                tsslWeight.Image = Properties.Resources.no;
            }
        }

        private bool loginUser()
        {
            if (modeSimulation)
                return true;

            bool retVal;

            Program.autojetFinishingMain.Hm.Info("loginUser: part1");

            global.userList = global.getAllUsers();
            userLogin userLoginForm = new userLogin();

            Program.autojetFinishingMain.Hm.Info("loginUser: part2");

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                this.scannerSerialPorts[i].dataRecieved += userLoginForm.scannerSerialRecieved;
            }

            Program.autojetFinishingMain.Hm.Info("loginUser: part3");

            if (userLoginForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Program.autojetFinishingMain.Hm.Info("loginUser: part4");

                retVal = true;
            }
            else
            {
                Program.autojetFinishingMain.Hm.Info("loginUser: part5");

                retVal = false;
            }

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                Program.autojetFinishingMain.Hm.Info("loginUser: part6");

                this.scannerSerialPorts[i].dataRecieved -= userLoginForm.scannerSerialRecieved;
                if (this.scannerSerialPorts[i].eventCount()==0)     //attach event only if there is no other event attached
                    this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
            }

            return retVal;
        }

        private void OnConnectionStateChange(object sender, StateChangeEventArgs args)
        {
            switch (args.CurrentState)
            {
                case ConnectionState.Open:
                    {
                        this.statusStripeDb.Text = "Baza danych: połączono";
                        this.statusStripeDb.ForeColor = Color.Green;
                        this.statusStripeDb.Image = Properties.Resources.yes;

                        if (global.userAct.id == -1)
                        {
                            Program.autojetFinishingMain.Hm.Info("loginUser: Starting");
                            if (!loginUser())
                            {
                                Program.autojetFinishingMain.Hm.Info("loginUser: password incorrect");
                                Application.Exit();
                            }
                            else
                            {
                                Program.autojetFinishingMain.Hm.Info("loginUser: password correct, user login: {0}", global.userAct.login);
                                tsslUser.Text = "Użytkownik: " + global.userAct.login;
                                //lUserName.Text = "Użytkownik: " + global.userAct.login;
                                lUserName.Text = global.userAct.login;
                                if (!global.userAct.is_admin)
                                {
                                    tsmiUsers.Enabled = false;
                                }
                            }
                        }
                        //
                        rollbackAllPosters();
                        //updateGlobalData();
                        updateVisu();
                        global.scanErrorList = global.getAllErrors();

                        break;
                    }
                default:
                    {
                        this.statusStripeDb.Text = "Baza danych: nie połączono";
                        this.statusStripeDb.ForeColor = Color.Red;
                        this.statusStripeDb.Image = Properties.Resources.no;
                        break;
                    }
            }
        }



        //-----------------------------------------------------------------------------------------------------------
        private void bTestInsert_Click(object sender, EventArgs e)
        {
            //insertScanedItem(tbJdnr.Text, tbJdline.Text, tbTiles.Text, tbMachine.Text, 1, 1);
        }

        private void bSelect_Click(object sender, EventArgs e)
        {
            /*
            scanItem items = getOpenPosterItem(1);
            dgvMain.DataSource = items;

            if (items.Count > 0)
            {
                posterAct = new poster(getOpenPosterData(1, items));
            }
            else
            {
                posterAct = new poster();
            }
            updateVisu();*/
        }

        private void bSelect1_Click(object sender, EventArgs e)
        {
            bSelect_Click(sender, e);
        }

        private void bCheckScan_Click(object sender, EventArgs e)
        {
            //string jdnr = tbJdnr.Text;
            //string jdline = tbJdline.Text;
            //string tiles = tbTiles.Text;
            //string machine = tbMachine.Text;
            //int tilecounter = int.Parse(tbTilecounter.Text);

            //scanItem item = new scanItem(jdnr, jdline, tiles, tilecounter, machine);
            //int result = checkTileRescan1(item);
            //lCheckScanRes.Text = result.ToString();
        }
        //-----------------------------------------------------------------------------------------------------------

        #region updateVisu
        //-----------------------------------------------------------------------------------------------------------
        public void updateVisu()
        {
            if (posterAct.tilesList.Count > 0)
            {
                tlpMain.Visible = true;
                foreach (poster.tileType t in posterAct.tilesList)
                {
                    //Program.autojetFinishingMain.Hm.Info(" updateVisu(), posterAct.tilesList.colNum={0}, posterAct.tilesList.rowNum={1}, ", t.colNum.ToString(), t.rowNum.ToString());
                    if (t.tileScanDone)
                        buttonList[t.colNum - 1, t.rowNum - 1].BackColor = Color.Green;
                    else
                        buttonList[t.colNum - 1, t.rowNum - 1].BackColor = Color.Yellow;
                }

                bWorkModeSingleRow.Enabled = false;
                bWorkModeAllPoster.Enabled = false;
                bWorkModeFinishing2.Enabled = false;
            }
            else
            {
                tlpMain.Visible = false;
                bWorkModeSingleRow.Enabled = true;
                bWorkModeAllPoster.Enabled = true;
                bWorkModeFinishing2.Enabled = true;
            }
        }
        //-----------------------------------------------------------------------------------------------------------
        #endregion

        public void cleanJobData()
        {
            //
            lJobformat.Text = "";
            lQtyordered.Text = "";
            lCustomer.Text = "";
            lCampaigndesc.Text = "";
            lLinedesc.Text = "";
            lCampaignname.Text = "";
            lJdnrJdline.Text = "";
            lQtyClosed.Text = "";

            lWeightNet.Text = "";
            lWeightTare.Text = "";
            lWeightModel.Text = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>false = plakat nie zakończony</returns>
        public bool checkPosterDone()
        {
            foreach (poster.tileType l in posterAct.tilesList)
            {
                if (l.tileScanDone == false)
                    return false;
            }
            return true;
        }

        private void bTestInput_Click(object sender, EventArgs e)
        {
            //multiErrorList.Add("TEST 1" + Environment.NewLine);
            //multiErrorList.Add("TEST 1" + Environment.NewLine);
            //multiErrorList.Add("TEST 1" + Environment.NewLine);
            //multiErrorList.Add("TEST 1" + Environment.NewLine);

            //multiErrorShow();
            //return;

            //067052-001-0B01-2-045
            if (tbInputStr.Text.Length == 16)
            {
                Program.autojetFinishingMain.Hm.Warning("Starting: scanAnalizy()");
                scanAnalyze(tbInputStr.Text);
                Program.autojetFinishingMain.Hm.Warning("Done: scanAnalizy()");
            }
        }



        public void errorTextAdd(string text)
        {
            rtbErrorList.AppendText(DateTime.Now + " " + text + Environment.NewLine);
            rtbErrorList.SelectionStart = rtbErrorList.Text.Length;
            rtbErrorList.ScrollToCaret();
            //rtbErrorList.SelectionStart = rtbErrorList.TextLength;
            rtbErrorList.SelectionLength = 0;
        }

        public void dgvMainInit()
        {
            dgvMain.Columns["id"].HeaderText = "Id";
            dgvMain.Columns["jdnr"].HeaderText = "Nr pracy";
            dgvMain.Columns["jdline"].HeaderText = "Linia";
            dgvMain.Columns["tiles"].HeaderText = "Bryt";
            dgvMain.Columns["tilecounter"].HeaderText = "TC";
            dgvMain.Columns["machine"].HeaderText = "Maszyna";
            dgvMain.Columns["operatorId"].HeaderText = "ID Operatora";
            dgvMain.Columns["stationId"].HeaderText = "Plik";
            dgvMain.Columns["operationRollback"].HeaderText = "Operacja cofnieta";
            dgvMain.Columns["posterDone"].HeaderText = "Plakat zakonczny";
            dgvMain.Columns["operationTimeStamp"].HeaderText = "Czas";


            dgvMain.Columns["id"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["jdnr"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["jdline"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["tiles"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["tilecounter"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["machine"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["operatorId"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["stationId"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["operationRollback"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["posterDone"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["operationTimeStamp"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;

            dgvMain.Columns["id"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["jdnr"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["jdline"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["tiles"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["tilecounter"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["machine"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["operatorId"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["stationId"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["operationRollback"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["posterDone"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvMain.Columns["operationTimeStamp"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        }



        //=======================================================================================================================
        bool errorIsVisible = false;

        public List<string> multiErrorList = new List<string>();

        public void multiErrorShow()
        {
            SystemSounds.Beep.Play();
            plcWriteError(true);
            errorForm erroreFormShow = new errorForm();
            string errorText = string.Format("W czasie skanowania wykryto następujące błędy:{1}", multiScanDataPending.ToString(), Environment.NewLine);
            foreach (string s in multiErrorList)
            {
                errorText += s;
            }

            erroreFormShow.lErrorText.Text = errorText;
            Program.autojetFinishingMain.Hm.Info(errorText);
            //Program.autojetFinishingMain.Hm.Info("Błąd - w czasie skanowania wykryto błędy. Szczegóły w polu błędów");

            //zmiana uchwytu zdarzenia od skanerów ręcznych
            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
            }

            errorIsVisible = true;

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            erroreFormShow.ShowDialog();
            plcWriteError(false);
            errorIsVisible = false;

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                    this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
            }

            multiErrorList.Clear();
        }

        public void rescanTileError(string tileNum = "")
        {
            if (!multiScan)
            {
                SystemSounds.Beep.Play();
                errorForm erroreFormShow = new errorForm();
                erroreFormShow.lErrorText.Text = "Błąd - kafelek został wcześniej zeskanowany";
                errorTextAdd("Błąd - kafelek został wcześniej zeskanowany");
                Program.autojetFinishingMain.Hm.Info("Błąd - kafelek został wcześniej zeskanowany");
                errorIsVisible = true;

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                erroreFormShow.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }

                errorIsVisible = false;
            }
            else
            {
                multiErrorList.Add(string.Format("Błąd - kafelek {0} został wcześniej zeskanowany{1}", tileNum, Environment.NewLine));
            }
        }

        public void wrongTileError(string tileNum = "")
        {
            if (!multiScan)
            {
                SystemSounds.Beep.Play();
                errorForm erroreFormShow = new errorForm();
                erroreFormShow.lErrorText.Text = "Błąd - kafelek nie jest częścią plakatu";
                errorTextAdd("Błąd - kafelek nie jest częścią plakatu");
                Program.autojetFinishingMain.Hm.Info("Błąd - kafelek nie jest częścią plakatu");

                errorIsVisible = true;
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                erroreFormShow.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }
                errorIsVisible = false;
            }
            else
            {
                multiErrorList.Add(string.Format("Błąd - kafelek {0} nie jest częścią plakatu{1}", tileNum, Environment.NewLine));
            }
        }

        public void doubleTileError(string tileNum = "")
        {
            if (!multiScan)
            {
                SystemSounds.Beep.Play();
                errorForm erroreFormShow = new errorForm();
                erroreFormShow.lErrorText.Text = "Błąd - zdublowany kafelek";
                errorTextAdd("Błąd - zdublowany kafelek");
                Program.autojetFinishingMain.Hm.Info("Błąd - zdublowany kafelek");

                errorIsVisible = true;
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                erroreFormShow.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }
                errorIsVisible = false;
            }
            else
            {
                multiErrorList.Add(string.Format("Błąd - zdublowany kafelek {0}{1}", tileNum, Environment.NewLine));
            }
        }

        public void databasePrinttempError()
        {
            if (!multiScan)
            {
                SystemSounds.Beep.Play();
                errorForm erroreFormShow = new errorForm();
                erroreFormShow.lErrorText.Text = "Brak danych tabeli printtemp! Skontaktuj się z administratorem";
                errorTextAdd("Brak danych tabeli printtemp! Skontaktuj się z administratorem");
                Program.autojetFinishingMain.Hm.Info("Brak danych tabeli printtemp! Skontaktuj się z administratorem");

                errorIsVisible = true;
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                erroreFormShow.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }
                errorIsVisible = false;
            }
            else
            {
                multiErrorList.Add(string.Format("Błąd w bazie finishing_log! Skontaktuj się z administratorem{0}", Environment.NewLine));
            }
        }
        //multiErrorList.Add(string.Format("Błąd - plakat nie jest częścią kompletowanej wysyłki jdline={0}, jdnr={1}{2}", item.jdline, item.jdnr, Environment.NewLine));

        public void tileIsNotPartOfDispachError(scanItem item)
        {
            if (!multiScan)
            {
                SystemSounds.Beep.Play();
                errorForm erroreFormShow = new errorForm();
                erroreFormShow.lErrorText.Text = "Błąd - plakat nie jest częścią kompletowanej wysyłki";
                errorTextAdd("Błąd - plakat nie jest częścią kompletowanej wysyłki");
                Program.autojetFinishingMain.Hm.Info("Błąd - plakat nie jest częścią kompletowanej wysyłki");
                errorIsVisible = true;

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                erroreFormShow.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }

                errorIsVisible = false;
            }
            else
            {
                multiErrorList.Add(string.Format("Błąd - plakat nie jest częścią kompletowanej wysyłki{0}", Environment.NewLine));
            }
        }

        public void posterDoneMsg()
        {
            tMsgOk.Interval = 10000;
            tMsgOk.Enabled = true;
            errorTextAdd("Skomplementowano plakat");
            Program.autojetFinishingMain.Hm.Info("Skomplementowano plakat");
            bInfoOkErr.BackColor = Color.Green;
        }

        public void scanedBarcodeError(string barcode = "")
        {
            if (!multiScan)
            {
                SystemSounds.Beep.Play();
                errorForm erroreFormShow = new errorForm();
                erroreFormShow.lErrorText.Text = "Zeskanowano błędny kod kreskowy";
                errorTextAdd("Zeskanowano błędny kod kreskowy");
                Program.autojetFinishingMain.Hm.Info("Zeskanowano błędny kod kreskowy");

                errorIsVisible = true;
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += erroreFormShow.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                erroreFormShow.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= erroreFormShow.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }
                errorIsVisible = false;
            }
            else
            {
                multiErrorList.Add(string.Format("Zeskanowano błędny kod kreskowy: {0}{1}", barcode, Environment.NewLine));
            }
        }
        //=======================================================================================================================


        private void bTestUpdate_Click(object sender, EventArgs e)
        {
            closeActPoster();
        }

        bool debugMode = true;

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!debugMode)
            {
                tabControlMain.TabPages.Add(tabPageDebug);
                debugMode = !debugMode;
            }
            else
            {
                tabControlMain.TabPages.Remove(tabPageDebug);
                debugMode = !debugMode;
            }

            debugToolStripMenuItem.Checked = debugMode;
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutojetAboutBox aboutBox = new AutojetAboutBox();
            aboutBox.Show();
        }

        private void bOperationAddManual_Click(object sender, EventArgs e)
        {
            //=====================================================================
            //do 2018-01-04
            //foreach (poster.tileType t in posterAct.tilesList)
            //{
            //    if (t.tileScanDone == false)
            //    {
            //        t.isVirtual = true;
            //        t.tileScanDone = true;
            //    }
            //}
            //setPosterTilesAsVirtual(posterAct, global.userAct.id, Program.Settings.Data.stationId);

            //closeActPoster();
            //posterDoneMsg();
            ////updateGlobalData();
            //updateVisu();
            //=====================================================================

            //po 2018-01-04
            if (posterAct.tilesCount > 0)
            {
                closePosterErrorScan();
            }
        }



        private void bRollback_Click(object sender, EventArgs e)
        {
            //rollbackPoster();
            rollbackAllPosters();
        }

        /// <summary>
        /// anulowanie kompletacji, oznaczenie zeskanowanych brytów jako 'operationRollback=true'
        /// </summary>
        public void rollbackPoster()
        {
            string query = string.Format("update tjdata.finishing_log set operationRollback=true where posterDone=false and stationId={0}", Program.Settings.Data.stationId);

            SQLConnection.UpdateData(query);

            posterAct = new poster();
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //formatDispachDgv(null);

            Program.autojetFinishingMain.Hm.Warning("Anulowano kompletację plakatu");
        }

        /// <summary>
        /// anulowanie kompletacji, oznaczenie zeskanowanych brytów jako 'operationRollback=true'
        /// </summary>
        public void rollbackAllPosters()
        {
            string query = string.Format("update tjdata.finishing_log set operationRollback=true where posterDone=false and stationId={0}", Program.Settings.Data.stationId);

            SQLConnection.UpdateData(query);

            string query2 = string.Format("update tjdata.finishing2_log set operationRollback=true where posterDone=false and stationId={0}", Program.Settings.Data.stationId);

            SQLConnection.UpdateData(query2);

            Program.autojetFinishingMain.Hm.Warning("Anulowano kompletację plakatu");

            posterAct = new poster();
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //formatDispachDgv(null);
        }

        private void bOperationRollback_Click(object sender, EventArgs e)
        {
            //rollbackPoster();
            rollbackAllPosters();
            //updateGlobalData();
            updateVisu();
        }

        poster posterData;
        /// <summary>
        /// Tworzenie przykładowego plakatu - do testów
        /// </summary>
        public void newPoster()
        {
            posterData = new poster();
            global.actAddressAndItem = new FinishingDispatchAddressAndItem();
            dgvDispatchTable.DataSource = global.actAddressAndItem;
            //formatDispachDgv(null);

            poster.tileType tile1 = new poster.tileType();
            poster.tileType tile2 = new poster.tileType();
            poster.tileType tile3 = new poster.tileType();
            poster.tileType tile4 = new poster.tileType();
            poster.tileType tile5 = new poster.tileType();
            poster.tileType tile6 = new poster.tileType();

            tile1.tiles = "0A01";    //r01 c01   =>  
            tile2.tiles = "0A02";    //r01 c02
            tile3.tiles = "0A03";    //r02 c01
            tile4.tiles = "0B01";    //r02 c02
            tile5.tiles = "0B02";    //
            tile6.tiles = "0B03";    //

            posterData.tilesList.Add(tile1);
            posterData.tilesList.Add(tile2);
            posterData.tilesList.Add(tile3);
            posterData.tilesList.Add(tile4);
            posterData.tilesList.Add(tile5);
            posterData.tilesList.Add(tile6);
        }



        /// <summary>
        /// Obliczanie maksymalnego rozmiaru czcionki mieszczącego się na przycisku
        /// http://stackoverflow.com/questions/15571715/auto-resize-font-to-fit-rectangle
        /// </summary>
        /// <param name="b"></param>
        public int calcMaxFontSize(Button b)
        {
            for (int fontSize = 250; fontSize > 10; fontSize--)
            {
                string measureString = b.Text;
                Font stringFont = new Font("Arial", fontSize);

                // Measure string.
                SizeF stringSize = new SizeF();
                Graphics graphics = b.CreateGraphics();
                stringSize = graphics.MeasureString(measureString, stringFont);

                if (Convert.ToInt32(stringSize.Width) < (b.Width - 30))
                    return fontSize;
            }
            return 0;
        }

        private System.Windows.Forms.Button[,] buttonList = new Button[global.maxColCount, global.maxRowCount];

        /// <summary>
        /// inicjalizacja obiektów button symbolizujących bryty na plakacie
        /// </summary>
        //
        private void initButtons()
        {
            for (int i = 0; i < global.maxColCount; i++)
            {
                for (int j = 0; j < global.maxRowCount; j++)
                {
                    buttonList[i, j] = new Button();
                    this.tlpMain.Controls.Add(this.buttonList[i, j], i, j);

                    this.buttonList[i, j].Dock = System.Windows.Forms.DockStyle.Fill;
                    this.buttonList[i, j].Location = new System.Drawing.Point(58, 3);
                    this.buttonList[i, j].Name = "bTiles" + i.ToString() + j.ToString();
                    this.buttonList[i, j].Size = new System.Drawing.Size(49, 65);
                    this.buttonList[i, j].TabIndex = 1;
                    this.buttonList[i, j].Text = "bTiles" + i.ToString() + j.ToString();
                    this.buttonList[i, j].UseVisualStyleBackColor = true;

                }
            }
            buttonList[0, 0].SizeChanged += buttonSizeChanged;
        }

        /// <summary>
        /// zmiana rozmiaru napisów na przyciskach po zmianie rozmiaru przycisków
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSizeChanged(object sender, EventArgs e)
        {
            int fontSize = calcMaxFontSize(buttonList[0, 0]);
            if (fontSize > 0)
            {
                for (int i = 0; i < global.maxColCount; i++)
                {
                    for (int j = 0; j < global.maxRowCount; j++)
                    {
                        buttonList[i, j].Font = new Font("Arial", fontSize, FontStyle.Regular);
                    }
                }
            }
        }


        /// <summary>
        /// dodaje do bazy danych te bryty które w poster są oznaczone jako isVirtual=true
        /// </summary>
        /// <param name="p"></param>
        /// <param name="operatorId"></param>
        /// <param name="stationId"></param>
        public void setPosterTilesAsVirtual(poster p, int operatorId, int stationId)
        {
            foreach (poster.tileType t in p.tilesList)
            {
                if (t.isVirtual)
                {

                    //przed 2017.11.06
                    //string insertQuery = string.Format("INSERT INTO tjdata.finishing_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId, tileVirtual) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6}, true)", p.jdnr, p.jdline, t.tiles, "", 0, operatorId, stationId);    //p.machine

                    //po 2017.11.06
                    string insertQuery = string.Format("INSERT INTO tjdata.finishing_log(jdnr, jdline, tiles, machine, tilecounter, operatorId, operationTimeStamp, operationRollback, posterDone, stationId, tileVirtual, reference_id, ref_from, ref_to, subpack_from, subpack_to) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', NOW(), false, false, {6}, true, '{7}', {8}, {9}, {10}, {11})", p.jdnr, p.jdline, t.tiles, "", 0, operatorId, stationId, global.actAddressAndItem.item.reference_id, global.actAddressAndItem.item.ref_from, global.actAddressAndItem.item.ref_to, global.actAddressAndItem.item.subpack_from, global.actAddressAndItem.item.subpack_to);    //p.machine

                    SQLConnection.InsertData(insertQuery);
                }
            }
        }

        /// <summary>
        /// pobiera dane o plakacie z bazy Jobsdata
        /// </summary>
        /// <param name="p"></param>
        public void getJobsData(poster p)
        {
            if (!p.initDone)
                return;

            //SELECT * FROM Jobsdata WHERE jdnr=pana_nr and jdline=pana_nr
            string query = string.Format("SELECT * FROM Jobsdata WHERE jdnr={0} and jdline={1}", p.jdnr, p.jdline);

            DataTable dTable = SQLConnection.GetData(query);

            try
            {
                if (dTable.Rows.Count > 0)
                {
                    p.jobformat = dTable.Rows[0]["jobformat"].ToString();
                    p.qtyordered = dTable.Rows[0]["qtyordered"].ToString();
                    p.customer = dTable.Rows[0]["customer"].ToString();
                    p.campaignname = dTable.Rows[0]["campaignname"].ToString();
                    p.campaigndesc = dTable.Rows[0]["campaigndesc"].ToString();
                    p.linedesc = dTable.Rows[0]["linedesc"].ToString();
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Warning("getJobsData(): no data in Jobsdata table");
                }

                lJobformat.Text = p.jobformat;
                lQtyordered.Text = p.qtyordered + " / " + p.qtyPrinted.ToString();
                lCustomer.Text = p.customer;
                lCampaigndesc.Text = p.campaigndesc;
                lLinedesc.Text = p.linedesc;
                //lQtyPrinted.Text = p.qtyPrinted.ToString();
                lCampaignname.Text = p.campaignname;
                lJdnrJdline.Text = p.jdnr + " " + p.jdline;
                lQtyClosed.Text = p.qtyClosed.ToString() + "+" + p.qtyClosedVirtual.ToString() + "=" + (p.qtyClosed + p.qtyClosedVirtual).ToString();
            }
            catch (Exception)
            {
                Program.autojetFinishingMain.Hm.Warning("getJobsData(): error in parsing data from Jobsdata table");
                return;
            }
        }


        /// <summary>
        /// pobiera dane o plakacie z bazy Jobsdata
        /// </summary>
        /// <param name="p"></param>
        public string getPosterSize(poster p)
        {
            string retVal;

            //SELECT * FROM Jobsdata WHERE jdnr=pana_nr and jdline=pana_nr
            string query = string.Format("SELECT * FROM Jobsdata WHERE jdnr={0} and jdline={1}", p.jdnr, p.jdline);

            DataTable dTable = SQLConnection.GetData(query);
            try
            {
                retVal = dTable.Rows[0]["jobformat"].ToString();

                Program.autojetFinishingMain.Hm.Info("getPosterSize(): retVal={0}", retVal);

                return retVal;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// pobiera dane o plakacie z bazy Jobsdata
        /// </summary>
        /// <param name="p"></param>
        public string getPosterSize(scanItem p)
        {
            string retVal;

            //SELECT * FROM Jobsdata WHERE jdnr=pana_nr and jdline=pana_nr
            string query = string.Format("SELECT * FROM Jobsdata WHERE jdnr={0} and jdline={1}", p.jdnr, p.jdline);

            DataTable dTable = SQLConnection.GetData(query);
            try
            {
                retVal = dTable.Rows[0]["jobformat"].ToString();

                Program.autojetFinishingMain.Hm.Info("getPosterSize(): retVal={0}", retVal);

                return retVal;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void bWorkModeSingleRow_Click(object sender, EventArgs e)
        {
            Program.Settings.Data.workMode = workModeSingleRow;
            updateWorkModeVisu();
        }

        private void bWorkModeAllPoster_Click(object sender, EventArgs e)
        {
            Program.Settings.Data.workMode = workModeAllPoster;
            updateWorkModeVisu();
        }

        private void bWorkModeFinishing2_Click(object sender, EventArgs e)
        {
            Program.Settings.Data.workMode = workModeFinishing2;
            updateWorkModeVisu();
        }

        /// <summary>
        /// Formularz edycji i dodawania formatów do kompletacji typu B+A
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void formatyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formatListForm formatListFor = new formatListForm();

            string query = string.Format("SELECT * FROM typed_sizes_table");
            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTable.Rows)
                {
                    formatListForm.StringValue str = new formatListForm.StringValue(dRow[0].ToString());
                    formatListFor.formatList.Add(str);
                }
            }

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            formatListFor.ShowDialog();

            if (formatListFor.formatListNew.Count > 0)
            {
                foreach (formatListForm.StringValue str in formatListFor.formatListNew)
                {
                    string insertQuery = string.Format("INSERT INTO tjdata.typed_sizes_table(format) VALUES('{0}')", str.Value);

                    SQLConnection.InsertData(insertQuery);
                }
            }
        }

        private void autojetFinishingMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Settings.Save();

            if (pingPlcThread != null)
                pingPlcThread.Abort();
        }

        private void createDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string create = string.Format("CREATE TABLE `tjdata`.`finishing2_log` (`id` INT NOT NULL AUTO_INCREMENT, `jdnr` VARCHAR(6) NULL DEFAULT NULL,`jdline` VARCHAR(3) NULL DEFAULT NULL,`tilecounter` INT NULL DEFAULT NULL, `tiles` VARCHAR(15) NULL DEFAULT NULL,`machine` VARCHAR(15) NULL DEFAULT NULL,`operatorId` INT NULL DEFAULT NULL,`operationTimeStamp` DATETIME NULL DEFAULT NULL,`operationRollback` BIT(1) NULL DEFAULT false,`posterDone` BIT(1) NULL DEFAULT false,`stationId` INT NULL DEFAULT NULL,`finishingId` INT NULL DEFAULT NULL,`tileVirtual` BIT(1) NULL DEFAULT false,`finishing2Id` INT NULL DEFAULT NULL,PRIMARY KEY (`id`));");

            //string create = string.Format("ALTER TABLE `tjdata`.`user_table` ADD COLUMN `userLogHash` VARCHAR(45) NULL DEFAULT NULL AFTER `isActive`");

            //string create = string.Format("ALTER TABLE `tjdata`.`poster_closeed_log` ADD COLUMN `closedVirtual` BIT(1) NULL DEFAULT false AFTER `operationTimeStamp`;");

            //SQLConnection.UpdateData(create);
        }



        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                listLog.Items.Clear();

                //int alPort = 1000;

                //createTerminal(alPort);

                //cmdConnect.Enabled = false;
                //cmdClose.Enabled = true;
            }
            catch (Exception se)
            {
                MessageBox.Show(se.Message);
            }

        }

        private void tcpServerCgnex_OnConnect(tcpServer.TcpServerConnection connection)
        {
            PublishMessage(listLog, string.Format("Client on {0} has been connected!", connection.Socket.Client.RemoteEndPoint.ToString()));
            cognexEthConnected = true;
            global.cognexEthConnectedLast = false;
        }

        private void tcpServerCgnex_OnDataAvailable(tcpServer.TcpServerConnection connection)
        {
            byte[] data = readStream(connection.Socket);

            if (data != null)
            {
                //pomijaj przychodzące dane 
                if (global.skipScanerData)
                {
                    data = null;
                    return;
                }

                string dataStr = Encoding.ASCII.GetString(data);

                multiScanDataPending++;

                invokeDelegate del = () =>
                {
                    tcpScanStart(dataStr);
                };
                Invoke(del);

                //multiScanDataPending--;     //2018-01-30

                data = null;
            }
        }

        //private static readonly object SyncObject = new object();
        public int multiScanDataPending = 0;
        public bool multiScanErrorShow = false;

        public void tcpScanStart(string readStr)
        {
            PublishMessage(listMessages, readStr + Environment.NewLine);

            if (global.skipIncomingScan)
            {
                Program.autojetFinishingMain.Hm.Info(string.Format("tcpScanStart(): global.skipIncomingScan=true"));
                return;
            }

            Program.autojetFinishingMain.Hm.Info(string.Format("tcpScanStart(): read data: {0}, multiScanDataPending={1}", readStr, multiScanDataPending.ToString()));
            Program.autojetFinishingMain.Hm.Info(string.Format("tcpScanStart(): befor lock...id:{0}", readStr));

            lock (SyncObject)
            {
                //jeśli zakończono wszystkie wysyłki a przychodzą nowe dane to należy pokazać komunikat z błędem
                multiScanDataPending--;   //2018-01-30 - przeniesione do tcpServerCgnex_OnDataAvailable
                if (global.dispachAllDone && ((global.dispachMode == global.dispModeWaybillSbpack) || (global.dispachMode == global.dispModeWaybill)))
                {
                    Program.autojetFinishingMain.Hm.Info($"tcpScanStart(): global.dispachMode = {global.dispachMode}, No dispach aviable for incoming scan, data will be omitted");
                    shwoNoDispachAviableError();
                    return;
                }

                //multiScanDataPending--;   //2018-01-30 - przeniesione do tcpServerCgnex_OnDataAvailable

                Program.autojetFinishingMain.Hm.Info(string.Format("tcpScanStart(): lock is ON, id:{0}, multiScanDataPending={1}", readStr, multiScanDataPending.ToString()));
                multiScanAnalize(readStr);
                Program.autojetFinishingMain.Hm.Info(string.Format("tcpScanStart(): multiScanAnalize done...id:{0}", readStr));
            }
            Program.autojetFinishingMain.Hm.Info(string.Format("tcpScanStart(): lock is OFF, id:{0}", readStr));
        }

        private void shwoNoDispachAviableError()
        {
            if ((global.dispachMode == global.dispModeWaybillSbpack) || (global.dispachMode == global.dispModeStroerSubpack))
            {

                changeDispachModeForm dispachErrorForm = new changeDispachModeForm();
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += dispachErrorForm.scannerSerialRecieved;
                }

                dispachErrorForm.lInfo2.Text = "ZESKANUJ KOD KRESKOWY";
                dispachErrorForm.lInfo3.Text = "'OPUŚĆ TRYB ŁĄCZENIA'";
                dispachErrorForm.lInfo3.Text = "LUB ETYKIETĘ CZĘŚCIOWĄ";
                dispachErrorForm.mode = 2;      //1 - dispWaybill, 2 - dispSubpackAdding

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                dispachErrorForm.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= dispachErrorForm.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }

                //
                scannerSerialRecieved(dispachErrorForm.recStr);
            }
            else if (global.dispachMode == global.dispModeWaybill)
            {
                changeDispachModeForm dispachErrorForm = new changeDispachModeForm();
                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                    this.scannerSerialPorts[i].dataRecieved += dispachErrorForm.scannerSerialRecieved;
                }

                dispachErrorForm.lInfo2.Text = "ZESKANUJ KOD KRESKOWY";
                dispachErrorForm.lInfo3.Text = "'OPUŚĆ TRYB ŁĄCZENIA'";
                dispachErrorForm.mode = 1;      //1 - dispWaybill, 2 - dispSubpackAdding

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                dispachErrorForm.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    this.scannerSerialPorts[i].dataRecieved -= dispachErrorForm.scannerSerialRecieved;
                    if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                }

                //
                scannerSerialRecieved(dispachErrorForm.recStr);
            }

            Program.autojetFinishingMain.Hm.Info(string.Format("shwoNoDispachAviableError(): Done..."));
            return;
        }

        public delegate void invokeDelegate();

        protected byte[] readStream(TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            if (stream.DataAvailable)
            {
                byte[] data = new byte[client.Available];

                int bytesRead = 0;
                try
                {
                    bytesRead = stream.Read(data, 0, data.Length);
                }
                catch
                {
                }

                if (bytesRead < data.Length)
                {
                    byte[] lastData = data;
                    data = new byte[bytesRead];
                    Array.ConstrainedCopy(lastData, 0, data, 0, bytesRead);
                }
                return data;
            }
            return null;
        }

        private void tcpServerCgnex_OnError(tcpServer.TcpServer server, Exception e)
        {
            PublishMessage(listLog, string.Format("TCP Server Error. Error message is: {0} ", e.Data.ToString()));
        }

        private void PublishMessage(ListBox listBox, string mes)
        {
            if (InvokeRequired)
            {
                BeginInvoke((ThreadStart)delegate { PublishMessage(listBox, mes); });
                return;
            }

            listBox.Items.Add(mes);

            int visibleItems = listBox.ClientSize.Height / listBox.ItemHeight;
            listBox.TopIndex = Math.Max(listBox.Items.Count - visibleItems + 1, 0);
        }

        private void autojetFinishingMainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            tcpServerCgnex.Close();
        }

        private void tMsgOk_Tick(object sender, EventArgs e)
        {
            bInfoOkErr.BackColor = Color.Transparent;
            tMsgOk.Enabled = false;
        }

        //===========================================================================================
        private const int WM_SETREDRAW = 0x000B;
        private const int WM_USER = 0x400;
        private const int EM_GETEVENTMASK = (WM_USER + 59);
        private const int EM_SETEVENTMASK = (WM_USER + 69);

        private int drawStopCount = 0;

        [DllImport("user32", CharSet = CharSet.Auto)]
        private extern static IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, IntPtr lParam);

        IntPtr eventMask = IntPtr.Zero;

        public void StopDrawing()
        {
            if (drawStopCount == 0)
            {
                // Stop redrawing:
                SendMessage(this.Handle, WM_SETREDRAW, 0, IntPtr.Zero);
                // Stop sending of events:
                eventMask = SendMessage(this.Handle, EM_GETEVENTMASK, 0, IntPtr.Zero);
            }
            drawStopCount++;
        }

        public void StartDrawing()
        {
            drawStopCount--;
            if (drawStopCount == 0)
            {
                // turn on events
                SendMessage(this.Handle, EM_SETEVENTMASK, 0, eventMask);

                // turn on redrawing
                SendMessage(this.Handle, WM_SETREDRAW, 1, IntPtr.Zero);

                Invalidate();
                Refresh();
            }
        }

        private void tsmiUsers_Click(object sender, EventArgs e)
        {
            global.userList = global.getAllUsers();
            usersManagement users = new usersManagement();

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            users.ShowDialog();
        }

        private void cbPCErrorSymulaton_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPCErrorSymulaton.Checked)
                plcWriteError(cbPCErrorSymulaton.Checked);
        }

        /// <summary>
        /// reset wyjscia "Błąd kompletacji"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tErrorReset_Tick(object sender, EventArgs e)
        {
            global.posterCompletionError = false;

            if (cbPCErrorSymulaton.Checked)
                cbPCErrorSymulaton.Checked = false;

            plcWriteError(false);
            tErrorReset.Enabled = false;
        }

        private void showDebugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showDebug = !showDebug;
            this.Hm.skipLoging = !showDebug;
            showDebugToolStripMenuItem.Checked = showDebug;
        }

        private void tlpMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void initDgvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //dgvDispatchTable.DataSource
            List<dispatch_row> dispatchList = new List<dispatch_row>();
            dispatch_row row1 = new dispatch_row();
            dispatch_row row2 = new dispatch_row();
            dispatch_row row3 = new dispatch_row();

            row1.id = 1;
            row1.jdline = "1";
            row1.jdnr = "1";
            row1.address = "Adress 1";
            row1.name = "Name 1";
            row1.qtyToPack = 0;
            row1.qtyPacked = 1;

            row2.id = 2;
            row2.jdline = "11";
            row2.jdnr = "31";
            row2.address = "Adress 1adsfasf";
            row2.name = "Name zsvdsz 1";
            row2.qtyToPack = 0;
            row2.qtyPacked = 1;

            row3.id = 3;
            row3.jdline = "41";
            row3.jdnr = "14";
            row3.address = "Adress Asfasfd";
            row3.name = "Zsvdsz 1";
            row3.qtyToPack = 0;
            row3.qtyPacked = 1;

            dispatchList.Add(row1);
            dispatchList.Add(row2);
            dispatchList.Add(row3);

            dgvDispatchTable.DataSource = dispatchList;

            dispatchTableFormat();
        }

        private void dispatchTableFormat()
        {
            dgvDispatchTable.Columns[1].Visible = false;
            dgvDispatchTable.Columns[2].Visible = false;

            dgvDispatchTable.Columns[0].HeaderText = "ID";
            dgvDispatchTable.Columns[1].HeaderText = "Linia";
            dgvDispatchTable.Columns[2].HeaderText = "Karta";
            dgvDispatchTable.Columns[3].HeaderText = "Adres";
            dgvDispatchTable.Columns[4].HeaderText = "Nazwa";
            dgvDispatchTable.Columns[5].HeaderText = "Zad.";
            dgvDispatchTable.Columns[6].HeaderText = "Spak.";

            DataGridViewColumn column = dgvDispatchTable.Columns[3];
            column.Width = 200;

            dgvDispatchTable.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            //dgvDispatchTable.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvDispatchTable.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvDispatchTable.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            dgvDispatchTable.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

            dgvDispatchTable.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            foreach (DataGridViewColumn col in dgvDispatchTable.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        private void testPrinterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendToPrinter(@"C:\test\Test.pdf");
            //SendToPrinterZPL();
        }

        private void initToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
                        List<FinishingDispatchAddressAndItem> items = new List<AutojetFinishing.FinishingDispatchAddressAndItem>();

                        FinishingDispatchAddressAndItem i = new FinishingDispatchAddressAndItem();
                        i.address.dispatch_company = "test company aaaaa";
                        i.address.city = "miasto d dlugiej nazwie";
                        i.address.street = "jakas ulica";
                        i.item.qty_packed = 15;
                        i.item.qty_packed = 111;
                        i.item.partial_item = true;

                        items.Add(i);

                        dgvDispatchTable.DataSource = items;
                        UpdateDispachDGV();
              */

            //FinishingDispatchAddressAndItem itttt = new FinishingDispatchAddressAndItem();

            //if (itttt.item_reference_id == null)
            //{
            //    initToolStripMenuItem.Text = "AAAA";
            //}
            //List<poster> p = new List<poster>();
            //for (int i = 0; i < 20; i++)
            //{
            //    poster pp = new poster();
            //    pp.jdline = "1";
            //    pp.jdnr = "12345";
            //}

            poster pp = new poster();
            pp.jdline = "001";
            pp.jdnr = "085253";
            posterAct = pp;
            //findDispach(pp);
            //findAdresses("1", "12345");
        }


        private void tDispatchDone_Tick(object sender, EventArgs e)
        {
            //stop beep and timer
            plcWriteError(false);
            tDispatchDone.Enabled = false;
        }

        private void lQtyClosed_Click(object sender, EventArgs e)
        {

        }

        private void findDispachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            findDispach(posterAct);
        }

        private void findAddressesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            findAdresses(posterAct.jdline, posterAct.jdnr);
        }

        private void updateDispachCountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            updateDispachCount(posterAct);
            findAdresses(posterAct.jdline, posterAct.jdnr);
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            updateDispachCount(posterAct);
            findAdresses(posterAct.jdline, posterAct.jdnr);
        }

        private void bModeDispachAddSubpack_Click(object sender, EventArgs e)
        {

        }

        private void findSubpackToZPLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            findSubpackToZPL("085253082315034616_00201010101");
        }

        private void bDispachModeNoWaybill_Click(object sender, EventArgs e)
        {

        }

        private void tstToolStripMenuItem_Click(object sender, EventArgs e)
        {
            global.actAddressAndItem.item.partial_item = true;
            printDispachLabel(global.actAddressAndItem, global.zplData);

        }

        //waga -------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Metoda wywołana przy odebraniu danych z rs232 połączonego z wagą
        /// </summary>
        /// <param name="recStr"></param>
        public void weightSerialRecieved(string recStr)
        {
            Program.autojetFinishingMain.Hm.Info("weightSerialRecieved: recStr={0} ", recStr);

            weightAnalyzeMain(weightAnalyze(recStr));
        }

        /// <summary>
        /// Enkapsulacja masy w gramach z danych pochodzących z rs-232 z wagi
        /// </summary>
        /// <param name="scanStr"></param>
        public int weightAnalyze(string scanStr)
        {
            int weightRead = -1;
            try
            {
                //string gram = scanStr.Substring(13, 7);
                //rozwiązanie p. Łukasza 2018-04-13
                string gram = scanStr.Substring(Program.Settings.Data.scaleFrom, Program.Settings.Data.scaleHowMany);
                gram = gram.Trim();

                if (Program.Settings.Data.scaleMass.Equals("kg"))
                {
                    double weightTemp = 0;
                    gram = gram.Replace(".", ",");
                    weightTemp = (double.Parse(gram) * 1000);
                    gram = weightTemp.ToString();
                }
                int weightGr = int.Parse(gram);
                weightRead = weightGr;
                Program.autojetFinishingMain.Hm.Info($"weightAnalyze: weightRead = {weightGr} ");
            }
            catch (Exception e)
            {
                string gram = scanStr.Substring(Program.Settings.Data.scaleFrom, Program.Settings.Data.scaleHowMany);
                Program.autojetFinishingMain.Hm.Info($"weightAnalyze: gramStr = {gram} ");
            }

            Program.autojetFinishingMain.Hm.Info($"weightAnalyze: retVal = {weightRead} ");
            return weightRead;
        }

        bool weightErrorShow = false;

        /// <summary>
        /// Analiza czy dane z wagi są poprawne
        /// </summary>
        /// <param name="weight"></param>
        public void weightAnalyzeMain(int weight)
        {
            if (weightErrorShow)    //jesli 
            {
                Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: weightErrorShow = true");
                return;
            }
            //wyświetlenie wagi na formularzu
            lWeightNet.Text = "Brutto: " + weight.ToString() + " [g]";

            if ((global.actAddressAndItem.item_reference_id != null) && (global.weightModel.id > -1))
            {
                int weightExpected = global.weightModel.weight_tare + (global.actAddressAndItem.qtyToPack * global.weightModel.weight_model) + global.weightModel.weight_base;

                //usuniete na prosbe p. Łukasza 2018-03-01
                //int weightMin = global.weightModel.weight_tare + (global.actAddressAndItem.qtyToPack * global.weightModel.weight_model) - (global.weightModel.weight_model / 10);
                //int weightMax = global.weightModel.weight_tare + (global.actAddressAndItem.qtyToPack * global.weightModel.weight_model) + (25 * global.weightModel.weight_model) /100;

                //dodane na prosbe p. Łukasza 2018-03-01
                int weightMin = weightExpected - Program.Settings.Data.weightDevMin;
                int weightMax = weightExpected + Program.Settings.Data.weightDevMax;

                //lJdnrJdline.Text = weightMin.ToString() + " " + weightMax.ToString();
                Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: weight = {weight.ToString()}, weightMin = {weightMin.ToString()},  weightMax = {weightMax.ToString()}, weight_base = {global.weightModel.weight_base}");

                if ((weight >= weightMin) && (weight <= weightMax))
                {
                    acceptWeight(weight);
                }
                else if (weight >= weightMax)
                {
                    //zbyt duza waga, pokaż pytanie czy ją zaakceptować.
                    overWeightErrorForm overWeightError = new overWeightErrorForm();
                    overWeightError.line1 = $"Zbyt duża waga – {weight} [g]. Waga powinna być w zakresie {weightMin} [g] – {weightMax} [g].";
                    overWeightError.line2 = $"Jeżeli celowo coś dołożyłeś zatwierdź to skanując kod AKCEPTUJ.";
                    overWeightError.line3 = $"Jeżeli jest to błąd i chcesz ponownie spakować paczkę zeskanuj ZAMKNIĘCIE KOMUNIKATU.";

                    for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                    {
                        this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                        this.scannerSerialPorts[i].dataRecieved += overWeightError.scannerSerialRecieved;
                    }

                    //DOPISANE ŁP 2018-09-05
                    Program.autojetFinishingMain.Activate();

                    overWeightError.ShowDialog();

                    for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                    {
                        this.scannerSerialPorts[i].dataRecieved -= overWeightError.scannerSerialRecieved;
                        if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                            this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                    }

                    if (overWeightError.saveVal)
                    {
                        acceptWeight(weight);
                        Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: acceptWeight()");
                    }
                    else
                        return;
                }
                else
                {
                    //zła waga, pokaż komunikat
                    weightErrorShow = true;

                    Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: weight wrong, show form");
                    weightWrong weightWrongForm = new weightWrong();
                    weightWrongForm.msgStr = $"Waga nieprawidłowa - {weight.ToString()} [g] Waga powinna być w zakresie {weightMin} [g] – {weightMax} [g], Sprawdź poprawność pakowania lub dokonaj ponownego wzorcowania";

                    for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                    {
                        this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                        this.scannerSerialPorts[i].dataRecieved += weightWrongForm.scannerSerialRecieved;
                    }

                    //DOPISANE ŁP 2018-09-05
                    Program.autojetFinishingMain.Activate();

                    weightWrongForm.ShowDialog();

                    weightErrorShow = false;

                    for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                    {
                        this.scannerSerialPorts[i].dataRecieved -= weightWrongForm.scannerSerialRecieved;
                        if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                            this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
                    }
                }
            }
        }

        public void acceptWeight(int weight)
        {
            //close item id DB
            global.actAddressAndItem.item.qty_packed = global.actAddressAndItem.qtyToPack;
            global.actAddressAndItem.item.weight_act = weight;
            global.actAddressAndItem.item.weight_model = global.weightModel.id;
            dispachDataAcces.UpdateDispachItemPackedQty(global.actAddressAndItem.item);           //update packed_qty
            dispachDataAcces.UpdateDispachItemcompletationDone(global.actAddressAndItem.item, global.userAct.id, Program.Settings.Data.stationId);    //update completation_done and booked_station_id
                                                                                                                                                        //print label depending of type of package
            printDispachLabel(global.actAddressAndItem, global.zplData, weight);
            //save data for last label printing
            global.lastLabelAdresAndItem = global.actAddressAndItem;
            global.lastLabelZpl = global.zplData;
            global.lastLabelWeight = weight;
            //
            Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: label printed OK, looking for adresses for: jdline = {posterAct.jdline}, jdnr = {posterAct.jdnr}");

            //get next dispach
            findDispach(posterAct);                         //pobiera adres do zmiennej global.actAddressAndItem i rezerwuje go w bazie
            findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach            
            if (global.actAddressAndItem.item_reference_id == null)
            {
                formatDispachDgv(null);
                return;                                     //nie ma więcej wysyłek, przerywam
            }
            getJobsData(posterAct);

            //check if selected weight model is proper
            if ((global.weightModel.jdnr == global.actAddressAndItem.item.jdnr) && (global.weightModel.jdline == global.actAddressAndItem.item.jdline) && (global.weightModel.pack_type == global.actAddressAndItem.address.pack_type))
            {
                //weight model OK
            }
            else
            {
                //check if model is in DB
                global.weightBarcodeScan jdnrScan = new global.weightBarcodeScan();
                jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
                jdnrScan.jdline = global.actAddressAndItem.item.jdline;
                jdnrScan.pack_type = global.actAddressAndItem.address.pack_type;
                jdnrScan.tileName = global.actAddressAndItem.item.tileName;
                jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
                jdnrScan.modelType = dispachDataAcces.GetModelModeFromAdresAndItem(global.actAddressAndItem);
                jdnrScan.model_scan_count = 1;//global.actAddressAndItem.item.qty_to_pack;

                global.weightModelDataType weightModelFromDB = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
                Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: weightModelFromDB.id = {weightModelFromDB.id} ");
                if (weightModelFromDB.id > -1)
                {
                    Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: weightModelFromDB OK");
                    //pobrano model z DB, przepisać do 
                    global.weightModel.jdnr = weightModelFromDB.jdnr;
                    global.weightModel.jdline = weightModelFromDB.jdline;
                    global.weightModel.pack_type = weightModelFromDB.pack_type;
                    global.weightModel.id = weightModelFromDB.id;
                    global.weightModel.weight_model = weightModelFromDB.weight_model;
                    global.weightModel.weight_tare = weightModelFromDB.weight_tare;
                    Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: pobierałem weightModel z DB");
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: pobieram weightModel z formularza ");
                    //pobrać model z wagi:
                    global.weightModelDataType model = getWeightModelFromForm(jdnrScan, Convert.ToInt32(!Program.Settings.Data.stationType));       //

                    dispachDataAcces.insertWeightModel(model);
                    Program.autojetFinishingMain.Hm.Info($"weightAnalyzeMain: pobrałem weightModel z formularza ");
                    global.weightModel = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
                    weightModelVisuUpdate(global.weightModel);
                }
            }
        }

        /// <summary>
        /// Analiza skanu kodu kreskowego jdnr ze skanera ręcznego
        /// </summary>
        /// <param name="recStr"></param>
        public void jdnrScanAnalizeMain(string recStr)
        {
            global.weightBarcodeScan jdnrScan = new global.weightBarcodeScan();
            //Zmiana ŁP 2018-12-20 etykoety z inną długością etykiety częściowej xle rozkłada
            if (recStr.Length < 15)
            {
                //określenie danych kompletacji na podstawie skany jdnr i jdline
                jdnrScan = jdnrScanAnalize(recStr);                        //rozłorzenie danych ze skanera na jdnr i jdline
            }
            else
            {
                //określenie danych kompletacji na podstawie skanu etykiety częściowej
                jdnrScan = partialLabelScanAnalize(recStr);
            }

            if (jdnrScan.modelType == -1)
            {
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain(): jdnrScan.modelType == -1");
                return;     //błędny kod kreskowy, przerywam analizę!
            }

            //wyswietlenie danych z tabeli jobsdata
            poster p = new poster();
            p.jdnr = jdnrScan.jdnr;
            p.jdline = jdnrScan.jdline;
            p.initDone = true;

            Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain(): getJobsData");
            getJobsData(p);
            
            //sprawdzenie, czy istnieje wysyłka do zeskanowanego typu plakatu
            if ((global.dispachMode == global.dispModeWaybillSbpack) || (global.dispachMode == global.dispModeStroerSubpack))
            {
                //w trybie dispModeWaybillSubpackAdding
                if (!isPosterOnSubpackList(global.subpackList, jdnrScan.jdline, jdnrScan.jdnr))
                {
                    //brak wysyłek dla zeskanowanego kodu, pokazać komunikat i przerwać 
                    Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain(): global.dispachMode == global.dispModeWaybillSubpackAdding ");
					showNoDispachError();
                    return;
                }

                markActAdressAndItem(jdnrScan.jdnr, jdnrScan.jdline);

                jdnrScan.pack_type = global.actAddressAndItem.address.pack_type;                                //pobranie pack_type
                jdnrScan.tileName = global.actAddressAndItem.item.tileName;                                     //pobranie tileName
                jdnrScan.modelType = dispachDataAcces.GetModelModeFromAdresAndItem(global.actAddressAndItem);   //okreslenie modelType
                jdnrScan.model_scan_count = 1;// adresAndItem.item.qty_to_pack;
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: jdnrScan.jdnr = {jdnrScan.jdnr}, jdnrScan.jdline = {jdnrScan.jdline} ");

                //dalsza obsługa ważenia odbywa się w oddzielnym oknie:
                global.actAddressAndItem.item.qty_packed = global.actAddressAndItem.item.qty_to_pack;
                //start beep
                tDispatchDone.Enabled = true;
                plcWriteError(true);

                registerPackageWeight();

                //close item id DB
                dispachDataAcces.UpdateDispachItemPackedQty(global.actAddressAndItem.item);           //update packed_qty
                dispachDataAcces.UpdateDispachItemcompletationDone(global.actAddressAndItem.item, global.userAct.id, Program.Settings.Data.stationId);    //update completation_done and booked_station_id
                //print label depending of type of package
                printDispachLabel(global.actAddressAndItem, global.zplData, global.actAddressAndItem.item.weight_act);

                //save data
                global.lastLabelAdresAndItem = global.actAddressAndItem;
                global.lastLabelZpl = global.zplData;
                global.lastLabelWeight = global.actAddressAndItem.item.weight_act;

                //clear data
                global.subpackAddingComplete = true;

                //pokazuje i koloruje listę paczek
                findAdressesForZPL(global.zplData, Program.Settings.Data.stationId);    //show all aviable dispach for ZPL

                return;
            }
            else
            {
                //w trybie innym niż dispModeWaybillSubpackAdding
                FinishingDispatchAddressAndItem adresAndItem = dispachDataAcces.findNextDispachAddressAndItem(jdnrScan.jdline, jdnrScan.jdnr, Program.Settings.Data.stationId);

                if (adresAndItem.item_reference_id == null)
                {
                    //brak wysyłek dla zeskanowanego kodu, pokazać komunikat i przerwać 
                    Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain(): global.dispachMode != global.dispModeWaybillSubpackAdding ");
					showNoDispachError();
                    return;
                }

                jdnrScan.pack_type = adresAndItem.address.pack_type;                                //pobranie pack_type
                jdnrScan.tileName = adresAndItem.item.tileName;                                     //pobranie tileName
                jdnrScan.modelType = dispachDataAcces.GetModelModeFromAdresAndItem(adresAndItem);   //okreslenie modelType
                jdnrScan.model_scan_count = 1;// adresAndItem.item.qty_to_pack;
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: jdnrScan.jdnr = {jdnrScan.jdnr}, jdnrScan.jdline = {jdnrScan.jdline} ");
            }

            //

            //wyznaczenie weightModel 
            //1. sprawdzenie czy nie ma go w bazie danych
            global.weightModelDataType weightModelFromDB = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
            Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: weightModelFromDB.id = {weightModelFromDB.id} ");
            if (weightModelFromDB.id > -1)
            {
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: weightModelFromDB OK");
                //pobrano model z DB, przepisać do 
                global.weightModel.jdnr = weightModelFromDB.jdnr;
                global.weightModel.jdline = weightModelFromDB.jdline;
                global.weightModel.pack_type = weightModelFromDB.pack_type;
                global.weightModel.id = weightModelFromDB.id;
                global.weightModel.weight_model = weightModelFromDB.weight_model;
                global.weightModel.weight_tare = weightModelFromDB.weight_tare;
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: pobierałem weightModel z DB");
            }
            else
            {
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: pobieram weightModel z formularza ");
                //pobrać model z wagi:
                global.weightModelDataType model = getWeightModelFromForm(jdnrScan, Convert.ToInt32(!Program.Settings.Data.stationType));       //
                                                                                                                                                
                dispachDataAcces.insertWeightModel(model);
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalizeMain: pobrałem weightModel z formularza ");
                global.weightModel = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
                weightModelVisuUpdate(global.weightModel);
            }

            posterAct = new poster();
            posterAct.jdline = jdnrScan.jdline;
            posterAct.jdnr = jdnrScan.jdnr;

            findDispach(posterAct);                         //pobiera adres do zmiennej global.actAddressAndItem i rezerwuje go w bazie
            findAdresses(posterAct.jdline, posterAct.jdnr); //show all aviable dispach            
            getJobsData(posterAct);
            weightModelVisuUpdate(global.weightModel);

        }

        public void markActAdressAndItem(string jdnr, string jdline)
        {
            foreach (FinishingDispatchAddressAndItem item in global.subpackList)
            {
                //Program.autojetFinishingMain.Hm.Info($"isPosterOnSubpackList(), cheking: jdline={item.item.jdline}, jdnr={item.item.jdnr}");
                int itemJdline = int.Parse(item.item.jdline);
                int itemJdnr = int.Parse(item.item.jdnr);

                int cmpJdline = int.Parse(jdline);
                int cmpJdnr = int.Parse(jdnr);

                //if (item.item.jdline.Equals(jdline) && item.item.jdnr.Equals(jdnr))
                if ((cmpJdline == itemJdline) && (cmpJdnr == itemJdnr) && (!item.item.completation_done))
                {
                    findDispachForZPL(global.subpackList, jdline, jdnr);
                    //lDispSubpackAddInfo.Text = $"Zważ {global.actAddressAndItem.item.qty_to_pack} szt. plakatu {global.actAddressAndItem.item.jdnr} {global.actAddressAndItem.item.jdline}";
                    tableLayoutPanel7.RowStyles[2].Height = 70;
                }
            }
        }

        private void showNoDispachError()
        {
			//DOPISANE ŁP 2018-09-05
			Program.autojetFinishingMain.Activate();
			//brak wysyłek dla zeskanowanego kodu, pokazać komunikat i przerwać 
			Program.autojetFinishingMain.Hm.Info($"showNoDispachError(), ");

            noDispachErrorForm noDispachError = new noDispachErrorForm();
            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= scannerSerialRecieved;
                this.scannerSerialPorts[i].dataRecieved += noDispachError.scannerSerialRecieved;
            }

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            noDispachError.ShowDialog();

            for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
            {
                this.scannerSerialPorts[i].dataRecieved -= noDispachError.scannerSerialRecieved;
                if (this.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                    this.scannerSerialPorts[i].dataRecieved += scannerSerialRecieved;
            }

            return;
        }

        public void weightModelVisuUpdate(global.weightModelDataType model)
        {
            //lWeightNet.Text =
            lWeightModel.Text = "Wzorzec: " + model.weight_model.ToString() + " [g]";
            lWeightTare.Text = "Tara: " + model.weight_tare.ToString() + " [g]";
        }

        /// <summary>
        /// Podział stringa wejściowego ze skanera na jdnr i jdline
        /// </summary>
        /// <param name="recStr"></param>
        /// <returns></returns>
        public global.weightBarcodeScan jdnrScanAnalize(string recStr)
        {
            Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalize: recStr = {recStr} ");
            global.weightBarcodeScan retVal = new global.weightBarcodeScan();
            retVal.modelType = -1;

            try
            {
                string[] strArray = recStr.Split('.');

                if ((strArray[0].Length >= 5) && (strArray[0].Length <= 6) && (strArray[1].Length >= 1) && (strArray[1].Length <= 3))
                {
                    retVal.scanData = recStr;
                    retVal.jdnr = strArray[0];
                    retVal.jdline = strArray[1];
                    retVal.modelType = 0;
                }
                else
                {
                    //zeskanowano kod kreskowy z brytu
                    /*
                    SELECT jdnr,jdline FROM Printtemp where id=string_from_scanner
                    UNION ALL
                    SELECT jdnr,jdline FROM Archive where id=string_from_scanner    
                    */                    

                    string query = $"SELECT jdnr, jdline FROM Printtemp where id = {recStr} ";
                    query += $"UNION ALL ";
                    query += $"SELECT jdnr, jdline FROM Archive where id = {recStr} ";

                    DataTable dTabel = SQLConnection.GetData(query);
                    if (dTabel.Rows.Count > 0)
                    {
                        retVal.jdnr = dTabel.Rows[0]["jdnr"].ToString();
                        retVal.jdline = dTabel.Rows[0]["jdline"].ToString();
                        retVal.modelType = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalize: error 0x95");
            }

            return retVal;
        }

        /// <summary>
        /// Podział stringa wejściowego ze skanera na jdnr i jdline
        /// </summary>
        /// <param name="recStr"></param>
        /// <returns></returns>
        public global.weightBarcodeScan partialLabelScanAnalize(string recStr)
        {
            Program.autojetFinishingMain.Hm.Info($"partialLabelScanAnalize: recStr = {recStr} ");
            global.weightBarcodeScan retVal = new global.weightBarcodeScan();
            retVal.modelType = -1;

            try
            {
                /* mail p. Łukasza z 2018-06-01
                select jdnr,jdline FROM dispatch_item_table WHERE barcode_data='kod z czytnika bez litery'
                UNION ALL
                select jdnr,jdline FROM dispatch_item_table_archive WHERE barcode_data='kod z czytnika bez litery' 
                 */

                //Zmiana ŁP 2018-12-20 długość na sztywno zle działa dla dłuższych etykiet

                int lastIndexOfK = recStr.LastIndexOf("K");
                int lastIndexOfC = recStr.LastIndexOf("C");

                if (lastIndexOfC > 0 || lastIndexOfK > 0)
                {
                    recStr = recStr.Substring(0, recStr.Length - 1);
                }
                string query = $"select jdnr,jdline FROM dispatch_item_table WHERE barcode_data='{recStr}'  ";
                query += $"UNION ALL ";
                query += $"select jdnr,jdline FROM dispatch_item_table_archive WHERE barcode_data='{recStr}' ";

                DataTable dTabel = SQLConnection.GetData(query);
                if (dTabel.Rows.Count > 0)
                {
                    retVal.jdnr = dTabel.Rows[0]["jdnr"].ToString();
                    retVal.jdline = dTabel.Rows[0]["jdline"].ToString();
                    retVal.modelType = 0;
                }

            }
            catch (Exception ex)
            {
                Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalize: error 0x95");
            }

            Program.autojetFinishingMain.Hm.Info($"jdnrScanAnalize: retVal.modelType = {retVal.modelType}");
            return retVal;
        }

        /// <summary>
        /// testy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newmodelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            jdnrScanAnalizeMain("12349.1");
        }

        /// <summary>
        /// gets new weightModel from scale
        /// </summary>
        private void getNewWeightModel()
        {
            global.weightBarcodeScan jdnrScan = new global.weightBarcodeScan();
            jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
            jdnrScan.jdline = global.actAddressAndItem.item.jdline;
            jdnrScan.pack_type = global.actAddressAndItem.address.pack_type;
            jdnrScan.tileName = global.actAddressAndItem.item.tileName;
            jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
            jdnrScan.modelType = dispachDataAcces.GetModelModeFromAdresAndItem(global.actAddressAndItem);
            jdnrScan.model_scan_count = 1;// global.actAddressAndItem.item.qty_to_pack;

            Program.autojetFinishingMain.Hm.Info($"getWeightModel: jdnrScan.jdnr = {jdnrScan.jdnr}, jdnrScan.jdline = {jdnrScan.jdline} ");
            
            global.weightModelDataType model = getWeightModelFromForm(jdnrScan, Convert.ToInt32(!Program.Settings.Data.stationType));       //

            dispachDataAcces.insertWeightModel(model);
            Program.autojetFinishingMain.Hm.Info($"getWeightModel: pobrałem weightModel z formularza ");
            global.weightModel = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
            weightModelVisuUpdate(global.weightModel);
        }

        public global.weightModelDataType getWeightModelFromForm(global.weightBarcodeScan scan, int modelMode)
        {
            global.weightModelDataType retVal = new global.weightModelDataType();

            weightModelScan weightModelForm = new weightModelScan();

            Program.autojetFinishingMain.Hm.Info($"getWeightModelFromForm: part 1 ");

            this.weightSerialPorts.dataRecieved -= weightSerialRecieved;
            this.weightSerialPorts.dataRecieved += weightModelForm.weightSerialRecieved;

            Program.autojetFinishingMain.Hm.Info($"getWeightModelFromForm: part 2 ");
            weightModelForm.modelMode = modelMode;      //0 = model weight + tare weight, 1 = model, 2 = tare
            weightModelForm.modelType = scan.modelType;      //
            weightModelForm.weightModel.jdnr = scan.jdnr;
            weightModelForm.weightModel.jdline = scan.jdline;
            weightModelForm.weightModel.pack_type = scan.pack_type;
            weightModelForm.weightModel.tileName = scan.tileName;
            weightModelForm.model_scan_count = scan.model_scan_count;
            Program.autojetFinishingMain.Hm.Info($"getWeightModelFromForm: part 3 ");

            plcWriteHoldScan(true);

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

        
            weightModelForm.ShowDialog();
            plcWriteHoldScan(false);

            Program.autojetFinishingMain.Hm.Info($"getWeightModelFromForm: part 4 ");

            retVal = weightModelForm.weightModel;

            Program.autojetFinishingMain.Hm.Info($"getWeightModelFromForm: part 5 ");
            this.weightSerialPorts.dataRecieved -= weightModelForm.weightSerialRecieved;
            if (this.weightSerialPorts.eventCount() == 0)     //attach event only if there is no other event attached
                this.weightSerialPorts.dataRecieved += weightSerialRecieved;

            Program.autojetFinishingMain.Hm.Info($"getWeightModelFromForm: part 6, weightModel.weight_model = {retVal.weight_model}, weightModel.weight_tare = {retVal.weight_tare}");
            return retVal;
        }

        private void checkmodelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //weightAnalyzeMain(int.Parse(tbWeight.Text.ToString()));
            }
            catch
            {
            }
        }

        private void getnumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (weightSerialPorts != null) { }
                    //tbWeight.Text = weightSerialPorts.eventCount().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void plusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.weightSerialPorts.dataRecieved -= weightSerialRecieved;
        }

        private void tabControlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlMain.SelectedTab == tpReports)
                showReportTab();
        }

        private void bReportMake11_Click(object sender, EventArgs e)
        {
            generateReport();
        }

        private void bReportMake21_Click(object sender, EventArgs e)
        {
            generateReport();
        }

        private void bReportMake31_Click(object sender, EventArgs e)
        {
            generateReport();
        }

    }
}
