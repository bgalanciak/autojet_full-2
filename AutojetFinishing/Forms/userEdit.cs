﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class userEdit : Form
    {
        public userEdit()
        {
            InitializeComponent();
        }

        const bool NEW_UESER=false;
        const bool MOD_UESER=true;

        public user actUserData = new user();
        string password2;
        public bool mode=NEW_UESER;

        private void userEdit_Load(object sender, EventArgs e)
        {
            if (mode==MOD_UESER)
            {
                this.Text = "Edycja użytkownika";
                lInfoTxt.Text = "Edytuj użytkownika";
                tbFName.Text = actUserData.f_name;
                tbLName.Text = actUserData.l_name;
                tbPassword.Text = actUserData.password;
                tbPassword2.Text = actUserData.password;
                tbQCNum.Text = actUserData.qc_num.ToString();
                cbIsAdmin.Checked = actUserData.is_admin;
                cbIsOperator.Checked = actUserData.is_operator;
                cbIsActive.Checked = actUserData.is_active;
            }
            else
            {
                this.Text = "Dodawanie użytkownika";
                lInfoTxt.Text = "Dodaj użytkownika";
            }
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            if ((tbFName.Text.Length > 0) && (tbPassword2.Text == tbPassword.Text) && (!string.IsNullOrEmpty(tbPassword.Text)) && (tbQCNum.Text.Length > 0))
            {
                
                actUserData.f_name = tbFName.Text;
                actUserData.l_name = tbLName.Text;
                actUserData.password = tbPassword.Text;

                actUserData.qc_num = int.Parse(tbQCNum.Text);
                actUserData.is_admin = cbIsAdmin.Checked;
                actUserData.is_operator = cbIsOperator.Checked;
                actUserData.is_active = cbIsActive.Checked;

                user u = global.findUserByHash(actUserData.userLogHash, global.userList);

                if (u.id != -1)
                {
                    MessageBox.Show("Błąd: dane istnieją już w bazie.\nZmień imię, nazwisko lub hasło", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                MessageBox.Show("Użytkownik poprawnie zapisany\nHash skopiowany do schowka systemowego", "Zapis poprawny", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clipboard.SetText(actUserData.userLogHash);

                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show("Błąd: nie wypełniono poprawnie wyszystkich danych.\nDane niezbędne to: Imię, Hasło, Numer QC", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tbFPassword2_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbFPassword2_Leave(object sender, EventArgs e)
        {
            if ((tbPassword2.Text != tbPassword.Text) || (string.IsNullOrEmpty(tbPassword.Text)))
            {
                tbPassword2.BackColor = Color.MediumVioletRed;
                bSave.Enabled = false;
            }
            else
            {
                tbPassword2.BackColor = SystemColors.Window;
                bSave.Enabled = true;
            }

        }

        private void tbQCNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            //allow only numbers
            //http://stackoverflow.com/questions/463299/how-do-i-make-a-textbox-that-only-accepts-numbers
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if ((sender as TextBox).Text.Length >= 2)
            {
                e.Handled = true;
            }
            
            // only allow one decimal point
            //if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
            //    e.Handled = true;
            //}
        }

        private void tbFName_TextChanged(object sender, EventArgs e)
        {
            actUserData.userLogHash = calcLogHash(tbFName.Text, tbLName.Text, tbPassword.Text);
            tbUserLogHash.Text = actUserData.userLogHash;
        }

        
        private void tbLName_TextChanged(object sender, EventArgs e)
        {
            actUserData.userLogHash = calcLogHash(tbFName.Text, tbLName.Text, tbPassword.Text);
            tbUserLogHash.Text = actUserData.userLogHash;
        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            actUserData.userLogHash = calcLogHash(tbFName.Text, tbLName.Text, tbPassword.Text);
            tbUserLogHash.Text = actUserData.userLogHash;
        }

        public string calcLogHash(string fName, string lName, string password)
        {
            return utilities.CalculateMD5Hash(fName + lName + password).Substring(0, 20);
        }

    }
}
