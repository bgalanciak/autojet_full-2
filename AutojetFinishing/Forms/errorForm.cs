﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class errorForm : Form
    {
        public errorForm()
        {
            InitializeComponent();
        }

        private void errorForm_Load(object sender, EventArgs e)
        {
            tPlcRead.Enabled = true; 
        }

        //disable form from resize and move
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void bAccept_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tPlcRead_Tick(object sender, EventArgs e)
        {
            if (Program.autojetFinishingMain.plcRead() == true)
            {
                label1.Text = "end";
                this.Close();
            }
        }

        //odczyt danych ze skanera kodów kreskowych
        public void scannerSerialRecieved(string recStr)
        {
            
            lErrorTxt.Text = recStr;
            if (recStr.Length == 20)
            {
                errorSourceAnalyze(recStr);
            }
            else
            {
            }
        }

        private void errorSourceAnalyze(string recStr)
        {
            lErrorTxt.Text = recStr;
            if (recStr.Equals("XC000000000000000001"))
            {
                this.Close();
            }
            else
            {
                //lErrorTxt.Text = recStr;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lErrorTxt.Text = "buton test";
        }
    }
}
