﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class weightRegistrationForm : Form
    {

        public global.weightModelDataType weightModel = new global.weightModelDataType();
        
        //disable form from resize and move
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public weightRegistrationForm()
        {
            InitializeComponent();
        }

        public List<String> weightMotives = new List<string>();

        private void weightRegistrationForm_Load(object sender, EventArgs e)
        {
            label1.Text = "Zważ paczkę składającą się z: " + Environment.NewLine;
            foreach (string s in weightMotives)
            {
                label1.Text += s;// + Environment.NewLine;
            }
            if (global.weightModel.weight_base>0)
            {
                label3.Text = global.weightModel.weight_base.ToString() + " [g]";
            }
        }

        public void weightSerialRecieved(string recStr)
        {
            //Program.autojetFinishingMain.Hm.Info("weightSerialRecieved: recStr={0} ", recStr);
            commandMain(weightAnalyze(recStr));
        }

        public int weightAnalyze(string scanStr)
        {
            int weightRead = -1;
            try
            {
                //string gram = scanStr.Substring(13, 7);    //7
                //rozwiązanie p. Łukasza 2018-04-13
                string gram = scanStr.Substring(Program.Settings.Data.scaleFrom, Program.Settings.Data.scaleHowMany);
                gram = gram.Trim();

                if (Program.Settings.Data.scaleMass.Equals("kg"))
                {
                    double weightTemp = 0;
                    gram = gram.Replace(".", ",");
                    weightTemp = (double.Parse(gram) * 1000);
                    gram = weightTemp.ToString();
                }

                int weightGr = int.Parse(gram);
                weightRead = weightGr;
                Program.autojetFinishingMain.Hm.Info($"weightAnalyze: weightRead = {weightGr} ");
            }
            catch (Exception e)
            {
                string gram = scanStr.Substring(Program.Settings.Data.scaleFrom, Program.Settings.Data.scaleHowMany);    //7
                Program.autojetFinishingMain.Hm.Info($"weightAnalyze: gramStr = {gram} ");
            }

            Program.autojetFinishingMain.Hm.Info($"weightAnalyze: retVal = {weightRead} ");
            return weightRead;
        }

        public void commandMain(int weight)
        {
            weightAnalyzeMain(weight);
        }

        bool weightErrorShow = false;

        /// <summary>
        /// Analiza czy dane z wagi są poprawne
        /// </summary>
        /// <param name="weight"></param>
        public void weightAnalyzeMain(int weight)
        {
            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            if (weightErrorShow)
                return;

            //wyświetlenie wagi na formularzu
            lInfo.Text = "Brutto: " + weight.ToString() + " [g]";
           
            int weightExpected = global.weightModel.weight_tare + (global.actAddressAndItem.qtyToPack * global.weightModel.weight_model) + global.weightModel.weight_base;
            //usuniete na prosbe p. Łukasza 2018-03-01
            //int weightMin = global.weightModel.weight_tare + (global.actAddressAndItem.qtyToPack * global.weightModel.weight_model) - (global.weightModel.weight_model / 10);
            //int weightMax = global.weightModel.weight_tare + (global.actAddressAndItem.qtyToPack * global.weightModel.weight_model) + (25 * global.weightModel.weight_model) /100;

            //dodane na prosbe p. Łukasza 2018-03-01
            int weightMin = weightExpected - Program.Settings.Data.weightDevMin;
            int weightMax = weightExpected + Program.Settings.Data.weightDevMax;

            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, weightAnalyzeMain: weight = {weight.ToString()}, weightMin = {weightMin.ToString()},  weightMax = {weightMax.ToString()}, weight_base = {global.weightModel.weight_base}");

            if ((weight >= weightMin) && (weight <= weightMax))
            {
                acceptWeight(weight);
            }
            else if (weight >= weightMax)
            {
                //zbyt duza waga, pokaż pytanie czy ją zaakceptować.
                overWeightErrorForm overWeightError = new overWeightErrorForm();
                overWeightError.line1 = $"Zbyt duża waga – {weight} [g]. Waga powinna być w zakresie {weightMin} [g] – {weightMax} [g].";
                overWeightError.line2 = $"Jeżeli celowo coś dołożyłeś zatwierdź to skanując kod AKCEPTUJ.";
                overWeightError.line3 = $"Jeżeli jest to błąd i chcesz ponownie spakować paczkę zeskanuj ZAMKNIĘCIE KOMUNIKATU.";

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved -= this.scannerSerialRecieved;
                    Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved += overWeightError.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                overWeightError.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved -= overWeightError.scannerSerialRecieved;
                    if (Program.autojetFinishingMain.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved += this.scannerSerialRecieved;
                }

                if (overWeightError.saveVal)
                {
                    acceptWeight(weight);
                    Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, weightAnalyzeMain: acceptWeight()");
                }
                else
                    return;
            }
            else
            {
                //zła waga, pokaż komunikat
                weightErrorShow = true;

                Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, weightAnalyzeMain: weight wrong, show form");
                weightWrong weightWrongForm = new weightWrong();
                weightWrongForm.msgStr = $"Waga nieprawidłowa - {weight.ToString()} [g] Waga powinna być w zakresie {weightMin} [g] – {weightMax} [g], Sprawdź poprawność pakowania lub dokonaj ponownego wzorcowania";

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved -= this.scannerSerialRecieved;
                    Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved += weightWrongForm.scannerSerialRecieved;
                }

                //DOPISANE ŁP 2018-09-05
                Program.autojetFinishingMain.Activate();

                weightWrongForm.ShowDialog();

                for (int i = 0; i < Program.Settings.Data.scanerCount; i++)
                {
                    Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved -= weightWrongForm.scannerSerialRecieved;
                    if (Program.autojetFinishingMain.scannerSerialPorts[i].eventCount() == 0)     //attach event only if there is no other event attached
                        Program.autojetFinishingMain.scannerSerialPorts[i].dataRecieved += this.scannerSerialRecieved;
                }
                weightErrorShow = false;
            }
        }

        public void acceptWeight(int weight)
        {
            global.actAddressAndItem.item.weight_act = weight;
            global.actAddressAndItem.item.weight_model = global.weightModel.id;

            this.Close();
        }

        public void scannerSerialRecieved(string recStr)
        {
            if (recStr.Equals("XW000000000000000001"))
            {
                //get weight model 
                Program.autojetFinishingMain.Hm.Info("weightRegistrationForm, commandAnalyze(): starting getWeightModel()");
                if (global.actAddressAndItem.item_reference_id != null)
                    getNewWeightModel();
            }
        }

        /// <summary>
        /// gets new weightModel from scale
        /// </summary>
        private void getNewWeightModel()
        {
            global.weightBarcodeScan jdnrScan = new global.weightBarcodeScan();
            jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
            jdnrScan.jdline = global.actAddressAndItem.item.jdline;
            jdnrScan.pack_type = global.actAddressAndItem.address.pack_type;
            jdnrScan.tileName = global.actAddressAndItem.item.tileName;
            jdnrScan.jdnr = global.actAddressAndItem.item.jdnr;
            jdnrScan.modelType = dispachDataAcces.GetModelModeFromAdresAndItem(global.actAddressAndItem);
            jdnrScan.model_scan_count = 1;// global.actAddressAndItem.item.qty_to_pack;

            Program.autojetFinishingMain.Hm.Info($"getWeightModel: jdnrScan.jdnr = {jdnrScan.jdnr}, jdnrScan.jdline = {jdnrScan.jdline} ");
            //

            global.weightModelDataType model = getWeightModelFromForm(jdnrScan, 1);       //
            //if ((model.weight_model > 50) && (model.weight_tare > 50))
            //{
                //global.weightModel = model;
                dispachDataAcces.insertWeightModel(model);
                Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModel: pobrałem weightModel z formularza ");
                global.weightModel = dispachDataAcces.GetWeightModelFromDB(jdnrScan);
                //weightModelVisuUpdate(global.weightModel);
            //}
            //else
            //{ }
        }

        public global.weightModelDataType getWeightModelFromForm(global.weightBarcodeScan scan, int modelMode)
        {
            global.weightModelDataType retVal = new global.weightModelDataType();

            weightModelScan weightModelForm = new weightModelScan();

            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModelFromForm: part 1 ");

            Program.autojetFinishingMain.weightSerialPorts.dataRecieved -= this.weightSerialRecieved;
            Program.autojetFinishingMain.weightSerialPorts.dataRecieved += weightModelForm.weightSerialRecieved;

            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModelFromForm: part 2 ");
            weightModelForm.modelMode = modelMode;      //0 = model weight + tare weight, 1 = model, 2 = tare
            weightModelForm.modelType = scan.modelType;      //
            weightModelForm.weightModel.jdnr = scan.jdnr;
            weightModelForm.weightModel.jdline = scan.jdline;
            weightModelForm.weightModel.pack_type = scan.pack_type;
            weightModelForm.weightModel.tileName = scan.tileName;
            weightModelForm.model_scan_count = scan.model_scan_count;
            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModelFromForm: part 3 ");
            Program.autojetFinishingMain.plcWriteHoldScan(true);

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();


            weightModelForm.ShowDialog();
            Program.autojetFinishingMain.plcWriteHoldScan(false);
            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModelFromForm: part 4 ");

            retVal = weightModelForm.weightModel;

            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModelFromForm: part 5 ");
            Program.autojetFinishingMain.weightSerialPorts.dataRecieved -= weightModelForm.weightSerialRecieved;
            if (Program.autojetFinishingMain.weightSerialPorts.eventCount() == 0)     //attach event only if there is no other event attached
                Program.autojetFinishingMain.weightSerialPorts.dataRecieved += this.weightSerialRecieved;

            Program.autojetFinishingMain.Hm.Info($"weightRegistrationForm, getWeightModelFromForm: part 6, weightModel.weight_model = {retVal.weight_model}, weightModel.weight_tare = {retVal.weight_tare}");
            return retVal;
        }
    }
}
