﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class overWeightErrorForm : Form
    {
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        public string line1, line2, line3;
        public bool saveVal = false;

        public overWeightErrorForm()
        {
            InitializeComponent();
        }

        //odczyt danych ze skanera kodów kreskowych
        public void scannerSerialRecieved(string recStr)
        {

            if (recStr.Length == 20)
            {
                errorSourceAnalyze(recStr);
            }
            else
            {
            }
        }

        private void errorSourceAnalyze(string recStr)
        {
            if (recStr.Equals("XC000000000000000001"))
            {
                saveVal = false;
                this.Close();
            }
            else if (recStr.Equals("XE000000000000000007"))
            {
                saveVal = true;
                this.Close();
            }
        }

        private void overWeightErrorForm_Load(object sender, EventArgs e)
        {
            label1.Text = line1;
            label2.Text = line2;
            label3.Text = line3;
        }
    }
}
