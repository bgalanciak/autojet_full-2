﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using System.ComponentModel;

namespace DispachWarehouse
{
    public class zebraPrinter
    {
        private int stationId;

        public zebraPrinter(int id)
        {
            stationId = id;
        }

        public void SendToPrinterUnitNotSimple(storageUnit unit) //Maks. << Chester
        {
            string label = "";

            label += "^XA";
            label += "^CF0,60,50";
            label += "^FO50,50^FDMIEJSCE ODKLADCZE^FS";
            label += "^FO50,100^GB700,1,3^FS";
            label += "^CF0,40";
            label += "^FO50,150^FDKurier:^FS";
            label += "^FO50,200^FDMiejsce:^FS";
            label += "^CF0,20";
            // label += "^FO600,1190^FDData:^FS";
            label += "^CF0,40";
            label += "^FO50,250^FDData wysyłki:^FS"; //Courier manager data
            //label += "^FO50,350^FDDataPM:^FS";
            label += "^FO30,840^FDOdbiorca:^FS";
            label += $"^FO250,150^FD{unit.Courier_type}^FS";
            label += $"^FO250,200^FDU{unit.Id.ToString("D10")}^FS";
            label += "^CF0,90";
            label += $"^FO250,315^FD{unit.Dispatch_date_CM.Day.ToString() + "." + unit.Dispatch_date_CM.Month.ToString() + "." + unit.Dispatch_date_CM.Year}^FS"; //courier manager data

            label += "^CF0,52";
            label += $"^FO30,900^FD{unit.dispachShowName}^FS";
            label += "^CF0,90";
            label += "^BY5,2,270";
            label += $"^FO125,420^BC,170,Y,N,N,A^FDU{unit.Id.ToString("D10")}^FS";

            label += "^CF0,20";
            label += $"^FO648,1190^FD{unit.Create_time_stamp.ToString()}^FS"; // data wydruku etykiety
            label += "^CF0,40";
            label += "^XZ";


            label = RemoveDiacritics(label);

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();

            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, label);
        }
        public void SendToPrinterUnitSimple(storageUnit unit)
        {
            string label = "";

            label += "^XA";
            label += "^CF0,60,50";
            label += "^FO50,50^FDMIEJSCE ODKLADCZE^FS";
            label += "^FO50,100^GB700,1,3^FS";
            label += "^CF0,40";
            label += "^FO50,150^FDKurier:^FS";
            label += "^FO50,200^FDMiejsce:^FS";
            label += "^FO50,250^FDData wysylki:^FS";
            //label += "^FO50,330^FDData wysylki:^FS";
            label += $"^FO250,150^FD{unit.Courier_type}^FS";
            label += $"^FO250,200^FDU{unit.Id.ToString("D10")}^FS";
            label += "^CF0,90";
            label += $"^FO250,310^FD{unit.Dispatch_date_CM.Day.ToString() + "." + unit.Dispatch_date_CM.Month.ToString() + "." + unit.Dispatch_date_CM.Year}^FS";
            label += "^CF0,38";
            label += "^BY5,2,280";
            label += $"^FO125,440^BC,170,Y,N,N,A^FDU{unit.Id.ToString("D10")}^FS";
            label += "^CF0,20";
            label += $"^FO648,1190^FD{unit.Create_time_stamp.ToString()}^FS"; // data wydruku etykiety
            label += "^CF0,40";
            label += "^XZ";

            label = RemoveDiacritics(label);

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();

            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, label);
        }

        public string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            stringBuilder = stringBuilder.Replace("Ł", "L");
            stringBuilder = stringBuilder.Replace("ł", "l");

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public class RawPrinterHelper
        {
            // Structure and API declarions:
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
            public class DOCINFOA
            {
                [MarshalAs(UnmanagedType.LPStr)]
                public string pDocName;
                [MarshalAs(UnmanagedType.LPStr)]
                public string pOutputFile;
                [MarshalAs(UnmanagedType.LPStr)]
                public string pDataType;
            }
            [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

            [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool ClosePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

            [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool EndDocPrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool StartPagePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool EndPagePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

            // SendBytesToPrinter()
            // When the function is given a printer name and an unmanaged array
            // of bytes, the function sends those bytes to the print queue.
            // Returns true on success, false on failure.
            public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
            {
                Int32 dwError = 0, dwWritten = 0;
                IntPtr hPrinter = new IntPtr(0);
                DOCINFOA di = new DOCINFOA();
                bool bSuccess = false; // Assume failure unless you specifically succeed.

                di.pDocName = "My C#.NET RAW Document";
                di.pDataType = "RAW";

                // Open the printer.
                if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
                {
                    // Start a document.
                    if (StartDocPrinter(hPrinter, 1, di))
                    {
                        // Start a page.
                        if (StartPagePrinter(hPrinter))
                        {
                            // Write your bytes.
                            bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                            EndPagePrinter(hPrinter);
                        }
                        EndDocPrinter(hPrinter);
                    }
                    ClosePrinter(hPrinter);
                }
                // If you did not succeed, GetLastError may give more information
                // about why not.
                if (bSuccess == false)
                {
                    dwError = Marshal.GetLastWin32Error();
                }
                return bSuccess;
            }

            public static bool SendFileToPrinter(string szPrinterName, string szFileName)
            {
                // Open the file.
                FileStream fs = new FileStream(szFileName, FileMode.Open);
                // Create a BinaryReader on the file.
                BinaryReader br = new BinaryReader(fs);
                // Dim an array of bytes big enough to hold the file's contents.
                Byte[] bytes = new Byte[fs.Length];
                bool bSuccess = false;
                // Your unmanaged pointer.
                IntPtr pUnmanagedBytes = new IntPtr(0);
                int nLength;

                nLength = Convert.ToInt32(fs.Length);
                // Read the contents of the file into the array.
                bytes = br.ReadBytes(nLength);
                // Allocate some unmanaged memory for those bytes.
                pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
                // Copy the managed byte array into the unmanaged array.
                Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
                // Send the unmanaged bytes to the printer.
                bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
                // Free the unmanaged memory that you allocated earlier.
                Marshal.FreeCoTaskMem(pUnmanagedBytes);
                return bSuccess;
            }

            public static bool SendStringToPrinter(string szPrinterName, string szString)
            {
                IntPtr pBytes;
                Int32 dwCount;
                // How many characters are in the string?
                dwCount = szString.Length;
                // Assume that the printer is expecting ANSI text, and then convert
                // the string to ANSI text.
                pBytes = Marshal.StringToCoTaskMemAnsi(szString);
                // Send the converted ANSI string to the printer.
                SendBytesToPrinter(szPrinterName, pBytes, dwCount);
                Marshal.FreeCoTaskMem(pBytes);
                return true;
            }
        }
    }
}
