﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWarehouse
{
    public class dispatchDelivery
    {
        private int id;
        private int storage_unit_id;
        private string reference_id;
        private int ref_from;
        private int ref_to;
        private DateTime allocation_time_stamp;
        private int allocation_operator_id;
        private bool allocation_obsolete;
        private string dispatch_waybill;
        private bool dispatch_released;
        private DateTime? release_time_stamp;
        private int release_operator_id;

        public int Id { get => id; set => id = value; }
        public int Storage_unit_id { get => storage_unit_id; set => storage_unit_id = value; }
        public string Reference_id { get => reference_id; set => reference_id = value; }
        public int Ref_from { get => ref_from; set => ref_from = value; }
        public int Ref_to { get => ref_to; set => ref_to = value; }
        public DateTime Allocation_time_stamp { get => allocation_time_stamp; set => allocation_time_stamp = value; }
        public int Allocation_operator_id { get => allocation_operator_id; set => allocation_operator_id = value; }
        public bool Allocation_obsolete { get => allocation_obsolete; set => allocation_obsolete = value; }
        public string Dispatch_waybill { get => dispatch_waybill; set => dispatch_waybill = value; }
        public bool Dispatch_released { get => dispatch_released; set => dispatch_released = value; }
        public DateTime? Release_time_stamp { get => release_time_stamp; set => release_time_stamp = value; }
        public int Release_operator_id { get => release_operator_id; set => release_operator_id = value; }
        /*        
        id 
        storage_unit_id 
        reference_id 
        ref_from 
        ref_to 
        allocation_time_stamp 
        allocation_operator_id 
        allocation_obsolete 
        dispatch_waybill 
        dispatch_released 
        release_time_stamp 
        release_operator_id 
        */
    }
}
