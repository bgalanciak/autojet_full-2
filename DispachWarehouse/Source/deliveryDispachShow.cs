﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWarehouse
{
    public class dispatchDeliveryShow
    {
        /*
            string selectQuery = $"SELECT";
            selectQuery += $" CONCAT(di.jdnr,'.',di.jdline) AS 'KP_LKP',";
            selectQuery += $" CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END AS 'Odbiorca',";
            selectQuery += $" di.qty_packed AS 'Spak',";
            selectQuery += $" da.pack_type AS 'Typ',";
            selectQuery += $" IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectQuery += $" IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectQuery += $" courier_type AS 'Kurier',";
            selectQuery += $" di.tilename AS 'Bryty',";
            selectQuery += $" IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') AS 'D_Wys'";
            selectQuery += $" FROM dispatch_delivery dd";
            selectQuery += $" LEFT JOIN dispatch_address_table da on dd.reference_id=da.reference_id and dd.ref_from=da.ref_from and dd.ref_to=da.ref_to";
            selectQuery += $" LEFT JOIN dispatch_item_table di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectQuery += $" LEFT JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectQuery += $" WHERE dd.dispatch_released=0 AND dd.storage_unit_id={unitId.ToString()}";
            selectQuery += $" ORDER BY CONCAT(di.jdnr,'.',di.jdline),di.jdnr,di.jdline; ";
         */

        public string KP_LKP { get; set; }
        public string Odbiorca { get; set; }
        public string Spak { get; set; }
        public string Typ { get; set; }
        public string D_Pak { get; set; }
        public string D_Alok { get; set; }
        public string Kurier { get; set; }
        public string Bryty { get; set; }
        public string D_Wys { get; set; }

    }
}
