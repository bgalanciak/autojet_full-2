﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace DispachWarehouse
{
    public class SQLConnection
    {  
        private MySqlConnection connection;
        private int id;
        private string server;
        private string userID;
        private string password;
        private string database;

        public MySqlConnection Connection { get => connection; set => connection = value; }
        public MySqlConnection Connection2 { get => connection; set => connection = value; }
        public int Id { get => id; set => id = value; }
        public string Server { get => server; set => server = value; }
        public string UserID { get => userID; set => userID = value; }
        public string Password { get => password; set => password = value; }
        public string Database { get => database; set => database = value; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="server"></param>
        /// <param name="userID"></param>
        /// <param name="password"></param>
        /// <param name="database"></param>
        public SQLConnection(int id, string server, string userID, string password, string database)
        {
            //connection = new SqlConnection();
            Connection = new MySqlConnection();
            Connection2 = new MySqlConnection();
            this.Id = id;
            this.Server = server;
            this.UserID = userID;
            this.Password = password;
            this.Database = database;
        }

        public bool Connect(StateChangeEventHandler stateChangeHandler)
        {
            if (Connection.State != System.Data.ConnectionState.Open)
            {
                // Register the event change handler, make sure that only once.
                Connection.StateChange -= new StateChangeEventHandler(stateChangeHandler);
                Connection.StateChange += new StateChangeEventHandler(stateChangeHandler);

                // Build a string to connect to database.
                //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();

                builder.Server = this.Server;
                builder.UserID = this.UserID;
                builder.Password = this.Password;
                builder.Database = this.Database;
                builder.CharacterSet = "utf8";

                string connString = builder.ToString(); //"server=127.0.0.1;user id=root;password=root;database=tjdata;charset=utf8";//builder.ToString();
                Connection.ConnectionString = connString;
                Connection2.ConnectionString = connString;

                try
                {
                    //connection.com
                    Connection.Open();
                    Connection2.Open();
                }
                catch (InvalidOperationException)
                {

                    //HealthMonitor.ErrorDlg("dlg_db_connection_failed_invalid_op_msg");

                    return (false);
                }
                catch (SqlException)
                {
                    //HealthMonitor.ErrorDlg("dlg_db_connection_failed_sql_error_msg");

                    return (false);
                }
                catch
                {
                    //HealthMonitor.ErrorDlg("dlg_db_connection_failed_msg");

                    return (false);
                }
            }

            return (true);
        }

        public void Disconnect()
        {
            Connection.Close();
            Connection.Dispose();
        }

        public bool IsActive()
        {
            try
            {
                MySqlCommand SQLCmd = new MySqlCommand("SELECT NOW();", Connection);                
                DateTime date = (DateTime)SQLCmd.ExecuteScalar();
            }
            catch 
            {
            }

            return (Connection.State == System.Data.ConnectionState.Open);
        }

        public void CheckReconnect()
        {
            try
            {
                switch (Connection.State)
                {
                    case ConnectionState.Broken:
                        Connection.Close();
                        Connection.Open();
                        break;
                    case ConnectionState.Closed:
                        Connection.Open();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Program.dispachWarehouseMain.station[this.Id].DebugMsg.Add($"Failed with {ex.Message}");
            }
        }

        /// <summary>
        /// Get database data.
        /// </summary>
        /// <param name="query">SQL query to use.</param>
        /// <returns>Data table holding the results or null on error.</returns>
        public DataTable GetData(string query)
        {
            DataTable retval = null;

            if (IsActive())
            {
                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Getting data using '{query}'");

                MySqlDataAdapter SQLDataAdapter = new MySqlDataAdapter(query, Connection);
                SQLDataAdapter.SelectCommand.CommandTimeout = 300;
                
                retval = new DataTable();
                SQLDataAdapter.Fill(retval);

                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Done: getting data ");
            }
            else
            {
                retval = new DataTable();
            }

            return (retval);
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="query">SQL query to use.</param>
        /// <returns></returns>
        public bool InsertData(string query)
        {
            if (IsActive())
            {
                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Inserting data using '{query}'");

                //insert data
                //SqlCommand SQLCmd = new SqlCommand(query, connection);

                MySqlCommand SQLCmd = new MySqlCommand(query, Connection);
                SQLCmd.ExecuteScalar();

                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Done: inserting data using '{query}'");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update data to database.
        /// </summary>
        /// <param name="query">SQL query to use.</param>
        /// <returns></returns>
        public bool UpdateData(string query)
        {
            if (IsActive())
            {
                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Updating data using '{query}'");

                //update data
                //SqlCommand SQLCmd = new SqlCommand(query, connection);

                MySqlCommand SQLCmd = new MySqlCommand(query, Connection);
                SQLCmd.ExecuteScalar();

                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Done: updating data using '{query}'");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get count from database.
        /// </summary>
        /// <param name="query">The query to use.</param>
        /// <param name="value">Where to store the result.</param>
        /// <returns>True when successful, otherwise false.</returns>
        public bool GetCount(string query, out int value)
        {
            if (IsActive())
            {
                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Getting count data using '{query}'");

                //SqlCommand SQLCmd = new SqlCommand(query, connection);
                
                MySqlCommand SQLCmd = new MySqlCommand(query, Connection);
                value = (int)SQLCmd.ExecuteScalar();

                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Done: getting count data using '{query}'");
                return (true);
            }

            value = 0;
            return (false);
        }

        public DateTime GetDateTime()
        {
            // Provide some reasonable value by default.
            DateTime current = DateTime.Now;

            if (IsActive())
            {
                string query = "SELECT GETDATE();";
                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Getting current time data using '{query}'");

                MySqlCommand SQLCmd = new MySqlCommand(query, Connection);
                current = (DateTime)SQLCmd.ExecuteScalar();

                Program.dispachWarehouseMain.station[this.id].DebugMsg.Add($"Done: getting current time data using '{query}'");
                return (current);
            }

            return (current);
        }

        /// <summary>
        /// Convert int data (NULL is possible) from database to bool.
        /// </summary>
        /// <param name="dr">data from datatbase</param>
        /// <returns>Convertion result</returns>
        public bool dbIntNullToBool(object dr)
        {
            if (dr != DBNull.Value)
            {
                int val = Int32.Parse(dr.ToString());
                if (val == 0)
                    return false;
                else
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Convert bool data (NULL is possible) from database to bool.
        /// </summary>
        /// <param name="dr">data from datatbase</param>
        /// <returns>Convertion result</returns>
        public bool dbBoolNullToBool(object dr)
        {
            if (dr != DBNull.Value)
            {
                bool outVal;
                bool isBool = Boolean.TryParse(dr.ToString(), out outVal);
                //bool val = Convert.ToBoolean(dr);
                ////int val = Int32.Parse(dr.ToString());
                //if (val == false)
                //    return false;
                //else
                //    return true;
                if (isBool)
                {
                    return outVal;
                }
                else
                {
                    int intVal;
                    bool isInt = int.TryParse(dr.ToString(), out intVal);
                    if (isInt)
                    {
                        return Convert.ToBoolean(intVal);
                    }
                    else
                    return Convert.ToBoolean(Convert.ToString(dr));
                }
            }
            return false;
        }

        public DateTime dbDateTimeNullToDateTime2(object dr)
        {
            string input = dr.ToString();
            DateTime date;
            DateTime.TryParse(input, out date);
            return date;
        }
    }
}
