﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Threading;
using System.Data;

namespace DispachWarehouse
{
    public class stationClass
    {
        private int id;
        private RichTextBox debug;
        private int modeAct;
        private HealthMonitor hm;
        private SQLConnection sqlConnection;
        private ToolStripStatusLabel tsslDatabaseStatus;
        private ToolStripStatusLabel tsslScanerStatus;
        private ToolStripStatusLabel tsslUser;
        private user actUser;
        private int scannerID;
        private int cradleId;
        private bool scanerLEDRed;
        private TabControl tcMain;
        private TabPage tpLogin;
        private TabPage tpStorageUnit;
        private TabPage tpMain;
        private TabPage tpIssuing;
        private TabPage tpAllocation;
        private TabPage tpAllocationChange;
        private TabPage tpReport;
        private TabPage tpStoragePrintLabel;
        private ComboBox cbLoginUsers;
        private TextBox tbLoginPass;
        private Label lInfo;
        private System.Timers.Timer tLogInfoOff;
        private dataAcces data;
        private zebraPrinter zebraPrinter;
        private Label lStorageUnitNewCourierType;
        private Label lStorageUnitNewUnit;
        private Label lStorageUnitNewTimeStamp;
        private Label lStorageUnitNewAdress;
        private DataGridView dgvStorageList;
        private BlockingCollection<String> debugMsg;
        private Task updateDebugMsq;
        private AutoResetEvent barcodeAnalizeOff;
        private Label lSatationName;
        private storageUnit storageUnitNew;
        private List<storageUnit> storageUnits;
        private Button bModeAct;
        private Label lOperatorName;
        private System.Timers.Timer tErrorShow;
        private bool showError;
        private Label lAllocationInfo;
        private TabControl tcRaportParam;
        private TabPage tpParamDate;
        private TabPage tpParamUnit;
        private TabPage tpParamKp;
        private TabPage tpParamKpLkp;
        private TabPage tpParamZpl;
        private TabPage tpReportInfo;

        private string reportWhere;
        private DateTimePicker dtpReportParam;
        private TextBox tbReportKp;
        private TextBox tbReportKpLkp;
        private TextBox tbReportZpl;
        private TextBox tbReportUnit;
        private DateTimePicker dtpReportUnit;
        private DataTable reportData;

        private string reportSelect;
        private string reportName;
        private string reportParam1;
        private string reportParam2;
        private int notPacked1;
        private int notPacked2;
        private int notSent1;
        private int notSent2;

        private ComboBox courier1;
        private ComboBox courier2;

        private int reportType;
        private storageUnit lastMadeStorage;

        private Label lAllocationName;
        private Label lAllocationCourierType;
        private Label lAllocationUnit;
        private Label lAllocationTimeStamp;
        private Label lAllocationCount;
        private Label lAllocationAdress;
        private DataGridView dgvAllocationList;

        private Label lReAllocationSourceName;
        private Label lReAllocationSourceCourierType;
        private Label lReAllocationSourceUnit;
        private Label lReAllocationSourceTimeStamp;
        private Label lReAllocationSourceCount;
        private Label lReAllocationSourceAdress;
        private DataGridView dgvReAllocationSourceList;
        private Label lReAllocationDestinationName;
        private Label lReAllocationDestinationCourierType;
        private Label lReAllocationDestinationUnit;
        private Label lReAllocationDestinationTimeStamp;
        private Label lReAllocationDestinationCount;
        private Label lReAllocationDestinationAdress;

        private Label lIssuingName;
        private Label lIssuingCourierType;
        private Label lIssuingUnit;
        private Label lIssuingTimeStamp;
        private Label lIssuingCount;
        private Label lIssuingAdress;
        private DataGridView dgvIssuingList;
        private List<dispachReportClass> dispachReports;
        private Label lReportKpError;
        private Label lReportKpLkpError;
        private Label lReportZplError;
        private Label lReportUnitError;
        private DataGridView dgvReport;

        public int Id { get => id; set => id = value; }
        public RichTextBox Debug { get => debug; set => debug = value; }
        public int ModeAct { get => modeAct; set => modeAct = value; }
        public HealthMonitor Hm { get => hm; set => hm = value; }
        public SQLConnection SqlConnection { get => sqlConnection; set => sqlConnection = value; }
        public ToolStripStatusLabel TsslDatabaseStatus { get => tsslDatabaseStatus; set => tsslDatabaseStatus = value; }
        public ToolStripStatusLabel TsslScanerStatus { get => tsslScanerStatus; set => tsslScanerStatus = value; }
        public ToolStripStatusLabel TsslUser { get => tsslUser; set => tsslUser = value; }
        public user ActUser { get => actUser; set => actUser = value; }
        public int ScannerID { get => scannerID; set => scannerID = value; }
        public int CradleId { get => cradleId; set => cradleId = value; }
        public bool ScanerLEDRed { get => scanerLEDRed; set => scanerLEDRed = value; }
        public TabControl TcMain { get => tcMain; set => tcMain = value; }
        public TabPage TpLogin { get => tpLogin; set => tpLogin = value; }
        public TabPage TpStorageUnit { get => tpStorageUnit; set => tpStorageUnit = value; }
        public TabPage TpIssuing { get => tpIssuing; set => tpIssuing = value; }
        public TabPage TpAllocation { get => tpAllocation; set => tpAllocation = value; }
        public ComboBox CbLoginUsers { get => cbLoginUsers; set => cbLoginUsers = value; }
        public TabPage TpMain { get => tpMain; set => tpMain = value; }
        public Label LInfo { get => lInfo; set => lInfo = value; }
        public System.Timers.Timer TLogInfoOff { get => tLogInfoOff; set => tLogInfoOff = value; }
        public TextBox TbLoginPass { get => tbLoginPass; set => tbLoginPass = value; }
        public TabPage TpReport { get => tpReport; set => tpReport = value; }
        public dataAcces Data { get => data; set => data = value; }
        public zebraPrinter ZebraPrinter { get => zebraPrinter; set => zebraPrinter = value; }
        public Label LStorageUnitNewCourierType { get => lStorageUnitNewCourierType; set => lStorageUnitNewCourierType = value; }
        public Label LStorageUnitNewUnit { get => lStorageUnitNewUnit; set => lStorageUnitNewUnit = value; }
        public Label LStorageUnitNewTimeStamp { get => lStorageUnitNewTimeStamp; set => lStorageUnitNewTimeStamp = value; }
        public Label LStorageUnitNewAdress { get => lStorageUnitNewAdress; set => lStorageUnitNewAdress = value; }
        public BlockingCollection<string> DebugMsg { get => debugMsg; set => debugMsg = value; }
        public Task UpdateDebugMsq { get => updateDebugMsq; set => updateDebugMsq = value; }
        public AutoResetEvent BarcodeAnalizeOff { get => barcodeAnalizeOff; set => barcodeAnalizeOff = value; }
        public Label LSatationName { get => lSatationName; set => lSatationName = value; }
        public DataGridView DgvStorageList { get => dgvStorageList; set => dgvStorageList = value; }
        public storageUnit StorageUnitNew { get => storageUnitNew; set => storageUnitNew = value; }
        public List<storageUnit> StorageUnits { get => storageUnits; set => storageUnits = value; }
        public Label LAllocationName { get => lAllocationName; set => lAllocationName = value; }
        public Label LAllocationCourierType { get => lAllocationCourierType; set => lAllocationCourierType = value; }
        public Label LAllocationUnit { get => lAllocationUnit; set => lAllocationUnit = value; }
        public Label LAllocationTimeStamp { get => lAllocationTimeStamp; set => lAllocationTimeStamp = value; }
        public Label LAllocationCount { get => lAllocationCount; set => lAllocationCount = value; }
        public Label LAllocationAdress { get => lAllocationAdress; set => lAllocationAdress = value; }
        public DataGridView DgvAllocationList { get => dgvAllocationList; set => dgvAllocationList = value; }
        public Button BModeAct { get => bModeAct; set => bModeAct = value; }
        public Label LOperatorName { get => lOperatorName; set => lOperatorName = value; }
        public System.Timers.Timer TErrorShow { get => tErrorShow; set => tErrorShow = value; }
        public bool ShowError { get => showError; set => showError = value; }
        public TabPage TpStoragePrintLabel { get => tpStoragePrintLabel; set => tpStoragePrintLabel = value; }
        public Label LAllocationInfo { get => lAllocationInfo; set => lAllocationInfo = value; }
        public TabControl TcRaportParam { get => tcRaportParam; set => tcRaportParam = value; }
        public TabPage TpParamDate { get => tpParamDate; set => tpParamDate = value; }
        public TabPage TpParamKp { get => tpParamKp; set => tpParamKp = value; }
        public TabPage TpParamKpLkp { get => tpParamKpLkp; set => tpParamKpLkp = value; }
        public TabPage TpParamZpl { get => tpParamZpl; set => tpParamZpl = value; }
        public allocationStepClass Allocation { get => allocation; set => allocation = value; }
        public allocationStepClass Issuing { get => issuing; set => issuing = value; }
        public int ReportType { get => reportType; set => reportType = value; }
        public List<dispachReportClass> DispachReports { get => dispachReports; set => dispachReports = value; }
        public DataGridView DgvReport { get => dgvReport; set => dgvReport = value; }
        public TabPage TpReportInfo { get => tpReportInfo; set => tpReportInfo = value; }
        public string ReportWhere { get => reportWhere; set => reportWhere = value; }
        public DateTimePicker DtpReportParam { get => dtpReportParam; set => dtpReportParam = value; }
       
        public TextBox TbReportKp { get => tbReportKp; set => tbReportKp = value; }
        public TextBox TbReportKpLkp { get => tbReportKpLkp; set => tbReportKpLkp = value; }
        public TextBox TbReportZpl { get => tbReportZpl; set => tbReportZpl = value; }
        public Label LReportKpError { get => lReportKpError; set => lReportKpError = value; }
        public Label LReportKpLkpError { get => lReportKpLkpError; set => lReportKpLkpError = value; }
        public Label LReportZplError { get => lReportZplError; set => lReportZplError = value; }
        public storageUnit LastMadeStorage { get => lastMadeStorage; set => lastMadeStorage = value; }
        public TextBox TbReportUnit { get => tbReportUnit; set => tbReportUnit = value; }
        public DateTimePicker DtpReportUnit { get => dtpReportUnit; set => dtpReportUnit = value; }
        public TabPage TpParamUnit { get => tpParamUnit; set => tpParamUnit = value; }
        public Label LReportUnitError { get => lReportUnitError; set => lReportUnitError = value; }
        public Label LIssuingName { get => lIssuingName; set => lIssuingName = value; }
        public Label LIssuingCourierType { get => lIssuingCourierType; set => lIssuingCourierType = value; }
        public Label LIssuingUnit { get => lIssuingUnit; set => lIssuingUnit = value; }
        public Label LIssuingTimeStamp { get => lIssuingTimeStamp; set => lIssuingTimeStamp = value; }
        public Label LIssuingCount { get => lIssuingCount; set => lIssuingCount = value; }
        public Label LIssuingAdress { get => lIssuingAdress; set => lIssuingAdress = value; }
        public DataGridView DgvIssuingList { get => dgvIssuingList; set => dgvIssuingList = value; }



        public allocationStepClass ReAllocationSource { get => reAllocationSource; set => reAllocationSource = value; }
        public allocationStepClass ReAllocationDestination { get => reAllocationDestination; set => reAllocationDestination = value; }
        public Label LReAllocationSourceName { get => lReAllocationSourceName; set => lReAllocationSourceName = value; }
        public Label LReAllocationSourceCourierType { get => lReAllocationSourceCourierType; set => lReAllocationSourceCourierType = value; }
        public Label LReAllocationSourceUnit { get => lReAllocationSourceUnit; set => lReAllocationSourceUnit = value; }
        public Label LReAllocationSourceTimeStamp { get => lReAllocationSourceTimeStamp; set => lReAllocationSourceTimeStamp = value; }
        public Label LReAllocationSourceCount { get => lReAllocationSourceCount; set => lReAllocationSourceCount = value; }
        public Label LReAllocationSourceAdress { get => lReAllocationSourceAdress; set => lReAllocationSourceAdress = value; }
        public DataGridView DgvReAllocationSourceList { get => dgvReAllocationSourceList; set => dgvReAllocationSourceList = value; }
        public Label LReAllocationDestinationName { get => lReAllocationDestinationName; set => lReAllocationDestinationName = value; }
        public Label LReAllocationDestinationCourierType { get => lReAllocationDestinationCourierType; set => lReAllocationDestinationCourierType = value; }
        public Label LReAllocationDestinationUnit { get => lReAllocationDestinationUnit; set => lReAllocationDestinationUnit = value; }
        public Label LReAllocationDestinationTimeStamp { get => lReAllocationDestinationTimeStamp; set => lReAllocationDestinationTimeStamp = value; }
        public Label LReAllocationDestinationCount { get => lReAllocationDestinationCount; set => lReAllocationDestinationCount = value; }
        public Label LReAllocationDestinationAdress { get => lReAllocationDestinationAdress; set => lReAllocationDestinationAdress = value; }
        public TabPage TpAllocationChange { get => tpAllocationChange; set => tpAllocationChange = value; }
        public int NotPacked1 { get => notPacked1; set => notPacked1 = value; }
        public int NotPacked2 { get => notPacked2; set => notPacked2 = value; }
        public int NotSent1 { get => notSent1; set => notSent1 = value; }
        public int NotSent2 { get => notSent2; set => notSent2 = value; }
        public ComboBox Courier1 { get => courier1; set => courier1 = value; }
        public ComboBox Courier2 { get => courier2; set => courier2 = value; }
        public raportDataSet raportSet { get; set; }
        public DataTable ReportData { get => reportData; set => reportData = value; }
        public string ReportSelect { get => reportSelect; set => reportSelect = value; }
        public string ReportName { get => reportName; set => reportName = value; }
        public string ReportParam1 { get => reportParam1; set => reportParam1 = value; }
        public string ReportParam2 { get => reportParam2; set => reportParam2 = value; }

        public Button bReportPrint1;
        public Button bReportPrint2;
        public Button bReportPrint3;
        public Button bReportPrint4;

        //--------------------------------------------------------------------------------------------------------------------------
        //allokacja
        private allocationStepClass allocation;
        private allocationStepClass issuing;
        private allocationStepClass reAllocationSource;
        private allocationStepClass reAllocationDestination;

        public class allocationStepClass
        {
            private bool storageScanDone;
            private storageUnit storage;
            private dispatchDelivery dispatchDelivery;
            private bool zplScanDone;
            private List<dispatchDeliveryShow> deliveryDispachShows;
            private global.zplShortDataType zplShort;
            private int packCounter;

            public storageUnit Storage { get => storage; set => storage = value; }
            public dispatchDelivery DispatchDelivery { get => dispatchDelivery; set => dispatchDelivery = value; }
            public bool StorageScanDone { get => storageScanDone; set => storageScanDone = value; }
            public bool ZplScanDone { get => zplScanDone; set => zplScanDone = value; }
            public global.zplShortDataType ZplShort { get => zplShort; set => zplShort = value; }
            public List<dispatchDeliveryShow> DeliveryDispachShows { get => deliveryDispachShows; set => deliveryDispachShows = value; }
            public int PackCounter { get => packCounter; set => packCounter = value; } 

            public allocationStepClass()
            {
                this.Storage = new storageUnit();
                this.DispatchDelivery = new dispatchDelivery();
                this.StorageScanDone = false;
                this.zplScanDone = false;
                this.DeliveryDispachShows = new List<dispatchDeliveryShow>();
                this.packCounter = 0;
            }
        }

        //--------------------------------------------------------------------------------------------------------------------------

    }
}
