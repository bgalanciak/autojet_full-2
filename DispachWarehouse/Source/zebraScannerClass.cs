﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreScanner;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace DispachWarehouse
{
    public class zebraScannerClass
    {
        //private int 

        private DispachWarehouseMain refForm;
        private CCoreScannerClass cCoreScannerClass;

        private delegate void barcodeDataRecieved(string barcode, string scannerId);
        private barcodeDataRecieved BarcodeDataRecieved;

        private delegate void publicMsg(string msg, int id);
        private publicMsg PublicMsg;

        private delegate void scannerPNPEvent(List<scanner> scannerList);
        private scannerPNPEvent ScannerPNPEvent;

        public zebraScannerClass(DispachWarehouseMain ownerForm)
        {
            this.refForm = ownerForm;
            PublicMsg = refForm.scannerPublicMsg;
            BarcodeDataRecieved = refForm.scannerBarcodeRevieved;
            ScannerPNPEvent = refForm.onZebraScannerPNP;
        }

        public List<scanner> Connect()
        {
            List<scanner> scannerList = new List<scanner>();

            try
            {
                cCoreScannerClass = new CCoreScannerClass();
                //Call Open API
                short[] scannerTypes = new short[1];        // Scanner Types you are interested in
                scannerTypes[0] = 1;                        // 1 for all scanner types
                short numberOfScannerTypes = 1;             // Size of the scannerTypes array
                int status;                                 // Extended API return code
                cCoreScannerClass.Open(0, scannerTypes, numberOfScannerTypes, out status);
                if (status == 0)
                {
                    publicScannerMsg("CoreScanner API: Open Successful", 1);
                }
                else
                {
                    publicScannerMsg("CoreScanner API: Open Failed", 1);
                }

                //pobieranie listy skanerów
                // Lets list down all the scanners connected to the host
                short numberOfScanners;                 // Number of scanners expect to be used
                int[] connectedScannerIDList = new int[255];
                // List of scanner IDs to be returned
                string outXML;                          //Scanner details output
                cCoreScannerClass.GetScanners(out numberOfScanners, connectedScannerIDList, out outXML, out status);

                publicScannerMsg($"connectScanner, GetScanners: {outXML}", 1);

                scannerList = getScannersFromXml(outXML);

                // Subscribe for barcode events in cCoreScannerClass
                cCoreScannerClass.BarcodeEvent += new CoreScanner._ICoreScannerEvents_BarcodeEventEventHandler(OnBarcodeEvent);
                cCoreScannerClass.PNPEvent += new CoreScanner._ICoreScannerEvents_PNPEventEventHandler(OnPNPEvent);

                // Let's subscribe for events
                int opcode = 1001; // Method for Subscribe events
                string outXML2; // XML Output
                string inXML = "<inArgs>" +
                                "<cmdArgs>" +
                                "<arg-int>2</arg-int>" + // Number of events you want to subscribe
                                "<arg-int>1,16</arg-int>" + // Comma separated event IDs
                                "</cmdArgs>" +
                                "</inArgs>";

                cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML2, out status);

                publicScannerMsg($"connectScanner, ExecCommand: {outXML2}", 1);
            }
            catch (Exception exp)
            {
                publicScannerMsg($"connectScanner: Something wrong please check... {exp.Message}", 1);
            }

            return scannerList;
        }

        public void OnPNPEvent(short eventType, ref string ppnpData)
        {

            publicScannerMsg($"OnPNPEvent, ppnpData: {ppnpData}", 1);

            List<scanner> scaners = new List<scanner>();
            try
            {
                //pobieranie skanerów do zmiennej zebraScanner, z aplikacji demo
                zebraScanner[] arScanr;
                string sStatus = String.Empty;
                ReadXmlString_AttachDetachMulti(ppnpData, out arScanr, out sStatus);

                //tworzenie obiektów scanner na podstawie zebraScanner
                foreach (zebraScanner s in arScanr)
                {
                    if (s != null)
                    {
                        scanner scan = new scanner();
                        scan.serialnumber = s.SERIALNO;
                        scan.scannerID = s.SCANNERID;
                        scaners.Add(scan);
                    }
                }

                //sprawdzenie, czy skaner został podłączony czy odłączony
                if (eventType == 0) //ATTACHED
                {
                    foreach (scanner s in scaners)
                    {
                        s.connected = true;
                    }
                }
                else if (eventType == 1) //DETACHED
                {
                    foreach (scanner s in scaners)
                    {
                        s.connected = false;
                    }
                }

                refForm.BeginInvoke(ScannerPNPEvent, scaners);

                ////wywołanie metody w DispachWarehouseMain
                //if (refForm.InvokeRequired)
                //{
                //    // Invoke the delegate.
                //    refForm.Invoke(ScannerPNPEvent, scaners);
                //}
                //else
                //{
                //    refForm.onZebraScannerPNP(scaners);
                //}
            }
            catch
            { }
        }

        public List<scanner> getScannersFromXml(string inXml)
        {
            List<scanner> retVal = new List<scanner>();

            XmlSerializer serializer = new XmlSerializer(typeof(List<scanner>), new XmlRootAttribute("scanners"));
            StringReader stringReader = new StringReader(inXml);
            retVal = (List<scanner>)serializer.Deserialize(stringReader);

            return retVal;
        }

        //z aplikacji demo
        const string TAG_PNP = "pnp";

        //z aplikacji demo
        public void ReadXmlString_AttachDetachMulti(string strXml, out zebraScanner[] arScanr, out string sStatus)
        {
            arScanr = new zebraScanner[8];
            for (int index = 0; index < 5; index++)
            {
                arScanr.SetValue(null, index);
            }

            sStatus = "";
            if (String.IsNullOrEmpty(strXml))
            {
                return;
            }

            try
            {
                XmlTextReader xmlRead = new XmlTextReader(new StringReader(strXml));
                // Skip non-significant whitespace   
                xmlRead.WhitespaceHandling = WhitespaceHandling.Significant;

                string sElementName = "", sElmValue = "";
                bool bScanner = false;
                int nScannerCount = 0;//for multiple scanners as in cradle+cascaded
                int nIndex = 0;
                while (xmlRead.Read())
                {
                    switch (xmlRead.NodeType)
                    {
                        case XmlNodeType.Element:
                            sElementName = xmlRead.Name;
                            string strScannerType = xmlRead.GetAttribute(zebraScanner.TAG_SCANNER_TYPE);
                            if (xmlRead.HasAttributes && (
                                (zebraScanner.TAG_SCANNER_SNAPI == strScannerType) ||
                                (zebraScanner.TAG_SCANNER_SSI == strScannerType) ||
                                (zebraScanner.TAG_SCANNER_IBMHID == strScannerType) ||
                                (zebraScanner.TAG_SCANNER_OPOS == strScannerType) ||
                                (zebraScanner.TAG_SCANNER_IMBTT == strScannerType) ||
                                (zebraScanner.TAG_SCALE_IBM == strScannerType) ||
                                (zebraScanner.SCANNER_SSI_BT == strScannerType) ||
                                (zebraScanner.TAG_SCANNER_HIDKB == strScannerType)))//n = xmlRead.AttributeCount;
                            {
                                nIndex = nScannerCount;
                                arScanr.SetValue(new zebraScanner(), nIndex);
                                nScannerCount++;
                                arScanr[nIndex].SCANNERTYPE = strScannerType;
                            }
                            if ((null != arScanr[nIndex]) && zebraScanner.TAG_SCANNER_ID == sElementName)
                            {
                                bScanner = true;
                            }
                            break;

                        case XmlNodeType.Text:
                            if (bScanner && (null != arScanr[nIndex]))
                            {
                                sElmValue = xmlRead.Value;
                                switch (sElementName)
                                {
                                    case zebraScanner.TAG_SCANNER_ID:
                                        arScanr[nIndex].SCANNERID = sElmValue;
                                        break;
                                    case zebraScanner.TAG_SCANNER_SERIALNUMBER:
                                        arScanr[nIndex].SERIALNO = sElmValue;
                                        break;
                                    case zebraScanner.TAG_SCANNER_MODELNUMBER:
                                        arScanr[nIndex].MODELNO = sElmValue;
                                        break;
                                    case zebraScanner.TAG_SCANNER_GUID:
                                        arScanr[nIndex].GUID = sElmValue;
                                        break;
                                    case zebraScanner.TAG_SCANNER_TYPE:
                                        arScanr[nIndex].SCANNERTYPE = sElmValue;
                                        break;
                                    case zebraScanner.TAG_SCANNER_FW:
                                        arScanr[nIndex].SCANNERFIRMWARE = sElmValue;
                                        break;
                                    case zebraScanner.TAG_SCANNER_DOM:
                                        arScanr[nIndex].SCANNERMNFDATE = sElmValue;
                                        break;
                                    case zebraScanner.TAG_STATUS:
                                        sStatus = sElmValue;
                                        break;
                                    case TAG_PNP:
                                        if ("0" == sElmValue)
                                        {
                                            arScanr[nIndex] = null;
                                            nScannerCount--;
                                        }
                                        break;
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void OnBarcodeEvent(short eventType, ref string pscanData)
        {
            string tmpScanData = pscanData;
            //this.Invoke((MethodInvoker)delegate { textBox1.Text = barcode; });
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(tmpScanData);

            string barcode = xmlDoc.DocumentElement.GetElementsByTagName("datalabel").Item(0).InnerText;
            string symbology = xmlDoc.DocumentElement.GetElementsByTagName("datatype").Item(0).InnerText;
            string scannerId = xmlDoc.DocumentElement.GetElementsByTagName("scannerID").Item(0).InnerText;

            string code = barcode.Replace("0x", "");                    //MC
            string codeShort = Regex.Replace(code, @"\s+", "");         //https://stackoverflow.com/questions/6219454/efficient-way-to-remove-all-whitespace-from-string
            byte[] codeByte = utilities.StringToByteArray(codeShort);

            UTF8Encoding utf8 = new UTF8Encoding();                     //https://social.msdn.microsoft.com/Forums/vstudio/en-US/de328b22-0f6c-421e-9f72-5765569c6f86/conversion-of-hex-string-to-unicodeutf8?forum=netfxbcl
            String decodedString = utf8.GetString(codeByte);

            string[] args = new string[] { decodedString, scannerId };
            refForm.BeginInvoke(BarcodeDataRecieved, args);

            //if (refForm.InvokeRequired)
            //{
            //    // Make an array containing the parameters
            //    // to pass to the method.
            //    string[] args = new string[] { decodedString, scannerId };

            //    // Invoke the delegate.
            //    refForm.Invoke(BarcodeDataRecieved, args);
            //}
            //else
            //{
            //    refForm.scannerBarcodeRevieved(barcode, scannerId);
            //}
        }

        public void scannerBeep(int id)
        {
            int status; // Extended API return code

            // Let's beep the beeper
            int opcode = 6000; // Method for Beep the beeper
            string outXML; // Output
            string inXML = $"<inArgs>" +
                    $"<scannerID>{id}</scannerID>" +    // The scanner you need to beep
                    "<cmdArgs>" +
                    "<arg-int>4</arg-int>" +            // 4 high short beep pattern
                    "</cmdArgs>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
        }

        public void scannerLEDRedOn(int id)
        {
            int status; // Extended API return code

            // Let's beep the beeper
            int opcode = 6000; // Method for Beep the beeper
            string outXML; // Output
            string inXML = $"<inArgs>" +
                    $"<scannerID>{id}</scannerID>" +    // The scanner you need to beep
                    "<cmdArgs>" +
                    "<arg-int>47</arg-int>" +            // 47 - Red LED on
                    "</cmdArgs>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
        }

        public void scannerLEDRedOff(int id)
        {
            int status; // Extended API return code

            // Let's beep the beeper
            int opcode = 6000; // Method for Beep the beeper
            string outXML; // Output
            string inXML = $"<inArgs>" +
                    $"<scannerID>{id}</scannerID>" +    // The scanner you need to beep
                    "<cmdArgs>" +
                    "<arg-int>48</arg-int>" +            // 48 - Red LED off
                    "</cmdArgs>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
        }

        public void publicScannerMsg(string msg, int scannerId)
        {
            object[] args = new object[] { msg, scannerId };
            refForm.BeginInvoke(PublicMsg, args);

            //if (refForm.InvokeRequired)
            //{
            //    // Make an array containing the parameters
            //    // to pass to the method.

            //    //string[] args = new string[] { msg, scannerId.ToString()};
            //    object[] args = new object[] { msg, scannerId };

            //    // Invoke the delegate.
            //    refForm.Invoke(PublicMsg, args);
            //}
            //else
            //{
            //    refForm.scannerPublicMsg(msg, scannerId);
            //}
        }

        public class scanner
        {
            public string scannerID;
            public string serialnumber;
            public string GUID;
            public string VID;
            public string PID;
            public string modelnumber;
            public string DoM;
            public string firmware;
            public bool connected;         //indicates whether scanner is connected or disconnected
        }
        /*<scanners>
          <scanner type = "USBIBMHID" >
            < scannerID > 2 </ scannerID >
            < serialnumber > 1811300500845 </ serialnumber >
            < GUID > 35DF3425260535478025D6C3376ACB18</GUID>
            <VID>1504</VID>
            <PID>2080</PID>
            <modelnumber>CR0078-SC10007WR</modelnumber>
            <DoM>24APR18</DoM>
            <firmware>NBCACAAS</firmware>
          </scanner>
          <scanner type = "USBIBMHID" >
            < scannerID > 3 </ scannerID >
            < serialnumber > 1812200501377 </ serialnumber >
            < modelnumber > LI4278 - SR20007WR </ modelnumber >
            < DoM > 03MAY18</DoM>
            <firmware>PAABIS00-004-R00</firmware>
          </scanner>
        </scanners>*/
        
    }
}
