﻿namespace DispachWarehouse
{
    partial class DispachWarehouseMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslDatabaseStatus1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslScanerStatus1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslUser1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslSpring = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslDatabaseStatus2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslScanerStatus2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslUser2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tConnection = new System.Windows.Forms.Timer(this.components);
            this.gbInfo1 = new System.Windows.Forms.GroupBox();
            this.lInfo1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lOperatorName1 = new System.Windows.Forms.Label();
            this.bModeAct1 = new System.Windows.Forms.Button();
            this.lSatationName1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.lOperatorName2 = new System.Windows.Forms.Label();
            this.bModeAct2 = new System.Windows.Forms.Button();
            this.lSatationName2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tcMainG1 = new System.Windows.Forms.TabControl();
            this.tpMain1 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.tpLogin1 = new System.Windows.Forms.TabPage();
            this.bLoginLog1 = new System.Windows.Forms.Button();
            this.tbLoginPass1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbLoginUsers1 = new System.Windows.Forms.ComboBox();
            this.tpStorageUnit1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dgvStorageList1 = new System.Windows.Forms.DataGridView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lStorageUnitNewAdress1 = new System.Windows.Forms.Label();
            this.lStorageUnitNewTimeStamp1 = new System.Windows.Forms.Label();
            this.lStorageUnitNewUnit1 = new System.Windows.Forms.Label();
            this.lStorageUnitNewCourierType1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.tpStoragePrintLabel1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.tpAllocation1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.dgvAllocationList1 = new System.Windows.Forms.DataGridView();
            this.panel15 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.lAllocationAdress1 = new System.Windows.Forms.Label();
            this.lAllocationCount1 = new System.Windows.Forms.Label();
            this.lAllocationTimeStamp1 = new System.Windows.Forms.Label();
            this.lAllocationUnit1 = new System.Windows.Forms.Label();
            this.lAllocationCourierType1 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.lAllocationInfo1 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lAllocationName1 = new System.Windows.Forms.Label();
            this.tpAllocationChange1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label55 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationAdress1 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationCount1 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationTimeStamp1 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationUnit1 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationCourierType1 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label38 = new System.Windows.Forms.Label();
            this.lReAllocationSourceAdress1 = new System.Windows.Forms.Label();
            this.lReAllocationSourceCount1 = new System.Windows.Forms.Label();
            this.lReAllocationSourceTimeStamp1 = new System.Windows.Forms.Label();
            this.lReAllocationSourceUnit1 = new System.Windows.Forms.Label();
            this.lReAllocationSourceCourierType1 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.dgvReAllocationSourceList1 = new System.Windows.Forms.DataGridView();
            this.tpIssuing1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.dgvIssuingList1 = new System.Windows.Forms.DataGridView();
            this.panel19 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.lIssuingAdress1 = new System.Windows.Forms.Label();
            this.lIssuingCount1 = new System.Windows.Forms.Label();
            this.lIssuingTimeStamp1 = new System.Windows.Forms.Label();
            this.lIssuingUnit1 = new System.Windows.Forms.Label();
            this.lIssuingCourierType1 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.tpReport1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvReport1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bReportStroerWaw1 = new System.Windows.Forms.Button();
            this.bReportStroer1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bReportUnit1 = new System.Windows.Forms.Button();
            this.bReportZpl1 = new System.Windows.Forms.Button();
            this.bReportKpLkp1 = new System.Windows.Forms.Button();
            this.bReportKp1 = new System.Windows.Forms.Button();
            this.bReportDate1 = new System.Windows.Forms.Button();
            this.tcReport1 = new System.Windows.Forms.TabControl();
            this.tpParamDate1 = new System.Windows.Forms.TabPage();
            this.label39 = new System.Windows.Forms.Label();
            this.Courier1 = new System.Windows.Forms.ComboBox();
            this.dtpReportNS1 = new System.Windows.Forms.CheckBox();
            this.dtpReportNP1 = new System.Windows.Forms.CheckBox();
            this.bReportPrint11 = new System.Windows.Forms.Button();
            this.bReportMake11 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.dtpReportParam1 = new System.Windows.Forms.DateTimePicker();
            this.tpParamKp1 = new System.Windows.Forms.TabPage();
            this.bReportPrint21 = new System.Windows.Forms.Button();
            this.lReportKpError1 = new System.Windows.Forms.Label();
            this.bReportMake21 = new System.Windows.Forms.Button();
            this.tbReportKp1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tpParamKpLkp1 = new System.Windows.Forms.TabPage();
            this.bReportPrint31 = new System.Windows.Forms.Button();
            this.lReportKpLkpError1 = new System.Windows.Forms.Label();
            this.bReportMake31 = new System.Windows.Forms.Button();
            this.tbReportKpLkp1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tpParamZpl1 = new System.Windows.Forms.TabPage();
            this.bReportPrint41 = new System.Windows.Forms.Button();
            this.lReportZplError1 = new System.Windows.Forms.Label();
            this.bReportMake41 = new System.Windows.Forms.Button();
            this.tbReportZpl1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tpParamUnit1 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.lReportUnitError1 = new System.Windows.Forms.Label();
            this.tbReportUnit1 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.bReportMake51 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.dtpReportUnit1 = new System.Windows.Forms.DateTimePicker();
            this.tpReportInfo1 = new System.Windows.Forms.TabPage();
            this.label34 = new System.Windows.Forms.Label();
            this.tpDebug1 = new System.Windows.Forms.TabPage();
            this.rtbDebugG1 = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tcMainG2 = new System.Windows.Forms.TabControl();
            this.tpMain2 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.tpLogin2 = new System.Windows.Forms.TabPage();
            this.bLoginLog2 = new System.Windows.Forms.Button();
            this.tbLoginPass2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbLoginUsers2 = new System.Windows.Forms.ComboBox();
            this.tpStorageUnit2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.dgvStorageList2 = new System.Windows.Forms.DataGridView();
            this.panel26 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.lStorageUnitNewAdress2 = new System.Windows.Forms.Label();
            this.lStorageUnitNewTimeStamp2 = new System.Windows.Forms.Label();
            this.lStorageUnitNewUnit2 = new System.Windows.Forms.Label();
            this.lStorageUnitNewCourierType2 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.tpStoragePrintLabel2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.tpAllocation2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.dgvAllocationList2 = new System.Windows.Forms.DataGridView();
            this.panel32 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.label78 = new System.Windows.Forms.Label();
            this.lAllocationAdress2 = new System.Windows.Forms.Label();
            this.lAllocationCount2 = new System.Windows.Forms.Label();
            this.lAllocationTimeStamp2 = new System.Windows.Forms.Label();
            this.lAllocationUnit2 = new System.Windows.Forms.Label();
            this.lAllocationCourierType2 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.lAllocationInfo2 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.label89 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.lAllocationName2 = new System.Windows.Forms.Label();
            this.tpAllocationChange2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.label95 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationAdress2 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationCount2 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationTimeStamp2 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationUnit2 = new System.Windows.Forms.Label();
            this.lReAllocationDestinationCourierType2 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.label105 = new System.Windows.Forms.Label();
            this.lReAllocationSourceAdress2 = new System.Windows.Forms.Label();
            this.lReAllocationSourceCount2 = new System.Windows.Forms.Label();
            this.lReAllocationSourceTimeStamp2 = new System.Windows.Forms.Label();
            this.lReAllocationSourceUnit2 = new System.Windows.Forms.Label();
            this.lReAllocationSourceCourierType2 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.dgvReAllocationSourceList2 = new System.Windows.Forms.DataGridView();
            this.tpIssuing2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.dgvIssuingList2 = new System.Windows.Forms.DataGridView();
            this.panel39 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.label115 = new System.Windows.Forms.Label();
            this.lIssuingAdress2 = new System.Windows.Forms.Label();
            this.lIssuingCount2 = new System.Windows.Forms.Label();
            this.lIssuingTimeStamp2 = new System.Windows.Forms.Label();
            this.lIssuingUnit2 = new System.Windows.Forms.Label();
            this.lIssuingCourierType2 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label125 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label126 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label127 = new System.Windows.Forms.Label();
            this.tpReport2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvReport2 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bReportStroerWaw2 = new System.Windows.Forms.Button();
            this.bReportStroer2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bReportUnit2 = new System.Windows.Forms.Button();
            this.bReportZpl2 = new System.Windows.Forms.Button();
            this.bReportKpLkp2 = new System.Windows.Forms.Button();
            this.bReportKp2 = new System.Windows.Forms.Button();
            this.bReportDate2 = new System.Windows.Forms.Button();
            this.tcReport2 = new System.Windows.Forms.TabControl();
            this.tpParamDate2 = new System.Windows.Forms.TabPage();
            this.label40 = new System.Windows.Forms.Label();
            this.dtpReportNS2 = new System.Windows.Forms.CheckBox();
            this.Courier2 = new System.Windows.Forms.ComboBox();
            this.bReportPrint12 = new System.Windows.Forms.Button();
            this.dtpReportNP2 = new System.Windows.Forms.CheckBox();
            this.bReportMake12 = new System.Windows.Forms.Button();
            this.label128 = new System.Windows.Forms.Label();
            this.dtpReportParam2 = new System.Windows.Forms.DateTimePicker();
            this.tpParamKp2 = new System.Windows.Forms.TabPage();
            this.bReportPrint22 = new System.Windows.Forms.Button();
            this.lReportKpError2 = new System.Windows.Forms.Label();
            this.bReportMake22 = new System.Windows.Forms.Button();
            this.tbReportKp2 = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.tpParamKpLkp2 = new System.Windows.Forms.TabPage();
            this.bReportPrint32 = new System.Windows.Forms.Button();
            this.lReportKpLkpError2 = new System.Windows.Forms.Label();
            this.bReportMake32 = new System.Windows.Forms.Button();
            this.tbReportKpLkp2 = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.tpParamZpl2 = new System.Windows.Forms.TabPage();
            this.bReportPrint42 = new System.Windows.Forms.Button();
            this.lReportZplError2 = new System.Windows.Forms.Label();
            this.bReportMake42 = new System.Windows.Forms.Button();
            this.tbReportZpl2 = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.tpParamUnit2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.lReportUnitError2 = new System.Windows.Forms.Label();
            this.tbReportUnit2 = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.bReportMake52 = new System.Windows.Forms.Button();
            this.label137 = new System.Windows.Forms.Label();
            this.dtpReportUnit2 = new System.Windows.Forms.DateTimePicker();
            this.tpReportInfo2 = new System.Windows.Forms.TabPage();
            this.label138 = new System.Windows.Forms.Label();
            this.tpDebug2 = new System.Windows.Forms.TabPage();
            this.rtbDebugG2 = new System.Windows.Forms.RichTextBox();
            this.gbInfo2 = new System.Windows.Forms.GroupBox();
            this.lInfo2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.gbInfo1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tcMainG1.SuspendLayout();
            this.tpMain1.SuspendLayout();
            this.tpLogin1.SuspendLayout();
            this.tpStorageUnit1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStorageList1)).BeginInit();
            this.panel9.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tpStoragePrintLabel1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel17.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tpAllocation1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllocationList1)).BeginInit();
            this.panel15.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tpAllocationChange1.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.panel24.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.panel23.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReAllocationSourceList1)).BeginInit();
            this.tpIssuing1.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssuingList1)).BeginInit();
            this.panel19.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.tpReport1.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tcReport1.SuspendLayout();
            this.tpParamDate1.SuspendLayout();
            this.tpParamKp1.SuspendLayout();
            this.tpParamKpLkp1.SuspendLayout();
            this.tpParamZpl1.SuspendLayout();
            this.tpParamUnit1.SuspendLayout();
            this.tpReportInfo1.SuspendLayout();
            this.tpDebug1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tcMainG2.SuspendLayout();
            this.tpMain2.SuspendLayout();
            this.tpLogin2.SuspendLayout();
            this.tpStorageUnit2.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStorageList2)).BeginInit();
            this.panel26.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            this.tpStoragePrintLabel2.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.panel29.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.panel30.SuspendLayout();
            this.tpAllocation2.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllocationList2)).BeginInit();
            this.panel32.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.tpAllocationChange2.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.panel36.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.panel37.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReAllocationSourceList2)).BeginInit();
            this.tpIssuing2.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.panel38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssuingList2)).BeginInit();
            this.panel39.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            this.tpReport2.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport2)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tcReport2.SuspendLayout();
            this.tpParamDate2.SuspendLayout();
            this.tpParamKp2.SuspendLayout();
            this.tpParamKpLkp2.SuspendLayout();
            this.tpParamZpl2.SuspendLayout();
            this.tpParamUnit2.SuspendLayout();
            this.tpReportInfo2.SuspendLayout();
            this.tpDebug2.SuspendLayout();
            this.gbInfo2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pomocToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1180, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramieToolStripMenuItem1});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // oProgramieToolStripMenuItem1
            // 
            this.oProgramieToolStripMenuItem1.Name = "oProgramieToolStripMenuItem1";
            this.oProgramieToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.oProgramieToolStripMenuItem1.Text = "O programie...";
            this.oProgramieToolStripMenuItem1.Click += new System.EventHandler(this.oProgramieToolStripMenuItem1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslDatabaseStatus1,
            this.tsslScanerStatus1,
            this.tsslUser1,
            this.tsslSpring,
            this.tsslDatabaseStatus2,
            this.tsslScanerStatus2,
            this.tsslUser2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 709);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1180, 24);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslDatabaseStatus1
            // 
            this.tsslDatabaseStatus1.AutoSize = false;
            this.tsslDatabaseStatus1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslDatabaseStatus1.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslDatabaseStatus1.Name = "tsslDatabaseStatus1";
            this.tsslDatabaseStatus1.Size = new System.Drawing.Size(180, 19);
            this.tsslDatabaseStatus1.Text = "Baza danych: nie połączono";
            this.tsslDatabaseStatus1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslScanerStatus1
            // 
            this.tsslScanerStatus1.AutoSize = false;
            this.tsslScanerStatus1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslScanerStatus1.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslScanerStatus1.Name = "tsslScanerStatus1";
            this.tsslScanerStatus1.Size = new System.Drawing.Size(140, 19);
            this.tsslScanerStatus1.Text = "Skaner: nie połączony";
            this.tsslScanerStatus1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslUser1
            // 
            this.tsslUser1.AutoSize = false;
            this.tsslUser1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslUser1.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslUser1.Name = "tsslUser1";
            this.tsslUser1.Size = new System.Drawing.Size(180, 19);
            this.tsslUser1.Text = "Operator: BRAK";
            this.tsslUser1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslSpring
            // 
            this.tsslSpring.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslSpring.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslSpring.Name = "tsslSpring";
            this.tsslSpring.Size = new System.Drawing.Size(165, 19);
            this.tsslSpring.Spring = true;
            // 
            // tsslDatabaseStatus2
            // 
            this.tsslDatabaseStatus2.AutoSize = false;
            this.tsslDatabaseStatus2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslDatabaseStatus2.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslDatabaseStatus2.Name = "tsslDatabaseStatus2";
            this.tsslDatabaseStatus2.Size = new System.Drawing.Size(180, 19);
            this.tsslDatabaseStatus2.Text = "Baza danych: nie połączono";
            this.tsslDatabaseStatus2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslScanerStatus2
            // 
            this.tsslScanerStatus2.AutoSize = false;
            this.tsslScanerStatus2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslScanerStatus2.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslScanerStatus2.Name = "tsslScanerStatus2";
            this.tsslScanerStatus2.Size = new System.Drawing.Size(140, 19);
            this.tsslScanerStatus2.Text = "Skaner: nie połączony";
            this.tsslScanerStatus2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslUser2
            // 
            this.tsslUser2.AutoSize = false;
            this.tsslUser2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslUser2.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslUser2.Name = "tsslUser2";
            this.tsslUser2.Size = new System.Drawing.Size(180, 19);
            this.tsslUser2.Text = "Operator: BRAK";
            this.tsslUser2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tConnection
            // 
            this.tConnection.Interval = 3000;
            this.tConnection.Tick += new System.EventHandler(this.tConnection_Tick);
            // 
            // gbInfo1
            // 
            this.gbInfo1.Controls.Add(this.lInfo1);
            this.gbInfo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gbInfo1.Location = new System.Drawing.Point(3, 585);
            this.gbInfo1.Name = "gbInfo1";
            this.gbInfo1.Size = new System.Drawing.Size(584, 51);
            this.gbInfo1.TabIndex = 6;
            this.gbInfo1.TabStop = false;
            this.gbInfo1.Text = " Informacje ";
            // 
            // lInfo1
            // 
            this.lInfo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lInfo1.ForeColor = System.Drawing.Color.Red;
            this.lInfo1.Location = new System.Drawing.Point(3, 22);
            this.lInfo1.Name = "lInfo1";
            this.lInfo1.Size = new System.Drawing.Size(578, 26);
            this.lInfo1.TabIndex = 0;
            this.lInfo1.Text = " ";
            this.lInfo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(584, 44);
            this.panel4.TabIndex = 3;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel6.Controls.Add(this.lOperatorName1, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.bModeAct1, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.lSatationName1, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(584, 44);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // lOperatorName1
            // 
            this.lOperatorName1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lOperatorName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lOperatorName1.Location = new System.Drawing.Point(3, 0);
            this.lOperatorName1.Name = "lOperatorName1";
            this.lOperatorName1.Size = new System.Drawing.Size(244, 44);
            this.lOperatorName1.TabIndex = 3;
            this.lOperatorName1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bModeAct1
            // 
            this.bModeAct1.BackColor = System.Drawing.Color.Lime;
            this.bModeAct1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bModeAct1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bModeAct1.Location = new System.Drawing.Point(334, 0);
            this.bModeAct1.Margin = new System.Windows.Forms.Padding(0);
            this.bModeAct1.Name = "bModeAct1";
            this.bModeAct1.Size = new System.Drawing.Size(250, 44);
            this.bModeAct1.TabIndex = 1;
            this.bModeAct1.Text = "Tryb";
            this.bModeAct1.UseVisualStyleBackColor = false;
            this.bModeAct1.Visible = false;
            this.bModeAct1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lSatationName1
            // 
            this.lSatationName1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSatationName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lSatationName1.Location = new System.Drawing.Point(253, 0);
            this.lSatationName1.Name = "lSatationName1";
            this.lSatationName1.Size = new System.Drawing.Size(78, 44);
            this.lSatationName1.TabIndex = 0;
            this.lSatationName1.Text = "Stacja 1";
            this.lSatationName1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel27);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(593, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(584, 44);
            this.panel3.TabIndex = 2;
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 3;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel27.Controls.Add(this.lOperatorName2, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.bModeAct2, 2, 0);
            this.tableLayoutPanel27.Controls.Add(this.lSatationName2, 1, 0);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(584, 44);
            this.tableLayoutPanel27.TabIndex = 3;
            // 
            // lOperatorName2
            // 
            this.lOperatorName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lOperatorName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lOperatorName2.Location = new System.Drawing.Point(3, 0);
            this.lOperatorName2.Name = "lOperatorName2";
            this.lOperatorName2.Size = new System.Drawing.Size(244, 44);
            this.lOperatorName2.TabIndex = 3;
            this.lOperatorName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bModeAct2
            // 
            this.bModeAct2.BackColor = System.Drawing.Color.Lime;
            this.bModeAct2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bModeAct2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bModeAct2.Location = new System.Drawing.Point(334, 0);
            this.bModeAct2.Margin = new System.Windows.Forms.Padding(0);
            this.bModeAct2.Name = "bModeAct2";
            this.bModeAct2.Size = new System.Drawing.Size(250, 44);
            this.bModeAct2.TabIndex = 1;
            this.bModeAct2.Text = "Tryb";
            this.bModeAct2.UseVisualStyleBackColor = false;
            this.bModeAct2.Visible = false;
            // 
            // lSatationName2
            // 
            this.lSatationName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSatationName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lSatationName2.Location = new System.Drawing.Point(253, 0);
            this.lSatationName2.Name = "lSatationName2";
            this.lSatationName2.Size = new System.Drawing.Size(78, 44);
            this.lSatationName2.TabIndex = 0;
            this.lSatationName2.Text = "Stacja 2";
            this.lSatationName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tcMainG1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 53);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(584, 526);
            this.panel2.TabIndex = 1;
            // 
            // tcMainG1
            // 
            this.tcMainG1.Controls.Add(this.tpMain1);
            this.tcMainG1.Controls.Add(this.tpLogin1);
            this.tcMainG1.Controls.Add(this.tpStorageUnit1);
            this.tcMainG1.Controls.Add(this.tpStoragePrintLabel1);
            this.tcMainG1.Controls.Add(this.tpAllocation1);
            this.tcMainG1.Controls.Add(this.tpAllocationChange1);
            this.tcMainG1.Controls.Add(this.tpIssuing1);
            this.tcMainG1.Controls.Add(this.tpReport1);
            this.tcMainG1.Controls.Add(this.tpDebug1);
            this.tcMainG1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMainG1.Location = new System.Drawing.Point(0, 0);
            this.tcMainG1.Name = "tcMainG1";
            this.tcMainG1.SelectedIndex = 0;
            this.tcMainG1.Size = new System.Drawing.Size(584, 526);
            this.tcMainG1.TabIndex = 0;
            // 
            // tpMain1
            // 
            this.tpMain1.Controls.Add(this.label8);
            this.tpMain1.Location = new System.Drawing.Point(4, 22);
            this.tpMain1.Name = "tpMain1";
            this.tpMain1.Padding = new System.Windows.Forms.Padding(3);
            this.tpMain1.Size = new System.Drawing.Size(576, 500);
            this.tpMain1.TabIndex = 0;
            this.tpMain1.Text = "Praca";
            this.tpMain1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(570, 494);
            this.label8.TabIndex = 0;
            this.label8.Text = "Aby rozpocząć zeskanuj kod z trybem pracy";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpLogin1
            // 
            this.tpLogin1.Controls.Add(this.bLoginLog1);
            this.tpLogin1.Controls.Add(this.tbLoginPass1);
            this.tpLogin1.Controls.Add(this.label3);
            this.tpLogin1.Controls.Add(this.label2);
            this.tpLogin1.Controls.Add(this.label1);
            this.tpLogin1.Controls.Add(this.cbLoginUsers1);
            this.tpLogin1.Location = new System.Drawing.Point(4, 22);
            this.tpLogin1.Name = "tpLogin1";
            this.tpLogin1.Size = new System.Drawing.Size(576, 500);
            this.tpLogin1.TabIndex = 2;
            this.tpLogin1.Text = "Login";
            this.tpLogin1.UseVisualStyleBackColor = true;
            // 
            // bLoginLog1
            // 
            this.bLoginLog1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bLoginLog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bLoginLog1.Location = new System.Drawing.Point(63, 251);
            this.bLoginLog1.Name = "bLoginLog1";
            this.bLoginLog1.Size = new System.Drawing.Size(371, 38);
            this.bLoginLog1.TabIndex = 10;
            this.bLoginLog1.Text = "Zaloguj";
            this.bLoginLog1.UseVisualStyleBackColor = true;
            this.bLoginLog1.Click += new System.EventHandler(this.bLoginLog1_Click);
            // 
            // tbLoginPass1
            // 
            this.tbLoginPass1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLoginPass1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLoginPass1.Location = new System.Drawing.Point(63, 207);
            this.tbLoginPass1.Name = "tbLoginPass1";
            this.tbLoginPass1.Size = new System.Drawing.Size(371, 29);
            this.tbLoginPass1.TabIndex = 9;
            this.tbLoginPass1.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(74, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Hasło";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(74, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Użytkownik";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(63, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(371, 31);
            this.label1.TabIndex = 6;
            this.label1.Text = "Logowanie";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbLoginUsers1
            // 
            this.cbLoginUsers1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLoginUsers1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLoginUsers1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbLoginUsers1.FormattingEnabled = true;
            this.cbLoginUsers1.Location = new System.Drawing.Point(63, 122);
            this.cbLoginUsers1.Name = "cbLoginUsers1";
            this.cbLoginUsers1.Size = new System.Drawing.Size(371, 32);
            this.cbLoginUsers1.TabIndex = 4;
            // 
            // tpStorageUnit1
            // 
            this.tpStorageUnit1.Controls.Add(this.tableLayoutPanel2);
            this.tpStorageUnit1.Location = new System.Drawing.Point(4, 22);
            this.tpStorageUnit1.Name = "tpStorageUnit1";
            this.tpStorageUnit1.Size = new System.Drawing.Size(576, 500);
            this.tpStorageUnit1.TabIndex = 3;
            this.tpStorageUnit1.Text = "Miejsce odkładcze";
            this.tpStorageUnit1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.Controls.Add(this.panel8, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel9, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel10, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel11, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel2.SetColumnSpan(this.panel8, 3);
            this.panel8.Controls.Add(this.dgvStorageList1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 28);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(570, 314);
            this.panel8.TabIndex = 1;
            // 
            // dgvStorageList1
            // 
            this.dgvStorageList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStorageList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStorageList1.Location = new System.Drawing.Point(0, 0);
            this.dgvStorageList1.Name = "dgvStorageList1";
            this.dgvStorageList1.Size = new System.Drawing.Size(566, 310);
            this.dgvStorageList1.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel2.SetColumnSpan(this.panel9, 3);
            this.panel9.Controls.Add(this.tableLayoutPanel3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 373);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(570, 124);
            this.panel9.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lStorageUnitNewAdress1, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lStorageUnitNewTimeStamp1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.lStorageUnitNewUnit1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lStorageUnitNewCourierType1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(566, 120);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lStorageUnitNewAdress1
            // 
            this.lStorageUnitNewAdress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewAdress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewAdress1.Location = new System.Drawing.Point(286, 90);
            this.lStorageUnitNewAdress1.Name = "lStorageUnitNewAdress1";
            this.lStorageUnitNewAdress1.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewAdress1.TabIndex = 9;
            this.lStorageUnitNewAdress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStorageUnitNewTimeStamp1
            // 
            this.lStorageUnitNewTimeStamp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewTimeStamp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewTimeStamp1.Location = new System.Drawing.Point(286, 60);
            this.lStorageUnitNewTimeStamp1.Name = "lStorageUnitNewTimeStamp1";
            this.lStorageUnitNewTimeStamp1.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewTimeStamp1.TabIndex = 7;
            this.lStorageUnitNewTimeStamp1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStorageUnitNewUnit1
            // 
            this.lStorageUnitNewUnit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewUnit1.Location = new System.Drawing.Point(286, 30);
            this.lStorageUnitNewUnit1.Name = "lStorageUnitNewUnit1";
            this.lStorageUnitNewUnit1.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewUnit1.TabIndex = 6;
            this.lStorageUnitNewUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStorageUnitNewCourierType1
            // 
            this.lStorageUnitNewCourierType1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewCourierType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewCourierType1.Location = new System.Drawing.Point(286, 0);
            this.lStorageUnitNewCourierType1.Name = "lStorageUnitNewCourierType1";
            this.lStorageUnitNewCourierType1.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewCourierType1.TabIndex = 5;
            this.lStorageUnitNewCourierType1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(3, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(277, 30);
            this.label16.TabIndex = 4;
            this.label16.Text = "Odbiorca";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(3, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(277, 30);
            this.label14.TabIndex = 2;
            this.label14.Text = "Data";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(3, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(277, 30);
            this.label13.TabIndex = 1;
            this.label13.Text = "Miejsce";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(277, 30);
            this.label12.TabIndex = 0;
            this.label12.Text = "Kurier";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel10, 3);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(576, 25);
            this.panel10.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(576, 25);
            this.label10.TabIndex = 0;
            this.label10.Text = "Lista";
            // 
            // panel11
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel11, 3);
            this.panel11.Controls.Add(this.label11);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 345);
            this.panel11.Margin = new System.Windows.Forms.Padding(0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(576, 25);
            this.panel11.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(576, 25);
            this.label11.TabIndex = 1;
            this.label11.Text = "Nowe miejsce odkładcze";
            // 
            // tpStoragePrintLabel1
            // 
            this.tpStoragePrintLabel1.Controls.Add(this.tableLayoutPanel7);
            this.tpStoragePrintLabel1.Location = new System.Drawing.Point(4, 22);
            this.tpStoragePrintLabel1.Name = "tpStoragePrintLabel1";
            this.tpStoragePrintLabel1.Size = new System.Drawing.Size(576, 500);
            this.tpStoragePrintLabel1.TabIndex = 7;
            this.tpStoragePrintLabel1.Text = "Dodruk";
            this.tpStoragePrintLabel1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel7.Controls.Add(this.panel17, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel7.SetColumnSpan(this.panel17, 3);
            this.panel17.Controls.Add(this.tableLayoutPanel8);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(3, 43);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(570, 124);
            this.panel17.TabIndex = 2;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label7, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.label15, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.label19, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.label20, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label21, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.label22, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label27, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label29, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(566, 120);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(286, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(277, 30);
            this.label7.TabIndex = 9;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(286, 60);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(277, 30);
            this.label15.TabIndex = 7;
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(286, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(277, 30);
            this.label19.TabIndex = 6;
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(286, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(277, 30);
            this.label20.TabIndex = 5;
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.Location = new System.Drawing.Point(3, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(277, 30);
            this.label21.TabIndex = 4;
            this.label21.Text = "Odbiorca";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.Location = new System.Drawing.Point(3, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(277, 30);
            this.label22.TabIndex = 2;
            this.label22.Text = "Data";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.Location = new System.Drawing.Point(3, 30);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(277, 30);
            this.label27.TabIndex = 1;
            this.label27.Text = "Miejsce";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label29.Location = new System.Drawing.Point(3, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(277, 30);
            this.label29.TabIndex = 0;
            this.label29.Text = "Kurier";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.tableLayoutPanel7.SetColumnSpan(this.panel5, 3);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(576, 40);
            this.panel5.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(576, 40);
            this.label30.TabIndex = 1;
            this.label30.Text = "Dodruk etykiety dla:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tpAllocation1
            // 
            this.tpAllocation1.Controls.Add(this.tableLayoutPanel4);
            this.tpAllocation1.Location = new System.Drawing.Point(4, 22);
            this.tpAllocation1.Name = "tpAllocation1";
            this.tpAllocation1.Size = new System.Drawing.Size(576, 500);
            this.tpAllocation1.TabIndex = 4;
            this.tpAllocation1.Text = "Alokacja";
            this.tpAllocation1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel16, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.panel15, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.panel14, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.panel13, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel12, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel4.SetColumnSpan(this.panel16, 3);
            this.panel16.Controls.Add(this.dgvAllocationList1);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(3, 248);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(570, 249);
            this.panel16.TabIndex = 7;
            // 
            // dgvAllocationList1
            // 
            this.dgvAllocationList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllocationList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAllocationList1.Location = new System.Drawing.Point(0, 0);
            this.dgvAllocationList1.Name = "dgvAllocationList1";
            this.dgvAllocationList1.Size = new System.Drawing.Size(566, 245);
            this.dgvAllocationList1.TabIndex = 0;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel4.SetColumnSpan(this.panel15, 3);
            this.panel15.Controls.Add(this.tableLayoutPanel5);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(3, 73);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(570, 144);
            this.panel15.TabIndex = 6;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.label28, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.lAllocationAdress1, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.lAllocationCount1, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.lAllocationTimeStamp1, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.lAllocationUnit1, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.lAllocationCourierType1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label23, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label25, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label26, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(566, 140);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.Location = new System.Drawing.Point(3, 112);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(277, 28);
            this.label28.TabIndex = 11;
            this.label28.Text = "Odbiorca";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationAdress1
            // 
            this.lAllocationAdress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationAdress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationAdress1.Location = new System.Drawing.Point(286, 112);
            this.lAllocationAdress1.Name = "lAllocationAdress1";
            this.lAllocationAdress1.Size = new System.Drawing.Size(277, 28);
            this.lAllocationAdress1.TabIndex = 10;
            this.lAllocationAdress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationCount1
            // 
            this.lAllocationCount1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationCount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationCount1.Location = new System.Drawing.Point(286, 84);
            this.lAllocationCount1.Name = "lAllocationCount1";
            this.lAllocationCount1.Size = new System.Drawing.Size(277, 28);
            this.lAllocationCount1.TabIndex = 9;
            this.lAllocationCount1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationTimeStamp1
            // 
            this.lAllocationTimeStamp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationTimeStamp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationTimeStamp1.Location = new System.Drawing.Point(286, 56);
            this.lAllocationTimeStamp1.Name = "lAllocationTimeStamp1";
            this.lAllocationTimeStamp1.Size = new System.Drawing.Size(277, 28);
            this.lAllocationTimeStamp1.TabIndex = 7;
            this.lAllocationTimeStamp1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationUnit1
            // 
            this.lAllocationUnit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationUnit1.Location = new System.Drawing.Point(286, 28);
            this.lAllocationUnit1.Name = "lAllocationUnit1";
            this.lAllocationUnit1.Size = new System.Drawing.Size(277, 28);
            this.lAllocationUnit1.TabIndex = 6;
            this.lAllocationUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationCourierType1
            // 
            this.lAllocationCourierType1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationCourierType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationCourierType1.Location = new System.Drawing.Point(286, 0);
            this.lAllocationCourierType1.Name = "lAllocationCourierType1";
            this.lAllocationCourierType1.Size = new System.Drawing.Size(277, 28);
            this.lAllocationCourierType1.TabIndex = 5;
            this.lAllocationCourierType1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.Location = new System.Drawing.Point(3, 84);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(277, 28);
            this.label23.TabIndex = 4;
            this.label23.Text = "Ilość paczek / podpaczek";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.Location = new System.Drawing.Point(3, 56);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(277, 28);
            this.label24.TabIndex = 2;
            this.label24.Text = "Data";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.Location = new System.Drawing.Point(3, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(277, 28);
            this.label25.TabIndex = 1;
            this.label25.Text = "Miejsce";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.Location = new System.Drawing.Point(3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(277, 28);
            this.label26.TabIndex = 0;
            this.label26.Text = "Kurier";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.panel14, 3);
            this.panel14.Controls.Add(this.lAllocationInfo1);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 220);
            this.panel14.Margin = new System.Windows.Forms.Padding(0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(576, 25);
            this.panel14.TabIndex = 5;
            // 
            // lAllocationInfo1
            // 
            this.lAllocationInfo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationInfo1.ForeColor = System.Drawing.Color.Red;
            this.lAllocationInfo1.Location = new System.Drawing.Point(0, 0);
            this.lAllocationInfo1.Name = "lAllocationInfo1";
            this.lAllocationInfo1.Size = new System.Drawing.Size(576, 25);
            this.lAllocationInfo1.TabIndex = 1;
            this.lAllocationInfo1.Text = "Lista paczek";
            this.lAllocationInfo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.panel13, 3);
            this.panel13.Controls.Add(this.label17);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 45);
            this.panel13.Margin = new System.Windows.Forms.Padding(0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(576, 25);
            this.panel13.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(576, 25);
            this.label17.TabIndex = 0;
            this.label17.Text = "Miejsce odkładcze";
            // 
            // panel12
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.panel12, 3);
            this.panel12.Controls.Add(this.lAllocationName1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(570, 39);
            this.panel12.TabIndex = 2;
            // 
            // lAllocationName1
            // 
            this.lAllocationName1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationName1.Location = new System.Drawing.Point(0, 0);
            this.lAllocationName1.Name = "lAllocationName1";
            this.lAllocationName1.Size = new System.Drawing.Size(570, 39);
            this.lAllocationName1.TabIndex = 0;
            this.lAllocationName1.Text = "Allokacja paczek";
            this.lAllocationName1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpAllocationChange1
            // 
            this.tpAllocationChange1.Controls.Add(this.tableLayoutPanel12);
            this.tpAllocationChange1.Location = new System.Drawing.Point(4, 22);
            this.tpAllocationChange1.Name = "tpAllocationChange1";
            this.tpAllocationChange1.Padding = new System.Windows.Forms.Padding(3);
            this.tpAllocationChange1.Size = new System.Drawing.Size(576, 500);
            this.tpAllocationChange1.TabIndex = 8;
            this.tpAllocationChange1.Text = "Zmiana alokacji";
            this.tpAllocationChange1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.label68, 0, 5);
            this.tableLayoutPanel12.Controls.Add(this.label67, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.label66, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.label65, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.panel24, 0, 6);
            this.tableLayoutPanel12.Controls.Add(this.panel23, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.dgvReAllocationSourceList1, 0, 4);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 7;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(570, 494);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // label68
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.label68, 2);
            this.label68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label68.Location = new System.Drawing.Point(3, 343);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(564, 25);
            this.label68.TabIndex = 13;
            this.label68.Text = "Nowe miejsce odkładcze";
            // 
            // label67
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.label67, 2);
            this.label67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label67.Location = new System.Drawing.Point(3, 194);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(564, 25);
            this.label67.TabIndex = 12;
            this.label67.Text = "Paczka";
            // 
            // label66
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.label66, 2);
            this.label66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label66.Location = new System.Drawing.Point(3, 45);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(564, 25);
            this.label66.TabIndex = 11;
            this.label66.Text = "Aktualne miejsce odkładcze";
            // 
            // label65
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.label65, 2);
            this.label65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label65.Location = new System.Drawing.Point(3, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(564, 45);
            this.label65.TabIndex = 10;
            this.label65.Text = "Zmiana alokacji";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel12.SetColumnSpan(this.panel24, 3);
            this.panel24.Controls.Add(this.tableLayoutPanel14);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(3, 371);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(564, 120);
            this.panel24.TabIndex = 8;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.label55, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.lReAllocationDestinationAdress1, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.lReAllocationDestinationCount1, 1, 3);
            this.tableLayoutPanel14.Controls.Add(this.lReAllocationDestinationTimeStamp1, 1, 2);
            this.tableLayoutPanel14.Controls.Add(this.lReAllocationDestinationUnit1, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.lReAllocationDestinationCourierType1, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.label61, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.label62, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.label63, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label64, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 5;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(560, 116);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // label55
            // 
            this.label55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label55.Location = new System.Drawing.Point(3, 92);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(274, 24);
            this.label55.TabIndex = 11;
            this.label55.Text = "Odbiorca";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationAdress1
            // 
            this.lReAllocationDestinationAdress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationAdress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationAdress1.Location = new System.Drawing.Point(283, 92);
            this.lReAllocationDestinationAdress1.Name = "lReAllocationDestinationAdress1";
            this.lReAllocationDestinationAdress1.Size = new System.Drawing.Size(274, 24);
            this.lReAllocationDestinationAdress1.TabIndex = 10;
            this.lReAllocationDestinationAdress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationCount1
            // 
            this.lReAllocationDestinationCount1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationCount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationCount1.Location = new System.Drawing.Point(283, 69);
            this.lReAllocationDestinationCount1.Name = "lReAllocationDestinationCount1";
            this.lReAllocationDestinationCount1.Size = new System.Drawing.Size(274, 23);
            this.lReAllocationDestinationCount1.TabIndex = 9;
            this.lReAllocationDestinationCount1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationTimeStamp1
            // 
            this.lReAllocationDestinationTimeStamp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationTimeStamp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationTimeStamp1.Location = new System.Drawing.Point(283, 46);
            this.lReAllocationDestinationTimeStamp1.Name = "lReAllocationDestinationTimeStamp1";
            this.lReAllocationDestinationTimeStamp1.Size = new System.Drawing.Size(274, 23);
            this.lReAllocationDestinationTimeStamp1.TabIndex = 7;
            this.lReAllocationDestinationTimeStamp1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationUnit1
            // 
            this.lReAllocationDestinationUnit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationUnit1.Location = new System.Drawing.Point(283, 23);
            this.lReAllocationDestinationUnit1.Name = "lReAllocationDestinationUnit1";
            this.lReAllocationDestinationUnit1.Size = new System.Drawing.Size(274, 23);
            this.lReAllocationDestinationUnit1.TabIndex = 6;
            this.lReAllocationDestinationUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationCourierType1
            // 
            this.lReAllocationDestinationCourierType1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationCourierType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationCourierType1.Location = new System.Drawing.Point(283, 0);
            this.lReAllocationDestinationCourierType1.Name = "lReAllocationDestinationCourierType1";
            this.lReAllocationDestinationCourierType1.Size = new System.Drawing.Size(274, 23);
            this.lReAllocationDestinationCourierType1.TabIndex = 5;
            this.lReAllocationDestinationCourierType1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label61
            // 
            this.label61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label61.Location = new System.Drawing.Point(3, 69);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(274, 23);
            this.label61.TabIndex = 4;
            this.label61.Text = "Ilość paczek / podpaczek";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label62
            // 
            this.label62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label62.Location = new System.Drawing.Point(3, 46);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(274, 23);
            this.label62.TabIndex = 2;
            this.label62.Text = "Data";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label63
            // 
            this.label63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label63.Location = new System.Drawing.Point(3, 23);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(274, 23);
            this.label63.TabIndex = 1;
            this.label63.Text = "Miejsce";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label64
            // 
            this.label64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label64.Location = new System.Drawing.Point(3, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(274, 23);
            this.label64.TabIndex = 0;
            this.label64.Text = "Kurier";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel12.SetColumnSpan(this.panel23, 3);
            this.panel23.Controls.Add(this.tableLayoutPanel13);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(3, 73);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(564, 118);
            this.panel23.TabIndex = 7;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.label38, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.lReAllocationSourceAdress1, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.lReAllocationSourceCount1, 1, 3);
            this.tableLayoutPanel13.Controls.Add(this.lReAllocationSourceTimeStamp1, 1, 2);
            this.tableLayoutPanel13.Controls.Add(this.lReAllocationSourceUnit1, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.lReAllocationSourceCourierType1, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.label51, 0, 3);
            this.tableLayoutPanel13.Controls.Add(this.label52, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.label53, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.label54, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 5;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(560, 114);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label38.Location = new System.Drawing.Point(3, 88);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(274, 26);
            this.label38.TabIndex = 11;
            this.label38.Text = "Odbiorca";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceAdress1
            // 
            this.lReAllocationSourceAdress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceAdress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceAdress1.Location = new System.Drawing.Point(283, 88);
            this.lReAllocationSourceAdress1.Name = "lReAllocationSourceAdress1";
            this.lReAllocationSourceAdress1.Size = new System.Drawing.Size(274, 26);
            this.lReAllocationSourceAdress1.TabIndex = 10;
            this.lReAllocationSourceAdress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceCount1
            // 
            this.lReAllocationSourceCount1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceCount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceCount1.Location = new System.Drawing.Point(283, 66);
            this.lReAllocationSourceCount1.Name = "lReAllocationSourceCount1";
            this.lReAllocationSourceCount1.Size = new System.Drawing.Size(274, 22);
            this.lReAllocationSourceCount1.TabIndex = 9;
            this.lReAllocationSourceCount1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceTimeStamp1
            // 
            this.lReAllocationSourceTimeStamp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceTimeStamp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceTimeStamp1.Location = new System.Drawing.Point(283, 44);
            this.lReAllocationSourceTimeStamp1.Name = "lReAllocationSourceTimeStamp1";
            this.lReAllocationSourceTimeStamp1.Size = new System.Drawing.Size(274, 22);
            this.lReAllocationSourceTimeStamp1.TabIndex = 7;
            this.lReAllocationSourceTimeStamp1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceUnit1
            // 
            this.lReAllocationSourceUnit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceUnit1.Location = new System.Drawing.Point(283, 22);
            this.lReAllocationSourceUnit1.Name = "lReAllocationSourceUnit1";
            this.lReAllocationSourceUnit1.Size = new System.Drawing.Size(274, 22);
            this.lReAllocationSourceUnit1.TabIndex = 6;
            this.lReAllocationSourceUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceCourierType1
            // 
            this.lReAllocationSourceCourierType1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceCourierType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceCourierType1.Location = new System.Drawing.Point(283, 0);
            this.lReAllocationSourceCourierType1.Name = "lReAllocationSourceCourierType1";
            this.lReAllocationSourceCourierType1.Size = new System.Drawing.Size(274, 22);
            this.lReAllocationSourceCourierType1.TabIndex = 5;
            this.lReAllocationSourceCourierType1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label51.Location = new System.Drawing.Point(3, 66);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(274, 22);
            this.label51.TabIndex = 4;
            this.label51.Text = "Ilość paczek / podpaczek";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label52.Location = new System.Drawing.Point(3, 44);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(274, 22);
            this.label52.TabIndex = 2;
            this.label52.Text = "Data";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            this.label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label53.Location = new System.Drawing.Point(3, 22);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(274, 22);
            this.label53.TabIndex = 1;
            this.label53.Text = "Miejsce";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label54.Location = new System.Drawing.Point(3, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(274, 22);
            this.label54.TabIndex = 0;
            this.label54.Text = "Kurier";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvReAllocationSourceList1
            // 
            this.dgvReAllocationSourceList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel12.SetColumnSpan(this.dgvReAllocationSourceList1, 2);
            this.dgvReAllocationSourceList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReAllocationSourceList1.Location = new System.Drawing.Point(3, 222);
            this.dgvReAllocationSourceList1.Name = "dgvReAllocationSourceList1";
            this.dgvReAllocationSourceList1.Size = new System.Drawing.Size(564, 118);
            this.dgvReAllocationSourceList1.TabIndex = 9;
            // 
            // tpIssuing1
            // 
            this.tpIssuing1.Controls.Add(this.tableLayoutPanel10);
            this.tpIssuing1.Location = new System.Drawing.Point(4, 22);
            this.tpIssuing1.Name = "tpIssuing1";
            this.tpIssuing1.Size = new System.Drawing.Size(576, 500);
            this.tpIssuing1.TabIndex = 5;
            this.tpIssuing1.Text = "Wydawanie";
            this.tpIssuing1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.panel18, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.panel19, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.panel20, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.panel21, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.panel22, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 5;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel10.SetColumnSpan(this.panel18, 3);
            this.panel18.Controls.Add(this.dgvIssuingList1);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(3, 248);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(570, 249);
            this.panel18.TabIndex = 7;
            // 
            // dgvIssuingList1
            // 
            this.dgvIssuingList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIssuingList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIssuingList1.Location = new System.Drawing.Point(0, 0);
            this.dgvIssuingList1.Name = "dgvIssuingList1";
            this.dgvIssuingList1.Size = new System.Drawing.Size(566, 245);
            this.dgvIssuingList1.TabIndex = 0;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel10.SetColumnSpan(this.panel19, 3);
            this.panel19.Controls.Add(this.tableLayoutPanel11);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(3, 73);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(570, 144);
            this.panel19.TabIndex = 6;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.label37, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.lIssuingAdress1, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.lIssuingCount1, 1, 3);
            this.tableLayoutPanel11.Controls.Add(this.lIssuingTimeStamp1, 1, 2);
            this.tableLayoutPanel11.Controls.Add(this.lIssuingUnit1, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.lIssuingCourierType1, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label43, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.label44, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.label45, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.label46, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 5;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(566, 140);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label37.Location = new System.Drawing.Point(3, 112);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(277, 28);
            this.label37.TabIndex = 11;
            this.label37.Text = "Odbiorca";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingAdress1
            // 
            this.lIssuingAdress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingAdress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingAdress1.Location = new System.Drawing.Point(286, 112);
            this.lIssuingAdress1.Name = "lIssuingAdress1";
            this.lIssuingAdress1.Size = new System.Drawing.Size(277, 28);
            this.lIssuingAdress1.TabIndex = 10;
            this.lIssuingAdress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingCount1
            // 
            this.lIssuingCount1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingCount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingCount1.Location = new System.Drawing.Point(286, 84);
            this.lIssuingCount1.Name = "lIssuingCount1";
            this.lIssuingCount1.Size = new System.Drawing.Size(277, 28);
            this.lIssuingCount1.TabIndex = 9;
            this.lIssuingCount1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingTimeStamp1
            // 
            this.lIssuingTimeStamp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingTimeStamp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingTimeStamp1.Location = new System.Drawing.Point(286, 56);
            this.lIssuingTimeStamp1.Name = "lIssuingTimeStamp1";
            this.lIssuingTimeStamp1.Size = new System.Drawing.Size(277, 28);
            this.lIssuingTimeStamp1.TabIndex = 7;
            this.lIssuingTimeStamp1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingUnit1
            // 
            this.lIssuingUnit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingUnit1.Location = new System.Drawing.Point(286, 28);
            this.lIssuingUnit1.Name = "lIssuingUnit1";
            this.lIssuingUnit1.Size = new System.Drawing.Size(277, 28);
            this.lIssuingUnit1.TabIndex = 6;
            this.lIssuingUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingCourierType1
            // 
            this.lIssuingCourierType1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingCourierType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingCourierType1.Location = new System.Drawing.Point(286, 0);
            this.lIssuingCourierType1.Name = "lIssuingCourierType1";
            this.lIssuingCourierType1.Size = new System.Drawing.Size(277, 28);
            this.lIssuingCourierType1.TabIndex = 5;
            this.lIssuingCourierType1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label43.Location = new System.Drawing.Point(3, 84);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(277, 28);
            this.label43.TabIndex = 4;
            this.label43.Text = "Ilość paczek / podpaczek";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label44.Location = new System.Drawing.Point(3, 56);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(277, 28);
            this.label44.TabIndex = 2;
            this.label44.Text = "Data";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label45.Location = new System.Drawing.Point(3, 28);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(277, 28);
            this.label45.TabIndex = 1;
            this.label45.Text = "Miejsce";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label46.Location = new System.Drawing.Point(3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(277, 28);
            this.label46.TabIndex = 0;
            this.label46.Text = "Kurier";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel20
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.panel20, 3);
            this.panel20.Controls.Add(this.label47);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(0, 220);
            this.panel20.Margin = new System.Windows.Forms.Padding(0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(576, 25);
            this.panel20.TabIndex = 5;
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(576, 25);
            this.label47.TabIndex = 1;
            this.label47.Text = "Lista paczek wydanych z tego miejsca";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel21
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.panel21, 3);
            this.panel21.Controls.Add(this.label48);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(0, 45);
            this.panel21.Margin = new System.Windows.Forms.Padding(0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(576, 25);
            this.panel21.TabIndex = 4;
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label48.Location = new System.Drawing.Point(0, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(576, 25);
            this.label48.TabIndex = 0;
            this.label48.Text = "Miejsce odkładcze";
            // 
            // panel22
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.panel22, 3);
            this.panel22.Controls.Add(this.label49);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(570, 39);
            this.panel22.TabIndex = 2;
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label49.Location = new System.Drawing.Point(0, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(570, 39);
            this.label49.TabIndex = 0;
            this.label49.Text = "Wydawanie paczek";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpReport1
            // 
            this.tpReport1.Controls.Add(this.tableLayoutPanel9);
            this.tpReport1.Location = new System.Drawing.Point(4, 22);
            this.tpReport1.Name = "tpReport1";
            this.tpReport1.Size = new System.Drawing.Size(576, 500);
            this.tpReport1.TabIndex = 6;
            this.tpReport1.Text = "Raport";
            this.tpReport1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.dgvReport1, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.tcReport1, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // dgvReport1
            // 
            this.dgvReport1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel9.SetColumnSpan(this.dgvReport1, 2);
            this.dgvReport1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReport1.Location = new System.Drawing.Point(3, 256);
            this.dgvReport1.Name = "dgvReport1";
            this.dgvReport1.Size = new System.Drawing.Size(570, 241);
            this.dgvReport1.TabIndex = 0;
            this.dgvReport1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReport1_CellValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bReportStroerWaw1);
            this.groupBox1.Controls.Add(this.bReportStroer1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(282, 77);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Raporty STROER";
            // 
            // bReportStroerWaw1
            // 
            this.bReportStroerWaw1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportStroerWaw1.Location = new System.Drawing.Point(6, 48);
            this.bReportStroerWaw1.Name = "bReportStroerWaw1";
            this.bReportStroerWaw1.Size = new System.Drawing.Size(270, 23);
            this.bReportStroerWaw1.TabIndex = 1;
            this.bReportStroerWaw1.Text = "Raport po dacie wysyłki STROER WARSZAWA";
            this.bReportStroerWaw1.UseVisualStyleBackColor = true;
            this.bReportStroerWaw1.Click += new System.EventHandler(this.bReportStroerWaw1_Click);
            // 
            // bReportStroer1
            // 
            this.bReportStroer1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportStroer1.Location = new System.Drawing.Point(6, 19);
            this.bReportStroer1.Name = "bReportStroer1";
            this.bReportStroer1.Size = new System.Drawing.Size(270, 23);
            this.bReportStroer1.TabIndex = 0;
            this.bReportStroer1.Text = "Raport po dacie wysyłki STROER";
            this.bReportStroer1.UseVisualStyleBackColor = true;
            this.bReportStroer1.Click += new System.EventHandler(this.bReportStroer1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bReportUnit1);
            this.groupBox2.Controls.Add(this.bReportZpl1);
            this.groupBox2.Controls.Add(this.bReportKpLkp1);
            this.groupBox2.Controls.Add(this.bReportKp1);
            this.groupBox2.Controls.Add(this.bReportDate1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 86);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(282, 164);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Raporty pozostałe";
            // 
            // bReportUnit1
            // 
            this.bReportUnit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportUnit1.Location = new System.Drawing.Point(6, 135);
            this.bReportUnit1.Name = "bReportUnit1";
            this.bReportUnit1.Size = new System.Drawing.Size(270, 23);
            this.bReportUnit1.TabIndex = 5;
            this.bReportUnit1.Text = "Raport po dacie wysyłki i miejscu";
            this.bReportUnit1.UseVisualStyleBackColor = true;
            this.bReportUnit1.Click += new System.EventHandler(this.bReportUnit1_Click);
            // 
            // bReportZpl1
            // 
            this.bReportZpl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportZpl1.Location = new System.Drawing.Point(6, 106);
            this.bReportZpl1.Name = "bReportZpl1";
            this.bReportZpl1.Size = new System.Drawing.Size(270, 23);
            this.bReportZpl1.TabIndex = 4;
            this.bReportZpl1.Text = "Raport po kodzie z plakatu (CAŁA KP)";
            this.bReportZpl1.UseVisualStyleBackColor = true;
            this.bReportZpl1.Click += new System.EventHandler(this.bReportZpl1_Click);
            // 
            // bReportKpLkp1
            // 
            this.bReportKpLkp1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportKpLkp1.Location = new System.Drawing.Point(6, 77);
            this.bReportKpLkp1.Name = "bReportKpLkp1";
            this.bReportKpLkp1.Size = new System.Drawing.Size(270, 23);
            this.bReportKpLkp1.TabIndex = 3;
            this.bReportKpLkp1.Text = "Raport po numerze KP i LKP";
            this.bReportKpLkp1.UseVisualStyleBackColor = true;
            this.bReportKpLkp1.Click += new System.EventHandler(this.bReportKpLkp1_Click);
            // 
            // bReportKp1
            // 
            this.bReportKp1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportKp1.Location = new System.Drawing.Point(6, 48);
            this.bReportKp1.Name = "bReportKp1";
            this.bReportKp1.Size = new System.Drawing.Size(270, 23);
            this.bReportKp1.TabIndex = 2;
            this.bReportKp1.Text = "Raport po numerze KP";
            this.bReportKp1.UseVisualStyleBackColor = true;
            this.bReportKp1.Click += new System.EventHandler(this.bReportKp1_Click);
            // 
            // bReportDate1
            // 
            this.bReportDate1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportDate1.Location = new System.Drawing.Point(6, 19);
            this.bReportDate1.Name = "bReportDate1";
            this.bReportDate1.Size = new System.Drawing.Size(270, 23);
            this.bReportDate1.TabIndex = 1;
            this.bReportDate1.Text = "Raport po dacie wysyłki";
            this.bReportDate1.UseVisualStyleBackColor = true;
            this.bReportDate1.Click += new System.EventHandler(this.bReportDate1_Click);
            // 
            // tcReport1
            // 
            this.tcReport1.Controls.Add(this.tpParamDate1);
            this.tcReport1.Controls.Add(this.tpParamKp1);
            this.tcReport1.Controls.Add(this.tpParamKpLkp1);
            this.tcReport1.Controls.Add(this.tpParamZpl1);
            this.tcReport1.Controls.Add(this.tpParamUnit1);
            this.tcReport1.Controls.Add(this.tpReportInfo1);
            this.tcReport1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReport1.Location = new System.Drawing.Point(291, 3);
            this.tcReport1.Name = "tcReport1";
            this.tableLayoutPanel9.SetRowSpan(this.tcReport1, 2);
            this.tcReport1.SelectedIndex = 0;
            this.tcReport1.Size = new System.Drawing.Size(282, 247);
            this.tcReport1.TabIndex = 3;
            this.tcReport1.Click += new System.EventHandler(this.button13_Click);
            // 
            // tpParamDate1
            // 
            this.tpParamDate1.Controls.Add(this.label39);
            this.tpParamDate1.Controls.Add(this.Courier1);
            this.tpParamDate1.Controls.Add(this.dtpReportNS1);
            this.tpParamDate1.Controls.Add(this.dtpReportNP1);
            this.tpParamDate1.Controls.Add(this.bReportPrint11);
            this.tpParamDate1.Controls.Add(this.bReportMake11);
            this.tpParamDate1.Controls.Add(this.label31);
            this.tpParamDate1.Controls.Add(this.dtpReportParam1);
            this.tpParamDate1.Location = new System.Drawing.Point(4, 22);
            this.tpParamDate1.Name = "tpParamDate1";
            this.tpParamDate1.Padding = new System.Windows.Forms.Padding(3);
            this.tpParamDate1.Size = new System.Drawing.Size(274, 221);
            this.tpParamDate1.TabIndex = 0;
            this.tpParamDate1.Text = "Parametry";
            this.tpParamDate1.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 53);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(34, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "Kurier";
            this.label39.Click += new System.EventHandler(this.label39_Click);
            // 
            // Courier1
            // 
            this.Courier1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Courier1.FormattingEnabled = true;
            this.Courier1.Location = new System.Drawing.Point(8, 70);
            this.Courier1.Margin = new System.Windows.Forms.Padding(2);
            this.Courier1.Name = "Courier1";
            this.Courier1.Size = new System.Drawing.Size(260, 21);
            this.Courier1.TabIndex = 6;
            this.Courier1.Text = "WSZYSCY";
            // 
            // dtpReportNS1
            // 
            this.dtpReportNS1.AutoSize = true;
            this.dtpReportNS1.Location = new System.Drawing.Point(8, 116);
            this.dtpReportNS1.Margin = new System.Windows.Forms.Padding(2);
            this.dtpReportNS1.Name = "dtpReportNS1";
            this.dtpReportNS1.Size = new System.Drawing.Size(138, 17);
            this.dtpReportNS1.TabIndex = 5;
            this.dtpReportNS1.Text = "Pokaż tylko niewysłane";
            this.dtpReportNS1.UseVisualStyleBackColor = true;
            // 
            // dtpReportNP1
            // 
            this.dtpReportNP1.AutoSize = true;
            this.dtpReportNP1.Location = new System.Drawing.Point(8, 94);
            this.dtpReportNP1.Margin = new System.Windows.Forms.Padding(2);
            this.dtpReportNP1.Name = "dtpReportNP1";
            this.dtpReportNP1.Size = new System.Drawing.Size(153, 17);
            this.dtpReportNP1.TabIndex = 4;
            this.dtpReportNP1.Text = "Pokaż tylko niespakowane";
            this.dtpReportNP1.UseVisualStyleBackColor = true;
            this.dtpReportNP1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // bReportPrint11
            // 
            this.bReportPrint11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint11.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint11.Name = "bReportPrint11";
            this.bReportPrint11.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint11.TabIndex = 3;
            this.bReportPrint11.Text = "Drukuj";
            this.bReportPrint11.UseVisualStyleBackColor = true;
            this.bReportPrint11.Visible = false;
            this.bReportPrint11.Click += new System.EventHandler(this.button13_Click);
            // 
            // bReportMake11
            // 
            this.bReportMake11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake11.Location = new System.Drawing.Point(6, 167);
            this.bReportMake11.Name = "bReportMake11";
            this.bReportMake11.Size = new System.Drawing.Size(261, 48);
            this.bReportMake11.TabIndex = 2;
            this.bReportMake11.Text = "GENERUJ RAPORT";
            this.bReportMake11.UseVisualStyleBackColor = true;
            this.bReportMake11.Click += new System.EventHandler(this.bReportMake1_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(30, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "Data";
            // 
            // dtpReportParam1
            // 
            this.dtpReportParam1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportParam1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportParam1.Location = new System.Drawing.Point(6, 20);
            this.dtpReportParam1.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.dtpReportParam1.Name = "dtpReportParam1";
            this.dtpReportParam1.Size = new System.Drawing.Size(262, 31);
            this.dtpReportParam1.TabIndex = 0;
            // 
            // tpParamKp1
            // 
            this.tpParamKp1.Controls.Add(this.bReportPrint21);
            this.tpParamKp1.Controls.Add(this.lReportKpError1);
            this.tpParamKp1.Controls.Add(this.bReportMake21);
            this.tpParamKp1.Controls.Add(this.tbReportKp1);
            this.tpParamKp1.Controls.Add(this.label18);
            this.tpParamKp1.Location = new System.Drawing.Point(4, 22);
            this.tpParamKp1.Name = "tpParamKp1";
            this.tpParamKp1.Padding = new System.Windows.Forms.Padding(3);
            this.tpParamKp1.Size = new System.Drawing.Size(274, 221);
            this.tpParamKp1.TabIndex = 1;
            this.tpParamKp1.Text = "Parametry";
            this.tpParamKp1.UseVisualStyleBackColor = true;
            // 
            // bReportPrint21
            // 
            this.bReportPrint21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint21.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint21.Name = "bReportPrint21";
            this.bReportPrint21.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint21.TabIndex = 5;
            this.bReportPrint21.Text = "Drukuj";
            this.bReportPrint21.UseVisualStyleBackColor = true;
            this.bReportPrint21.Visible = false;
            this.bReportPrint21.Click += new System.EventHandler(this.button13_Click);
            // 
            // lReportKpError1
            // 
            this.lReportKpError1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportKpError1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportKpError1.ForeColor = System.Drawing.Color.Red;
            this.lReportKpError1.Location = new System.Drawing.Point(6, 66);
            this.lReportKpError1.Name = "lReportKpError1";
            this.lReportKpError1.Size = new System.Drawing.Size(261, 23);
            this.lReportKpError1.TabIndex = 4;
            this.lReportKpError1.Text = "Błędny numer KP";
            this.lReportKpError1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake21
            // 
            this.bReportMake21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake21.Location = new System.Drawing.Point(6, 167);
            this.bReportMake21.Name = "bReportMake21";
            this.bReportMake21.Size = new System.Drawing.Size(261, 48);
            this.bReportMake21.TabIndex = 3;
            this.bReportMake21.Text = "GENERUJ RAPORT";
            this.bReportMake21.UseVisualStyleBackColor = true;
            this.bReportMake21.Click += new System.EventHandler(this.bReportMake1_Click);
            // 
            // tbReportKp1
            // 
            this.tbReportKp1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportKp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportKp1.Location = new System.Drawing.Point(6, 32);
            this.tbReportKp1.Name = "tbReportKp1";
            this.tbReportKp1.Size = new System.Drawing.Size(262, 31);
            this.tbReportKp1.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Numeru KP";
            // 
            // tpParamKpLkp1
            // 
            this.tpParamKpLkp1.Controls.Add(this.bReportPrint31);
            this.tpParamKpLkp1.Controls.Add(this.lReportKpLkpError1);
            this.tpParamKpLkp1.Controls.Add(this.bReportMake31);
            this.tpParamKpLkp1.Controls.Add(this.tbReportKpLkp1);
            this.tpParamKpLkp1.Controls.Add(this.label32);
            this.tpParamKpLkp1.Location = new System.Drawing.Point(4, 22);
            this.tpParamKpLkp1.Name = "tpParamKpLkp1";
            this.tpParamKpLkp1.Size = new System.Drawing.Size(274, 221);
            this.tpParamKpLkp1.TabIndex = 2;
            this.tpParamKpLkp1.Text = "Parametry";
            this.tpParamKpLkp1.UseVisualStyleBackColor = true;
            // 
            // bReportPrint31
            // 
            this.bReportPrint31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint31.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint31.Name = "bReportPrint31";
            this.bReportPrint31.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint31.TabIndex = 9;
            this.bReportPrint31.Text = "Drukuj";
            this.bReportPrint31.UseVisualStyleBackColor = true;
            this.bReportPrint31.Visible = false;
            this.bReportPrint31.Click += new System.EventHandler(this.button13_Click);
            // 
            // lReportKpLkpError1
            // 
            this.lReportKpLkpError1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportKpLkpError1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportKpLkpError1.ForeColor = System.Drawing.Color.Red;
            this.lReportKpLkpError1.Location = new System.Drawing.Point(6, 66);
            this.lReportKpLkpError1.Name = "lReportKpLkpError1";
            this.lReportKpLkpError1.Size = new System.Drawing.Size(261, 23);
            this.lReportKpLkpError1.TabIndex = 8;
            this.lReportKpLkpError1.Text = "Błędny numer KP.LKP";
            this.lReportKpLkpError1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake31
            // 
            this.bReportMake31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake31.Location = new System.Drawing.Point(6, 167);
            this.bReportMake31.Name = "bReportMake31";
            this.bReportMake31.Size = new System.Drawing.Size(261, 48);
            this.bReportMake31.TabIndex = 6;
            this.bReportMake31.Text = "GENERUJ RAPORT";
            this.bReportMake31.UseVisualStyleBackColor = true;
            this.bReportMake31.Click += new System.EventHandler(this.bReportMake1_Click);
            // 
            // tbReportKpLkp1
            // 
            this.tbReportKpLkp1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportKpLkp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportKpLkp1.Location = new System.Drawing.Point(6, 32);
            this.tbReportKpLkp1.Name = "tbReportKpLkp1";
            this.tbReportKpLkp1.Size = new System.Drawing.Size(262, 31);
            this.tbReportKpLkp1.TabIndex = 5;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 13);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(84, 13);
            this.label32.TabIndex = 4;
            this.label32.Text = "Numeru KP.LKP";
            // 
            // tpParamZpl1
            // 
            this.tpParamZpl1.Controls.Add(this.bReportPrint41);
            this.tpParamZpl1.Controls.Add(this.lReportZplError1);
            this.tpParamZpl1.Controls.Add(this.bReportMake41);
            this.tpParamZpl1.Controls.Add(this.tbReportZpl1);
            this.tpParamZpl1.Controls.Add(this.label33);
            this.tpParamZpl1.Location = new System.Drawing.Point(4, 22);
            this.tpParamZpl1.Name = "tpParamZpl1";
            this.tpParamZpl1.Size = new System.Drawing.Size(274, 221);
            this.tpParamZpl1.TabIndex = 3;
            this.tpParamZpl1.Text = "Parametry";
            this.tpParamZpl1.UseVisualStyleBackColor = true;
            // 
            // bReportPrint41
            // 
            this.bReportPrint41.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint41.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint41.Name = "bReportPrint41";
            this.bReportPrint41.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint41.TabIndex = 11;
            this.bReportPrint41.Text = "Drukuj";
            this.bReportPrint41.UseVisualStyleBackColor = true;
            this.bReportPrint41.Visible = false;
            this.bReportPrint41.Click += new System.EventHandler(this.button13_Click);
            // 
            // lReportZplError1
            // 
            this.lReportZplError1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportZplError1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportZplError1.ForeColor = System.Drawing.Color.Red;
            this.lReportZplError1.Location = new System.Drawing.Point(6, 66);
            this.lReportZplError1.Name = "lReportZplError1";
            this.lReportZplError1.Size = new System.Drawing.Size(261, 23);
            this.lReportZplError1.TabIndex = 10;
            this.lReportZplError1.Text = "Błędny kod";
            this.lReportZplError1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake41
            // 
            this.bReportMake41.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake41.Location = new System.Drawing.Point(6, 167);
            this.bReportMake41.Name = "bReportMake41";
            this.bReportMake41.Size = new System.Drawing.Size(261, 48);
            this.bReportMake41.TabIndex = 9;
            this.bReportMake41.Text = "GENERUJ RAPORT";
            this.bReportMake41.UseVisualStyleBackColor = true;
            this.bReportMake41.Click += new System.EventHandler(this.bReportMake1_Click);
            // 
            // tbReportZpl1
            // 
            this.tbReportZpl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportZpl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportZpl1.Location = new System.Drawing.Point(6, 32);
            this.tbReportZpl1.Name = "tbReportZpl1";
            this.tbReportZpl1.Size = new System.Drawing.Size(262, 31);
            this.tbReportZpl1.TabIndex = 8;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 13);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 13);
            this.label33.TabIndex = 7;
            this.label33.Text = "Kod z plakatu";
            // 
            // tpParamUnit1
            // 
            this.tpParamUnit1.Controls.Add(this.button2);
            this.tpParamUnit1.Controls.Add(this.lReportUnitError1);
            this.tpParamUnit1.Controls.Add(this.tbReportUnit1);
            this.tpParamUnit1.Controls.Add(this.label36);
            this.tpParamUnit1.Controls.Add(this.bReportMake51);
            this.tpParamUnit1.Controls.Add(this.label35);
            this.tpParamUnit1.Controls.Add(this.dtpReportUnit1);
            this.tpParamUnit1.Location = new System.Drawing.Point(4, 22);
            this.tpParamUnit1.Name = "tpParamUnit1";
            this.tpParamUnit1.Size = new System.Drawing.Size(274, 221);
            this.tpParamUnit1.TabIndex = 5;
            this.tpParamUnit1.Text = "Parametry";
            this.tpParamUnit1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(6, 138);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(262, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Drukuj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button13_Click);
            // 
            // lReportUnitError1
            // 
            this.lReportUnitError1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportUnitError1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportUnitError1.ForeColor = System.Drawing.Color.Red;
            this.lReportUnitError1.Location = new System.Drawing.Point(6, 116);
            this.lReportUnitError1.Name = "lReportUnitError1";
            this.lReportUnitError1.Size = new System.Drawing.Size(261, 23);
            this.lReportUnitError1.TabIndex = 11;
            this.lReportUnitError1.Text = "Błędny kod";
            this.lReportUnitError1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbReportUnit1
            // 
            this.tbReportUnit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportUnit1.Location = new System.Drawing.Point(6, 82);
            this.tbReportUnit1.Name = "tbReportUnit1";
            this.tbReportUnit1.Size = new System.Drawing.Size(262, 31);
            this.tbReportUnit1.TabIndex = 7;
            this.tbReportUnit1.TextChanged += new System.EventHandler(this.tbReportUnit1_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(4, 67);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(97, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Miejsce odkładcze";
            // 
            // bReportMake51
            // 
            this.bReportMake51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake51.Location = new System.Drawing.Point(6, 167);
            this.bReportMake51.Name = "bReportMake51";
            this.bReportMake51.Size = new System.Drawing.Size(261, 48);
            this.bReportMake51.TabIndex = 5;
            this.bReportMake51.Text = "GENERUJ RAPORT";
            this.bReportMake51.UseVisualStyleBackColor = true;
            this.bReportMake51.Click += new System.EventHandler(this.bReportMake1_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 13);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(30, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "Data";
            // 
            // dtpReportUnit1
            // 
            this.dtpReportUnit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportUnit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportUnit1.Location = new System.Drawing.Point(6, 32);
            this.dtpReportUnit1.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.dtpReportUnit1.Name = "dtpReportUnit1";
            this.dtpReportUnit1.Size = new System.Drawing.Size(262, 31);
            this.dtpReportUnit1.TabIndex = 3;
            // 
            // tpReportInfo1
            // 
            this.tpReportInfo1.Controls.Add(this.label34);
            this.tpReportInfo1.Location = new System.Drawing.Point(4, 22);
            this.tpReportInfo1.Name = "tpReportInfo1";
            this.tpReportInfo1.Size = new System.Drawing.Size(274, 221);
            this.tpReportInfo1.TabIndex = 4;
            this.tpReportInfo1.Text = "INFORMACJA";
            this.tpReportInfo1.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(274, 221);
            this.label34.TabIndex = 0;
            this.label34.Text = "GENERUJE RAPORT";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpDebug1
            // 
            this.tpDebug1.Controls.Add(this.rtbDebugG1);
            this.tpDebug1.Location = new System.Drawing.Point(4, 22);
            this.tpDebug1.Name = "tpDebug1";
            this.tpDebug1.Padding = new System.Windows.Forms.Padding(3);
            this.tpDebug1.Size = new System.Drawing.Size(576, 500);
            this.tpDebug1.TabIndex = 1;
            this.tpDebug1.Text = "Debug";
            this.tpDebug1.UseVisualStyleBackColor = true;
            // 
            // rtbDebugG1
            // 
            this.rtbDebugG1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbDebugG1.Location = new System.Drawing.Point(3, 3);
            this.rtbDebugG1.Name = "rtbDebugG1";
            this.rtbDebugG1.Size = new System.Drawing.Size(570, 494);
            this.rtbDebugG1.TabIndex = 0;
            this.rtbDebugG1.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tcMainG2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(593, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 526);
            this.panel1.TabIndex = 0;
            // 
            // tcMainG2
            // 
            this.tcMainG2.Controls.Add(this.tpMain2);
            this.tcMainG2.Controls.Add(this.tpLogin2);
            this.tcMainG2.Controls.Add(this.tpStorageUnit2);
            this.tcMainG2.Controls.Add(this.tpStoragePrintLabel2);
            this.tcMainG2.Controls.Add(this.tpAllocation2);
            this.tcMainG2.Controls.Add(this.tpAllocationChange2);
            this.tcMainG2.Controls.Add(this.tpIssuing2);
            this.tcMainG2.Controls.Add(this.tpReport2);
            this.tcMainG2.Controls.Add(this.tpDebug2);
            this.tcMainG2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMainG2.Location = new System.Drawing.Point(0, 0);
            this.tcMainG2.Name = "tcMainG2";
            this.tcMainG2.SelectedIndex = 0;
            this.tcMainG2.Size = new System.Drawing.Size(584, 526);
            this.tcMainG2.TabIndex = 0;
            // 
            // tpMain2
            // 
            this.tpMain2.Controls.Add(this.label9);
            this.tpMain2.Location = new System.Drawing.Point(4, 22);
            this.tpMain2.Name = "tpMain2";
            this.tpMain2.Padding = new System.Windows.Forms.Padding(3);
            this.tpMain2.Size = new System.Drawing.Size(576, 500);
            this.tpMain2.TabIndex = 0;
            this.tpMain2.Text = "Praca";
            this.tpMain2.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(3, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(570, 494);
            this.label9.TabIndex = 1;
            this.label9.Text = "Aby rozpocząć zeskanuj kod z trybem pracy";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpLogin2
            // 
            this.tpLogin2.Controls.Add(this.bLoginLog2);
            this.tpLogin2.Controls.Add(this.tbLoginPass2);
            this.tpLogin2.Controls.Add(this.label4);
            this.tpLogin2.Controls.Add(this.label5);
            this.tpLogin2.Controls.Add(this.label6);
            this.tpLogin2.Controls.Add(this.cbLoginUsers2);
            this.tpLogin2.Location = new System.Drawing.Point(4, 22);
            this.tpLogin2.Name = "tpLogin2";
            this.tpLogin2.Size = new System.Drawing.Size(576, 500);
            this.tpLogin2.TabIndex = 2;
            this.tpLogin2.Text = "Login";
            this.tpLogin2.UseVisualStyleBackColor = true;
            // 
            // bLoginLog2
            // 
            this.bLoginLog2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bLoginLog2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bLoginLog2.Location = new System.Drawing.Point(63, 251);
            this.bLoginLog2.Name = "bLoginLog2";
            this.bLoginLog2.Size = new System.Drawing.Size(370, 38);
            this.bLoginLog2.TabIndex = 22;
            this.bLoginLog2.Text = "Zaloguj";
            this.bLoginLog2.UseVisualStyleBackColor = true;
            this.bLoginLog2.Click += new System.EventHandler(this.bLoginLog2_Click);
            // 
            // tbLoginPass2
            // 
            this.tbLoginPass2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLoginPass2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLoginPass2.Location = new System.Drawing.Point(63, 207);
            this.tbLoginPass2.Name = "tbLoginPass2";
            this.tbLoginPass2.Size = new System.Drawing.Size(370, 29);
            this.tbLoginPass2.TabIndex = 21;
            this.tbLoginPass2.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(74, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 25);
            this.label4.TabIndex = 20;
            this.label4.Text = "Hasło";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(74, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 25);
            this.label5.TabIndex = 19;
            this.label5.Text = "Użytkownik";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(63, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(370, 31);
            this.label6.TabIndex = 18;
            this.label6.Text = "Logowanie";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbLoginUsers2
            // 
            this.cbLoginUsers2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLoginUsers2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLoginUsers2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbLoginUsers2.FormattingEnabled = true;
            this.cbLoginUsers2.Location = new System.Drawing.Point(63, 122);
            this.cbLoginUsers2.Name = "cbLoginUsers2";
            this.cbLoginUsers2.Size = new System.Drawing.Size(370, 32);
            this.cbLoginUsers2.TabIndex = 17;
            // 
            // tpStorageUnit2
            // 
            this.tpStorageUnit2.Controls.Add(this.tableLayoutPanel15);
            this.tpStorageUnit2.Location = new System.Drawing.Point(4, 22);
            this.tpStorageUnit2.Name = "tpStorageUnit2";
            this.tpStorageUnit2.Size = new System.Drawing.Size(576, 500);
            this.tpStorageUnit2.TabIndex = 3;
            this.tpStorageUnit2.Text = "Miejsce odkładcze";
            this.tpStorageUnit2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 3;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel15.Controls.Add(this.panel25, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.panel26, 0, 3);
            this.tableLayoutPanel15.Controls.Add(this.panel27, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.panel28, 0, 2);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 4;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel15.SetColumnSpan(this.panel25, 3);
            this.panel25.Controls.Add(this.dgvStorageList2);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(3, 28);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(570, 314);
            this.panel25.TabIndex = 1;
            // 
            // dgvStorageList2
            // 
            this.dgvStorageList2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStorageList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStorageList2.Location = new System.Drawing.Point(0, 0);
            this.dgvStorageList2.Name = "dgvStorageList2";
            this.dgvStorageList2.Size = new System.Drawing.Size(566, 310);
            this.dgvStorageList2.TabIndex = 0;
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel15.SetColumnSpan(this.panel26, 3);
            this.panel26.Controls.Add(this.tableLayoutPanel16);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(3, 373);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(570, 124);
            this.panel26.TabIndex = 2;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.lStorageUnitNewAdress2, 1, 3);
            this.tableLayoutPanel16.Controls.Add(this.lStorageUnitNewTimeStamp2, 1, 2);
            this.tableLayoutPanel16.Controls.Add(this.lStorageUnitNewUnit2, 1, 1);
            this.tableLayoutPanel16.Controls.Add(this.lStorageUnitNewCourierType2, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.label50, 0, 3);
            this.tableLayoutPanel16.Controls.Add(this.label56, 0, 2);
            this.tableLayoutPanel16.Controls.Add(this.label57, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.label58, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 4;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(566, 120);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // lStorageUnitNewAdress2
            // 
            this.lStorageUnitNewAdress2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewAdress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewAdress2.Location = new System.Drawing.Point(286, 90);
            this.lStorageUnitNewAdress2.Name = "lStorageUnitNewAdress2";
            this.lStorageUnitNewAdress2.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewAdress2.TabIndex = 9;
            this.lStorageUnitNewAdress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStorageUnitNewTimeStamp2
            // 
            this.lStorageUnitNewTimeStamp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewTimeStamp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewTimeStamp2.Location = new System.Drawing.Point(286, 60);
            this.lStorageUnitNewTimeStamp2.Name = "lStorageUnitNewTimeStamp2";
            this.lStorageUnitNewTimeStamp2.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewTimeStamp2.TabIndex = 7;
            this.lStorageUnitNewTimeStamp2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStorageUnitNewUnit2
            // 
            this.lStorageUnitNewUnit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewUnit2.Location = new System.Drawing.Point(286, 30);
            this.lStorageUnitNewUnit2.Name = "lStorageUnitNewUnit2";
            this.lStorageUnitNewUnit2.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewUnit2.TabIndex = 6;
            this.lStorageUnitNewUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStorageUnitNewCourierType2
            // 
            this.lStorageUnitNewCourierType2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lStorageUnitNewCourierType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStorageUnitNewCourierType2.Location = new System.Drawing.Point(286, 0);
            this.lStorageUnitNewCourierType2.Name = "lStorageUnitNewCourierType2";
            this.lStorageUnitNewCourierType2.Size = new System.Drawing.Size(277, 30);
            this.lStorageUnitNewCourierType2.TabIndex = 5;
            this.lStorageUnitNewCourierType2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label50.Location = new System.Drawing.Point(3, 90);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(277, 30);
            this.label50.TabIndex = 4;
            this.label50.Text = "Odbiorca";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label56.Location = new System.Drawing.Point(3, 60);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(277, 30);
            this.label56.TabIndex = 2;
            this.label56.Text = "Data";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label57.Location = new System.Drawing.Point(3, 30);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(277, 30);
            this.label57.TabIndex = 1;
            this.label57.Text = "Miejsce";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label58.Location = new System.Drawing.Point(3, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(277, 30);
            this.label58.TabIndex = 0;
            this.label58.Text = "Kurier";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel27
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.panel27, 3);
            this.panel27.Controls.Add(this.label59);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Margin = new System.Windows.Forms.Padding(0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(576, 25);
            this.panel27.TabIndex = 3;
            // 
            // label59
            // 
            this.label59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label59.Location = new System.Drawing.Point(0, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(576, 25);
            this.label59.TabIndex = 0;
            this.label59.Text = "Lista";
            // 
            // panel28
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.panel28, 3);
            this.panel28.Controls.Add(this.label60);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(0, 345);
            this.panel28.Margin = new System.Windows.Forms.Padding(0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(576, 25);
            this.panel28.TabIndex = 4;
            // 
            // label60
            // 
            this.label60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label60.Location = new System.Drawing.Point(0, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(576, 25);
            this.label60.TabIndex = 1;
            this.label60.Text = "Nowe miejsce odkładcze";
            // 
            // tpStoragePrintLabel2
            // 
            this.tpStoragePrintLabel2.Controls.Add(this.tableLayoutPanel17);
            this.tpStoragePrintLabel2.Location = new System.Drawing.Point(4, 22);
            this.tpStoragePrintLabel2.Name = "tpStoragePrintLabel2";
            this.tpStoragePrintLabel2.Padding = new System.Windows.Forms.Padding(3);
            this.tpStoragePrintLabel2.Size = new System.Drawing.Size(576, 500);
            this.tpStoragePrintLabel2.TabIndex = 7;
            this.tpStoragePrintLabel2.Text = "Dodruk";
            this.tpStoragePrintLabel2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 3;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel17.Controls.Add(this.panel29, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.panel30, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 3;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(570, 494);
            this.tableLayoutPanel17.TabIndex = 2;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel17.SetColumnSpan(this.panel29, 3);
            this.panel29.Controls.Add(this.tableLayoutPanel18);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(3, 43);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(564, 124);
            this.panel29.TabIndex = 2;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.label69, 1, 3);
            this.tableLayoutPanel18.Controls.Add(this.label70, 1, 2);
            this.tableLayoutPanel18.Controls.Add(this.label71, 1, 1);
            this.tableLayoutPanel18.Controls.Add(this.label72, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.label73, 0, 3);
            this.tableLayoutPanel18.Controls.Add(this.label74, 0, 2);
            this.tableLayoutPanel18.Controls.Add(this.label75, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.label76, 0, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 4;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(560, 120);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // label69
            // 
            this.label69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label69.Location = new System.Drawing.Point(283, 90);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(274, 30);
            this.label69.TabIndex = 9;
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label70
            // 
            this.label70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label70.Location = new System.Drawing.Point(283, 60);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(274, 30);
            this.label70.TabIndex = 7;
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label71
            // 
            this.label71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label71.Location = new System.Drawing.Point(283, 30);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(274, 30);
            this.label71.TabIndex = 6;
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label72
            // 
            this.label72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label72.Location = new System.Drawing.Point(283, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(274, 30);
            this.label72.TabIndex = 5;
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label73
            // 
            this.label73.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label73.Location = new System.Drawing.Point(3, 90);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(274, 30);
            this.label73.TabIndex = 4;
            this.label73.Text = "Odbiorca";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label74
            // 
            this.label74.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label74.Location = new System.Drawing.Point(3, 60);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(274, 30);
            this.label74.TabIndex = 2;
            this.label74.Text = "Data";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label75
            // 
            this.label75.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label75.Location = new System.Drawing.Point(3, 30);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(274, 30);
            this.label75.TabIndex = 1;
            this.label75.Text = "Miejsce";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label76
            // 
            this.label76.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label76.Location = new System.Drawing.Point(3, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(274, 30);
            this.label76.TabIndex = 0;
            this.label76.Text = "Kurier";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel30
            // 
            this.tableLayoutPanel17.SetColumnSpan(this.panel30, 3);
            this.panel30.Controls.Add(this.label77);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Margin = new System.Windows.Forms.Padding(0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(570, 40);
            this.panel30.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label77.Location = new System.Drawing.Point(0, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(570, 40);
            this.label77.TabIndex = 1;
            this.label77.Text = "Dodruk etykiety dla:";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tpAllocation2
            // 
            this.tpAllocation2.Controls.Add(this.tableLayoutPanel19);
            this.tpAllocation2.Location = new System.Drawing.Point(4, 22);
            this.tpAllocation2.Name = "tpAllocation2";
            this.tpAllocation2.Size = new System.Drawing.Size(576, 500);
            this.tpAllocation2.TabIndex = 4;
            this.tpAllocation2.Text = "Alokacja";
            this.tpAllocation2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.panel31, 0, 4);
            this.tableLayoutPanel19.Controls.Add(this.panel32, 0, 2);
            this.tableLayoutPanel19.Controls.Add(this.panel33, 0, 3);
            this.tableLayoutPanel19.Controls.Add(this.panel34, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.panel35, 0, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 5;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel19.TabIndex = 1;
            // 
            // panel31
            // 
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel19.SetColumnSpan(this.panel31, 3);
            this.panel31.Controls.Add(this.dgvAllocationList2);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(3, 248);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(570, 249);
            this.panel31.TabIndex = 7;
            // 
            // dgvAllocationList2
            // 
            this.dgvAllocationList2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllocationList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAllocationList2.Location = new System.Drawing.Point(0, 0);
            this.dgvAllocationList2.Name = "dgvAllocationList2";
            this.dgvAllocationList2.Size = new System.Drawing.Size(566, 245);
            this.dgvAllocationList2.TabIndex = 0;
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel19.SetColumnSpan(this.panel32, 3);
            this.panel32.Controls.Add(this.tableLayoutPanel20);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(3, 73);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(570, 144);
            this.panel32.TabIndex = 6;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.label78, 0, 4);
            this.tableLayoutPanel20.Controls.Add(this.lAllocationAdress2, 0, 4);
            this.tableLayoutPanel20.Controls.Add(this.lAllocationCount2, 1, 3);
            this.tableLayoutPanel20.Controls.Add(this.lAllocationTimeStamp2, 1, 2);
            this.tableLayoutPanel20.Controls.Add(this.lAllocationUnit2, 1, 1);
            this.tableLayoutPanel20.Controls.Add(this.lAllocationCourierType2, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.label84, 0, 3);
            this.tableLayoutPanel20.Controls.Add(this.label85, 0, 2);
            this.tableLayoutPanel20.Controls.Add(this.label86, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.label87, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 5;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(566, 140);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // label78
            // 
            this.label78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label78.Location = new System.Drawing.Point(3, 112);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(277, 28);
            this.label78.TabIndex = 11;
            this.label78.Text = "Odbiorca";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationAdress2
            // 
            this.lAllocationAdress2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationAdress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationAdress2.Location = new System.Drawing.Point(286, 112);
            this.lAllocationAdress2.Name = "lAllocationAdress2";
            this.lAllocationAdress2.Size = new System.Drawing.Size(277, 28);
            this.lAllocationAdress2.TabIndex = 10;
            this.lAllocationAdress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationCount2
            // 
            this.lAllocationCount2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationCount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationCount2.Location = new System.Drawing.Point(286, 84);
            this.lAllocationCount2.Name = "lAllocationCount2";
            this.lAllocationCount2.Size = new System.Drawing.Size(277, 28);
            this.lAllocationCount2.TabIndex = 9;
            this.lAllocationCount2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationTimeStamp2
            // 
            this.lAllocationTimeStamp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationTimeStamp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationTimeStamp2.Location = new System.Drawing.Point(286, 56);
            this.lAllocationTimeStamp2.Name = "lAllocationTimeStamp2";
            this.lAllocationTimeStamp2.Size = new System.Drawing.Size(277, 28);
            this.lAllocationTimeStamp2.TabIndex = 7;
            this.lAllocationTimeStamp2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationUnit2
            // 
            this.lAllocationUnit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationUnit2.Location = new System.Drawing.Point(286, 28);
            this.lAllocationUnit2.Name = "lAllocationUnit2";
            this.lAllocationUnit2.Size = new System.Drawing.Size(277, 28);
            this.lAllocationUnit2.TabIndex = 6;
            this.lAllocationUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAllocationCourierType2
            // 
            this.lAllocationCourierType2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationCourierType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationCourierType2.Location = new System.Drawing.Point(286, 0);
            this.lAllocationCourierType2.Name = "lAllocationCourierType2";
            this.lAllocationCourierType2.Size = new System.Drawing.Size(277, 28);
            this.lAllocationCourierType2.TabIndex = 5;
            this.lAllocationCourierType2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label84
            // 
            this.label84.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label84.Location = new System.Drawing.Point(3, 84);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(277, 28);
            this.label84.TabIndex = 4;
            this.label84.Text = "Ilość paczek / podpaczek";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label85
            // 
            this.label85.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label85.Location = new System.Drawing.Point(3, 56);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(277, 28);
            this.label85.TabIndex = 2;
            this.label85.Text = "Data";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label86
            // 
            this.label86.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label86.Location = new System.Drawing.Point(3, 28);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(277, 28);
            this.label86.TabIndex = 1;
            this.label86.Text = "Miejsce";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label87
            // 
            this.label87.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label87.Location = new System.Drawing.Point(3, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(277, 28);
            this.label87.TabIndex = 0;
            this.label87.Text = "Kurier";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel33
            // 
            this.tableLayoutPanel19.SetColumnSpan(this.panel33, 3);
            this.panel33.Controls.Add(this.lAllocationInfo2);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel33.Location = new System.Drawing.Point(0, 220);
            this.panel33.Margin = new System.Windows.Forms.Padding(0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(576, 25);
            this.panel33.TabIndex = 5;
            // 
            // lAllocationInfo2
            // 
            this.lAllocationInfo2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationInfo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationInfo2.ForeColor = System.Drawing.Color.Red;
            this.lAllocationInfo2.Location = new System.Drawing.Point(0, 0);
            this.lAllocationInfo2.Name = "lAllocationInfo2";
            this.lAllocationInfo2.Size = new System.Drawing.Size(576, 25);
            this.lAllocationInfo2.TabIndex = 1;
            this.lAllocationInfo2.Text = "Lista paczek";
            this.lAllocationInfo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.tableLayoutPanel19.SetColumnSpan(this.panel34, 3);
            this.panel34.Controls.Add(this.label89);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel34.Location = new System.Drawing.Point(0, 45);
            this.panel34.Margin = new System.Windows.Forms.Padding(0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(576, 25);
            this.panel34.TabIndex = 4;
            // 
            // label89
            // 
            this.label89.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label89.Location = new System.Drawing.Point(0, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(576, 25);
            this.label89.TabIndex = 0;
            this.label89.Text = "Miejsce odkładcze";
            // 
            // panel35
            // 
            this.tableLayoutPanel19.SetColumnSpan(this.panel35, 3);
            this.panel35.Controls.Add(this.lAllocationName2);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel35.Location = new System.Drawing.Point(3, 3);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(570, 39);
            this.panel35.TabIndex = 2;
            // 
            // lAllocationName2
            // 
            this.lAllocationName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lAllocationName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lAllocationName2.Location = new System.Drawing.Point(0, 0);
            this.lAllocationName2.Name = "lAllocationName2";
            this.lAllocationName2.Size = new System.Drawing.Size(570, 39);
            this.lAllocationName2.TabIndex = 0;
            this.lAllocationName2.Text = "Allokacja paczek";
            this.lAllocationName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpAllocationChange2
            // 
            this.tpAllocationChange2.Controls.Add(this.tableLayoutPanel21);
            this.tpAllocationChange2.Location = new System.Drawing.Point(4, 22);
            this.tpAllocationChange2.Name = "tpAllocationChange2";
            this.tpAllocationChange2.Size = new System.Drawing.Size(576, 500);
            this.tpAllocationChange2.TabIndex = 8;
            this.tpAllocationChange2.Text = "Zmiana alokacji";
            this.tpAllocationChange2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Controls.Add(this.label91, 0, 5);
            this.tableLayoutPanel21.Controls.Add(this.label92, 0, 3);
            this.tableLayoutPanel21.Controls.Add(this.label93, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.label94, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.panel36, 0, 6);
            this.tableLayoutPanel21.Controls.Add(this.panel37, 0, 2);
            this.tableLayoutPanel21.Controls.Add(this.dgvReAllocationSourceList2, 0, 4);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 7;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel21.TabIndex = 1;
            // 
            // label91
            // 
            this.tableLayoutPanel21.SetColumnSpan(this.label91, 2);
            this.label91.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label91.Location = new System.Drawing.Point(3, 347);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(570, 25);
            this.label91.TabIndex = 13;
            this.label91.Text = "Nowe miejsce odkładcze";
            // 
            // label92
            // 
            this.tableLayoutPanel21.SetColumnSpan(this.label92, 2);
            this.label92.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label92.Location = new System.Drawing.Point(3, 196);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(570, 25);
            this.label92.TabIndex = 12;
            this.label92.Text = "Paczka";
            // 
            // label93
            // 
            this.tableLayoutPanel21.SetColumnSpan(this.label93, 2);
            this.label93.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label93.Location = new System.Drawing.Point(3, 45);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(570, 25);
            this.label93.TabIndex = 11;
            this.label93.Text = "Aktualne miejsce odkładcze";
            // 
            // label94
            // 
            this.tableLayoutPanel21.SetColumnSpan(this.label94, 2);
            this.label94.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label94.Location = new System.Drawing.Point(3, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(570, 45);
            this.label94.TabIndex = 10;
            this.label94.Text = "Zmiana alokacji";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel36
            // 
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel21.SetColumnSpan(this.panel36, 3);
            this.panel36.Controls.Add(this.tableLayoutPanel22);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel36.Location = new System.Drawing.Point(3, 375);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(570, 122);
            this.panel36.TabIndex = 8;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.label95, 0, 4);
            this.tableLayoutPanel22.Controls.Add(this.lReAllocationDestinationAdress2, 0, 4);
            this.tableLayoutPanel22.Controls.Add(this.lReAllocationDestinationCount2, 1, 3);
            this.tableLayoutPanel22.Controls.Add(this.lReAllocationDestinationTimeStamp2, 1, 2);
            this.tableLayoutPanel22.Controls.Add(this.lReAllocationDestinationUnit2, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.lReAllocationDestinationCourierType2, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.label101, 0, 3);
            this.tableLayoutPanel22.Controls.Add(this.label102, 0, 2);
            this.tableLayoutPanel22.Controls.Add(this.label103, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.label104, 0, 0);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 5;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(566, 118);
            this.tableLayoutPanel22.TabIndex = 0;
            // 
            // label95
            // 
            this.label95.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label95.Location = new System.Drawing.Point(3, 92);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(277, 26);
            this.label95.TabIndex = 11;
            this.label95.Text = "Odbiorca";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationAdress2
            // 
            this.lReAllocationDestinationAdress2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationAdress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationAdress2.Location = new System.Drawing.Point(286, 92);
            this.lReAllocationDestinationAdress2.Name = "lReAllocationDestinationAdress2";
            this.lReAllocationDestinationAdress2.Size = new System.Drawing.Size(277, 26);
            this.lReAllocationDestinationAdress2.TabIndex = 10;
            this.lReAllocationDestinationAdress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationCount2
            // 
            this.lReAllocationDestinationCount2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationCount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationCount2.Location = new System.Drawing.Point(286, 69);
            this.lReAllocationDestinationCount2.Name = "lReAllocationDestinationCount2";
            this.lReAllocationDestinationCount2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationDestinationCount2.TabIndex = 9;
            this.lReAllocationDestinationCount2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationTimeStamp2
            // 
            this.lReAllocationDestinationTimeStamp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationTimeStamp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationTimeStamp2.Location = new System.Drawing.Point(286, 46);
            this.lReAllocationDestinationTimeStamp2.Name = "lReAllocationDestinationTimeStamp2";
            this.lReAllocationDestinationTimeStamp2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationDestinationTimeStamp2.TabIndex = 7;
            this.lReAllocationDestinationTimeStamp2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationUnit2
            // 
            this.lReAllocationDestinationUnit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationUnit2.Location = new System.Drawing.Point(286, 23);
            this.lReAllocationDestinationUnit2.Name = "lReAllocationDestinationUnit2";
            this.lReAllocationDestinationUnit2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationDestinationUnit2.TabIndex = 6;
            this.lReAllocationDestinationUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationDestinationCourierType2
            // 
            this.lReAllocationDestinationCourierType2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationDestinationCourierType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationDestinationCourierType2.Location = new System.Drawing.Point(286, 0);
            this.lReAllocationDestinationCourierType2.Name = "lReAllocationDestinationCourierType2";
            this.lReAllocationDestinationCourierType2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationDestinationCourierType2.TabIndex = 5;
            this.lReAllocationDestinationCourierType2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label101
            // 
            this.label101.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label101.Location = new System.Drawing.Point(3, 69);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(277, 23);
            this.label101.TabIndex = 4;
            this.label101.Text = "Ilość paczek / podpaczek";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label102
            // 
            this.label102.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label102.Location = new System.Drawing.Point(3, 46);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(277, 23);
            this.label102.TabIndex = 2;
            this.label102.Text = "Data";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label103
            // 
            this.label103.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label103.Location = new System.Drawing.Point(3, 23);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(277, 23);
            this.label103.TabIndex = 1;
            this.label103.Text = "Miejsce";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label104
            // 
            this.label104.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label104.Location = new System.Drawing.Point(3, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(277, 23);
            this.label104.TabIndex = 0;
            this.label104.Text = "Kurier";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel37
            // 
            this.panel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel21.SetColumnSpan(this.panel37, 3);
            this.panel37.Controls.Add(this.tableLayoutPanel23);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Location = new System.Drawing.Point(3, 73);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(570, 120);
            this.panel37.TabIndex = 7;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.label105, 0, 4);
            this.tableLayoutPanel23.Controls.Add(this.lReAllocationSourceAdress2, 0, 4);
            this.tableLayoutPanel23.Controls.Add(this.lReAllocationSourceCount2, 1, 3);
            this.tableLayoutPanel23.Controls.Add(this.lReAllocationSourceTimeStamp2, 1, 2);
            this.tableLayoutPanel23.Controls.Add(this.lReAllocationSourceUnit2, 1, 1);
            this.tableLayoutPanel23.Controls.Add(this.lReAllocationSourceCourierType2, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.label111, 0, 3);
            this.tableLayoutPanel23.Controls.Add(this.label112, 0, 2);
            this.tableLayoutPanel23.Controls.Add(this.label113, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.label114, 0, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 5;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(566, 116);
            this.tableLayoutPanel23.TabIndex = 0;
            // 
            // label105
            // 
            this.label105.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label105.Location = new System.Drawing.Point(3, 92);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(277, 24);
            this.label105.TabIndex = 11;
            this.label105.Text = "Odbiorca";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceAdress2
            // 
            this.lReAllocationSourceAdress2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceAdress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceAdress2.Location = new System.Drawing.Point(286, 92);
            this.lReAllocationSourceAdress2.Name = "lReAllocationSourceAdress2";
            this.lReAllocationSourceAdress2.Size = new System.Drawing.Size(277, 24);
            this.lReAllocationSourceAdress2.TabIndex = 10;
            this.lReAllocationSourceAdress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceCount2
            // 
            this.lReAllocationSourceCount2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceCount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceCount2.Location = new System.Drawing.Point(286, 69);
            this.lReAllocationSourceCount2.Name = "lReAllocationSourceCount2";
            this.lReAllocationSourceCount2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationSourceCount2.TabIndex = 9;
            this.lReAllocationSourceCount2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceTimeStamp2
            // 
            this.lReAllocationSourceTimeStamp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceTimeStamp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceTimeStamp2.Location = new System.Drawing.Point(286, 46);
            this.lReAllocationSourceTimeStamp2.Name = "lReAllocationSourceTimeStamp2";
            this.lReAllocationSourceTimeStamp2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationSourceTimeStamp2.TabIndex = 7;
            this.lReAllocationSourceTimeStamp2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceUnit2
            // 
            this.lReAllocationSourceUnit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceUnit2.Location = new System.Drawing.Point(286, 23);
            this.lReAllocationSourceUnit2.Name = "lReAllocationSourceUnit2";
            this.lReAllocationSourceUnit2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationSourceUnit2.TabIndex = 6;
            this.lReAllocationSourceUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lReAllocationSourceCourierType2
            // 
            this.lReAllocationSourceCourierType2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lReAllocationSourceCourierType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReAllocationSourceCourierType2.Location = new System.Drawing.Point(286, 0);
            this.lReAllocationSourceCourierType2.Name = "lReAllocationSourceCourierType2";
            this.lReAllocationSourceCourierType2.Size = new System.Drawing.Size(277, 23);
            this.lReAllocationSourceCourierType2.TabIndex = 5;
            this.lReAllocationSourceCourierType2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label111
            // 
            this.label111.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label111.Location = new System.Drawing.Point(3, 69);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(277, 23);
            this.label111.TabIndex = 4;
            this.label111.Text = "Ilość paczek / podpaczek";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label111.Click += new System.EventHandler(this.label111_Click);
            // 
            // label112
            // 
            this.label112.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label112.Location = new System.Drawing.Point(3, 46);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(277, 23);
            this.label112.TabIndex = 2;
            this.label112.Text = "Data";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label113
            // 
            this.label113.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label113.Location = new System.Drawing.Point(3, 23);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(277, 23);
            this.label113.TabIndex = 1;
            this.label113.Text = "Miejsce";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label114
            // 
            this.label114.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label114.Location = new System.Drawing.Point(3, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(277, 23);
            this.label114.TabIndex = 0;
            this.label114.Text = "Kurier";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvReAllocationSourceList2
            // 
            this.dgvReAllocationSourceList2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel21.SetColumnSpan(this.dgvReAllocationSourceList2, 2);
            this.dgvReAllocationSourceList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReAllocationSourceList2.Location = new System.Drawing.Point(3, 224);
            this.dgvReAllocationSourceList2.Name = "dgvReAllocationSourceList2";
            this.dgvReAllocationSourceList2.Size = new System.Drawing.Size(570, 120);
            this.dgvReAllocationSourceList2.TabIndex = 9;
            // 
            // tpIssuing2
            // 
            this.tpIssuing2.Controls.Add(this.tableLayoutPanel24);
            this.tpIssuing2.Location = new System.Drawing.Point(4, 22);
            this.tpIssuing2.Name = "tpIssuing2";
            this.tpIssuing2.Padding = new System.Windows.Forms.Padding(3);
            this.tpIssuing2.Size = new System.Drawing.Size(576, 500);
            this.tpIssuing2.TabIndex = 5;
            this.tpIssuing2.Text = "Wydawanie";
            this.tpIssuing2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Controls.Add(this.panel38, 0, 4);
            this.tableLayoutPanel24.Controls.Add(this.panel39, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.panel40, 0, 3);
            this.tableLayoutPanel24.Controls.Add(this.panel41, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.panel42, 0, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 5;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(570, 494);
            this.tableLayoutPanel24.TabIndex = 2;
            // 
            // panel38
            // 
            this.panel38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel24.SetColumnSpan(this.panel38, 3);
            this.panel38.Controls.Add(this.dgvIssuingList2);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel38.Location = new System.Drawing.Point(3, 248);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(564, 243);
            this.panel38.TabIndex = 7;
            // 
            // dgvIssuingList2
            // 
            this.dgvIssuingList2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIssuingList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIssuingList2.Location = new System.Drawing.Point(0, 0);
            this.dgvIssuingList2.Name = "dgvIssuingList2";
            this.dgvIssuingList2.Size = new System.Drawing.Size(560, 239);
            this.dgvIssuingList2.TabIndex = 0;
            // 
            // panel39
            // 
            this.panel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel24.SetColumnSpan(this.panel39, 3);
            this.panel39.Controls.Add(this.tableLayoutPanel25);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel39.Location = new System.Drawing.Point(3, 73);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(564, 144);
            this.panel39.TabIndex = 6;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Controls.Add(this.label115, 0, 4);
            this.tableLayoutPanel25.Controls.Add(this.lIssuingAdress2, 0, 4);
            this.tableLayoutPanel25.Controls.Add(this.lIssuingCount2, 1, 3);
            this.tableLayoutPanel25.Controls.Add(this.lIssuingTimeStamp2, 1, 2);
            this.tableLayoutPanel25.Controls.Add(this.lIssuingUnit2, 1, 1);
            this.tableLayoutPanel25.Controls.Add(this.lIssuingCourierType2, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.label121, 0, 3);
            this.tableLayoutPanel25.Controls.Add(this.label122, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.label123, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.label124, 0, 0);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 5;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(560, 140);
            this.tableLayoutPanel25.TabIndex = 0;
            // 
            // label115
            // 
            this.label115.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label115.Location = new System.Drawing.Point(3, 112);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(274, 28);
            this.label115.TabIndex = 11;
            this.label115.Text = "Odbiorca";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingAdress2
            // 
            this.lIssuingAdress2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingAdress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingAdress2.Location = new System.Drawing.Point(283, 112);
            this.lIssuingAdress2.Name = "lIssuingAdress2";
            this.lIssuingAdress2.Size = new System.Drawing.Size(274, 28);
            this.lIssuingAdress2.TabIndex = 10;
            this.lIssuingAdress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lIssuingAdress2.Click += new System.EventHandler(this.label116_Click);
            // 
            // lIssuingCount2
            // 
            this.lIssuingCount2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingCount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingCount2.Location = new System.Drawing.Point(283, 84);
            this.lIssuingCount2.Name = "lIssuingCount2";
            this.lIssuingCount2.Size = new System.Drawing.Size(274, 28);
            this.lIssuingCount2.TabIndex = 9;
            this.lIssuingCount2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingTimeStamp2
            // 
            this.lIssuingTimeStamp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingTimeStamp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingTimeStamp2.Location = new System.Drawing.Point(283, 56);
            this.lIssuingTimeStamp2.Name = "lIssuingTimeStamp2";
            this.lIssuingTimeStamp2.Size = new System.Drawing.Size(274, 28);
            this.lIssuingTimeStamp2.TabIndex = 7;
            this.lIssuingTimeStamp2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingUnit2
            // 
            this.lIssuingUnit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingUnit2.Location = new System.Drawing.Point(283, 28);
            this.lIssuingUnit2.Name = "lIssuingUnit2";
            this.lIssuingUnit2.Size = new System.Drawing.Size(274, 28);
            this.lIssuingUnit2.TabIndex = 6;
            this.lIssuingUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lIssuingCourierType2
            // 
            this.lIssuingCourierType2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lIssuingCourierType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lIssuingCourierType2.Location = new System.Drawing.Point(283, 0);
            this.lIssuingCourierType2.Name = "lIssuingCourierType2";
            this.lIssuingCourierType2.Size = new System.Drawing.Size(274, 28);
            this.lIssuingCourierType2.TabIndex = 5;
            this.lIssuingCourierType2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label121
            // 
            this.label121.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label121.Location = new System.Drawing.Point(3, 84);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(274, 28);
            this.label121.TabIndex = 4;
            this.label121.Text = "Ilość paczek / podpaczek";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label122
            // 
            this.label122.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label122.Location = new System.Drawing.Point(3, 56);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(274, 28);
            this.label122.TabIndex = 2;
            this.label122.Text = "Data";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label123
            // 
            this.label123.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label123.Location = new System.Drawing.Point(3, 28);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(274, 28);
            this.label123.TabIndex = 1;
            this.label123.Text = "Miejsce";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label124
            // 
            this.label124.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label124.Location = new System.Drawing.Point(3, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(274, 28);
            this.label124.TabIndex = 0;
            this.label124.Text = "Kurier";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel40
            // 
            this.tableLayoutPanel24.SetColumnSpan(this.panel40, 3);
            this.panel40.Controls.Add(this.label125);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel40.Location = new System.Drawing.Point(0, 220);
            this.panel40.Margin = new System.Windows.Forms.Padding(0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(570, 25);
            this.panel40.TabIndex = 5;
            // 
            // label125
            // 
            this.label125.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label125.ForeColor = System.Drawing.Color.Red;
            this.label125.Location = new System.Drawing.Point(0, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(570, 25);
            this.label125.TabIndex = 1;
            this.label125.Text = "Lista paczek wydanych z tego miejsca";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel41
            // 
            this.tableLayoutPanel24.SetColumnSpan(this.panel41, 3);
            this.panel41.Controls.Add(this.label126);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel41.Location = new System.Drawing.Point(0, 45);
            this.panel41.Margin = new System.Windows.Forms.Padding(0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(570, 25);
            this.panel41.TabIndex = 4;
            // 
            // label126
            // 
            this.label126.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label126.Location = new System.Drawing.Point(0, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(570, 25);
            this.label126.TabIndex = 0;
            this.label126.Text = "Miejsce odkładcze";
            // 
            // panel42
            // 
            this.tableLayoutPanel24.SetColumnSpan(this.panel42, 3);
            this.panel42.Controls.Add(this.label127);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel42.Location = new System.Drawing.Point(3, 3);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(564, 39);
            this.panel42.TabIndex = 2;
            // 
            // label127
            // 
            this.label127.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label127.Location = new System.Drawing.Point(0, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(564, 39);
            this.label127.TabIndex = 0;
            this.label127.Text = "Wydawanie paczek";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpReport2
            // 
            this.tpReport2.Controls.Add(this.tableLayoutPanel26);
            this.tpReport2.Location = new System.Drawing.Point(4, 22);
            this.tpReport2.Name = "tpReport2";
            this.tpReport2.Size = new System.Drawing.Size(576, 500);
            this.tpReport2.TabIndex = 6;
            this.tpReport2.Text = "Raport";
            this.tpReport2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 2;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Controls.Add(this.dgvReport2, 0, 2);
            this.tableLayoutPanel26.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.tcReport2, 1, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 3;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(576, 500);
            this.tableLayoutPanel26.TabIndex = 2;
            // 
            // dgvReport2
            // 
            this.dgvReport2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel26.SetColumnSpan(this.dgvReport2, 2);
            this.dgvReport2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReport2.Location = new System.Drawing.Point(3, 256);
            this.dgvReport2.Name = "dgvReport2";
            this.dgvReport2.Size = new System.Drawing.Size(570, 241);
            this.dgvReport2.TabIndex = 0;
            this.dgvReport2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReport2_CellValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bReportStroerWaw2);
            this.groupBox3.Controls.Add(this.bReportStroer2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(282, 77);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Raporty STROER";
            // 
            // bReportStroerWaw2
            // 
            this.bReportStroerWaw2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportStroerWaw2.Location = new System.Drawing.Point(6, 48);
            this.bReportStroerWaw2.Name = "bReportStroerWaw2";
            this.bReportStroerWaw2.Size = new System.Drawing.Size(270, 23);
            this.bReportStroerWaw2.TabIndex = 1;
            this.bReportStroerWaw2.Text = "Raport po dacie wysyłki STROER WARSZAWA";
            this.bReportStroerWaw2.UseVisualStyleBackColor = true;
            this.bReportStroerWaw2.Click += new System.EventHandler(this.bReportStroerWaw2_Click);
            // 
            // bReportStroer2
            // 
            this.bReportStroer2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportStroer2.Location = new System.Drawing.Point(6, 19);
            this.bReportStroer2.Name = "bReportStroer2";
            this.bReportStroer2.Size = new System.Drawing.Size(270, 23);
            this.bReportStroer2.TabIndex = 0;
            this.bReportStroer2.Text = "Raport po dacie wysyłki STROER";
            this.bReportStroer2.UseVisualStyleBackColor = true;
            this.bReportStroer2.Click += new System.EventHandler(this.bReportStroer2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.bReportUnit2);
            this.groupBox4.Controls.Add(this.bReportZpl2);
            this.groupBox4.Controls.Add(this.bReportKpLkp2);
            this.groupBox4.Controls.Add(this.bReportKp2);
            this.groupBox4.Controls.Add(this.bReportDate2);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 86);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(282, 164);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Raporty pozostałe";
            // 
            // bReportUnit2
            // 
            this.bReportUnit2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportUnit2.Location = new System.Drawing.Point(6, 135);
            this.bReportUnit2.Name = "bReportUnit2";
            this.bReportUnit2.Size = new System.Drawing.Size(270, 23);
            this.bReportUnit2.TabIndex = 5;
            this.bReportUnit2.Text = "Raport po dacie wysyłki i miejscu";
            this.bReportUnit2.UseVisualStyleBackColor = true;
            this.bReportUnit2.Click += new System.EventHandler(this.bReportUnit2_Click);
            // 
            // bReportZpl2
            // 
            this.bReportZpl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportZpl2.Location = new System.Drawing.Point(6, 106);
            this.bReportZpl2.Name = "bReportZpl2";
            this.bReportZpl2.Size = new System.Drawing.Size(270, 23);
            this.bReportZpl2.TabIndex = 4;
            this.bReportZpl2.Text = "Raport po kodzie z plakatu (CAŁA KP)";
            this.bReportZpl2.UseVisualStyleBackColor = true;
            this.bReportZpl2.Click += new System.EventHandler(this.bReportZpl2_Click);
            // 
            // bReportKpLkp2
            // 
            this.bReportKpLkp2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportKpLkp2.Location = new System.Drawing.Point(6, 77);
            this.bReportKpLkp2.Name = "bReportKpLkp2";
            this.bReportKpLkp2.Size = new System.Drawing.Size(270, 23);
            this.bReportKpLkp2.TabIndex = 3;
            this.bReportKpLkp2.Text = "Raport po numerze KP i LKP";
            this.bReportKpLkp2.UseVisualStyleBackColor = true;
            this.bReportKpLkp2.Click += new System.EventHandler(this.bReportKpLkp2_Click);
            // 
            // bReportKp2
            // 
            this.bReportKp2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportKp2.Location = new System.Drawing.Point(6, 48);
            this.bReportKp2.Name = "bReportKp2";
            this.bReportKp2.Size = new System.Drawing.Size(270, 23);
            this.bReportKp2.TabIndex = 2;
            this.bReportKp2.Text = "Raport po numerze KP";
            this.bReportKp2.UseVisualStyleBackColor = true;
            this.bReportKp2.Click += new System.EventHandler(this.bReportKp2_Click);
            // 
            // bReportDate2
            // 
            this.bReportDate2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportDate2.Location = new System.Drawing.Point(6, 19);
            this.bReportDate2.Name = "bReportDate2";
            this.bReportDate2.Size = new System.Drawing.Size(270, 23);
            this.bReportDate2.TabIndex = 1;
            this.bReportDate2.Text = "Raport po dacie wysyłki";
            this.bReportDate2.UseVisualStyleBackColor = true;
            this.bReportDate2.Click += new System.EventHandler(this.bReportDate2_Click);
            // 
            // tcReport2
            // 
            this.tcReport2.Controls.Add(this.tpParamDate2);
            this.tcReport2.Controls.Add(this.tpParamKp2);
            this.tcReport2.Controls.Add(this.tpParamKpLkp2);
            this.tcReport2.Controls.Add(this.tpParamZpl2);
            this.tcReport2.Controls.Add(this.tpParamUnit2);
            this.tcReport2.Controls.Add(this.tpReportInfo2);
            this.tcReport2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReport2.Location = new System.Drawing.Point(291, 3);
            this.tcReport2.Name = "tcReport2";
            this.tableLayoutPanel26.SetRowSpan(this.tcReport2, 2);
            this.tcReport2.SelectedIndex = 0;
            this.tcReport2.Size = new System.Drawing.Size(282, 247);
            this.tcReport2.TabIndex = 3;
            this.tcReport2.Click += new System.EventHandler(this.bReportMake2_Click);
            // 
            // tpParamDate2
            // 
            this.tpParamDate2.Controls.Add(this.label40);
            this.tpParamDate2.Controls.Add(this.dtpReportNS2);
            this.tpParamDate2.Controls.Add(this.Courier2);
            this.tpParamDate2.Controls.Add(this.bReportPrint12);
            this.tpParamDate2.Controls.Add(this.dtpReportNP2);
            this.tpParamDate2.Controls.Add(this.bReportMake12);
            this.tpParamDate2.Controls.Add(this.label128);
            this.tpParamDate2.Controls.Add(this.dtpReportParam2);
            this.tpParamDate2.Location = new System.Drawing.Point(4, 22);
            this.tpParamDate2.Name = "tpParamDate2";
            this.tpParamDate2.Padding = new System.Windows.Forms.Padding(3);
            this.tpParamDate2.Size = new System.Drawing.Size(274, 221);
            this.tpParamDate2.TabIndex = 0;
            this.tpParamDate2.Text = "Parametry";
            this.tpParamDate2.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 53);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(34, 13);
            this.label40.TabIndex = 9;
            this.label40.Text = "Kurier";
            // 
            // dtpReportNS2
            // 
            this.dtpReportNS2.AutoSize = true;
            this.dtpReportNS2.Location = new System.Drawing.Point(8, 116);
            this.dtpReportNS2.Margin = new System.Windows.Forms.Padding(2);
            this.dtpReportNS2.Name = "dtpReportNS2";
            this.dtpReportNS2.Size = new System.Drawing.Size(138, 17);
            this.dtpReportNS2.TabIndex = 7;
            this.dtpReportNS2.Text = "Pokaż tylko niewysłane";
            this.dtpReportNS2.UseVisualStyleBackColor = true;
            // 
            // Courier2
            // 
            this.Courier2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Courier2.FormattingEnabled = true;
            this.Courier2.Location = new System.Drawing.Point(8, 70);
            this.Courier2.Margin = new System.Windows.Forms.Padding(2);
            this.Courier2.Name = "Courier2";
            this.Courier2.Size = new System.Drawing.Size(260, 21);
            this.Courier2.TabIndex = 8;
            this.Courier2.Text = "WSZYSCY";
            // 
            // bReportPrint12
            // 
            this.bReportPrint12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint12.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint12.Name = "bReportPrint12";
            this.bReportPrint12.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint12.TabIndex = 4;
            this.bReportPrint12.Text = "Drukuj";
            this.bReportPrint12.UseVisualStyleBackColor = true;
            this.bReportPrint12.Click += new System.EventHandler(this.bReportPrint12_Click);
            // 
            // dtpReportNP2
            // 
            this.dtpReportNP2.AutoSize = true;
            this.dtpReportNP2.Location = new System.Drawing.Point(8, 94);
            this.dtpReportNP2.Margin = new System.Windows.Forms.Padding(2);
            this.dtpReportNP2.Name = "dtpReportNP2";
            this.dtpReportNP2.Size = new System.Drawing.Size(153, 17);
            this.dtpReportNP2.TabIndex = 6;
            this.dtpReportNP2.Text = "Pokaż tylko niespakowane";
            this.dtpReportNP2.UseVisualStyleBackColor = true;
            // 
            // bReportMake12
            // 
            this.bReportMake12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake12.Location = new System.Drawing.Point(6, 167);
            this.bReportMake12.Name = "bReportMake12";
            this.bReportMake12.Size = new System.Drawing.Size(261, 48);
            this.bReportMake12.TabIndex = 2;
            this.bReportMake12.Text = "GENERUJ RAPORT";
            this.bReportMake12.UseVisualStyleBackColor = true;
            this.bReportMake12.Click += new System.EventHandler(this.bReportMake12_Click);
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(5, 3);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(30, 13);
            this.label128.TabIndex = 1;
            this.label128.Text = "Data";
            // 
            // dtpReportParam2
            // 
            this.dtpReportParam2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportParam2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportParam2.Location = new System.Drawing.Point(6, 20);
            this.dtpReportParam2.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.dtpReportParam2.Name = "dtpReportParam2";
            this.dtpReportParam2.Size = new System.Drawing.Size(262, 31);
            this.dtpReportParam2.TabIndex = 0;
            // 
            // tpParamKp2
            // 
            this.tpParamKp2.Controls.Add(this.bReportPrint22);
            this.tpParamKp2.Controls.Add(this.lReportKpError2);
            this.tpParamKp2.Controls.Add(this.bReportMake22);
            this.tpParamKp2.Controls.Add(this.tbReportKp2);
            this.tpParamKp2.Controls.Add(this.label130);
            this.tpParamKp2.Location = new System.Drawing.Point(4, 22);
            this.tpParamKp2.Name = "tpParamKp2";
            this.tpParamKp2.Padding = new System.Windows.Forms.Padding(3);
            this.tpParamKp2.Size = new System.Drawing.Size(274, 221);
            this.tpParamKp2.TabIndex = 1;
            this.tpParamKp2.Text = "Parametry";
            this.tpParamKp2.UseVisualStyleBackColor = true;
            // 
            // bReportPrint22
            // 
            this.bReportPrint22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint22.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint22.Name = "bReportPrint22";
            this.bReportPrint22.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint22.TabIndex = 5;
            this.bReportPrint22.Text = "Drukuj";
            this.bReportPrint22.UseVisualStyleBackColor = true;
            this.bReportPrint22.Visible = false;
            this.bReportPrint22.Click += new System.EventHandler(this.bReportPrint12_Click);
            // 
            // lReportKpError2
            // 
            this.lReportKpError2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportKpError2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportKpError2.ForeColor = System.Drawing.Color.Red;
            this.lReportKpError2.Location = new System.Drawing.Point(3, 66);
            this.lReportKpError2.Name = "lReportKpError2";
            this.lReportKpError2.Size = new System.Drawing.Size(261, 23);
            this.lReportKpError2.TabIndex = 4;
            this.lReportKpError2.Text = "Błędny numer KP";
            this.lReportKpError2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake22
            // 
            this.bReportMake22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake22.Location = new System.Drawing.Point(6, 167);
            this.bReportMake22.Name = "bReportMake22";
            this.bReportMake22.Size = new System.Drawing.Size(261, 48);
            this.bReportMake22.TabIndex = 3;
            this.bReportMake22.Text = "GENERUJ RAPORT";
            this.bReportMake22.UseVisualStyleBackColor = true;
            this.bReportMake22.Click += new System.EventHandler(this.bReportMake2_Click);
            // 
            // tbReportKp2
            // 
            this.tbReportKp2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportKp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportKp2.Location = new System.Drawing.Point(6, 32);
            this.tbReportKp2.Name = "tbReportKp2";
            this.tbReportKp2.Size = new System.Drawing.Size(262, 31);
            this.tbReportKp2.TabIndex = 1;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(6, 13);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(61, 13);
            this.label130.TabIndex = 0;
            this.label130.Text = "Numeru KP";
            // 
            // tpParamKpLkp2
            // 
            this.tpParamKpLkp2.Controls.Add(this.bReportPrint32);
            this.tpParamKpLkp2.Controls.Add(this.lReportKpLkpError2);
            this.tpParamKpLkp2.Controls.Add(this.bReportMake32);
            this.tpParamKpLkp2.Controls.Add(this.tbReportKpLkp2);
            this.tpParamKpLkp2.Controls.Add(this.label132);
            this.tpParamKpLkp2.Location = new System.Drawing.Point(4, 22);
            this.tpParamKpLkp2.Name = "tpParamKpLkp2";
            this.tpParamKpLkp2.Size = new System.Drawing.Size(274, 221);
            this.tpParamKpLkp2.TabIndex = 2;
            this.tpParamKpLkp2.Text = "Parametry";
            this.tpParamKpLkp2.UseVisualStyleBackColor = true;
            // 
            // bReportPrint32
            // 
            this.bReportPrint32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint32.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint32.Name = "bReportPrint32";
            this.bReportPrint32.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint32.TabIndex = 10;
            this.bReportPrint32.Text = "Drukuj";
            this.bReportPrint32.UseVisualStyleBackColor = true;
            this.bReportPrint32.Visible = false;
            this.bReportPrint32.Click += new System.EventHandler(this.bReportPrint12_Click);
            // 
            // lReportKpLkpError2
            // 
            this.lReportKpLkpError2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportKpLkpError2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportKpLkpError2.ForeColor = System.Drawing.Color.Red;
            this.lReportKpLkpError2.Location = new System.Drawing.Point(6, 66);
            this.lReportKpLkpError2.Name = "lReportKpLkpError2";
            this.lReportKpLkpError2.Size = new System.Drawing.Size(261, 23);
            this.lReportKpLkpError2.TabIndex = 8;
            this.lReportKpLkpError2.Text = "Błędny numer KP.LKP";
            this.lReportKpLkpError2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake32
            // 
            this.bReportMake32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake32.Location = new System.Drawing.Point(6, 167);
            this.bReportMake32.Name = "bReportMake32";
            this.bReportMake32.Size = new System.Drawing.Size(261, 48);
            this.bReportMake32.TabIndex = 6;
            this.bReportMake32.Text = "GENERUJ RAPORT";
            this.bReportMake32.UseVisualStyleBackColor = true;
            this.bReportMake32.Click += new System.EventHandler(this.bReportMake2_Click);
            // 
            // tbReportKpLkp2
            // 
            this.tbReportKpLkp2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportKpLkp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportKpLkp2.Location = new System.Drawing.Point(6, 32);
            this.tbReportKpLkp2.Name = "tbReportKpLkp2";
            this.tbReportKpLkp2.Size = new System.Drawing.Size(262, 31);
            this.tbReportKpLkp2.TabIndex = 5;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(6, 13);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(84, 13);
            this.label132.TabIndex = 4;
            this.label132.Text = "Numeru KP.LKP";
            // 
            // tpParamZpl2
            // 
            this.tpParamZpl2.Controls.Add(this.bReportPrint42);
            this.tpParamZpl2.Controls.Add(this.lReportZplError2);
            this.tpParamZpl2.Controls.Add(this.bReportMake42);
            this.tpParamZpl2.Controls.Add(this.tbReportZpl2);
            this.tpParamZpl2.Controls.Add(this.label134);
            this.tpParamZpl2.Location = new System.Drawing.Point(4, 22);
            this.tpParamZpl2.Name = "tpParamZpl2";
            this.tpParamZpl2.Size = new System.Drawing.Size(274, 221);
            this.tpParamZpl2.TabIndex = 3;
            this.tpParamZpl2.Text = "Parametry";
            this.tpParamZpl2.UseVisualStyleBackColor = true;
            // 
            // bReportPrint42
            // 
            this.bReportPrint42.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportPrint42.Location = new System.Drawing.Point(6, 138);
            this.bReportPrint42.Name = "bReportPrint42";
            this.bReportPrint42.Size = new System.Drawing.Size(262, 23);
            this.bReportPrint42.TabIndex = 12;
            this.bReportPrint42.Text = "Drukuj";
            this.bReportPrint42.UseVisualStyleBackColor = true;
            this.bReportPrint42.Visible = false;
            this.bReportPrint42.Click += new System.EventHandler(this.bReportPrint12_Click);
            // 
            // lReportZplError2
            // 
            this.lReportZplError2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportZplError2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportZplError2.ForeColor = System.Drawing.Color.Red;
            this.lReportZplError2.Location = new System.Drawing.Point(6, 66);
            this.lReportZplError2.Name = "lReportZplError2";
            this.lReportZplError2.Size = new System.Drawing.Size(261, 23);
            this.lReportZplError2.TabIndex = 10;
            this.lReportZplError2.Text = "Błędny kod";
            this.lReportZplError2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake42
            // 
            this.bReportMake42.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake42.Location = new System.Drawing.Point(6, 167);
            this.bReportMake42.Name = "bReportMake42";
            this.bReportMake42.Size = new System.Drawing.Size(261, 48);
            this.bReportMake42.TabIndex = 9;
            this.bReportMake42.Text = "GENERUJ RAPORT";
            this.bReportMake42.UseVisualStyleBackColor = true;
            this.bReportMake42.Click += new System.EventHandler(this.bReportMake2_Click);
            // 
            // tbReportZpl2
            // 
            this.tbReportZpl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportZpl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportZpl2.Location = new System.Drawing.Point(6, 32);
            this.tbReportZpl2.Name = "tbReportZpl2";
            this.tbReportZpl2.Size = new System.Drawing.Size(262, 31);
            this.tbReportZpl2.TabIndex = 8;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(6, 13);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(72, 13);
            this.label134.TabIndex = 7;
            this.label134.Text = "Kod z plakatu";
            // 
            // tpParamUnit2
            // 
            this.tpParamUnit2.Controls.Add(this.button1);
            this.tpParamUnit2.Controls.Add(this.lReportUnitError2);
            this.tpParamUnit2.Controls.Add(this.tbReportUnit2);
            this.tpParamUnit2.Controls.Add(this.label136);
            this.tpParamUnit2.Controls.Add(this.bReportMake52);
            this.tpParamUnit2.Controls.Add(this.label137);
            this.tpParamUnit2.Controls.Add(this.dtpReportUnit2);
            this.tpParamUnit2.Location = new System.Drawing.Point(4, 22);
            this.tpParamUnit2.Name = "tpParamUnit2";
            this.tpParamUnit2.Size = new System.Drawing.Size(274, 221);
            this.tpParamUnit2.TabIndex = 5;
            this.tpParamUnit2.Text = "Parametry";
            this.tpParamUnit2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(6, 138);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(262, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Drukuj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.bReportPrint12_Click);
            // 
            // lReportUnitError2
            // 
            this.lReportUnitError2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportUnitError2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportUnitError2.ForeColor = System.Drawing.Color.Red;
            this.lReportUnitError2.Location = new System.Drawing.Point(6, 116);
            this.lReportUnitError2.Name = "lReportUnitError2";
            this.lReportUnitError2.Size = new System.Drawing.Size(261, 23);
            this.lReportUnitError2.TabIndex = 11;
            this.lReportUnitError2.Text = "Błędny kod";
            this.lReportUnitError2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbReportUnit2
            // 
            this.tbReportUnit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportUnit2.Location = new System.Drawing.Point(6, 82);
            this.tbReportUnit2.Name = "tbReportUnit2";
            this.tbReportUnit2.Size = new System.Drawing.Size(262, 31);
            this.tbReportUnit2.TabIndex = 7;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(4, 67);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(97, 13);
            this.label136.TabIndex = 6;
            this.label136.Text = "Miejsce odkładcze";
            // 
            // bReportMake52
            // 
            this.bReportMake52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake52.Location = new System.Drawing.Point(6, 167);
            this.bReportMake52.Name = "bReportMake52";
            this.bReportMake52.Size = new System.Drawing.Size(261, 48);
            this.bReportMake52.TabIndex = 5;
            this.bReportMake52.Text = "GENERUJ RAPORT";
            this.bReportMake52.UseVisualStyleBackColor = true;
            this.bReportMake52.Click += new System.EventHandler(this.bReportMake2_Click);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(6, 13);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(30, 13);
            this.label137.TabIndex = 4;
            this.label137.Text = "Data";
            // 
            // dtpReportUnit2
            // 
            this.dtpReportUnit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportUnit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportUnit2.Location = new System.Drawing.Point(6, 32);
            this.dtpReportUnit2.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.dtpReportUnit2.Name = "dtpReportUnit2";
            this.dtpReportUnit2.Size = new System.Drawing.Size(262, 31);
            this.dtpReportUnit2.TabIndex = 3;
            // 
            // tpReportInfo2
            // 
            this.tpReportInfo2.Controls.Add(this.label138);
            this.tpReportInfo2.Location = new System.Drawing.Point(4, 22);
            this.tpReportInfo2.Name = "tpReportInfo2";
            this.tpReportInfo2.Size = new System.Drawing.Size(274, 221);
            this.tpReportInfo2.TabIndex = 4;
            this.tpReportInfo2.Text = "INFORMACJA";
            this.tpReportInfo2.UseVisualStyleBackColor = true;
            // 
            // label138
            // 
            this.label138.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label138.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label138.Location = new System.Drawing.Point(0, 0);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(274, 221);
            this.label138.TabIndex = 0;
            this.label138.Text = "GENERUJE RAPORT";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpDebug2
            // 
            this.tpDebug2.Controls.Add(this.rtbDebugG2);
            this.tpDebug2.Location = new System.Drawing.Point(4, 22);
            this.tpDebug2.Name = "tpDebug2";
            this.tpDebug2.Padding = new System.Windows.Forms.Padding(3);
            this.tpDebug2.Size = new System.Drawing.Size(576, 500);
            this.tpDebug2.TabIndex = 1;
            this.tpDebug2.Text = "Debug";
            this.tpDebug2.UseVisualStyleBackColor = true;
            // 
            // rtbDebugG2
            // 
            this.rtbDebugG2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbDebugG2.Location = new System.Drawing.Point(3, 3);
            this.rtbDebugG2.Name = "rtbDebugG2";
            this.rtbDebugG2.Size = new System.Drawing.Size(570, 494);
            this.rtbDebugG2.TabIndex = 0;
            this.rtbDebugG2.Text = "";
            // 
            // gbInfo2
            // 
            this.gbInfo2.Controls.Add(this.lInfo2);
            this.gbInfo2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbInfo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gbInfo2.Location = new System.Drawing.Point(593, 585);
            this.gbInfo2.Name = "gbInfo2";
            this.gbInfo2.Size = new System.Drawing.Size(584, 51);
            this.gbInfo2.TabIndex = 7;
            this.gbInfo2.TabStop = false;
            this.gbInfo2.Text = " Informacje ";
            // 
            // lInfo2
            // 
            this.lInfo2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lInfo2.Location = new System.Drawing.Point(3, 22);
            this.lInfo2.Name = "lInfo2";
            this.lInfo2.Size = new System.Drawing.Size(578, 26);
            this.lInfo2.TabIndex = 1;
            this.lInfo2.Text = " ";
            this.lInfo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.gbInfo2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gbInfo1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1180, 685);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // DispachWarehouseMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 733);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "DispachWarehouseMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DispachWarehouseMain_FormClosing);
            this.Load += new System.EventHandler(this.DispachWarehouseMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gbInfo1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tcMainG1.ResumeLayout(false);
            this.tpMain1.ResumeLayout(false);
            this.tpLogin1.ResumeLayout(false);
            this.tpLogin1.PerformLayout();
            this.tpStorageUnit1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStorageList1)).EndInit();
            this.panel9.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.tpStoragePrintLabel1.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.tpAllocation1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllocationList1)).EndInit();
            this.panel15.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.tpAllocationChange1.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReAllocationSourceList1)).EndInit();
            this.tpIssuing1.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssuingList1)).EndInit();
            this.panel19.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.tpReport1.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tcReport1.ResumeLayout(false);
            this.tpParamDate1.ResumeLayout(false);
            this.tpParamDate1.PerformLayout();
            this.tpParamKp1.ResumeLayout(false);
            this.tpParamKp1.PerformLayout();
            this.tpParamKpLkp1.ResumeLayout(false);
            this.tpParamKpLkp1.PerformLayout();
            this.tpParamZpl1.ResumeLayout(false);
            this.tpParamZpl1.PerformLayout();
            this.tpParamUnit1.ResumeLayout(false);
            this.tpParamUnit1.PerformLayout();
            this.tpReportInfo1.ResumeLayout(false);
            this.tpDebug1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tcMainG2.ResumeLayout(false);
            this.tpMain2.ResumeLayout(false);
            this.tpLogin2.ResumeLayout(false);
            this.tpLogin2.PerformLayout();
            this.tpStorageUnit2.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStorageList2)).EndInit();
            this.panel26.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.tpStoragePrintLabel2.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.tpAllocation2.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllocationList2)).EndInit();
            this.panel32.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.tpAllocationChange2.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReAllocationSourceList2)).EndInit();
            this.tpIssuing2.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssuingList2)).EndInit();
            this.panel39.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.tpReport2.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tcReport2.ResumeLayout(false);
            this.tpParamDate2.ResumeLayout(false);
            this.tpParamDate2.PerformLayout();
            this.tpParamKp2.ResumeLayout(false);
            this.tpParamKp2.PerformLayout();
            this.tpParamKpLkp2.ResumeLayout(false);
            this.tpParamKpLkp2.PerformLayout();
            this.tpParamZpl2.ResumeLayout(false);
            this.tpParamZpl2.PerformLayout();
            this.tpParamUnit2.ResumeLayout(false);
            this.tpParamUnit2.PerformLayout();
            this.tpReportInfo2.ResumeLayout(false);
            this.tpDebug2.ResumeLayout(false);
            this.gbInfo2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem1;
        private System.Windows.Forms.ToolStripStatusLabel tsslDatabaseStatus1;
        private System.Windows.Forms.ToolStripStatusLabel tsslDatabaseStatus2;
        private System.Windows.Forms.Timer tConnection;
        private System.Windows.Forms.ToolStripStatusLabel tsslSpring;
        private System.Windows.Forms.ToolStripStatusLabel tsslScanerStatus2;
        private System.Windows.Forms.ToolStripStatusLabel tsslScanerStatus1;
        private System.Windows.Forms.ToolStripStatusLabel tsslUser1;
        private System.Windows.Forms.ToolStripStatusLabel tsslUser2;
        private System.Windows.Forms.GroupBox gbInfo1;
        private System.Windows.Forms.Label lInfo1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lOperatorName1;
        private System.Windows.Forms.Button bModeAct1;
        private System.Windows.Forms.Label lSatationName1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tcMainG1;
        private System.Windows.Forms.TabPage tpMain1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tpLogin1;
        private System.Windows.Forms.Button bLoginLog1;
        private System.Windows.Forms.TextBox tbLoginPass1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbLoginUsers1;
        private System.Windows.Forms.TabPage tpStorageUnit1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridView dgvStorageList1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lStorageUnitNewAdress1;
        private System.Windows.Forms.Label lStorageUnitNewTimeStamp1;
        private System.Windows.Forms.Label lStorageUnitNewUnit1;
        private System.Windows.Forms.Label lStorageUnitNewCourierType1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tpStoragePrintLabel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tpAllocation1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.DataGridView dgvAllocationList1;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lAllocationAdress1;
        private System.Windows.Forms.Label lAllocationCount1;
        private System.Windows.Forms.Label lAllocationTimeStamp1;
        private System.Windows.Forms.Label lAllocationUnit1;
        private System.Windows.Forms.Label lAllocationCourierType1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label lAllocationInfo1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lAllocationName1;
        private System.Windows.Forms.TabPage tpAllocationChange1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label lReAllocationDestinationAdress1;
        private System.Windows.Forms.Label lReAllocationDestinationCount1;
        private System.Windows.Forms.Label lReAllocationDestinationTimeStamp1;
        private System.Windows.Forms.Label lReAllocationDestinationUnit1;
        private System.Windows.Forms.Label lReAllocationDestinationCourierType1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lReAllocationSourceAdress1;
        private System.Windows.Forms.Label lReAllocationSourceCount1;
        private System.Windows.Forms.Label lReAllocationSourceTimeStamp1;
        private System.Windows.Forms.Label lReAllocationSourceUnit1;
        private System.Windows.Forms.Label lReAllocationSourceCourierType1;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DataGridView dgvReAllocationSourceList1;
        private System.Windows.Forms.TabPage tpIssuing1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.DataGridView dgvIssuingList1;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lIssuingAdress1;
        private System.Windows.Forms.Label lIssuingCount1;
        private System.Windows.Forms.Label lIssuingTimeStamp1;
        private System.Windows.Forms.Label lIssuingUnit1;
        private System.Windows.Forms.Label lIssuingCourierType1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage tpReport1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.DataGridView dgvReport1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bReportStroerWaw1;
        private System.Windows.Forms.Button bReportStroer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bReportUnit1;
        private System.Windows.Forms.Button bReportZpl1;
        private System.Windows.Forms.Button bReportKpLkp1;
        private System.Windows.Forms.Button bReportKp1;
        private System.Windows.Forms.Button bReportDate1;
        private System.Windows.Forms.TabControl tcReport1;
        private System.Windows.Forms.TabPage tpParamDate1;
        private System.Windows.Forms.Button bReportMake11;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dtpReportParam1;
        private System.Windows.Forms.TabPage tpParamKp1;
        private System.Windows.Forms.Label lReportKpError1;
        private System.Windows.Forms.Button bReportMake21;
        private System.Windows.Forms.TextBox tbReportKp1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tpParamKpLkp1;
        private System.Windows.Forms.Label lReportKpLkpError1;
        private System.Windows.Forms.Button bReportMake31;
        private System.Windows.Forms.TextBox tbReportKpLkp1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tpParamZpl1;
        private System.Windows.Forms.Label lReportZplError1;
        private System.Windows.Forms.Button bReportMake41;
        private System.Windows.Forms.TextBox tbReportZpl1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage tpParamUnit1;
        private System.Windows.Forms.Label lReportUnitError1;
        private System.Windows.Forms.TextBox tbReportUnit1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button bReportMake51;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DateTimePicker dtpReportUnit1;
        private System.Windows.Forms.TabPage tpReportInfo1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tpDebug1;
        private System.Windows.Forms.RichTextBox rtbDebugG1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tcMainG2;
        private System.Windows.Forms.TabPage tpMain2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tpLogin2;
        private System.Windows.Forms.Button bLoginLog2;
        private System.Windows.Forms.TextBox tbLoginPass2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbLoginUsers2;
        private System.Windows.Forms.TabPage tpStorageUnit2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.DataGridView dgvStorageList2;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label lStorageUnitNewAdress2;
        private System.Windows.Forms.Label lStorageUnitNewTimeStamp2;
        private System.Windows.Forms.Label lStorageUnitNewUnit2;
        private System.Windows.Forms.Label lStorageUnitNewCourierType2;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TabPage tpStoragePrintLabel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TabPage tpAllocation2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.DataGridView dgvAllocationList2;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label lAllocationAdress2;
        private System.Windows.Forms.Label lAllocationCount2;
        private System.Windows.Forms.Label lAllocationTimeStamp2;
        private System.Windows.Forms.Label lAllocationUnit2;
        private System.Windows.Forms.Label lAllocationCourierType2;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label lAllocationInfo2;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label lAllocationName2;
        private System.Windows.Forms.TabPage tpAllocationChange2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lReAllocationDestinationAdress2;
        private System.Windows.Forms.Label lReAllocationDestinationCount2;
        private System.Windows.Forms.Label lReAllocationDestinationTimeStamp2;
        private System.Windows.Forms.Label lReAllocationDestinationUnit2;
        private System.Windows.Forms.Label lReAllocationDestinationCourierType2;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label lReAllocationSourceAdress2;
        private System.Windows.Forms.Label lReAllocationSourceCount2;
        private System.Windows.Forms.Label lReAllocationSourceTimeStamp2;
        private System.Windows.Forms.Label lReAllocationSourceUnit2;
        private System.Windows.Forms.Label lReAllocationSourceCourierType2;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.DataGridView dgvReAllocationSourceList2;
        private System.Windows.Forms.TabPage tpIssuing2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.DataGridView dgvIssuingList2;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label lIssuingAdress2;
        private System.Windows.Forms.Label lIssuingCount2;
        private System.Windows.Forms.Label lIssuingTimeStamp2;
        private System.Windows.Forms.Label lIssuingUnit2;
        private System.Windows.Forms.Label lIssuingCourierType2;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TabPage tpReport2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.DataGridView dgvReport2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bReportStroerWaw2;
        private System.Windows.Forms.Button bReportStroer2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button bReportUnit2;
        private System.Windows.Forms.Button bReportZpl2;
        private System.Windows.Forms.Button bReportKpLkp2;
        private System.Windows.Forms.Button bReportKp2;
        private System.Windows.Forms.Button bReportDate2;
        private System.Windows.Forms.TabControl tcReport2;
        private System.Windows.Forms.TabPage tpParamDate2;
        private System.Windows.Forms.Button bReportMake12;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.DateTimePicker dtpReportParam2;
        private System.Windows.Forms.TabPage tpParamKp2;
        private System.Windows.Forms.Label lReportKpError2;
        private System.Windows.Forms.Button bReportMake22;
        private System.Windows.Forms.TextBox tbReportKp2;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TabPage tpParamKpLkp2;
        private System.Windows.Forms.Label lReportKpLkpError2;
        private System.Windows.Forms.Button bReportMake32;
        private System.Windows.Forms.TextBox tbReportKpLkp2;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TabPage tpParamZpl2;
        private System.Windows.Forms.Label lReportZplError2;
        private System.Windows.Forms.Button bReportMake42;
        private System.Windows.Forms.TextBox tbReportZpl2;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TabPage tpParamUnit2;
        private System.Windows.Forms.Label lReportUnitError2;
        private System.Windows.Forms.TextBox tbReportUnit2;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Button bReportMake52;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.DateTimePicker dtpReportUnit2;
        private System.Windows.Forms.TabPage tpReportInfo2;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TabPage tpDebug2;
        private System.Windows.Forms.RichTextBox rtbDebugG2;
        private System.Windows.Forms.GroupBox gbInfo2;
        private System.Windows.Forms.Label lInfo2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Label lOperatorName2;
        private System.Windows.Forms.Button bModeAct2;
        private System.Windows.Forms.Label lSatationName2;
        private System.Windows.Forms.Button bReportPrint11;
        private System.Windows.Forms.Button bReportPrint21;
        private System.Windows.Forms.Button bReportPrint31;
        private System.Windows.Forms.Button bReportPrint41;
        private System.Windows.Forms.Button bReportPrint12;
        private System.Windows.Forms.Button bReportPrint22;
        private System.Windows.Forms.Button bReportPrint32;
        private System.Windows.Forms.Button bReportPrint42;
        private System.Windows.Forms.CheckBox dtpReportNS1;
        private System.Windows.Forms.CheckBox dtpReportNP1;
        private System.Windows.Forms.CheckBox dtpReportNS2;
        private System.Windows.Forms.CheckBox dtpReportNP2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox Courier1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox Courier2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

