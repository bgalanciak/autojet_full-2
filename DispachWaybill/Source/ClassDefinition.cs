﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DispachWaybill
{
    public class DispatchItem
    {
        /*
         CREATE TABLE `dispatch_item_table` (
         `reference_id` varchar(50) COLLATE utf8_polish_ci NOT NULL,
         `jdnr` varchar(6) CHARACTER SET latin1 NOT NULL,
         `jdline` varchar(3) CHARACTER SET latin1 NOT NULL,
         `qty_to_pack` int(11) NOT NULL,
         `qty_packed` int(11) NOT NULL,
         `partial_item` bit(1) DEFAULT b'0',
         `barcode_data` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
         `weight_act` int(11) DEFAULT NULL,
         `operatorId` int(11) DEFAULT NULL,
         `created_date` datetime NOT NULL,
         `mod_date` datetime NOT NULL,
         `pack_description` varchar(255) CHARACTER SET latin1 NOT NULL,
         `ref_from` int(4) NOT NULL,
         `ref_to` int(4) NOT NULL,
         `subpack_from` int(4) NOT NULL,
         `subpack_to` int(4) NOT NULL,
         PRIMARY KEY (`reference_id`,`ref_to`,`ref_from`,`subpack_from`,`subpack_to`)
        )
         ALTER TABLE dispatch_item_table ADD completation_done bit(1) DEFAULT b'0';
         ALTER TABLE dispatch_item_table ADD booked_station_id int(11) DEFAULT 0;
         */

        //public int id { get; set; }
        //public int dispatch_id { get; set; }
        public string reference_id { get; set; }       //new
        public string jdnr { get; set; }
        public string jdline { get; set; }
        public int qty_to_pack { get; set; }
        public int qty_packed { get; set; }
        public bool partial_item { get; set; }
        public string barcode_data { get; set; }
        public int weight_act { get; set; }
        public int operatorId { get; set; }
        public DateTime created_date { get; set; }
        public DateTime mod_date { get; set; }
        //public string description { get; set; }
        public string pack_description { get; set; }    //new
        public int ref_from { get; set; }               //new
        public int ref_to { get; set; }                 //new
        public int subpack_from { get; set; }           //new
        public int subpack_to { get; set; }             //new
        public bool completation_done { get; set; }
        public int booked_station_id { get; set; }
    }


    public class DispatchAddress
    {
        /*
         CREATE TABLE `dispatch_address_table` (
         `reference_id` varchar(50) CHARACTER SET latin1 NOT NULL,
         `file_name` varchar(255) CHARACTER SET latin1 NOT NULL,
         `created_date` datetime NOT NULL,
         `mod_date` datetime NOT NULL,
         `courier_type` varchar(255) CHARACTER SET latin1 NOT NULL,
         `dispatch_name` varchar(255) CHARACTER SET latin1 NOT NULL,
         `dispatch_surname` varchar(255) CHARACTER SET latin1 NOT NULL,
         `dispatch_tel` varchar(255) CHARACTER SET latin1 NOT NULL,
         `dispatch_date` date NOT NULL,
         `street` varchar(255) CHARACTER SET latin1 NOT NULL,
         `city` varchar(255) CHARACTER SET latin1 NOT NULL,
         `post_code` varchar(255) CHARACTER SET latin1 NOT NULL,
         `dispatch_company` varchar(255) CHARACTER SET latin1 NOT NULL,
         `list_description` varchar(255) CHARACTER SET latin1 NOT NULL,
         `pack_type` varchar(45) CHARACTER SET latin1 NOT NULL,
         `total_packs` int(4) NOT NULL,
         `ref_from` int(4) NOT NULL,
         `ref_to` int(4) NOT NULL,
         `deliv_list_no` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
         PRIMARY KEY (`reference_id`,`ref_from`,`ref_to`)
            )
         ALTER TABLE dispatch_address_table ADD comments varchar(255) NULL;

             */
        //public int id { get; set; }
        public string reference_id { get; set; }
        public string file_name { get; set; }
        public DateTime created_date { get; set; }
        public DateTime mod_date { get; set; }
        public string courier_type { get; set; }
        public string dispatch_name { get; set; }
        public string dispatch_surname { get; set; }
        public string dispatch_tel { get; set; }
        public DateTime dispatch_date { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string post_code { get; set; }
        public string dispatch_company { get; set; }
        //public string description { get; set; }
        public string list_description { get; set; }    //new
        public string pack_type { get; set; }           //new
        public int total_packs { get; set; }            //new
        public int ref_from { get; set; }               //new
        public int ref_to { get; set; }                 //new
        public string deliv_list_no { get; set; }       //new
        public string comments { get; set; }       //new

    }

    public class Files
    {
        public string fileName { get; set; }
        public FileInfo info { get; set; }
        public string text { get; set; }
        public string reference_id { get; set; }
        public int ref_from { get; set; }
        public int ref_to { get; set; }
        public string deliv_list_no { get; set; }       //new
    }

    public class FinishingDispatchAddressAndItem
    {
        public DispatchAddress address { get; set; }
        public DispatchItem item { get; set; }

        //public int item_id { get { return this.item.id; } }
        public string item_reference_id { get { return this.item.reference_id; } }       //new
        public int dispatch_id { get { return this.item.qty_packed; } }
        public string jdnr { get { return this.item.jdnr; } }
        public string jdline { get { return this.item.jdline; } }
        public int qty_to_pack { get { return this.item.qty_to_pack; } }
        public int qty_packed { get { return this.item.qty_packed; } }
        public bool partial_item { get { return this.item.partial_item; } }
        public string barcode_data { get { return this.item.barcode_data; } }
        public int weight_act { get { return this.item.weight_act; } }
        public int operatorId { get { return this.item.operatorId; } }
        public DateTime created_date { get { return this.item.created_date; } }
        public DateTime mod_date { get { return this.item.mod_date; } }
        //public string description { get { return this.item.description; } }
        public string pack_description { get { return this.item.pack_description; } }   //new
        public int item_ref_from { get { return this.item.ref_from; } }                 //new
        public int item_ref_to { get { return this.item.ref_to; } }                     //new
        public int subpack_from { get { return this.item.subpack_from; } }              //new
        public int subpack_to { get { return this.item.subpack_to; } }                  //new
        public bool completation_done { get { return this.item.completation_done; } }
        public int booked_station_id { get { return this.item.booked_station_id; } }

        //public int address_id { get { return this.address.id; } }
        public string address_reference_id { get { return this.address.reference_id; } }
        public string file_name { get { return this.address.file_name; } }
        public DateTime address_created_date { get { return this.address.created_date; } }
        public DateTime address_mod_date { get { return this.address.mod_date; } }
        public string courier_type { get { return this.address.courier_type; } }
        public string dispatch_name { get { return this.address.dispatch_name; } }
        public string dispatch_surname { get { return this.address.dispatch_surname; } }
        public string dispatch_tel { get { return this.address.dispatch_tel; } }
        public DateTime dispatch_date { get { return this.address.dispatch_date; } }
        public string street { get { return this.address.street; } }
        public string city { get { return this.address.city; } }
        public string post_code { get { return this.address.post_code; } }
        public string dispatch_company { get { return this.address.dispatch_company; } }
        //public string address_description { get { return this.address.description; } }
        public string list_description { get { return this.address.list_description; } }    //new
        public string pack_type { get { return this.address.pack_type; } }                  //new
        public int total_packs { get { return this.address.total_packs; } }                 //new
        public int address_ref_from { get { return this.address.ref_from; } }               //new
        public int address_ref_to { get { return this.address.ref_to; } }                   //new
        public string deliv_list_no { get { return this.address.deliv_list_no; } }          //new
        public string comments { get { return this.address.comments; } }          //new

        public FinishingDispatchAddressAndItem()
        {
            this.address = new DispatchAddress();
            this.item = new DispatchItem();
        }
    }


}
