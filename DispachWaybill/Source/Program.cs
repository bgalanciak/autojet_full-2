﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime;

    namespace DispachWaybill
{
    static class Program
    {
        public static DispachWaybillForm DispachWaybillForm;
        public static FileTree Ft;
        public static Settings Settings;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ProfileOptimization.SetProfileRoot(Application.StartupPath);
            ProfileOptimization.StartProfile("Startup.Profile1");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DispachWaybillForm = new DispachWaybillForm();

            //
            Program.Ft = new FileTree();
            Program.Settings = new DispachWaybill.Settings(Program.Ft.Get(FileTree.ID.FT_DEFAULT_SETTINGS));

            if (utilities.chceckMultipleInstatnce())
            {
                MessageBox.Show("Aplikacja jest już uruchomiona.\n", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MultipleInstatnceExit = true;
                Application.Exit();
                return;
            }
            else
            {
                Application.Run(DispachWaybillForm);
            }
        }
    }
}
