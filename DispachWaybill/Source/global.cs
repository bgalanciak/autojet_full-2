﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWaybill
{
    public static class global
    {
        public static List<DispatchAddress> DispatchAddressList = new List<DispatchAddress>();
        public static List<Files> FileList = new List<Files>();
        public static int fileMoveOk;
        public static int fileMoveErr;

        //for finishing simulation:
        //public static DispatchItem actItem;
        public static FinishingDispatchAddressAndItem actAddressAndItem;
        public static int stationId = 1;
    }
}
