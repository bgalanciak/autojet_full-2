﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TikaOnDotNet.TextExtraction;

namespace DispachWaybill
{
    public partial class DispachWaybillForm : Form
    {
        bool devMode = false;   //true = bez połączenia z bazą danych
        public HealthMonitor Hm;
        public fShowProgress progressForm = new fShowProgress();

        public DispachWaybillForm()
        {
            InitializeComponent();
            //global.actAddressAndItem = new FinishingDispatchAddressAndItem();
        }

        private void DispachWaybillForm_Load(object sender, EventArgs e)
        {
            this.Text = utilities.setWindowTitle();
            utilities.logAppStart();

            this.Hm = new HealthMonitor(this.debugTestBox, Program.Ft.Get(FileTree.ID.FT_DEBUG_DMP), true);

            var textExtractor = new TextExtractor();

            var wordDocContents = textExtractor.Extract(Application.StartupPath + "\\test.pdf");

            TabPage tp1 = mainTabControl.TabPages[1];
            TabPage tp2 = mainTabControl.TabPages[2];
            TabPage tp3 = mainTabControl.TabPages[3];
            mainTabControl.TabPages.Remove(tp1);
            mainTabControl.TabPages.Remove(tp2);
            mainTabControl.TabPages.Remove(tp3);
        }


        private void bDispachListRef_Click(object sender, EventArgs e)
        {
            DispatchAddressListRefresh();
        }

        private void DispatchAddressListRefresh()
        {
            global.DispatchAddressList = null;
            global.DispatchAddressList = dataAcces.getAllEmptyDispatchAddresses();
            dgvDispachList.DataSource = global.DispatchAddressList;
        }

        private void OnConnectionStateChange(object sender, StateChangeEventArgs args)
        {
            switch (args.CurrentState)
            {
                case ConnectionState.Open:
                    {
                        this.statusStripeDb.Text = "Baza danych: połączono";
                        this.statusStripeDb.ForeColor = Color.Green;
                        //this.statusStripeDb.Image = Properties.Resources.yes;
                        break;
                    }
                default:
                    {
                        this.statusStripeDb.Text = "Baza danych: nie połączono";
                        this.statusStripeDb.ForeColor = Color.Red;
                        //this.statusStripeDb.Image = Properties.Resources.no;
                        break;
                    }
            }
        }

        private void tConnection_Tick(object sender, EventArgs e)
        {
            //'
            if (!devMode)
                if (!SQLConnection.IsActive())
                    SQLConnection.Connect(OnConnectionStateChange);
        }

        private void bFileListRef_Click(object sender, EventArgs e)
        {
            FileListRefresh();
        }

        private void FileListRefresh()
        {
            global.FileList = null;
            global.FileList = dataAcces.GetFileList(Program.Settings.Data.SourceFilePath);

            foreach (Files f in global.FileList)
            {
                f.text = dataAcces.openPdfFile(f.info);
                f.reference_id = dataAcces.findReferenceId(f);
                f.ref_from = dataAcces.findRefFrom(f);
                f.ref_to = dataAcces.findRefTo(f);
                f.deliv_list_no = dataAcces.findDeliveryNo(f);
            }
            dgvFileList.DataSource = global.FileList;
        }

        private void bStart_Click(object sender, EventArgs e)
        {
            progressForm = new fShowProgress();
            progressForm.progress.all = dataAcces.GetFileList(Program.Settings.Data.SourceFilePath).Count();

            bwShowProgress.RunWorkerAsync();
            progressForm.ShowDialog();
            //updateAllFiles();
            //dodano 2018-07-10
            DispatchAddressListRefresh();
            FileListRefresh();
        }

        public void updateAllFiles()
        {
            int addresCount = global.DispatchAddressList.Count;
            int fileCount = global.FileList.Count;
            int fileMoveOk = 0;
            int fileMoveErr = 0;
            List<string> errorList = new List<string>();

            foreach (Files f in global.FileList)
            {
                if (f.reference_id != null)
                {
                    //find address for file
                    DispatchAddress dispachAddress = dataAcces.FIndDispachAdress(f);

                    if (dispachAddress != null)
                    {
                        //determind file_name
                        dispachAddress.file_name = $"{dispachAddress.reference_id}_{DateTime.Now.Year}-{DateTime.Now.Month.ToString("D2")}-{DateTime.Now.Day.ToString("D2")} {DateTime.Now.Hour.ToString("D2")}-{DateTime.Now.Minute.ToString("D2")}-{DateTime.Now.Second.ToString("D2")}.pdf";

                        //update data in database
                        dataAcces.UpdateDispachAdress(dispachAddress);

                        //move file
                        //destFile = System.IO.Path.Combine(targetPath, fileName);
                        try
                        {
                            System.IO.File.Move(f.fileName, System.IO.Path.Combine(Program.Settings.Data.DestFilePath, dispachAddress.file_name));
                            //System.IO.File.Move(f.fileName, "d:\\tst.pdf");
                            fileMoveOk++;
                        }
                        catch (Exception e)
                        {
                            fileMoveErr++;
                            errorList.Add(e.Message);
                        }

                    }
                }
            }

        }

        private void bwShowProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            global.DispatchAddressList = null;
            global.DispatchAddressList = dataAcces.getAllEmptyDispatchAddresses();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            global.FileList = null;
            global.FileList = dataAcces.GetFileList(Program.Settings.Data.SourceFilePath);

            int actFile = 0;
            foreach (Files f in global.FileList)
            {
                f.text = dataAcces.openPdfFile(f.info);
                f.reference_id = dataAcces.findReferenceId(f);
                f.ref_from = dataAcces.findRefFrom(f);
                f.ref_to = dataAcces.findRefTo(f);
                f.deliv_list_no = dataAcces.findDeliveryNo(f);
                actFile++;
                bwShowProgress.ReportProgress(actFile);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            int addresCount = global.DispatchAddressList.Count;
            int fileCount = global.FileList.Count;
            global.fileMoveOk = 0;
            global.fileMoveErr = 0;
            List<string> errorList = new List<string>();

            foreach (Files f in global.FileList)
            {
                if (f.reference_id != null)
                {
                    //find address for file
                    DispatchAddress dispachAddress = dataAcces.FIndDispachAdress(f);

                    if (dispachAddress != null)
                    {
                        //determind file_name
                        dispachAddress.file_name = $"{dispachAddress.reference_id}_{dispachAddress.ref_from}_{dispachAddress.ref_to}_{DateTime.Now.Year}-{DateTime.Now.Month.ToString("D2")}-{DateTime.Now.Day.ToString("D2")} {DateTime.Now.Hour.ToString("D2")}-{DateTime.Now.Minute.ToString("D2")}-{DateTime.Now.Second.ToString("D2")}.pdf";

                        //dodane 2018-07-10
                        dispachAddress.deliv_list_no = f.deliv_list_no;

                        //move file
                        //destFile = System.IO.Path.Combine(targetPath, fileName);
                        try
                        {
                            System.IO.File.Move(f.fileName, System.IO.Path.Combine(Program.Settings.Data.DestFilePath, dispachAddress.file_name));
                            //System.IO.File.Move(f.fileName, "d:\\tst.pdf");
                            global.fileMoveOk++;

                            //update data in database
                            //zmiana 2018-07-10
                            dataAcces.UpdateDispachAdress(dispachAddress);
                        }
                        catch (Exception ex)
                        {
                            global.fileMoveErr++;
                            errorList.Add(ex.Message);
                        }

                    }
                }
            }
        }

        private void bwShowProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int i = e.ProgressPercentage;

            progressForm.progress.done = i;
            progressForm.updateUI();
        }

        private void bwShowProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dgvFileList.DataSource = global.FileList;
            dgvDispachList.DataSource = global.DispatchAddressList;
            progressForm.Close();
            if (global.fileMoveErr > 0)
            {
                MessageBox.Show($"Znaleziono plików: {global.FileList.Count.ToString()}\nPrzeniesiono: {global.fileMoveOk.ToString()}\nNie przeniesiono{global.fileMoveErr.ToString()}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show($"Znaleziono plików: {global.FileList.Count.ToString()}\nPrzeniesiono: {global.fileMoveOk.ToString()}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        List<FinishingDispatchAddressAndItem> FinishingDispatchAddressAndItemList;

        private void bFindAdresses_Click(object sender, EventArgs e)
        {
            int jdline, jdnr;

            bool isDigit1, isDigit2;
            isDigit1 = int.TryParse(tbJdnr.Text, out jdnr);
            isDigit2 = int.TryParse(tbJdline.Text, out jdline);

            if ((isDigit1) && (isDigit2))
            {
                findAdresses(jdline, jdnr);
            }
        }
        

        private void findAdresses(int jdline, int jdnr)
        {
            FinishingDispatchAddressAndItemList = new List<FinishingDispatchAddressAndItem>();
            
            FinishingDispatchAddressAndItemList = dataAcces.findAllDispach(jdline, jdnr);
            dgvDispAddresses.DataSource = FinishingDispatchAddressAndItemList;
            if (global.actAddressAndItem!=null)
                markActItemDgv(global.actAddressAndItem.item);
        }

        private void bBookDispach_Click(object sender, EventArgs e)
        {
            int jdnr, jdline;

            bool isDigit1, isDigit2;
            isDigit1 = int.TryParse(tbJdnr.Text, out jdnr);
            isDigit2 = int.TryParse(tbJdline.Text, out jdline);

            if ((isDigit1) && (isDigit2))
            {
                //global.actAddressAndItem.item = dataAcces.FindNextOpenDispach(jdline, jdnr, global.stationId);
                global.actAddressAndItem = dataAcces.findNextDispachAddressAndItem(jdline, jdnr, global.stationId);
                //if it's not booked, book it
                if (global.actAddressAndItem.item.booked_station_id==0)
                {
                    //book
                    dataAcces.BookOpenDispach(global.actAddressAndItem.item, global.stationId);
                    global.actAddressAndItem.item.booked_station_id = global.stationId;
                }
                findAdresses(jdline, jdnr);
                
            }
        }

        private void markActItemDgv(DispatchItem actItem)
        {
            foreach(DataGridViewRow row in dgvDispAddresses.Rows)
            {
                string refId = row.Cells["item_reference_id"].Value.ToString();
                int refFrom = int.Parse(row.Cells["item_ref_from"].Value.ToString());
                int refTo = int.Parse(row.Cells["item_ref_to"].Value.ToString());
                if (actItem.reference_id.Equals(refId) && actItem.ref_from == refFrom && actItem.ref_to == refTo)
                {
                    DataGridViewCellStyle a = new DataGridViewCellStyle(row.DefaultCellStyle);
                    a.BackColor = Color.Green;
                    row.DefaultCellStyle = a;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (global.actAddressAndItem.item!=null)
            {
                global.actAddressAndItem.item.qty_packed++;
                if (global.actAddressAndItem.item.qty_packed>= global.actAddressAndItem.item.qty_to_pack)
                {
                    //close item and print label
                    dataAcces.UpdateDispachItemPackedQty(global.actAddressAndItem.item);           //update packed_qty
                    dataAcces.UpdateDispachItemcompletationDone(global.actAddressAndItem.item);    //update completation_done and booked_station_id
                    if ((!global.actAddressAndItem.item.partial_item)&&(global.actAddressAndItem.address.file_name.Length>0))
                    {
                        SendToPrinter(System.IO.Path.Combine(Program.Settings.Data.DestFilePath, global.actAddressAndItem.address.file_name));
                    }
                }
                else
                {
                    dataAcces.UpdateDispachItemPackedQty(global.actAddressAndItem.item);
                }
            }
        }

        private void testDrukarkiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendToPrinter(Application.StartupPath + "\\test.pdf");
        }

        private void testDrukarkiZPLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string str = "asdfghjklmnbvcxzśążźćłóęńŚĄŻŹŁÓĘŃ";
            string str2 = RemoveDiacritics(str);
            SendToPrinterZPL(global.actAddressAndItem);
            SendToPrinterZPLPartial(global.actAddressAndItem);
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dispachWaybillAbout about = new dispachWaybillAbout();
            about.ShowDialog();
        }
    }
}
