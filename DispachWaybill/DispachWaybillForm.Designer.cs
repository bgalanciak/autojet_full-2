﻿namespace DispachWaybill
{
    partial class DispachWaybillForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testDrukarkiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testDrukarkiZPLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.statusStripeDb = new System.Windows.Forms.ToolStripStatusLabel();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bDispachListRef = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvDispachList = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.bStart = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bFileListRef = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvFileList = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.debugTestBox = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.bFinishingDone = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvDispAddresses = new System.Windows.Forms.DataGridView();
            this.bFindAdresses = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lJdline = new System.Windows.Forms.Label();
            this.lJdnr = new System.Windows.Forms.Label();
            this.tbJdline = new System.Windows.Forms.TextBox();
            this.tbJdnr = new System.Windows.Forms.TextBox();
            this.bBookDispach = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tConnection = new System.Windows.Forms.Timer(this.components);
            this.bwShowProgress = new System.ComponentModel.BackgroundWorker();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuStrip.SuspendLayout();
            this.mainStatusStrip.SuspendLayout();
            this.mainTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispachList)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFileList)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispAddresses)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pomocToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(770, 24);
            this.mainMenuStrip.TabIndex = 0;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testDrukarkiToolStripMenuItem,
            this.testDrukarkiZPLToolStripMenuItem,
            this.oProgramieToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // testDrukarkiToolStripMenuItem
            // 
            this.testDrukarkiToolStripMenuItem.Name = "testDrukarkiToolStripMenuItem";
            this.testDrukarkiToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.testDrukarkiToolStripMenuItem.Text = "Test drukarki PDF";
            this.testDrukarkiToolStripMenuItem.Click += new System.EventHandler(this.testDrukarkiToolStripMenuItem_Click);
            // 
            // testDrukarkiZPLToolStripMenuItem
            // 
            this.testDrukarkiZPLToolStripMenuItem.Name = "testDrukarkiZPLToolStripMenuItem";
            this.testDrukarkiZPLToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.testDrukarkiZPLToolStripMenuItem.Text = "Test drukarki ZPL";
            this.testDrukarkiZPLToolStripMenuItem.Click += new System.EventHandler(this.testDrukarkiZPLToolStripMenuItem_Click);
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStripeDb});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 352);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Size = new System.Drawing.Size(770, 22);
            this.mainStatusStrip.TabIndex = 1;
            this.mainStatusStrip.Text = "statusStrip1";
            // 
            // statusStripeDb
            // 
            this.statusStripeDb.AutoSize = false;
            this.statusStripeDb.Name = "statusStripeDb";
            this.statusStripeDb.Size = new System.Drawing.Size(200, 17);
            this.statusStripeDb.Text = "...";
            this.statusStripeDb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.tabPage1);
            this.mainTabControl.Controls.Add(this.tabPage2);
            this.mainTabControl.Controls.Add(this.tabPage3);
            this.mainTabControl.Controls.Add(this.tabPage4);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 24);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(770, 328);
            this.mainTabControl.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(762, 302);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Listy przewozowe";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(756, 296);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.bDispachListRef);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(372, 44);
            this.panel1.TabIndex = 0;
            // 
            // bDispachListRef
            // 
            this.bDispachListRef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bDispachListRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDispachListRef.Location = new System.Drawing.Point(0, 0);
            this.bDispachListRef.Name = "bDispachListRef";
            this.bDispachListRef.Size = new System.Drawing.Size(372, 44);
            this.bDispachListRef.TabIndex = 0;
            this.bDispachListRef.Text = "Wyszukaj wysyłki";
            this.bDispachListRef.UseVisualStyleBackColor = true;
            this.bDispachListRef.Click += new System.EventHandler(this.bDispachListRef_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvDispachList);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 53);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(372, 190);
            this.panel2.TabIndex = 1;
            // 
            // dgvDispachList
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDispachList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDispachList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDispachList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDispachList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDispachList.Location = new System.Drawing.Point(0, 0);
            this.dgvDispachList.Name = "dgvDispachList";
            this.dgvDispachList.Size = new System.Drawing.Size(372, 190);
            this.dgvDispachList.TabIndex = 0;
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.bStart);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 249);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(750, 44);
            this.panel3.TabIndex = 2;
            // 
            // bStart
            // 
            this.bStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bStart.Location = new System.Drawing.Point(0, 0);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(750, 44);
            this.bStart.TabIndex = 0;
            this.bStart.Text = "Identyfikuj i zapisz zmiany";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.bStart_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.bFileListRef);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(381, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(372, 44);
            this.panel4.TabIndex = 3;
            // 
            // bFileListRef
            // 
            this.bFileListRef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFileListRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bFileListRef.Location = new System.Drawing.Point(0, 0);
            this.bFileListRef.Name = "bFileListRef";
            this.bFileListRef.Size = new System.Drawing.Size(372, 44);
            this.bFileListRef.TabIndex = 0;
            this.bFileListRef.Text = "Wyszukaj pliki";
            this.bFileListRef.UseVisualStyleBackColor = true;
            this.bFileListRef.Click += new System.EventHandler(this.bFileListRef_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dgvFileList);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(381, 53);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(372, 190);
            this.panel5.TabIndex = 4;
            // 
            // dgvFileList
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFileList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFileList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFileList.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFileList.Location = new System.Drawing.Point(0, 0);
            this.dgvFileList.Name = "dgvFileList";
            this.dgvFileList.Size = new System.Drawing.Size(372, 190);
            this.dgvFileList.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.debugTestBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(762, 302);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Debug";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // debugTestBox
            // 
            this.debugTestBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.debugTestBox.Location = new System.Drawing.Point(3, 3);
            this.debugTestBox.Name = "debugTestBox";
            this.debugTestBox.Size = new System.Drawing.Size(756, 296);
            this.debugTestBox.TabIndex = 0;
            this.debugTestBox.Text = "";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(762, 302);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Finishing";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.bFinishingDone, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.button2, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.button1, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.dgvDispAddresses, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.bFindAdresses, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.bBookDispach, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(756, 296);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // bFinishingDone
            // 
            this.bFinishingDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFinishingDone.Location = new System.Drawing.Point(381, 219);
            this.bFinishingDone.Name = "bFinishingDone";
            this.bFinishingDone.Size = new System.Drawing.Size(372, 34);
            this.bFinishingDone.TabIndex = 6;
            this.bFinishingDone.Text = "Skompletowano";
            this.bFinishingDone.UseVisualStyleBackColor = true;
            this.bFinishingDone.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(381, 259);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(372, 34);
            this.button2.TabIndex = 5;
            this.button2.Text = "Zarezerwuj wysyłkę";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 259);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(372, 34);
            this.button1.TabIndex = 4;
            this.button1.Text = "Zarezerwuj wysyłkę";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // dgvDispAddresses
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDispAddresses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDispAddresses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.dgvDispAddresses, 2);
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDispAddresses.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDispAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDispAddresses.Location = new System.Drawing.Point(3, 43);
            this.dgvDispAddresses.Name = "dgvDispAddresses";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDispAddresses.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvDispAddresses.Size = new System.Drawing.Size(750, 170);
            this.dgvDispAddresses.TabIndex = 0;
            // 
            // bFindAdresses
            // 
            this.bFindAdresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFindAdresses.Location = new System.Drawing.Point(3, 3);
            this.bFindAdresses.Name = "bFindAdresses";
            this.bFindAdresses.Size = new System.Drawing.Size(372, 34);
            this.bFindAdresses.TabIndex = 1;
            this.bFindAdresses.Text = "Znajdz wysyłki";
            this.bFindAdresses.UseVisualStyleBackColor = true;
            this.bFindAdresses.Click += new System.EventHandler(this.bFindAdresses_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lJdline);
            this.panel6.Controls.Add(this.lJdnr);
            this.panel6.Controls.Add(this.tbJdline);
            this.panel6.Controls.Add(this.tbJdnr);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(381, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(372, 34);
            this.panel6.TabIndex = 2;
            // 
            // lJdline
            // 
            this.lJdline.AutoSize = true;
            this.lJdline.Location = new System.Drawing.Point(214, 11);
            this.lJdline.Name = "lJdline";
            this.lJdline.Size = new System.Drawing.Size(31, 13);
            this.lJdline.TabIndex = 3;
            this.lJdline.Text = "jdline";
            // 
            // lJdnr
            // 
            this.lJdnr.AutoSize = true;
            this.lJdnr.Location = new System.Drawing.Point(22, 11);
            this.lJdnr.Name = "lJdnr";
            this.lJdnr.Size = new System.Drawing.Size(24, 13);
            this.lJdnr.TabIndex = 2;
            this.lJdnr.Text = "jdnr";
            // 
            // tbJdline
            // 
            this.tbJdline.Location = new System.Drawing.Point(255, 8);
            this.tbJdline.Name = "tbJdline";
            this.tbJdline.Size = new System.Drawing.Size(100, 20);
            this.tbJdline.TabIndex = 1;
            // 
            // tbJdnr
            // 
            this.tbJdnr.Location = new System.Drawing.Point(63, 8);
            this.tbJdnr.Name = "tbJdnr";
            this.tbJdnr.Size = new System.Drawing.Size(100, 20);
            this.tbJdnr.TabIndex = 0;
            // 
            // bBookDispach
            // 
            this.bBookDispach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bBookDispach.Location = new System.Drawing.Point(3, 219);
            this.bBookDispach.Name = "bBookDispach";
            this.bBookDispach.Size = new System.Drawing.Size(372, 34);
            this.bBookDispach.TabIndex = 3;
            this.bBookDispach.Text = "Zarezerwuj pierwszą wysyłkę";
            this.bBookDispach.UseVisualStyleBackColor = true;
            this.bBookDispach.Click += new System.EventHandler(this.bBookDispach_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(762, 302);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tConnection
            // 
            this.tConnection.Enabled = true;
            this.tConnection.Tick += new System.EventHandler(this.tConnection_Tick);
            // 
            // bwShowProgress
            // 
            this.bwShowProgress.WorkerReportsProgress = true;
            this.bwShowProgress.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwShowProgress_DoWork);
            this.bwShowProgress.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwShowProgress_ProgressChanged);
            this.bwShowProgress.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwShowProgress_RunWorkerCompleted);
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.oProgramieToolStripMenuItem.Text = "O programie...";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
            // 
            // DispachWaybillForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 374);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "DispachWaybillForm";
            this.Text = "DispachWaybill";
            this.Load += new System.EventHandler(this.DispachWaybillForm_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.mainTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispachList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFileList)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispAddresses)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dgvDispachList;
        private System.Windows.Forms.DataGridView dgvFileList;
        private System.Windows.Forms.Button bDispachListRef;
        private System.Windows.Forms.Button bFileListRef;
        private System.Windows.Forms.Timer tConnection;
        private System.Windows.Forms.ToolStripStatusLabel statusStripeDb;
        private System.Windows.Forms.RichTextBox debugTestBox;
        private System.Windows.Forms.Button bStart;
        private System.ComponentModel.BackgroundWorker bwShowProgress;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView dgvDispAddresses;
        private System.Windows.Forms.Button bFindAdresses;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label lJdline;
        private System.Windows.Forms.Label lJdnr;
        private System.Windows.Forms.TextBox tbJdline;
        private System.Windows.Forms.TextBox tbJdnr;
        private System.Windows.Forms.Button bBookDispach;
        private System.Windows.Forms.Button bFinishingDone;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem testDrukarkiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testDrukarkiZPLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
    }
}

