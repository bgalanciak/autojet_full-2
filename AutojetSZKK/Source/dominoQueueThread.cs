﻿using System;
using System.Threading;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Timers;

namespace AutojetSZKK
{
    class dominoQueueThread
    {
        public static bool isEnabled = true;
        public static bool doRestart = false;
        public static bool doHardRestart = false;
        public static bool hardRestartAllowed = false;
        public static bool waitPrinterResponse = false;
        public static byte[] lastCode = new byte[256];

        public static System.Timers.Timer printerACK;

        public static System.Timers.Timer printerRestartTimer;

        public static void main2()
        {
            printerRestartTimer = new System.Timers.Timer();
            printerRestartTimer.Elapsed += new ElapsedEventHandler(OnRestart_Tick);
            printerRestartTimer.Interval = 600000; //600000 10 min
            printerRestartTimer.Enabled = true;

            printerACK = new System.Timers.Timer();
            printerACK.Elapsed += new ElapsedEventHandler(OnACKReceived_Tick);
            printerACK.Interval = 5000;
            printerACK.Enabled = false;

            while (isEnabled)
            {
                Thread.Sleep(2000); //Dla bezpieczeństwa żeby za szybko nie wysyłał danych do drukarki
                byte[] buff = new byte[256];

                if (autojetSZKKMainForm.firstTimeConnected == 1)
                {
                    //dohardrestart -if no response after sheet printed
                    //dorestart - 10 minutes of no activity

                    //if (hardRestartAllowed == false)
                    //{
                        //Restart printer
                        if ((doRestart == true && !Program.autojetSZKKMain.plcPrintOn /*&& waitPrinterResponse == false*/) || (doHardRestart == true && !Program.autojetSZKKMain.plcPrintOn && hardRestartAllowed==true)) 
                        {//2018-12-04 dopisałem hardrestartallowed
                            doRestart = false;

                        //2018-12-04 dopisałem  i skomentowałem poniższe
                            hardRestartAllowed = false;
                         //   if (doHardRestart == true)
                          //  {
                                Program.autojetSZKKMain.Hm.Info("HARD Restart started");
                                doHardRestart = false;
     
                                waitPrinterResponse = false;

                                printerACK.Enabled = false;

                                try
                                {
                                    printerRestartTimer.Enabled = false;
                                    printerRestartTimer.Enabled = true;

                                    Program.autojetSZKKMain.Hm.Info("Connection Reset Start od port 703");
                                    Program.autojetSZKKMain.printerCodenet.Disconnect();
                                    Thread.Sleep(500);

                                    Program.autojetSZKKMain.Hm.Info("Connection Reset TCP Client");

                                    TcpClient intercomm = new TcpClient();
                                    intercomm.Connect(Program.Settings.Data.dominoPrinterIP2, 703);
                                    String str = "\x07" + "\x01" + "\x01" + "\x2C";
                                    Stream stm = intercomm.GetStream();
                                    ASCIIEncoding asen = new ASCIIEncoding();
                                    byte[] ba = asen.GetBytes(str);
                                    stm.Write(ba, 0, ba.Length);
                                    intercomm.Close();

                                    Thread.Sleep(500);

                                    Program.autojetSZKKMain.printerCodenet.Connect();
                                    Thread.Sleep(1000);

                                    if (Program.autojetSZKKMain.printerCodenet.connected)
                                    {
                                        Program.autojetSZKKMain.printerConnected();
                                        Program.autojetSZKKMain.Hm.Info("Connection Reset Done od port 703");
                                    }

                                    if (Program.autojetSZKKMain.printerCodenet.SendData(lastCode) == false)
                                    {
                                        Program.autojetSZKKMain.Hm.Info("Error: Wrong response to Send Data after Last Code Sent");
                                    }
                                    string s = System.Text.Encoding.Default.GetString(lastCode);
                                    Program.autojetSZKKMain.Hm.Info("dominoQueueThread 2 sent last Code: '{0}'", s);
                                }
                                catch (Exception ex)
                                {
                                    Program.autojetSZKKMain.printerCodenet.setAsDisconnected();
                                    Program.autojetSZKKMain.printerDisconnected();
                                    Program.autojetSZKKMain.Hm.Info("Error: during printer Restart {0}", ex.ToString());
                                }
                            //}

                            printerRestartTimer.Enabled = false;
                            printerRestartTimer.Enabled = true;
                        }

                        if (Program.autojetSZKKMain.printerCodenet.connected && waitPrinterResponse == false)
                        {
                            if (Program.autojetSZKKMain.printerQueue.Count > 0)
                            {
                                Program.autojetSZKKMain.Hm.Info("dominoQueueThread(): Printer2 out buffor count = {0}", Program.autojetSZKKMain.printerQueue.Count.ToString());

                                try
                                {
                                    if (Program.autojetSZKKMain.printerQueue.TryTake(out buff))
                                    {
                                        Array.Clear(lastCode, 0, lastCode.Length);
                                        buff.CopyTo(lastCode,0);

                                        Program.autojetSZKKMain.Hm.Info("dominoQueueThread 2, sending data (by TCPIP)");
                                        if (Program.autojetSZKKMain.printerCodenet.SendData(buff) == false)
                                        {
                                            Program.autojetSZKKMain.Hm.Info("Error: Wrong response to Send Data");
                                        }
                                        string s = System.Text.Encoding.Default.GetString(buff);
                                        Program.autojetSZKKMain.Hm.Info("dominoQueueThread 2 sent: '{0}'", s);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Program.autojetSZKKMain.Hm.Info("Error: in Queue Thread Send '{0}'", ex.ToString());
                                }
                            }
                        }
                        else if (!Program.autojetSZKKMain.printerCodenet.connected)
                        {
                            Program.autojetSZKKMain.Hm.Info("Printed Disconnected");
                            //doRestart = true;
                        }
                    //}
                }

            }
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private static void OnRestart_Tick(object source, ElapsedEventArgs e)
        {
            Program.autojetSZKKMain.Hm.Info("Restart 10 minutes tick REQUESTED");
            //doRestart = true;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private static void OnACKReceived_Tick(object source, ElapsedEventArgs e)
        {
            doHardRestart = true;
            //hardRestartAllowed = true; 2018-12-04 skomentowałem
            Program.autojetSZKKMain.Hm.Info("Warning: No response ACK, do hard restart REQUESTED");
            Program.autojetSZKKMain.printerCodenet.connected = false;
            printerACK.Enabled = false;
        }
    }
}

