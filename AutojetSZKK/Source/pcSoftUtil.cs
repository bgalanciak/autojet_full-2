﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcSoftUtilities
{
    public class dominoPrinter
    {
        //public md5Hash md5;

        public Byte[] printerIdentity()
        {
            //Printer Identity (RQ_PRINT_ID) 'A'
            //Esc/A/?/Eot

            //Esc/F/A/BBBB/Eot
            int size = 4;

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("A");
            retVal[2] = (byte)Convert.ToChar("?");
            retVal[3] = (byte)Convert.ToChar(4);

            return retVal;
        }


        /// <summary>Przykładowa etykieta dwurzędowa  
        /// <para>
        /// </summary>
        public Byte[] printerEmptyLabel()
        {
            int size1 = 11; //

            byte[] retVal = new byte[size1];
            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");
            retVal[6] = (byte)Convert.ToChar(27);
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("1");
            retVal[9] = (byte)Convert.ToChar(".");
            retVal[10] = (byte)Convert.ToChar(4);
            return retVal;
        }

        /// <summary>Przykładowa etykieta dwurzędowa  
        /// <para>
        /// </summary>
        public Byte[] printerLabel()
        {
            // Esc/u/2/                 - wielkość 2
            // Esc/q/6/ ..... Esc/q/0   - wstaw barcode Code 128 codeset B
            // SLCW0000000001
            // Esc/u/1/                 - wielkość 1
            // DECATUR 
            // data
            // godzina
            // QC1
            // Esc/r                    - nowa linia
            // 0000000001

            int size1 = 41; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");
            retVal[6] = (byte)Convert.ToChar(27);
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");
            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("6");



            retVal[12] = (byte)Convert.ToChar("0");   //0000001AAEC6D70ABC
            retVal[13] = (byte)Convert.ToChar("0");
            retVal[14] = (byte)Convert.ToChar("0");
            retVal[15] = (byte)Convert.ToChar("0");
            retVal[16] = (byte)Convert.ToChar("0");
            retVal[17] = (byte)Convert.ToChar("0");
            retVal[18] = (byte)Convert.ToChar("1");
            retVal[19] = (byte)Convert.ToChar("A");
            retVal[20] = (byte)Convert.ToChar("A");
            retVal[21] = (byte)Convert.ToChar("E");
            retVal[22] = (byte)Convert.ToChar("C");
            retVal[23] = (byte)Convert.ToChar("6");
            retVal[24] = (byte)Convert.ToChar("D");
            retVal[25] = (byte)Convert.ToChar("7");
            retVal[26] = (byte)Convert.ToChar("0");
            retVal[27] = (byte)Convert.ToChar("A");
            retVal[28] = (byte)Convert.ToChar("B");
            retVal[29] = (byte)Convert.ToChar("C");

            retVal[30] = (byte)Convert.ToChar(27);
            retVal[31] = (byte)Convert.ToChar("q");
            retVal[32] = (byte)Convert.ToChar("0");
            retVal[33] = (byte)Convert.ToChar(27);
            retVal[34] = (byte)Convert.ToChar("u");
            retVal[35] = (byte)Convert.ToChar("1");
            retVal[36] = (byte)Convert.ToChar("T");
            retVal[37] = (byte)Convert.ToChar("E");
            retVal[38] = (byte)Convert.ToChar("S");
            retVal[39] = (byte)Convert.ToChar("T");
            retVal[40] = (byte)Convert.ToChar(4);

            //Byte[] byteDateLine = Encoding.ASCII.GetBytes( m_tbMessage.Text.ToCharArray() );
            //m_sock.Send(retVal, retVal.Length, 0);
            return retVal;
        }

        /// <summary>Dwurzędowa etykieta Autojet dla Print&Display  
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabel(string kpracy, string lkpracy, string bryt, string serial, string printerNum)
        {
            /*
            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[indywidualny numer-licznik brytów na danej drukarce]_[drukarka]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 2 cyfry od 00 do 99
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia

            int size1 = 56; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("1");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar("-");
            retVal[19] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[20] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[21] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[22] = (byte)Convert.ToChar("-");
            retVal[23] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[24] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[25] = (byte)Convert.ToChar("-");
            retVal[26] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[27] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[28] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[29] = (byte)Convert.ToChar(27);
            retVal[30] = (byte)Convert.ToChar("q");
            retVal[31] = (byte)Convert.ToChar("0");

            retVal[32] = (byte)Convert.ToChar(27);   //wielkosc: 1
            retVal[33] = (byte)Convert.ToChar("u");
            retVal[34] = (byte)Convert.ToChar("1");

            retVal[35] = (byte)Convert.ToChar(" ");
            retVal[36] = (byte)Convert.ToChar(" ");
            retVal[37] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[38] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[39] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[40] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[41] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[42] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[43] = (byte)Convert.ToChar(" ");
            retVal[44] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[45] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[46] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[47] = (byte)Convert.ToChar(" ");
            retVal[48] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[49] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[50] = (byte)Convert.ToChar(" ");
            retVal[51] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[52] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[53] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[54] = (byte)Convert.ToChar(" ");

            retVal[55] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /// <summary>Dwurzędowa etykieta Autojet dla Print&Display  
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabel2(string kpracy, string lkpracy, string bryt, string printerNum, string serial)
        {
            /*
            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[nr_drukarki]_[indywidualny numer-licznik brytów na danej drukarce]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 4 cyfry od 0101
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia

            int size1 = 64; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("1");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar("-");
            retVal[19] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[20] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[21] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[22] = (byte)Convert.ToChar("-");
            retVal[23] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[24] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[25] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[26] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[27] = (byte)Convert.ToChar("-");
            retVal[28] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            retVal[29] = (byte)Convert.ToChar("-");
            retVal[30] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[31] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[32] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[33] = (byte)Convert.ToChar(27);
            retVal[34] = (byte)Convert.ToChar("q");
            retVal[35] = (byte)Convert.ToChar("0");

            retVal[36] = (byte)Convert.ToChar(27);   //wielkosc: 1
            retVal[37] = (byte)Convert.ToChar("u");
            retVal[38] = (byte)Convert.ToChar("1");

            retVal[39] = (byte)Convert.ToChar(" ");
            retVal[40] = (byte)Convert.ToChar(" ");
            retVal[41] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[42] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[43] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[44] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[45] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[46] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[47] = (byte)Convert.ToChar(" ");
            retVal[48] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[49] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[50] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[51] = (byte)Convert.ToChar(" ");
            retVal[52] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[53] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[54] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[55] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[56] = (byte)Convert.ToChar(" ");
            retVal[57] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            retVal[58] = (byte)Convert.ToChar(" ");
            retVal[59] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[60] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[61] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[62] = (byte)Convert.ToChar(" ");

            retVal[63] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /// <summary>Dwurzędowa krótka etykieta Autojet dla Print&Display, 
        /// na prośbę p. Pałysa, w kodzie kreskowym jest tylko 7 znakowy przypadkowy znaków
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabelShortRand(string kpracy, string lkpracy, string bryt, string printerNum, string serial)
        {
            /*
            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[nr_drukarki]_[indywidualny numer-licznik brytów na danej drukarce]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 4 cyfry od 0101
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
            */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia

            Random rnd = new Random();
            int randVal = rnd.Next(1, 9999999);
            string randStr = randVal.ToString("D7");

            int size1 = 50; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("1");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(randStr.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(randStr.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(randStr.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(randStr.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(randStr.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(randStr.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar(randStr.Substring(6, 1));

            retVal[19] = (byte)Convert.ToChar(27);
            retVal[20] = (byte)Convert.ToChar("q");
            retVal[21] = (byte)Convert.ToChar("0");

            retVal[22] = (byte)Convert.ToChar(27);   //wielkosc: 1
            retVal[23] = (byte)Convert.ToChar("u");
            retVal[24] = (byte)Convert.ToChar("1");

            retVal[25] = (byte)Convert.ToChar(" ");
            retVal[26] = (byte)Convert.ToChar(" ");
            retVal[27] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[28] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[29] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[30] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[31] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[32] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[33] = (byte)Convert.ToChar(" ");
            retVal[34] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[35] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[36] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[37] = (byte)Convert.ToChar(" ");
            retVal[38] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[39] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[40] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[41] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[42] = (byte)Convert.ToChar(" ");
            retVal[43] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            retVal[44] = (byte)Convert.ToChar(" ");
            retVal[45] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[46] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[47] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[48] = (byte)Convert.ToChar(" ");

            retVal[49] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /// <summary>Dwurzędowa etykieta Autojet dla Print&Display z kodem kresk typu Code 128
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabel128(string kpracy, string lkpracy, string bryt, string printerNum, string serial)
        {
            /*
            Nowy krotszy kod zawiera
            Nr KP – 6 cyfr
            Nr linii – 3 cyfry
            Nr brytu – 4 znaki
            Kolejny nr-  3 cyfry

            Etykieta składa się z: kod kreskowy x 2 + HRC

            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[nr_drukarki]_[indywidualny numer-licznik brytów na danej drukarce]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 4 cyfry od 0101
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia

            int size1 = 83; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            //kod kreskowy: #1
            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("6");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[19] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[20] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[21] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[22] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[23] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[24] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[25] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[26] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[27] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[28] = (byte)Convert.ToChar(27);
            retVal[29] = (byte)Convert.ToChar("q");
            retVal[30] = (byte)Convert.ToChar("0");

            //rozdzielenie (2x spacja)
            retVal[31] = (byte)Convert.ToChar(" ");
            retVal[32] = (byte)Convert.ToChar(" ");

            //kod kreskowy #2
            retVal[33] = (byte)Convert.ToChar(27);
            retVal[34] = (byte)Convert.ToChar("q");
            retVal[35] = (byte)Convert.ToChar("6");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[36] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[37] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[38] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[39] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[40] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[41] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[42] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[43] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[44] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[45] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[46] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[47] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[48] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[49] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[50] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[51] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[52] = (byte)Convert.ToChar(27);
            retVal[53] = (byte)Convert.ToChar("q");
            retVal[54] = (byte)Convert.ToChar("0");

            //HRC
            retVal[55] = (byte)Convert.ToChar(27);   //wielkosc: 1
            retVal[56] = (byte)Convert.ToChar("u");
            retVal[57] = (byte)Convert.ToChar("1");

            retVal[58] = (byte)Convert.ToChar(" ");
            retVal[59] = (byte)Convert.ToChar(" ");
            retVal[60] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[61] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[62] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[63] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[64] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[65] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[66] = (byte)Convert.ToChar(" ");
            retVal[67] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[68] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[69] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[70] = (byte)Convert.ToChar(" ");
            retVal[71] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[72] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[73] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[74] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[75] = (byte)Convert.ToChar(" ");
            retVal[76] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            retVal[77] = (byte)Convert.ToChar(" ");
            retVal[78] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[79] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[80] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[81] = (byte)Convert.ToChar(" ");

            retVal[82] = (byte)Convert.ToChar(4);

            return retVal;
        }


        /// <summary>Dwurzędowa krótka etykieta Autojet dla Print&Display z kodem kresk typu Code 128
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabel128Short(string kpracy, string lkpracy, string bryt, string printerNum, string serial)
        {
            /*
            Nowy krotszy kod zawiera
            Nr KP – 6 cyfr
            Nr linii – 3 cyfry
            Nr brytu – 4 znaki
            Kolejny nr-  3 cyfry

            Etykieta składa się z: kod kreskowy + HRC

            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[nr_drukarki]_[indywidualny numer-licznik brytów na danej drukarce]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 4 cyfry od 0101
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia

            int size1 = 61; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            //kod kreskowy: #1
            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("6");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[19] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[20] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[21] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[22] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[23] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[24] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[25] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[26] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[27] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[28] = (byte)Convert.ToChar(27);
            retVal[29] = (byte)Convert.ToChar("q");
            retVal[30] = (byte)Convert.ToChar("0");

            //rozdzielenie (2x spacja)
            retVal[31] = (byte)Convert.ToChar(" ");
            retVal[32] = (byte)Convert.ToChar(" ");

            //HRC
            retVal[33] = (byte)Convert.ToChar(27);   //wielkosc: 1
            retVal[34] = (byte)Convert.ToChar("u");
            retVal[35] = (byte)Convert.ToChar("1");

            retVal[36] = (byte)Convert.ToChar(" ");
            retVal[37] = (byte)Convert.ToChar(" ");
            retVal[38] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[39] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[40] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[41] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[42] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[43] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[44] = (byte)Convert.ToChar(" ");
            retVal[45] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[46] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[47] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[48] = (byte)Convert.ToChar(" ");
            retVal[49] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[50] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[51] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[52] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[53] = (byte)Convert.ToChar(" ");
            retVal[54] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            retVal[55] = (byte)Convert.ToChar(" ");
            retVal[56] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[57] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[58] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[59] = (byte)Convert.ToChar(" ");

            retVal[60] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /// <summary>Dwurzędowa krótka etykieta Autojet dla Print&Display  
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabelShort(string kpracy, string lkpracy, string bryt, string printerNum, string serial)
        {
            /*
            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[nr_drukarki]_[indywidualny numer-licznik brytów na danej drukarce]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 4 cyfry od 0101
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia

            int size1 = 37; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("1");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar("-");
            retVal[19] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[20] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[21] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[22] = (byte)Convert.ToChar("-");
            retVal[23] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[24] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[25] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[26] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[27] = (byte)Convert.ToChar("-");
            retVal[28] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            retVal[29] = (byte)Convert.ToChar("-");
            retVal[30] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[31] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[32] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[33] = (byte)Convert.ToChar(27);
            retVal[34] = (byte)Convert.ToChar("q");
            retVal[35] = (byte)Convert.ToChar("0");

            retVal[36] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /// <summary>Dwurzędowa krótka etykieta Autojet dla Print&Display  
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDDoubleLineleLabel2of5Id(string kpracy, string lkpracy, string bryt, string printerNum, string serial, int id, int barcodeMode)
        {
            /*
            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[nr_drukarki]_[indywidualny numer-licznik brytów na danej drukarce]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 4 cyfry od 0101
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/u/2/                 - wielkość 2
            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // 
            // Esc/u/1/                 - wielkość 1
            // 
            // Esc/r                    - nowa linia
            /*
             0
             1
             2
             3
             4
             5
             6
             7
             8
             9
             */
            string idStr = id.ToString("D8");
            string barcodeModeStr;
            //0: Code 39                                            = 1
            //1: 2 of 5 Interleaved, No checksum                    = 2
            //2: Code 128 codeset B                                 = 6
            //3: 2 of 5 Interleaved, Modulus 10 factor 3 checksum   = 7


            switch (barcodeMode)
            {
                case 0:
                    {
                        barcodeModeStr = "1";
                        break;
                    }
                case 1:
                    {
                        barcodeModeStr = "2";
                        break;
                    }
                case 2:
                    {
                        barcodeModeStr = "6";
                        break;
                    }
                case 3:
                    {
                        barcodeModeStr = "7";
                        break;
                    }
                default:
                    {
                        barcodeModeStr = "1";
                        break;
                    }
            }

            //int size1 = 95; //
            int size1 = 49;
            //int size1 = 81;

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);   //wielkosc: 2
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            //kod kreskowy: #1
            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            //retVal[11] = (byte)Convert.ToChar("2");  //.ToChar("7"); = 2 of 5 Interleaved, Modulus 10 factor 3 checksum .ToChar("6"); = code 128, .ToChar("1"); = code 39
            retVal[11] = (byte)Convert.ToChar(barcodeModeStr.Substring(0, 1));  //.ToChar("7"); = 2 of 5 Interleaved, Modulus 10 factor 3 checksum .ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[12] = (byte)Convert.ToChar(idStr.Substring(0, 1));
            retVal[13] = (byte)Convert.ToChar(idStr.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(idStr.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(idStr.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(idStr.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(idStr.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar(idStr.Substring(6, 1));
            retVal[19] = (byte)Convert.ToChar(idStr.Substring(7, 1));

            retVal[20] = (byte)Convert.ToChar(27);
            retVal[21] = (byte)Convert.ToChar("q");
            retVal[22] = (byte)Convert.ToChar("0");

            //rozdzielenie (1x spacja)
            retVal[23] = (byte)Convert.ToChar(" ");

            /*   
           //kod kreskowy #2
           retVal[24] = (byte)Convert.ToChar(27);
           retVal[25] = (byte)Convert.ToChar("q");
           retVal[26] = (byte)Convert.ToChar(barcodeModeStr.Substring(0, 1));  //.ToChar("7"); = 2 of 5 Interleaved, Modulus 10 factor 3 checksum .ToChar("6"); = code 128, .ToChar("1"); = code 39
           retVal[27] = (byte)Convert.ToChar(idStr.Substring(0, 1));
           retVal[28] = (byte)Convert.ToChar(idStr.Substring(1, 1));
           retVal[29] = (byte)Convert.ToChar(idStr.Substring(2, 1));
           retVal[30] = (byte)Convert.ToChar(idStr.Substring(3, 1));
           retVal[31] = (byte)Convert.ToChar(idStr.Substring(4, 1));
           retVal[32] = (byte)Convert.ToChar(idStr.Substring(5, 1));
           retVal[33] = (byte)Convert.ToChar(idStr.Substring(6, 1));
           retVal[34] = (byte)Convert.ToChar(idStr.Substring(7, 1));
           retVal[35] = (byte)Convert.ToChar(27);
           retVal[36] = (byte)Convert.ToChar("q");
           retVal[37] = (byte)Convert.ToChar("0");

           //rozdzielenie (1x spacja)
           retVal[38] = (byte)Convert.ToChar(" ");

           //kod kreskowy #3
           retVal[39] = (byte)Convert.ToChar(27);
           retVal[40] = (byte)Convert.ToChar("q");
           retVal[41] = (byte)Convert.ToChar(barcodeModeStr.Substring(0, 1));  //.ToChar("7"); = 2 of 5 Interleaved, Modulus 10 factor 3 checksum .ToChar("6"); = code 128, .ToChar("1"); = code 39
           retVal[42] = (byte)Convert.ToChar(idStr.Substring(0, 1));
           retVal[43] = (byte)Convert.ToChar(idStr.Substring(1, 1));
           retVal[44] = (byte)Convert.ToChar(idStr.Substring(2, 1));
           retVal[45] = (byte)Convert.ToChar(idStr.Substring(3, 1));
           retVal[46] = (byte)Convert.ToChar(idStr.Substring(4, 1));
           retVal[47] = (byte)Convert.ToChar(idStr.Substring(5, 1));
           retVal[48] = (byte)Convert.ToChar(idStr.Substring(6, 1));
           retVal[49] = (byte)Convert.ToChar(idStr.Substring(7, 1));
           retVal[50] = (byte)Convert.ToChar(27);
           retVal[51] = (byte)Convert.ToChar("q");
           retVal[52] = (byte)Convert.ToChar("0");

           //rozdzielenie (1x spacja)
           retVal[53] = (byte)Convert.ToChar(" ");

           //kod kreskowy #4
           retVal[54] = (byte)Convert.ToChar(27);
           retVal[55] = (byte)Convert.ToChar("q");
           retVal[56] = (byte)Convert.ToChar(barcodeModeStr.Substring(0, 1));  //.ToChar("7"); = 2 of 5 Interleaved, Modulus 10 factor 3 checksum .ToChar("6"); = code 128, .ToChar("1"); = code 39
           retVal[57] = (byte)Convert.ToChar(idStr.Substring(0, 1));
           retVal[58] = (byte)Convert.ToChar(idStr.Substring(1, 1));
           retVal[59] = (byte)Convert.ToChar(idStr.Substring(2, 1));
           retVal[60] = (byte)Convert.ToChar(idStr.Substring(3, 1));
           retVal[61] = (byte)Convert.ToChar(idStr.Substring(4, 1));
           retVal[62] = (byte)Convert.ToChar(idStr.Substring(5, 1));
           retVal[63] = (byte)Convert.ToChar(idStr.Substring(6, 1));
           retVal[64] = (byte)Convert.ToChar(idStr.Substring(7, 1));
           retVal[65] = (byte)Convert.ToChar(27);
           retVal[66] = (byte)Convert.ToChar("q");
           retVal[67] = (byte)Convert.ToChar("0");

           //HRC   
           retVal[68] = (byte)Convert.ToChar(27);   //wielkosc: 1
           retVal[69] = (byte)Convert.ToChar("u");
           retVal[70] = (byte)Convert.ToChar("1");
           retVal[71] = (byte)Convert.ToChar(" ");
           retVal[72] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
           retVal[73] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
           retVal[74] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
           retVal[75] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
           retVal[76] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
           retVal[77] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
           retVal[78] = (byte)Convert.ToChar(" ");
           retVal[79] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
           retVal[80] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
           retVal[81] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
           retVal[82] = (byte)Convert.ToChar(" ");
           retVal[83] = (byte)Convert.ToChar(bryt.Substring(0, 1));
           retVal[84] = (byte)Convert.ToChar(bryt.Substring(1, 1));
           retVal[85] = (byte)Convert.ToChar(bryt.Substring(2, 1));
           retVal[86] = (byte)Convert.ToChar(bryt.Substring(3, 1));
           retVal[87] = (byte)Convert.ToChar(" ");
           retVal[88] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
           retVal[89] = (byte)Convert.ToChar(" ");
           retVal[90] = (byte)Convert.ToChar(serial.Substring(0, 1));
           retVal[91] = (byte)Convert.ToChar(serial.Substring(1, 1));
           retVal[92] = (byte)Convert.ToChar(serial.Substring(2, 1));
           retVal[93] = (byte)Convert.ToChar(" ");
           retVal[94] = (byte)Convert.ToChar(4);
           */

            //HRC   
            retVal[24] = (byte)Convert.ToChar(27);   //wielkosc: 1
            retVal[25] = (byte)Convert.ToChar("u");
            retVal[26] = (byte)Convert.ToChar("1");
            retVal[27] = (byte)Convert.ToChar(" ");
            retVal[28] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[29] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[30] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[31] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[32] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[33] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[34] = (byte)Convert.ToChar(" ");
            retVal[35] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[36] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[37] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[38] = (byte)Convert.ToChar(" ");
            retVal[39] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[40] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[41] = (byte)Convert.ToChar(bryt.Substring(2, 1));
            retVal[42] = (byte)Convert.ToChar(bryt.Substring(3, 1));
            retVal[43] = (byte)Convert.ToChar(" ");
            //retVal[44] = (byte)Convert.ToChar(printerNum.Substring(0, 1));
            //retVal[45] = (byte)Convert.ToChar(" ");
            retVal[44] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[45] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[46] = (byte)Convert.ToChar(serial.Substring(2, 1));
            retVal[47] = (byte)Convert.ToChar(" ");
            retVal[48] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /// <summary>Jednorzędowa etykieta Autojet dla Print&Display  
        /// <para>
        /// </summary>
        public Byte[] printerAutojetPDSingleLineleLabel(string kpracy, string lkpracy, string bryt, string serial, string printerNum)
        {
            /*
            Kod kreskowy:
            [Numer karty pracy]_[numer linii karty pracy]_[nr brytu]_[indywidualny numer-licznik brytów na danej drukarce]_[drukarka]

            [Numer karty pracy] - 6 cyfr od 000000 do 999999
            [numer linii karty pracy] - 3 cyfry od 000 do 999
            [nr brytu] - 2 cyfry od 00 do 99
            [drukarka] - maszyna drukująca, cyfra 1,2 lub 3
            [indywidualny numer] - 3 cyfry (indywidulany numer brytu) (numeracja a w ramach jednej maszyny drukującej)
             */

            // Esc/q/1/ ..... Esc/q/0   - wstaw barcode 1 = Code 39
            // SLCW0000000001
            // Esc/u/1/                 - wielkość 1
            // DECATUR 
            // data
            // godzina
            // QC1
            // Esc/r                    - nowa linia
            // 0000000001

            int size1 = 57; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");
            retVal[6] = (byte)Convert.ToChar(27);
            retVal[7] = (byte)Convert.ToChar("q");
            retVal[8] = (byte)Convert.ToChar("1");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[9] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[10] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[11] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[12] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[13] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[14] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[15] = (byte)Convert.ToChar("*");
            retVal[16] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[17] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[18] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[19] = (byte)Convert.ToChar("*");
            retVal[20] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[21] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[22] = (byte)Convert.ToChar("*");
            retVal[23] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[24] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[25] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[26] = (byte)Convert.ToChar(27);
            retVal[27] = (byte)Convert.ToChar("q");
            retVal[28] = (byte)Convert.ToChar("0");

            retVal[29] = (byte)Convert.ToChar(" ");
            retVal[30] = (byte)Convert.ToChar(" ");
            retVal[31] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[32] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[33] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[34] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[35] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[36] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[37] = (byte)Convert.ToChar(" ");
            retVal[38] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[39] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[40] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[41] = (byte)Convert.ToChar(" ");
            retVal[42] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[43] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[44] = (byte)Convert.ToChar(" ");
            retVal[45] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[46] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[47] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[48] = (byte)Convert.ToChar(" ");
            retVal[49] = (byte)Convert.ToChar("A");
            retVal[50] = (byte)Convert.ToChar("U");
            retVal[51] = (byte)Convert.ToChar("T");
            retVal[52] = (byte)Convert.ToChar("O");
            retVal[53] = (byte)Convert.ToChar("J");
            retVal[54] = (byte)Convert.ToChar("E");
            retVal[55] = (byte)Convert.ToChar("T");

            retVal[56] = (byte)Convert.ToChar(4);

            return retVal;
        }


        //powtorzenie nadruku
        //UWAGA: kolejne etykiety drukowane są tylko przy wysokim stanie wejścia PD, 
        public Byte[] setRepeat(int repeatCount, int repeatPitch)
        {
            //Auto-repeat Printing (SET_REPEAT) 'G'
            //Esc/G/A/BB/CCCC/Eot
            //BB = 2 digits repeat number 00 to 99 (99 = continuous default 00)
            //CCCC = 4 digits repeat pitch, 0000 to 9999 strokes (default 0000)

            string n = repeatCount.ToString("D2");
            string p = repeatPitch.ToString("D3");

            int size = 10;

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("G");
            retVal[2] = (byte)Convert.ToChar("1");

            retVal[3] = (byte)Convert.ToChar(n.Substring(0, 1));
            retVal[4] = (byte)Convert.ToChar(n.Substring(1, 1));

            retVal[5] = (byte)Convert.ToChar("0");
            retVal[6] = (byte)Convert.ToChar(p.Substring(0, 1));
            retVal[7] = (byte)Convert.ToChar(p.Substring(1, 1));
            retVal[8] = (byte)Convert.ToChar(p.Substring(2, 1));

            retVal[9] = (byte)Convert.ToChar(4);

            return retVal;

        }

        //generuje długą, powtorzoną n razy etykiete addAutojetLabel(..)
        //UWAGA: drukarka zadziała tylko z max. 4 powtórzeniami
        public Byte[] printAutojetNLabel(string kpracy, string lkpracy, string bryt, string serial, string printerNum, int n)
        {
            //generuje nadruk:
            Byte[] label = addAutojetLabel(kpracy, lkpracy, bryt, serial, printerNum);

            int labelLen = label.Length;

            int size = n * labelLen + 7; //n*dlugosc nadruku + 7 nakow sterujacych

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");


            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 50; j++)
                {
                    retVal[(i * 50) + j + 6] = label[j];
                }
            }

            retVal[size - 1] = (byte)Convert.ToChar(4);

            return retVal;

        }

        //generuje tylko etykietkę typu printerAutojetSingLineleLabel(..) bez znaków kontrolnych
        //do wykozystania w funkcji printAutojetNLabel(..)
        public Byte[] addAutojetLabel(string kpracy, string lkpracy, string bryt, string serial, string printerNum)
        {
            byte[] retVal = new byte[50];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("q");
            retVal[2] = (byte)Convert.ToChar("1");  //.ToChar("6"); = code 128, .ToChar("1"); = code 39

            retVal[3] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[4] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[5] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[6] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[7] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[8] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[9] = (byte)Convert.ToChar("*");
            retVal[10] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[11] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[12] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[13] = (byte)Convert.ToChar("*");
            retVal[14] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[15] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[16] = (byte)Convert.ToChar("*");
            retVal[17] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[18] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[19] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[20] = (byte)Convert.ToChar(27);
            retVal[21] = (byte)Convert.ToChar("q");
            retVal[22] = (byte)Convert.ToChar("0");

            retVal[23] = (byte)Convert.ToChar(" ");
            retVal[24] = (byte)Convert.ToChar(" ");
            retVal[25] = (byte)Convert.ToChar(kpracy.Substring(0, 1));
            retVal[26] = (byte)Convert.ToChar(kpracy.Substring(1, 1));
            retVal[27] = (byte)Convert.ToChar(kpracy.Substring(2, 1));
            retVal[28] = (byte)Convert.ToChar(kpracy.Substring(3, 1));
            retVal[29] = (byte)Convert.ToChar(kpracy.Substring(4, 1));
            retVal[30] = (byte)Convert.ToChar(kpracy.Substring(5, 1));
            retVal[31] = (byte)Convert.ToChar(" ");
            retVal[32] = (byte)Convert.ToChar(lkpracy.Substring(0, 1));
            retVal[33] = (byte)Convert.ToChar(lkpracy.Substring(1, 1));
            retVal[34] = (byte)Convert.ToChar(lkpracy.Substring(2, 1));
            retVal[35] = (byte)Convert.ToChar(" ");
            retVal[36] = (byte)Convert.ToChar(bryt.Substring(0, 1));
            retVal[37] = (byte)Convert.ToChar(bryt.Substring(1, 1));
            retVal[38] = (byte)Convert.ToChar(" ");
            retVal[39] = (byte)Convert.ToChar(serial.Substring(0, 1));
            retVal[40] = (byte)Convert.ToChar(serial.Substring(1, 1));
            retVal[41] = (byte)Convert.ToChar(serial.Substring(2, 1));

            retVal[42] = (byte)Convert.ToChar(" ");
            retVal[43] = (byte)Convert.ToChar("A");
            retVal[44] = (byte)Convert.ToChar("U");
            retVal[45] = (byte)Convert.ToChar("T");
            retVal[46] = (byte)Convert.ToChar("O");
            retVal[47] = (byte)Convert.ToChar("J");
            retVal[48] = (byte)Convert.ToChar("E");
            retVal[49] = (byte)Convert.ToChar("T");

            return retVal;
        }

        public Byte[] printerDemoLabel()
        {
            // Esc/u/2/                 - wielkość 2
            // Esc/q/6/ ..... Esc/q/0   - wstaw barcode Code 128 codeset B
            // SLCW0000000001
            // Esc/u/1/                 - wielkość 1
            // DECATUR 
            // data
            // godzina
            // QC1
            // Esc/r                    - nowa linia
            // 0000000001

            int size1 = 41; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");
            retVal[6] = (byte)Convert.ToChar(27);
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");
            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("6");



            retVal[12] = (byte)Convert.ToChar("0");   //0000001AAEC6D70ABC
            retVal[13] = (byte)Convert.ToChar("0");
            retVal[14] = (byte)Convert.ToChar("0");
            retVal[15] = (byte)Convert.ToChar("0");
            retVal[16] = (byte)Convert.ToChar("0");
            retVal[17] = (byte)Convert.ToChar("0");
            retVal[18] = (byte)Convert.ToChar("1");
            retVal[19] = (byte)Convert.ToChar("A");
            retVal[20] = (byte)Convert.ToChar("A");
            retVal[21] = (byte)Convert.ToChar("E");
            retVal[22] = (byte)Convert.ToChar("C");
            retVal[23] = (byte)Convert.ToChar("6");
            retVal[24] = (byte)Convert.ToChar("D");
            retVal[25] = (byte)Convert.ToChar("7");
            retVal[26] = (byte)Convert.ToChar("0");
            retVal[27] = (byte)Convert.ToChar("A");
            retVal[28] = (byte)Convert.ToChar("B");
            retVal[29] = (byte)Convert.ToChar("C");

            retVal[30] = (byte)Convert.ToChar(27);
            retVal[31] = (byte)Convert.ToChar("q");
            retVal[32] = (byte)Convert.ToChar("0");
            retVal[33] = (byte)Convert.ToChar(27);
            retVal[34] = (byte)Convert.ToChar("u");
            retVal[35] = (byte)Convert.ToChar("1");
            retVal[36] = (byte)Convert.ToChar("T");
            retVal[37] = (byte)Convert.ToChar("E");
            retVal[38] = (byte)Convert.ToChar("S");
            retVal[39] = (byte)Convert.ToChar("T");
            retVal[40] = (byte)Convert.ToChar(4);

            return retVal;
        }

        /*
        //public Byte[] printerLabelUtal(int serialNum, int colour, int size, int type, char[] batch)
        public Byte[] printerLabelUtal(int serialNum, int colour, int size, string typeChar, char[] batch)
        {
            // Esc/O/Q/001/....         - Download message without save
            // Esc/u/2/                 - wielkość 2
            // Esc/q/6/ .....           - wstaw barcode Code 128 codeset B
            // 
            // Esc/q/0                  - koniec barkodu
            // Esc/u/1/                 - wielkość 1
            // 
            // 
            // 
            // 
            // Esc/r                    - nowa linia
            // 
            // Eot                      - END: Download message without save

            string serialNumLong = serialNum.ToString("D7");
            string batchStr = new string(batch);
            //string md5H = utalUtils.calcProPlateHash(serialNum, colour, size, type, batch);
            string md5H = utalUtils.calcProPlateHash(serialNum, colour, size, typeChar, batch);
            string qcNum = global.actUserQcNum.ToString("D2").Substring(0, 2);

            int size1 = 57; //

            byte[] retVal = new byte[size1];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("O");
            retVal[2] = (byte)Convert.ToChar("Q");
            retVal[3] = (byte)Convert.ToChar("0");
            retVal[4] = (byte)Convert.ToChar("0");
            retVal[5] = (byte)Convert.ToChar("1");

            retVal[6] = (byte)Convert.ToChar(27);
            retVal[7] = (byte)Convert.ToChar("u");
            retVal[8] = (byte)Convert.ToChar("2");

            retVal[9] = (byte)Convert.ToChar(27);
            retVal[10] = (byte)Convert.ToChar("q");
            retVal[11] = (byte)Convert.ToChar("6");

            retVal[12] = (byte)Convert.ToChar(serialNumLong.Substring(0, 1));   //serialNum
            retVal[13] = (byte)Convert.ToChar(serialNumLong.Substring(1, 1));
            retVal[14] = (byte)Convert.ToChar(serialNumLong.Substring(2, 1));
            retVal[15] = (byte)Convert.ToChar(serialNumLong.Substring(3, 1));
            retVal[16] = (byte)Convert.ToChar(serialNumLong.Substring(4, 1));
            retVal[17] = (byte)Convert.ToChar(serialNumLong.Substring(5, 1));
            retVal[18] = (byte)Convert.ToChar(serialNumLong.Substring(6, 1));
            retVal[19] = (byte)Convert.ToChar(batchStr.Substring(0, 1));
            retVal[20] = (byte)Convert.ToChar(batchStr.Substring(1, 1));
            retVal[21] = (byte)Convert.ToChar(md5H.Substring(0, 1));
            retVal[22] = (byte)Convert.ToChar(md5H.Substring(1, 1));
            retVal[23] = (byte)Convert.ToChar(md5H.Substring(2, 1));
            retVal[24] = (byte)Convert.ToChar(md5H.Substring(3, 1));
            retVal[25] = (byte)Convert.ToChar(md5H.Substring(4, 1));
            retVal[26] = (byte)Convert.ToChar(md5H.Substring(5, 1));
            retVal[27] = (byte)Convert.ToChar(colour.ToString().Substring(0, 1));
            retVal[28] = (byte)Convert.ToChar(size.ToString().Substring(0, 1));
            //retVal[29] = (byte)Convert.ToChar(type.ToString().Substring(0, 1));
            retVal[29] = (byte)Convert.ToChar(typeChar.ToString().Substring(0, 1));

            retVal[30] = (byte)Convert.ToChar(27);
            retVal[31] = (byte)Convert.ToChar("q");
            retVal[32] = (byte)Convert.ToChar("0");

            retVal[33] = (byte)Convert.ToChar(27);
            retVal[34] = (byte)Convert.ToChar("u");
            retVal[35] = (byte)Convert.ToChar("1");

            retVal[36] = (byte)Convert.ToChar(serialNumLong.Substring(0, 1));
            retVal[37] = (byte)Convert.ToChar(serialNumLong.Substring(1, 1));
            retVal[38] = (byte)Convert.ToChar(serialNumLong.Substring(2, 1));
            retVal[39] = (byte)Convert.ToChar(serialNumLong.Substring(3, 1));
            retVal[40] = (byte)Convert.ToChar(serialNumLong.Substring(4, 1));
            retVal[41] = (byte)Convert.ToChar(serialNumLong.Substring(5, 1));
            retVal[42] = (byte)Convert.ToChar(serialNumLong.Substring(6, 1));
            retVal[43] = (byte)Convert.ToChar(" ");
            retVal[44] = (byte)Convert.ToChar("Q");
            retVal[45] = (byte)Convert.ToChar("C");
            retVal[46] = (byte)Convert.ToChar(qcNum.Substring(0, 1));
            retVal[47] = (byte)Convert.ToChar(qcNum.Substring(1, 1));
            //retVal[46] = (byte)Convert.ToChar("0");
            //retVal[47] = (byte)Convert.ToChar("1");

            retVal[48] = (byte)Convert.ToChar(27);
            retVal[49] = (byte)Convert.ToChar("r");

            retVal[50] = (byte)Convert.ToChar(batchStr.Substring(0, 1));
            retVal[51] = (byte)Convert.ToChar(batchStr.Substring(1, 1));
            retVal[52] = (byte)Convert.ToChar(" ");
            retVal[53] = (byte)Convert.ToChar(colour.ToString().Substring(0, 1));
            retVal[54] = (byte)Convert.ToChar(size.ToString().Substring(0, 1));
            //retVal[55] = (byte)Convert.ToChar(type.ToString().Substring(0, 1));
            retVal[55] = (byte)Convert.ToChar(typeChar.ToString().Substring(0, 1));

            retVal[56] = (byte)Convert.ToChar(4);

            return retVal;
        }
        */

        public Byte[] printFormat(bool reverse, bool bold, bool doubleSpace, bool invert)
        {
            char rev;
            char bol;
            char ds;
            char inv;

            if (reverse)
                rev = 'Y';
            else
                rev = 'N';

            if (bold)
                bol = 'Y';
            else
                bol = 'N';

            if (doubleSpace)
                ds = 'Y';
            else
                ds = 'N';

            if (invert)
                inv = 'Y';
            else
                inv = 'N';

            //Global Print Format
            //Esc/g/A/B/C/D/E/F/Eot
            int size = 9;

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("g");
            retVal[2] = (byte)Convert.ToChar("1");
            retVal[3] = (byte)Convert.ToChar(rev);
            retVal[4] = (byte)Convert.ToChar(bol);
            retVal[5] = (byte)Convert.ToChar(ds);
            retVal[6] = (byte)Convert.ToChar(inv);
            retVal[7] = (byte)Convert.ToChar("N");
            retVal[8] = (byte)Convert.ToChar(4);

            return retVal;
        }

        public Byte[] printerInit()
        {
            //Printing Acknowledgement Flags (SET_ACK) 'I'
            //Esc/I/A/B/Eot
            int size = 5;

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("I");
            retVal[2] = (byte)Convert.ToChar("1");
            retVal[3] = (byte)Convert.ToChar("A");
            retVal[4] = (byte)Convert.ToChar(4);

            return retVal;
        }

        public Byte[] printGoDelay(int delay)
        {
            //Print Go Delay (SET_DELAY) 'F'
            //Esc/F/A/BBBB/Eot
            string strDelay = delay.ToString("D4");

            int size = 8;

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("F");
            retVal[2] = (byte)Convert.ToChar("1");
            retVal[3] = (byte)Convert.ToChar(strDelay[0]);
            retVal[4] = (byte)Convert.ToChar(strDelay[1]);
            retVal[5] = (byte)Convert.ToChar(strDelay[2]);
            retVal[6] = (byte)Convert.ToChar(strDelay[3]);
            retVal[7] = (byte)Convert.ToChar(4);

            return retVal;
        }

        public Byte[] printerSwGo()
        {
            //Software Print Go (SOFT_P_GO) 'N'
            //Esc/N/1/Eot
            int size = 4;

            byte[] retVal = new byte[size];

            retVal[0] = (byte)Convert.ToChar(27);
            retVal[1] = (byte)Convert.ToChar("N");
            retVal[2] = (byte)Convert.ToChar("1");
            retVal[3] = (byte)Convert.ToChar(4);

            return retVal;
        }
    }
}
