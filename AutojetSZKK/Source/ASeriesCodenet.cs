﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace AutojetSZKK
{
    public class ReceivedArgs : EventArgs
    {
        public string receivedText { get; private set; }

        public ReceivedArgs(string sReceivedText)
        {
            receivedText = sReceivedText;
        }
    }

    public class ASeriesCodenet
    {
        //variables
        #region

        private Socket clientTcp;
        private string ipAddress;
        public Boolean connected = false;
        private string buffer;
        private byte[] data = new byte[1024];
        private int size = 1024;


        public delegate void DataReceivedDelegate(object oSender, EventArgs oEventArgs);
        public delegate void ConnectedDelegate(object oSender, EventArgs oEventArgs);
        public delegate void ConnectionErrorDelegate(object oSender, EventArgs oEventArgs);
        public event ConnectedDelegate ConnectedSuccess;
        public event DataReceivedDelegate DataReceived;
        public event ConnectionErrorDelegate ConnectionError;

        #endregion

        //properties
        #region
        /// <summary>
        /// Printer IP Address. Use try catch to set value
        /// </summary>
        public string IpAddress
        {
            get
            {
                return ipAddress;
            }
        }

        /// <summary>
        /// Return TCP Client Connection 
        /// </summary>


        #endregion

        /// <summary>
        /// ASeriesUpdatableField constructor - use try catch to initialize
        /// </summary>
        /// <param name="ipAddress">Printer IP Address</param>
        public ASeriesCodenet(string ipAddress)
        {
            connected = false;
            try
            {
                IPAddress.Parse(ipAddress);
            }
            catch (FormatException)
            {
                throw new FormatException("Nieprawidłowy format adresu IP");
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("Argument ma wartość null");
            }
            this.ipAddress = ipAddress;
        }

        /// <summary>
        /// Start to connect with printer
        /// </summary>
        public void Connect()
        {
            Program.autojetSZKKMain.Hm.Info(String.Format("ASeriesCodenet Connect() Started"));

            try
            {
                connected = false;
                buffer = String.Empty;
                Socket newSock = new Socket(AddressFamily.InterNetwork,
                                   SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint iep = new IPEndPoint(IPAddress.Parse(ipAddress), 7000);
                newSock.BeginConnect(iep, new AsyncCallback(Connected), newSock);
                Program.autojetSZKKMain.Hm.Info(String.Format("ASeriesCodenet Socket Initialized on IP {0}", ipAddress));
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info(String.Format("ASeriesCodenet Connect - {0}", ex.Message));
            }
        }


        private void Connected(IAsyncResult iar)
        {
            Program.autojetSZKKMain.Hm.Info(String.Format("ASeriesCodenet Connected() Received"));

            try
            {
                clientTcp = (Socket)iar.AsyncState;
                if (clientTcp.Connected)
                {
                    Program.autojetSZKKMain.Hm.Info(String.Format("Client TCP Connected"));
                    connected = true;
                    clientTcp.BeginReceive(data, 0, size, SocketFlags.None,
                                  new AsyncCallback(ReceiveData), clientTcp);
                    clientTcp.EndConnect(iar);
                }
                else
                {
                    Program.autojetSZKKMain.Hm.Info(String.Format("Client TCP Not Connected"));
                    Connect();
                    return;
                }

                try
                {
                    //ustawienie wysyłania P jako potwierdzenia wydruku
                    //SendData("\x1B" + "I1P" + "\x04");
                    //czyszczenie kolejki
                    //SendData("\x1B" + "OE00000" + "\x04");

                    //Program.autojetSZKKMain.labelsSent++;
                    //Program.autojetSZKKMain.labelsSent++;
                    //wysyłka opóźnienia
                    //SendData("\x1B" + "F1" + Global.printerDelayOffset.PadLeft(4, '0') + "\x04");
                }
                catch (Exception ex)
                {
                    Program.autojetSZKKMain.Hm.Info(String.Format("Connected() Exception {0}", ex.ToString()));
                }

            }
            catch (SocketException sException)
            {
                Program.autojetSZKKMain.Hm.Info(String.Format("ASeriesCodenet Connected - {0}", sException.Message));
            }
        }

        void ReceiveData(IAsyncResult iar)
        {
            Program.autojetSZKKMain.Hm.Info("Receive Data in ASeries Codenet");

            try
            {
                Socket remote = (Socket)iar.AsyncState;
                if (remote.Connected == false)
                    return;
                int recv = remote.EndReceive(iar);
                string stringData = Encoding.ASCII.GetString(data, 0, recv);
                stringData.Replace("/0", String.Empty);
                buffer += stringData;

                if ((buffer.Contains("P")) || (buffer.Contains("\x06")))
                {
                    while (buffer.Contains("P"))
                    {
                        string response = "P";
                        buffer = buffer.Remove(0, buffer.IndexOf("P") + 1);
                        ReceivedArgs oReceivedData = new ReceivedArgs(response);
                        DataReceived(this, oReceivedData);
                    }
                    while (buffer.Contains("\x06"))
                    {
                        string response = "\x06";
                        buffer = buffer.Remove(0, buffer.IndexOf("\x06") + 1);
                        ReceivedArgs oReceivedData = new ReceivedArgs(response);
                        DataReceived(this, oReceivedData);
                    }
                    buffer = String.Empty;
                }
                if (connected)
                {
                    clientTcp.BeginReceive(data, 0, size, SocketFlags.None,
                  new AsyncCallback(ReceiveData), clientTcp);
                }
            }
            catch (SocketException sException)
            {
                // ReceivedArgs oReceivedData = new ReceivedArgs(sException.Message);
                //  ConnectionError(this, oReceivedData);
                Program.autojetSZKKMain.Hm.Info(String.Format("Error: Receiving Data - {0}", sException.Message));
            }
        }

        /// <summary>
        /// Send data to updatable field in printer.
        /// </summary>
        /// <param name="message">String send to printer</param>
        public Boolean SendData(string message)
        {
            try
            {

                byte[] data = Encoding.UTF8.GetBytes(message);
                if (connected)
                {
                    clientTcp.BeginSend(data, 0, data.Length, SocketFlags.None,
                                 new AsyncCallback(SendData), clientTcp);
                }

                dominoQueueThread.waitPrinterResponse = true;
                dominoQueueThread.printerACK.Enabled = true;
                Program.autojetSZKKMain.Hm.Info(String.Format("Data Sent: waitResponse = true"));

                return true;
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info(String.Format("Błąd podczas wysyłania danych do drukarki: {0}", ex.Message));
                return false;
            }
        }

        /// <summary>
        /// Send data to updatable field in printer.
        /// </summary>
        /// <param name="message">String send to printer</param>
        public Boolean SendData(byte[] data)
        {
            try
            {
                if (connected)
                {
                    clientTcp.BeginSend(data, 0, data.Length, SocketFlags.None,
                                 new AsyncCallback(SendData), clientTcp);
                }

                dominoQueueThread.waitPrinterResponse = true;
                dominoQueueThread.printerACK.Enabled = true;
                Program.autojetSZKKMain.Hm.Info(String.Format("Data Sent: waitResponse = true"));

                return true;
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info("Error during DataSend {0}", ex.ToString());
                return false;
            }
        }

        public void SendData(IAsyncResult iar)
        {
            try
            {
                Socket remote = (Socket)iar.AsyncState;
                int sent = remote.EndSend(iar);
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info(String.Format("Błąd podczas wysyłania danych do drukarki {0}", ex.Message));
                Connect();
            }
        }

        public void Disconnect()
        {
            Program.autojetSZKKMain.Hm.Info(String.Format("Codenet Printer Disconnected"));
            connected = false;
            clientTcp.Close();
        }

        public void setAsDisconnected()
        {
            connected = false;
        }
    }
}
