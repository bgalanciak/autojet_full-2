﻿using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Threading;
//using Microsoft.ApplicationInsights;

namespace AutojetSZKK
{
    class utilities
    {
        public static bool isEnabled = true;
        /// <summary>
        /// Sets the window title.
        /// </summary>
        public static string setWindowTitle()
        {
            return ("autojetSZKK " + GetProgramVersion());
        }

        public static void logAppStart()
        {
            //TelemetryClient tc = new TelemetryClient();

            //tc.InstrumentationKey = "9ef519a3-22c1-4837-bbf7-3fbf3a2d52f8";

            // Set session data:
            //tc.Context.User.Id = Environment.UserName;
            //tc.Context.Session.Id = Guid.NewGuid().ToString();
            //tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();

            //string s = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            //tc.TrackEvent("autojetSZKK_" + GetProgramVersion() + "_" + s + "_" + Environment.MachineName);

            //tc.Flush(); // only for desktop apps
        }

        /// <summary>
        /// Gets the program version.
        /// </summary>
        /// <returns>The assembly version in form major.minor.build.revision.</returns>
        public static string GetProgramVersion()
        {
            //string version = Application.ProductVersion;
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                version = ad.CurrentVersion.ToString();
            }

            return (version);
        }

        /// <summary>
        /// http://stackoverflow.com/questions/11800958/using-ping-in-c-sharp
        /// </summary>
        /// <param name="nameOrAddress"></param>
        /// <returns></returns>
        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        /// <summary>
        /// main method for threatS
        /// http://stackoverflow.com/questions/661561/how-to-update-the-gui-from-another-thread-in-c
        /// </summary>
        public static void pingPlcMain()
        {
            bool plcPingOk = false;

            while (isEnabled)
            {
                Thread.Sleep(10);

                plcPingOk = PingHost(Program.Settings.Data.plcIp);
                Program.autojetSZKKMain.Invoke((MethodInvoker)delegate
                {
                    Program.autojetSZKKMain.plcPingOk = plcPingOk;    //;
                    Program.autojetSZKKMain.ubdateUIFromThreat();
                    //Program.autojetSZKKMain.lPingRes.Text = plcPingOk.ToString(); // runs on UI thread
                });
            }
        }

    }


}
