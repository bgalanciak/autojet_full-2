﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;									// Endpoint
using System.Net.Sockets;							// Socket namespace
using System.Windows.Forms;


namespace AutojetSZKK
{
    //delegate void scanEventCallback(string scan);

    public class TcpStock
    {
        delegate void scanEventCallback(string scan);
        
        //21-01-2015
        public delegate void dataRecieved(string recStr);
        public event dataRecieved dataRec;
        
        private Socket m_sock;						// Server connection
        private byte[] m_byBuff = new byte[256];	          // Recieved data buffer
        private bool disposed = false;
		private bool connecting = false;

        private System.ComponentModel.Container components = null;        

        public TcpStock()
        {
            m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void OnConnect(IAsyncResult ar)
        {
            // Socket was the passed in object
            Socket sock = (Socket)ar.AsyncState;

            // Check if we were sucessfull
            try
            {
                //sock.EndConnect( ar );
				if (sock.Connected)
					SetupRecieveCallback(sock);
				else
					Program.autojetSZKKMain.Hm.Warning("TCP IP: Unable to connect to remote machine");
                    //MessageBox.Show(Program.proPlatePrintMainForm, "Unable to connect to remote machine", "Connect Failed!");
            }
            catch (Exception ex)
            {
				Program.autojetSZKKMain.Hm.Error($"TCP IP: OnConnect: Unusual error during Connect!:{ex.Message}");
				//MessageBox.Show(Program.proPlatePrintMainForm, ex.Message, "Unusual error during Connect!");
            }

            this.connecting = false;
        }

        public void OnRecievedData(IAsyncResult ar)
        {
            // Socket was the passed in object
            Socket sock = (Socket)ar.AsyncState;

            // Check if we got any data
            try
            {
                int nBytesRec = sock.EndReceive(ar);
                if (nBytesRec > 0)
                {
                    // Wrote the data to the List
                    string sRecieved = Encoding.ASCII.GetString(m_byBuff, 0, nBytesRec);

                    // WARNING : The following line is NOT thread safe. Invoke is
                    // m_lbRecievedData.Items.Add( sRecieved );
                    
                    //if (Program.proPlatePrintMainForm.InvokeRequired)
                    //Program.proPlatePrintMainForm.Invoke(m_AddMessage, new string[] { sRecieved });
                    
                    OnAddMessage(sRecieved);

                    // If the connection is still usable restablish the callback
                    SetupRecieveCallback(sock);
                }
                else
                {
                    // If no data was recieved then the connection is probably dead
					Program.autojetSZKKMain.Hm.Warning("TCP IP: Client {0}, disconnected", sock.RemoteEndPoint);
					//MessageBox.Show(Program.proPlatePrintMainForm, "Disconnected...", String.Format("Client {0}, disconnected", sock.RemoteEndPoint));
                    //Console.WriteLine("Client {0}, disconnected", sock.RemoteEndPoint);
                    sock.Shutdown(SocketShutdown.Both);
                    sock.Close();
                }
            }
            catch (Exception ex)
            {
				Program.autojetSZKKMain.Hm.Warning($"TCP IP: OnRecievedData: Unusual error during Recieve!:{ex.Message}");
				//MessageBox.Show(Program.proPlatePrintMainForm, ex.Message, "Unusual error during Recieve!");
            }
        }
        
        public void OnAddMessage(string sMessage)
        {
            if (Program.autojetSZKKMain.InvokeRequired)
            {
                {
                    scanEventCallback d = new scanEventCallback(OnAddMessage);
                    Program.autojetSZKKMain.BeginInvoke(d, sMessage);
                }
            }
            else
            {
                //Program.proPlatePrintMainForm.printerMsgReceived(sMessage);
                dataRec(sMessage);
            }
            
        }
        
        public void SetupRecieveCallback(Socket sock)
        {
            try
            {
                AsyncCallback recieveData = new AsyncCallback(OnRecievedData);
                sock.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, recieveData, sock);
            }
            catch (Exception ex)
            {
				Program.autojetSZKKMain.Hm.Warning("Setup Recieve Callback failed!");
				//MessageBox.Show(Program.proPlatePrintMainForm, ex.Message, "Setup Recieve Callback failed!");
            }
        }

        public void Connect(string IpAdress, int portNum)
        {
            try
            {
                // Close the socket if it is still open
                if ((m_sock != null) && (m_sock.Connected))
                {
                    //mc, 2017-11-15
                    //m_sock.Shutdown(SocketShutdown.Both);
                    //System.Threading.Thread.Sleep(10);
                    //m_sock.Close();

                    //mc, 2017-11-15
                    Disconnect();
                }

                // Create the socket object
                m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Define the Server address and port
                //IPEndPoint epServer = new IPEndPoint(IPAddress.Parse(IpAdress), 7000);
                IPEndPoint epServer = new IPEndPoint(IPAddress.Parse(IpAdress), portNum);

                // Connect to the server blocking method and setup callback for recieved data
                // m_sock.Connect( epServer );
                // SetupRecieveCallback( m_sock );

                // Connect to server non-Blocking method
                m_sock.Blocking = false;
                AsyncCallback onconnect = new AsyncCallback(OnConnect);
                m_sock.BeginConnect(epServer, onconnect, m_sock);
				
				//set connecting bit;
				this.connecting = true;
            }
            catch (Exception ex)
            {
                //todo
				Program.autojetSZKKMain.Hm.Warning("TCP IP Server connect failed!");
                //MessageBox.Show(Program.proPlatePrintMainForm, ex.Message, "Server Connect failed!");
				this.connecting = false;
			}
        }

        public void Send(string toSend)
        {
            // Check we are connected
            if ((m_sock == null) || (!m_sock.Connected))
            {
				Program.autojetSZKKMain.Hm.Warning("Must be connected to Send a message");
				//MessageBox.Show(Program.proPlatePrintMainForm, "Must be connected to Send a message");
                return;
            }

            // Read the message from the text box and send it
            try
            {
                // Convert to byte array and send.
                Byte[] byteDateLine = Encoding.ASCII.GetBytes(toSend.ToCharArray());
                m_sock.Send(byteDateLine, byteDateLine.Length, 0);
            }
            catch (Exception ex)
            {
				Program.autojetSZKKMain.Hm.Warning("Send Message Failed!");
				//MessageBox.Show(Program.proPlatePrintMainForm, ex.Message, "Send Message Failed!");
            }
        }

        public void Send(Byte[] toSend)
        {
            // Check we are connected
            if ((m_sock == null) || (!m_sock.Connected))
            {
                Program.autojetSZKKMain.Hm.Warning("Must be connected to Send a message");
				//MessageBox.Show(Program.proPlatePrintMainForm, "Must be connected to Send a message");
                return;
            }

            // Read the message from the text box and send it
            try
            {
                // Convert to byte array and send.
                //Byte[] byteDateLine = Encoding.ASCII.GetBytes(toSend.ToCharArray());
                m_sock.Send(toSend, toSend.Length, 0);
            }
            catch (Exception ex)
            {
				Program.autojetSZKKMain.Hm.Warning("Send Message Failed!");
				//MessageBox.Show(Program.proPlatePrintMainForm, ex.Message, "Send Message Failed!");
            }
        }

        ~TcpStock()
        {
            Dispose();
        }

        //mc, 2017-11-15
        //public bool Connected()
        //{
        //    return (this.m_sock.Connected);
        //}

        //form https://stackoverflow.com/questions/2661764/how-to-check-if-a-socket-is-connected-disconnected-in-c
        public bool Connected()
        {
            if ((m_sock == null) || (!m_sock.Connected))
            {
                return false;
            }
            else
            {
                return !((m_sock.Poll(1000, SelectMode.SelectRead) && (m_sock.Available == 0)) || !m_sock.Connected);
            }
            //bool part1 = m_sock.Poll(1000, SelectMode.SelectRead);
            //bool part2 = (m_sock.Available == 0);
            //if (part1 && part2)
            //    return false;
            //else
            //    return true;
        }

        public bool Connecting()
		{
			return this.connecting;
		}

        public void Disconnect()
        {
            /*
            http://vadmyst.blogspot.com/2008/04/proper-way-to-close-tcp-socket.html
            socket.Send(); //last data of the connection
            socket.Shutdown(SocketShutdown.Send);

            try
            {
                int read = 0;
                while( (read = socket.Receive()) > 0 ) /application data buffers
                {}
            }
            catch
            {
                //ignore
            }
            socket.Close();
            */

            if ((m_sock != null)&&(m_sock.Connected))
            {
                m_sock.Shutdown(SocketShutdown.Send);
            }

            System.Threading.Thread.Sleep(100);     //mc, 2017-11-15

            try
            {
                int read = 0;
                byte[] buffff = new byte[100];
                while ((read = m_sock.Receive(buffff)) > 0) //application data buffers
                {}
            }
            catch
            {
                //ignore
            }

            System.Threading.Thread.Sleep(100);     //mc, 2017-11-15

            m_sock.Close();
            m_sock.Dispose();       //mc 2018-08-08
        }

        public void Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }

                // Free unmanaged objects.
                if (this.Connected())
                {
                    //this.m_sock.Close();
                    this.Disconnect();      ////mc, 2017-11-15
                }
                disposed = true;
            }
        }
    }
}
