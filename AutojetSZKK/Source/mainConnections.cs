﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AutojetSZKK
{
    public partial class autojetSZKKMainForm
    {
      
        /// <summary>
        /// connection for PLC
        /// </summary>
        public void connectPLC()
        {
            //fds.rfd = libnodave.openSocket(102, "192.168.1.150");
            fds.rfd = libnodave.openSocket(102, Program.Settings.Data.plcIp);
            fds.wfd = fds.rfd;
            if (fds.rfd > 0)
            {
                di = new libnodave.daveInterface(fds, "IF1", 0, libnodave.daveProtoISOTCP, libnodave.daveSpeed187k);
                di.setTimeout(9999999);

                dc = new libnodave.daveConnection(di, 0, rack, slot);
                if (0 == dc.connectPLC())
                {
                  updateVisuPlcConnected();
                }
                else
                {
                  updateVisuPlcNotConnected();
                }
            }
        }

      public void updateVisuPlcNotConnected()
      {
         lPLCStatus.Text = "Disconnected";
         statusStripePLC.Text = "PLC: nie połączono";
         statusStripePLC.ForeColor = Color.Red;
         statusStripePLC.Image = Properties.Resources.no;
         plcConnected = false;
      }


      public void updateVisuPlcConnected()
      {
         lPLCStatus.Text = "Connected";
         statusStripePLC.Text = "PLC: połączono";
         statusStripePLC.ForeColor = Color.Green;
         statusStripePLC.Image = Properties.Resources.yes;
         plcConnected = true;
      }
}
}
