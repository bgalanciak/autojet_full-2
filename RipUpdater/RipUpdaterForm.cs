﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace RipUpdater
{
    public partial class RipUpdaterForm : Form
    {
        public RipUpdaterForm()
        {
            InitializeComponent();
        }

        private void bSelectFolder_Click(object sender, EventArgs e)
        {
            //folderBrowserDialog1.SelectedPath = @"E:\_tst";
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog1.SelectedPath))
            {
                global.searchFolderName = folderBrowserDialog1.SelectedPath;
                lSeclectedFolder.Text = global.searchFolderName;
                global.searchfolderSelected = true;
                searchPatternOK();
                changePatternOK();
                Properties.Settings.Default.path = folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void tbSearchText_TextChanged(object sender, EventArgs e)
        {
            searchPatternOK();
        }

        private void tbChangeText_TextChanged(object sender, EventArgs e)
        {
            changePatternOK();
        }

        public void searchPatternOK()
        {
            Regex regex = new Regex(@"\d{5,6}_\d{1,3}_");
            Match match = regex.Match(tbSearchText.Text);
            if (match.Success && global.searchfolderSelected)
            {
                bSearchFiles.Enabled = true;
                global.searchPattern = tbSearchText.Text;

            }
            else
                bSearchFiles.Enabled = false;
        }

        public void changePatternOK()
        {
            Regex regex = new Regex(@"\d{5,6}_\d{1,3}_");
            Match match = regex.Match(tbChangeText.Text);
            if (match.Success && global.searchfolderSelected)
            {
                bCopyFiles.Enabled = true;
                global.changePattern = tbChangeText.Text;
            }
            else
                bCopyFiles.Enabled = false;
        }

        private void bSearchFiles_Click(object sender, EventArgs e)
        {
            searchFolders();
        }

        private void searchFolders()
        {
            string[] files = Directory.GetFiles(global.searchFolderName, $"*{global.searchPattern}*", SearchOption.AllDirectories);

            List<String> directoryList = new List<string>();
            List<String> pathList = new List<string>();

            //convert list of files to its path only 
            foreach (string s in files)
            {
                string directory = Path.GetDirectoryName(s);
                directoryList.Add(directory);
            }

            //select only uniqe folder names
            foreach (string s in directoryList)
            {
                //var isNewPath = pathList.Select(e => e.Equals(s));
                //if (isNewPath.Count()==0)
                IEnumerable<string> isNewPath = from paths in pathList where paths.Equals(s) select paths;
                if (isNewPath.Count()==0)
                {
                    pathList.Add(s);
                }
            }

            //global.folderList
            global.folderList = new List<StringValue>();
            //convert path names (in string) to StringValue
            foreach (string s in pathList)
            {
                //get count of file in each localizations
                int fileCount = countFilesInPath(s, global.searchPattern);

                StringValue str = new StringValue(s);
                global.folderList.Add(str);
            }

            dgvFolderList.DataSource = null;
            dgvFolderList.DataSource = global.folderList;
            if (global.folderList.Count == 1)
            {
                DataGridViewRow row = dgvFolderList.Rows[0];
                row.Selected = true;
            }
            dgvFolderListInit();
            dgvColour(dgvFolderList);
        }

        private int countFilesInPath(string path, string searchPattern)
        {
            string[] files = Directory.GetFiles(path, $"*{searchPattern}*", SearchOption.TopDirectoryOnly);

            return files.Count();
        }

        public void dgvFolderListInit()
        {
            dgvFolderList.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        public void showMachingFiles(string path, string pattern)
        {
            string[] files = Directory.GetFiles(path, $"*{pattern}*", SearchOption.TopDirectoryOnly);

            global.fileList = new List<StringValue>();
            //convert path names (in string) to StringValue
            foreach (string s in files)
            {
                StringValue str = new StringValue(s);
                global.fileList.Add(str);
            }

            dgvFileList.DataSource = null;
            dgvFileList.DataSource = global.fileList;
            dgvFileListInit();
            dgvColour(dgvFileList);
        }

        public void dgvFileListInit()
        {
            dgvFileList.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void dgvFolderList_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvFolderList.SelectedRows.Count > 0)
            {
                showMachingFiles(dgvFolderList.SelectedRows[0].Cells[0].Value.ToString(), global.searchPattern);
                global.sourceFolderName = dgvFolderList.SelectedRows[0].Cells[0].Value.ToString();
            }
            else
            {
                dgvFileList.DataSource = null;
            }
        }

        public void dgvColour(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (i%2==0)
                {
                    //dgv.Rows[i].DefaultCellStyle.BackColor = Color.DimGray;
                }
                else
                {
                    dgv.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }

        public void copyFiles(string sourcePath, string newDirectory, List<StringValue> fileList)
        {
            string destPath = sourcePath + "\\" + newDirectory;
            Directory.CreateDirectory(destPath);

            foreach (StringValue s in fileList)
            {
                string fileName = Path.GetFileName(s.Value);
                string filePath = Path.GetDirectoryName(s.Value);

                string newFileName = newDirectory + fileName;
                string newFilePath = filePath + "\\" + newDirectory;
                File.Copy(s.Value, newFilePath + "\\" + newFileName);
            }

        }

        private void bCopyFiles_Click(object sender, EventArgs e)
        {
            //copyFiles(global.sourceFolderName, global.changePattern, global.fileList);
            resultForm form = new resultForm();
            form.parameters.fileList = global.fileList;
            form.parameters.newDirectory = global.changePattern;
            form.parameters.sourcePath = global.sourceFolderName;
            form.ShowDialog();

            tbChangeText.Text = "";
            tbSearchText.Text = "";
            dgvFolderList.DataSource = null;
            dgvFileList.DataSource = null;
            global.searchFolderName = "";
            global.changePattern = "";
            global.folderList = new List<StringValue>();
            global.searchfolderSelected = false;
            global.searchPattern = "";
            global.sourceFolderName = "";
        }

        private void bwCopyFiles_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void RipUpdaterForm_Load(object sender, EventArgs e)
        {
            this.Text = utilities.setWindowTitle();
            utilities.logAppStart();

            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.path))
            {
                if (!Directory.Exists(Properties.Settings.Default.path))
                {
                    Properties.Settings.Default.path = "";
                    Properties.Settings.Default.Save();
                }
                else
                {
                    global.searchFolderName = Properties.Settings.Default.path;
                    lSeclectedFolder.Text = global.searchFolderName;
                    global.searchfolderSelected = true;
                    searchPatternOK();
                    changePatternOK();
                }
            }
        }
    }
}
