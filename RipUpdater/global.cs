﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RipUpdater
{
    public static class global
    {
        public static List<StringValue> folderList = new List<StringValue>();
        public static List<StringValue> fileList = new List<StringValue>();
        public static String searchFolderName;
        public static bool searchfolderSelected = false;
        public static String sourceFolderName;

        public static String searchPattern;
        public static String changePattern;
    }

    public class StringValue
    {
        public StringValue(string s)
        {
            _value = s;
        }

        public string Value { get { return _value; } set { _value = value; } }
        string _value;
    }

    public class bgwParameters
    {
        //string sourcePath, string newDirectory, List<StringValue> fileList
        public string sourcePath { get; set; }
        public string newDirectory { get; set; }
        public List<StringValue> fileList { get; set; }
    }
}
