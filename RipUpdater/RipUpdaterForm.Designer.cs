﻿namespace RipUpdater
{
    partial class RipUpdaterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.Control = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lSeclectedFolder = new System.Windows.Forms.Label();
            this.tbChangeText = new System.Windows.Forms.TextBox();
            this.tbSearchText = new System.Windows.Forms.TextBox();
            this.bSelectFolder = new System.Windows.Forms.Button();
            this.bSearchFiles = new System.Windows.Forms.Button();
            this.bCopyFiles = new System.Windows.Forms.Button();
            this.dgvFolderList = new System.Windows.Forms.DataGridView();
            this.dgvFileList = new System.Windows.Forms.DataGridView();
            this.Debug = new System.Windows.Forms.TabPage();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainStatus = new System.Windows.Forms.StatusStrip();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.bwCopyFiles = new System.ComponentModel.BackgroundWorker();
            this.tcMain.SuspendLayout();
            this.Control.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFolderList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFileList)).BeginInit();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.Control);
            this.tcMain.Controls.Add(this.Debug);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(0, 24);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(810, 481);
            this.tcMain.TabIndex = 0;
            // 
            // Control
            // 
            this.Control.Controls.Add(this.tableLayoutPanel1);
            this.Control.Location = new System.Drawing.Point(4, 22);
            this.Control.Name = "Control";
            this.Control.Padding = new System.Windows.Forms.Padding(3);
            this.Control.Size = new System.Drawing.Size(802, 455);
            this.Control.TabIndex = 0;
            this.Control.Text = "Control";
            this.Control.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lSeclectedFolder, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbChangeText, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbSearchText, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.bSelectFolder, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.bSearchFiles, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.bCopyFiles, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.dgvFolderList, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.dgvFileList, 3, 9);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(796, 449);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(411, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(362, 30);
            this.label6.TabIndex = 5;
            this.label6.Text = "Lista plików:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(23, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(362, 30);
            this.label5.TabIndex = 4;
            this.label5.Text = "Lista folderów:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(411, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(362, 30);
            this.label4.TabIndex = 3;
            this.label4.Text = "Zmień na:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(23, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(362, 30);
            this.label3.TabIndex = 2;
            this.label3.Text = "Wyszukaj:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lSeclectedFolder
            // 
            this.lSeclectedFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSeclectedFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lSeclectedFolder.Location = new System.Drawing.Point(23, 75);
            this.lSeclectedFolder.Name = "lSeclectedFolder";
            this.lSeclectedFolder.Size = new System.Drawing.Size(362, 40);
            this.lSeclectedFolder.TabIndex = 1;
            this.lSeclectedFolder.Text = "...";
            this.lSeclectedFolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbChangeText
            // 
            this.tbChangeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbChangeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbChangeText.Location = new System.Drawing.Point(411, 153);
            this.tbChangeText.Name = "tbChangeText";
            this.tbChangeText.Size = new System.Drawing.Size(362, 31);
            this.tbChangeText.TabIndex = 2;
            this.tbChangeText.TextChanged += new System.EventHandler(this.tbChangeText_TextChanged);
            // 
            // tbSearchText
            // 
            this.tbSearchText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSearchText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbSearchText.Location = new System.Drawing.Point(23, 153);
            this.tbSearchText.Name = "tbSearchText";
            this.tbSearchText.Size = new System.Drawing.Size(362, 31);
            this.tbSearchText.TabIndex = 1;
            this.tbSearchText.TextChanged += new System.EventHandler(this.tbSearchText_TextChanged);
            // 
            // bSelectFolder
            // 
            this.bSelectFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bSelectFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelectFolder.Location = new System.Drawing.Point(23, 23);
            this.bSelectFolder.Name = "bSelectFolder";
            this.bSelectFolder.Size = new System.Drawing.Size(362, 44);
            this.bSelectFolder.TabIndex = 0;
            this.bSelectFolder.Text = "Wybierz folder...";
            this.bSelectFolder.UseVisualStyleBackColor = true;
            this.bSelectFolder.Click += new System.EventHandler(this.bSelectFolder_Click);
            // 
            // bSearchFiles
            // 
            this.bSearchFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bSearchFiles.Enabled = false;
            this.bSearchFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSearchFiles.Location = new System.Drawing.Point(23, 392);
            this.bSearchFiles.Name = "bSearchFiles";
            this.bSearchFiles.Size = new System.Drawing.Size(362, 34);
            this.bSearchFiles.TabIndex = 3;
            this.bSearchFiles.Text = "Wyszukaj";
            this.bSearchFiles.UseVisualStyleBackColor = true;
            this.bSearchFiles.Click += new System.EventHandler(this.bSearchFiles_Click);
            // 
            // bCopyFiles
            // 
            this.bCopyFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bCopyFiles.Enabled = false;
            this.bCopyFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCopyFiles.Location = new System.Drawing.Point(411, 392);
            this.bCopyFiles.Name = "bCopyFiles";
            this.bCopyFiles.Size = new System.Drawing.Size(362, 34);
            this.bCopyFiles.TabIndex = 4;
            this.bCopyFiles.Text = "Kopiuj";
            this.bCopyFiles.UseVisualStyleBackColor = true;
            this.bCopyFiles.Click += new System.EventHandler(this.bCopyFiles_Click);
            // 
            // dgvFolderList
            // 
            this.dgvFolderList.AllowUserToAddRows = false;
            this.dgvFolderList.AllowUserToDeleteRows = false;
            this.dgvFolderList.AllowUserToResizeColumns = false;
            this.dgvFolderList.AllowUserToResizeRows = false;
            this.dgvFolderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFolderList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFolderList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFolderList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvFolderList.Location = new System.Drawing.Point(23, 228);
            this.dgvFolderList.MultiSelect = false;
            this.dgvFolderList.Name = "dgvFolderList";
            this.dgvFolderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFolderList.Size = new System.Drawing.Size(362, 128);
            this.dgvFolderList.TabIndex = 11;
            this.dgvFolderList.SelectionChanged += new System.EventHandler(this.dgvFolderList_SelectionChanged);
            // 
            // dgvFileList
            // 
            this.dgvFileList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFileList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFileList.Location = new System.Drawing.Point(411, 228);
            this.dgvFileList.Name = "dgvFileList";
            this.dgvFileList.Size = new System.Drawing.Size(362, 128);
            this.dgvFileList.TabIndex = 12;
            // 
            // Debug
            // 
            this.Debug.Location = new System.Drawing.Point(4, 22);
            this.Debug.Name = "Debug";
            this.Debug.Padding = new System.Windows.Forms.Padding(3);
            this.Debug.Size = new System.Drawing.Size(802, 455);
            this.Debug.TabIndex = 1;
            this.Debug.Text = "Debug";
            this.Debug.UseVisualStyleBackColor = true;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pomocToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(810, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // mainStatus
            // 
            this.mainStatus.Location = new System.Drawing.Point(0, 505);
            this.mainStatus.Name = "mainStatus";
            this.mainStatus.Size = new System.Drawing.Size(810, 22);
            this.mainStatus.TabIndex = 2;
            this.mainStatus.Text = "statusStrip1";
            // 
            // bwCopyFiles
            // 
            this.bwCopyFiles.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCopyFiles_DoWork);
            // 
            // RipUpdaterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 527);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.mainMenu);
            this.Controls.Add(this.mainStatus);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "RipUpdaterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RipUpdateer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RipUpdaterForm_Load);
            this.tcMain.ResumeLayout(false);
            this.Control.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFolderList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFileList)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage Control;
        private System.Windows.Forms.TabPage Debug;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.StatusStrip mainStatus;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lSeclectedFolder;
        private System.Windows.Forms.TextBox tbChangeText;
        private System.Windows.Forms.TextBox tbSearchText;
        private System.Windows.Forms.Button bSelectFolder;
        private System.Windows.Forms.Button bSearchFiles;
        private System.Windows.Forms.Button bCopyFiles;
        private System.Windows.Forms.DataGridView dgvFolderList;
        private System.Windows.Forms.DataGridView dgvFileList;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.ComponentModel.BackgroundWorker bwCopyFiles;
    }
}

